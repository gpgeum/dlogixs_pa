﻿// DLDLCSetDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLDLCSetDlg.h"

#define	GRID_IDX_ID				0
#define	GRID_IDX_NAME			1
#define GRID_IDX_NUM			2


// DLDLCSetDlg 대화 상자

IMPLEMENT_DYNAMIC(CDLDLCSetDlg, CDialog)

CDLDLCSetDlg::CDLDLCSetDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(CDLDLCSetDlg::IDD, pParent)
{
	m_SelAddr = 1;		// 선택 Devide 주소
	m_SelItem = 1;		// 선택 Device Item(Output)

}

CDLDLCSetDlg::~CDLDLCSetDlg()
{
}

void CDLDLCSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDLDLCSetDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_NOTIFY(NM_CLICK, IDC_LST_DLC_LIST, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_LST_DLC_LIST, OnGridDblClick)
	ON_NOTIFY(GVN_SELCHANGED, IDC_LST_DLC_LIST, OnGridEndSelChange)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_LST_DLC_LIST, OnGridEndEdit)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// DLDLCSetDlg 메시지 처리기


void CDLDLCSetDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}


BOOL CDLDLCSetDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CDialog::OnEraseBkgnd(pDC);
}


BOOL CDLDLCSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initScreen();
	DrawDLCItem();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDLDLCSetDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN) {
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE) {
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDLDLCSetDlg::initScreen()
{
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;

	w_X = 10;
	w_Y = 10;
	w_W = 350;
#if defined(DEV_5000)
	w_H = 80;
#else
	w_H = 60;
#endif
	w_ButtonRc = CRect(w_X, w_Y, w_X + w_W, w_Y + w_H);
	m_GridDev.Create(w_ButtonRc, this, IDC_LST_DLC_LIST);
	m_GridDev.SetListMode(FALSE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.SetHeaderSort(FALSE);
	m_GridDev.SetSingleRowSelection(TRUE);
	m_GridDev.SetSingleColSelection(TRUE);
	//m_GridDev.SetEditable(m_bEditable);
	m_GridDev.EnableSelection(TRUE);
	m_GridDev.SetRowResize(TRUE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.EnableTitleTips(FALSE);
	m_GridDev.GetDefaultCell(FALSE, FALSE)->SetFormat(DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_NOPREFIX | DT_END_ELLIPSIS);

	InitGridHeader();
}

void CDLDLCSetDlg::InitGridHeader()
{
	int	i;
	char *Llistcolumn[] = { "번호", "설정지역명", "설정갯수" };
	int Lwidth[] = { 40, 190, 115 };

	m_GridDev.DeleteAllItems();
	m_GridDev.SetColumnCount(3);
	m_GridDev.SetRowCount(LPA_DLC_ITEM + 1);
	m_GridDev.SetFixedRowCount(1);

	for (i = 0; i < 3; i++)
	{
		m_GridDev.SetItemText(0, i, CA2T(Llistcolumn[i]));
		m_GridDev.SetColumnWidth(i, Lwidth[i]);
	}

	m_GridDev.SetRowHeight(0, 40);
	m_GridDev.SetSingleRowSelection(1);
	m_GridDev.SetHeaderSort(0);
	m_GridDev.SetListMode(1);
	m_GridDev.SetFixedColumnSelection(0);
}

void CDLDLCSetDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
}

void CDLDLCSetDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
}


void CDLDLCSetDlg::OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;

	//DL_LOG("OnGridEndSelChange:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
	m_SelItem = pItem->iRow;

	if (GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}

	RedrawDLCCount(pItem->iRow);
}



void CDLDLCSetDlg::OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	//DL_LOG("OnGridEndEdit:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	if (pItem->iColumn == GRID_IDX_NAME && pItem->iRow > 0) {
		CString w_csStr;

		w_csStr = m_GridDev.GetItemText(pItem->iRow, pItem->iColumn);

		m_SelItem = pItem->iRow;
		GetLowPA()->SetDlcName(CT2A(w_csStr), m_SelAddr, m_SelItem);

	}
}

void CDLDLCSetDlg::RedrawDLCCount(int a_Row)
{
	int w_Cnt;
	int w_nCol;
	CString w_csStr;

	if (a_Row > 0) {
		w_nCol = GRID_IDX_NUM;
		w_Cnt = GetLowPA()->GetDLCCount(m_SelAddr, m_SelItem);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
		m_GridDev.Invalidate();
	}
}

void CDLDLCSetDlg::DrawDLCItem()
{
	int		w_Addr = 1;
	int		w_nRow = 1;

	int		w_nCol;
	CString w_csStr;
	int		w_Cnt = 0;

	CString CA2TTemp;

	LPA_DATA_T *wp_PAData = GetPAData();
	if (!wp_PAData) {
		return;
	}
	//m_GridDev.SetCurSel(w_nRow);
	m_GridDev.SetRowCount(LPA_DLC_ITEM + 1);
	for (w_nRow = 1; w_nRow <= LPA_DLC_ITEM; w_nRow++)
	{
		w_nCol = GRID_IDX_ID;
		w_csStr.Format(_T("%d"), w_nRow);
		m_GridDev.SetItemState(w_nRow, w_nCol, m_GridDev.GetItemState(w_nRow, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(w_nRow, w_nCol, w_csStr);

		w_nCol = GRID_IDX_NAME;
		if (wp_PAData->m_DLCData[w_Addr - 1].m_Item[w_nRow - 1].m_Name[0]) {
			CA2TTemp = CA2T(wp_PAData->m_DLCData[w_Addr - 1].m_Item[w_nRow - 1].m_Name);
			w_csStr.Format(_T("%s"), CA2TTemp);
		}
		else {
			if (w_nRow == 1)
				w_csStr.Format(_T("LineCheck Nomal"));
			else if (w_nRow == 2)
				w_csStr.Format(_T("LineCheck Broadcast"));
			else
				w_csStr.Format(_T("DLC %d"), w_nRow);
		}
		m_GridDev.SetItemState(w_nRow, w_nCol, m_GridDev.GetItemState(w_nRow, w_nCol));
		m_GridDev.SetItemText(w_nRow, w_nCol, w_csStr);

		w_nCol = GRID_IDX_NUM;
		w_Cnt = GetLowPA()->GetDLCCount(w_Addr, w_nRow);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridDev.SetItemState(w_nRow, w_nCol, m_GridDev.GetItemState(w_nRow, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(w_nRow, w_nCol, w_csStr);
	}
	m_GridDev.Invalidate();
}

void CDLDLCSetDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
		// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;
	CRect w_Rect;
	GetClientRect(&w_Rect);


	if (m_GridDev.GetSafeHwnd())
	{
		w_X = w_Rect.Width() / 2 - 350 / 2 + 5;
		w_Y = 15;
		w_W = 350;
		w_H = 90;

		w_ButtonRc = CRect(w_X, w_Y, w_X + w_W, w_Y + w_H);
		m_GridDev.MoveWindow(&w_ButtonRc);
	}
}


LRESULT CDLDLCSetDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch (message)
	{
	case WM_SELPAGE:
	case WM_CHANGEDEQUIP:
	case WM_INVALIDATE:
		m_SelAddr = 1;
		m_SelItem = 1;
		DrawDLCItem();
		break;

	}

	return CDialog::DefWindowProc(message, wParam, lParam);
}
