#pragma once

#include "GridCtrl_src/GridCtrl.h"
#include "NewCellTypes/GridURLCell.h"
#include "NewCellTypes/GridCellCombo.h"
#include "NewCellTypes/GridCellCheck.h"
#include "NewCellTypes/GridCellNumeric.h"
#include "NewCellTypes/GridCellDateTime.h"
#include "afxwin.h"

#if defined(DEV_5000)

// CDLDMTMacroSetDlg 대화 상자입니다.

class CDLDMTMacroSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLDMTMacroSetDlg)

public:
	CDLDMTMacroSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLDMTMacroSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLDMTSET_DIALOG };

	int			m_SelAddr;					// 선택 Devide 주소
	int			m_SelItem;					// 선택 Device Item(Output)
public:

	void			initScreen();
	void			InitGridHeader();

	void			OnModYnClick(int a_Row);

	void			DrawDMTMacroItem(int a_Addr);
	void			DrawDMTMacroItem(int a_Addr, int a_Row);

	void			RedrawCount(int a_Row);

public:
	CGridCtrl		m_GridDev;
	CButton			m_btnAdd;
	CButton			m_btnDel;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};

#endif