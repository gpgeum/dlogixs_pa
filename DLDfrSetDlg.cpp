// DLDfrSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLDfrSetDlg.h"

#define	GRID_IDX_ID				0
#define	GRID_IDX_BUILDING		1
#define GRID_IDX_STAIR			2
#define GRID_IDX_ZONE			3
#define GRID_IDX_FLOOR			4
#define GRID_IDX_NUM			5
#define GRID_IDX_MODYN			6

// CDLDfrSetDlg 대화 상자입니다.
static TCHAR *gp_Zone[LPA_DFR_ZONE_MAX]={_T("지상"), _T("지하"), _T("옥탑"), _T("주차장") };

IMPLEMENT_DYNAMIC(CDLDfrSetDlg, CDialog)

CDLDfrSetDlg::CDLDfrSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLDfrSetDlg::IDD, pParent)
{
	m_SelAddr = 1;		// 선택 Devide 주소
	m_SelItem = 1;		// 선택 Device Item(Output)

}

CDLDfrSetDlg::~CDLDfrSetDlg()
{
}

void CDLDfrSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_DFR, m_comboDfr);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_btnAdd);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_btnDel);
	DDX_Control(pDX, IDC_RADIO_P, m_btnTypeP);
	DDX_Control(pDX, IDC_RADIO_R, m_btnTypeR);
	DDX_Control(pDX, IDC_RADIO_10, m_btnData10);
	DDX_Control(pDX, IDC_RADIO_12, m_btnData12);
	DDX_Control(pDX, IDC_BUTTON_CHECK, m_btnCheck);
}


BEGIN_MESSAGE_MAP(CDLDfrSetDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_NOTIFY(NM_CLICK, IDC_LST_DFR_LIST, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_LST_DFR_LIST, OnGridDblClick)
	ON_NOTIFY(GVN_SELCHANGED, IDC_LST_DFR_LIST, OnGridEndSelChange)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_LST_DFR_LIST, OnGridEndEdit)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CDLDfrSetDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CDLDfrSetDlg::OnBnClickedButtonDel)
	ON_CBN_SELCHANGE(IDC_COMBO_DFR, &CDLDfrSetDlg::OnCbnSelchangeComboDfr)
	ON_BN_CLICKED(IDC_RADIO_P, &CDLDfrSetDlg::OnBnClickedRadioP)
	ON_BN_CLICKED(IDC_RADIO_R, &CDLDfrSetDlg::OnBnClickedRadioR)
	ON_BN_CLICKED(IDC_RADIO_10, &CDLDfrSetDlg::OnBnClickedRadio10)
	ON_BN_CLICKED(IDC_RADIO_12, &CDLDfrSetDlg::OnBnClickedRadio12)
	ON_BN_CLICKED(IDC_BUTTON_CHECK, &CDLDfrSetDlg::OnBnClickedButtonCheck)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CDLDfrSetDlg 메시지 처리기입니다.

BOOL CDLDfrSetDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CDialog::OnEraseBkgnd(pDC);
}

void CDLDfrSetDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

BOOL CDLDfrSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetBtnType(0);
	SetBtnData(0);

	initScreen();
	DrawDfrCombo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDLDfrSetDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDLDfrSetDlg::initScreen()
{
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;

	w_X = 5;
	w_Y = 5;
	w_W = 350;
	w_H = 300;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_comboDfr.MoveWindow(&w_ButtonRc);

	w_X = 5;
	w_Y = 100;
	w_W = 350;
	w_H = 540;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_GridDev.Create(w_ButtonRc, this, IDC_LST_DFR_LIST);
	m_GridDev.SetListMode(FALSE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.SetHeaderSort(FALSE);
	m_GridDev.SetSingleRowSelection(TRUE);
	m_GridDev.SetSingleColSelection(TRUE);
	//m_GridDevP.SetEditable(m_bEditable);
	m_GridDev.EnableSelection(TRUE);
	m_GridDev.SetRowResize(TRUE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.EnableTitleTips(FALSE);
	m_GridDev.GetDefaultCell(FALSE, FALSE)->SetFormat(DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_NOPREFIX|DT_END_ELLIPSIS);	

	w_X = 50;
	w_Y = 645;
	w_W = 100;
	w_H = 30;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_btnAdd.MoveWindow(&w_ButtonRc);

	w_X = 50+100+10;
	w_Y = 645;
	w_W = 100;
	w_H = 30;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_btnDel.MoveWindow(&w_ButtonRc);

	w_X = 50+200+10;
	w_Y = 645;
	w_W = 100;
	w_H = 30;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_btnCheck.MoveWindow(&w_ButtonRc);

	InitGridHeader();
}

void CDLDfrSetDlg::InitGridHeader()
{
	int nRowNum;
	int	i;
	int w_nW = 0;


	char *Llistcolumn[]={"번호", "동", "라인", "지역", "층", "갯수","삭제"};
	int Lwidth[]       ={40, 50, 50, 50, 50, 50, 50};  

	m_GridDev.DeleteAllItems();
	m_GridDev.SetColumnCount(7);
	nRowNum = m_GridDev.InsertRow(_T(""));
	for (i = 0; i < 7; i++) 
	{		
		m_GridDev.SetItemText(nRowNum, i, CA2T(Llistcolumn[i]));
		m_GridDev.SetColumnWidth(i, Lwidth[i]);

		if(i == GRID_IDX_MODYN) {
			m_GridDev.SetCellType(nRowNum, i, RUNTIME_CLASS(CGridCellCheck));
			CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(nRowNum, i);
			if(pCell) {
				pCell->SetCheck(BST_UNCHECKED);
			}
		}
	}

	m_GridDev.SetFixedRowCount(1);
	m_GridDev.SetRowHeight(0, 30);
	m_GridDev.SetSingleRowSelection(1);
	m_GridDev.SetHeaderSort(0);
	m_GridDev.SetListMode(1);
	m_GridDev.SetFixedColumnSelection(0);
}



void CDLDfrSetDlg::OnModYnClick(int a_Row)
{
	int	w_ii;
	int	w_Flag = 0;
		
	CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(a_Row, GRID_IDX_MODYN);
	if(!pCell) {
		return;
	}
	if(pCell->GetCheck() == BST_CHECKED) {
		pCell->SetCheck(BST_UNCHECKED);
	} else {
		pCell->SetCheck(BST_CHECKED);
		w_Flag = 1;
	}

	if(a_Row == 0) {
		for(w_ii=1;w_ii<m_GridDev.GetRowCount();w_ii++) {
			CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, GRID_IDX_MODYN);
			if(!pCell) {
				continue;
			}
			if(w_Flag) {
				pCell->SetCheck(BST_CHECKED);
			} else {
				pCell->SetCheck(BST_UNCHECKED);
			}
		}
	}
}

void CDLDfrSetDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{

	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	if (pItem->iColumn == GRID_IDX_MODYN)
	{
		OnModYnClick(pItem->iRow);
	}
	
	//DL_LOG("OnGridClick:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
}

void CDLDfrSetDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}



void CDLDfrSetDlg::OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

//DL_LOG("OnGridEndSelChange:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	m_SelItem = pItem->iRow;

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}

	RedrawCount(pItem->iRow);
}

void CDLDfrSetDlg::OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	int	w_ii;
	CString strValue=_T("");
	int m_nRow,m_nCol;
	int w_Value=0;


	if(m_SelAddr < 1) {
		return;
	}
	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	m_nRow = pItem->iRow;
	m_nCol = pItem->iColumn;

	if (m_nRow > 0) {
		strValue = m_GridDev.GetItemText(m_nRow, m_nCol);
		if(m_nCol == GRID_IDX_BUILDING) {
			w_Value = dl_atoi1((CT2A)strValue);
			if(w_Value > 0) {						// 2016-04-14 Modify
				w_Value -= 1;
			} else {
				w_Value = 255;
			}
			wp_PAData->m_DfrData[m_SelAddr-1].m_Item[m_nRow-1].m_Pos.m_Building = w_Value;
		} else if(m_nCol == GRID_IDX_STAIR) {		
			w_Value = dl_atoi1((CT2A)strValue);
			if(w_Value > 0) {						// 2016-04-14 Modify
				w_Value -= 1;
			} else {
				w_Value = 255;
			}
			wp_PAData->m_DfrData[m_SelAddr-1].m_Item[m_nRow-1].m_Pos.m_Stair = w_Value;
		} else if(m_nCol == GRID_IDX_ZONE) {

			wp_PAData->m_DfrData[m_SelAddr-1].m_Item[m_nRow-1].m_Pos.m_Zone = 0;
			for(w_ii=0;w_ii<LPA_DFR_ZONE_MAX;w_ii++) {
				if(wcscmp((const wchar_t *)strValue, gp_Zone[w_ii]) == 0) {
					wp_PAData->m_DfrData[m_SelAddr-1].m_Item[m_nRow-1].m_Pos.m_Zone = w_ii;
					break;
				} 
			}
		} else if(m_nCol == GRID_IDX_FLOOR) {	
			w_Value = dl_atoi1((CT2A)strValue);
			if(w_Value > 0) {						// 2016-04-14 Modify
				w_Value -= 1;
			} else {
				w_Value = 255;
			}
			wp_PAData->m_DfrData[m_SelAddr-1].m_Item[m_nRow-1].m_Pos.m_Floor= w_Value;
		}
	}
}


void CDLDfrSetDlg::RedrawCount(int a_Row)
{
	int w_Cnt;
	int w_nCol;
	CString w_csStr;

	if(m_SelAddr < 1) {
		return;
	}
	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	if(a_Row > 0) {
		w_nCol = GRID_IDX_NUM;

		w_Cnt = GetLowPA()->GetDfrCount(m_SelAddr, m_SelItem);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
		m_GridDev.Invalidate();
	}

}


void CDLDfrSetDlg::DrawDfrCombo()
{
	CString		w_Str;
	int			w_ii;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	m_comboDfr.ResetContent();
	for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DFR_IDX];w_ii++) {
		w_Str.Format(_T("DFR %d"), w_ii+1);
		
		m_comboDfr.AddString(w_Str);
	}

	m_SelAddr = 1;
	if(m_comboDfr.GetCount() > 0) {
		m_comboDfr.SetCurSel(0);

		DrawDfrItem(m_SelAddr);
	} else {
		m_GridDev.SetRowCount(1);
	}
}


void CDLDfrSetDlg::DrawDfrItem(int a_Addr)
{
	int		w_ii;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData || a_Addr < 1) {
		return;
	}
	SetBtnType(wp_PAData->m_DfrData[a_Addr-1].m_PRFlag);	// P/R
	SetBtnData(wp_PAData->m_DfrData[a_Addr-1].m_BFlag);		// 10/12

	m_GridDev.SetRowCount(1);
	for(w_ii=0;w_ii<wp_PAData->m_DfrData[a_Addr-1].m_Cnt;w_ii++) {
		DrawDfrItem(a_Addr, w_ii+1);
	}
	m_SelItem = 1;
	m_GridDev.SetFocusCell(-1,-1);
	m_GridDev.SetCurSel(0);
	m_GridDev.Invalidate();
}


void CDLDfrSetDlg::DrawDfrItem(int a_Addr, int a_Row)
{
	int		w_nCol;
	CString w_csStr;
	CString w_csValue;
	int		w_Cnt = 0;
	int		w_Type;
	int		w_Byte;
	int		w_jj;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData || a_Addr < 1 || a_Row < 1) {
		return;
	}
	if(m_GridDev.GetRowCount() <= a_Row) {
		m_GridDev.SetRowCount(a_Row+1);
	}
	w_Type = wp_PAData->m_DfrData[a_Addr-1].m_PRFlag;
	w_Byte = wp_PAData->m_DfrData[a_Addr-1].m_BFlag;

	w_nCol = GRID_IDX_ID;
	w_csStr.Format(_T("%d"), a_Row);
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);

	w_nCol = GRID_IDX_BUILDING;
	if(wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Building == 255) {
		w_csStr.Format(_T("%d"), 0);
	} else {
		w_csStr.Format(_T("%d"), wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Building+1);
	}
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol));
	m_GridDev.SetCellType(a_Row, w_nCol, RUNTIME_CLASS(CGridCellCombo));
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
	CStringArray options1;
	for(w_jj=0;w_jj<= LPA_DFR_BUILDING_MAX;w_jj++)
	{
		w_csValue.Format(_T("%d"), w_jj);
		options1.Add(w_csValue);
	}
	CGridCellCombo *pCell1 = (CGridCellCombo*) m_GridDev.GetCell(a_Row, w_nCol);
	pCell1->SetOptions(options1);
	pCell1->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE

	w_nCol = GRID_IDX_STAIR;
	if(wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Stair == 255) {
		w_csStr.Format(_T("%d"), 0);
	} else {
		w_csStr.Format(_T("%d"), wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Stair+1);
	}
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol));
	m_GridDev.SetCellType(a_Row, w_nCol, RUNTIME_CLASS(CGridCellCombo));
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
	CStringArray options2;
	for(w_jj=0;w_jj<= LPA_DFR_STAIR_MAX;w_jj++)
	{
		w_csValue.Format(_T("%d"), w_jj);
		options2.Add(w_csValue);
	}
	CGridCellCombo *pCell2 = (CGridCellCombo*) m_GridDev.GetCell(a_Row, w_nCol);
	pCell2->SetOptions(options2);
	pCell2->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE

	w_nCol = GRID_IDX_ZONE;

// 2015.06.25 P형 ==> R형 10BYTE와 같게 수정
//	if(w_Byte == 1 || w_Type == 0) {	// R형:12 BYTE or P형
	if(w_Byte == 1 && w_Type == 1) {
		m_GridDev.SetItemText(a_Row, w_nCol, _T("-"));
		m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		CGridCell *pCell31 = (CGridCell*) m_GridDev.GetCell(a_Row, w_nCol);
		if(pCell31) {
			pCell31->SetBackClr(RGB(125,125,125));
		}
	} else {
		m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) & (~GVIS_READONLY));
		m_GridDev.SetCellType(a_Row, w_nCol, RUNTIME_CLASS(CGridCellCombo));
		CStringArray options3;
		for(w_jj=0;w_jj< LPA_DFR_ZONE_MAX;w_jj++)
		{
			options3.Add(gp_Zone[w_jj]);
		}
		CGridCellCombo *pCell32= (CGridCellCombo*) m_GridDev.GetCell(a_Row, w_nCol);
		pCell32->SetBackClr(RGB(255,255,255));
		pCell32->SetOptions(options3);
		pCell32->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE

		if(wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Zone  < LPA_DFR_ZONE_MAX) {
			w_csStr.Format(_T("%s"), gp_Zone[wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Zone]);
		} else {
			w_csStr.Format(_T("%s"), gp_Zone[0]);
		}
		m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
	}

	w_nCol = GRID_IDX_FLOOR;
	if(wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Floor == 255) {
		w_csStr.Format(_T("%d"), 0);
	} else {
		w_csStr.Format(_T("%d"), wp_PAData->m_DfrData[a_Addr-1].m_Item[a_Row-1].m_Pos.m_Floor+1);
	}
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol));
	m_GridDev.SetCellType(a_Row, w_nCol, RUNTIME_CLASS(CGridCellCombo));
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
	CStringArray options4;
	for(w_jj=0;w_jj<= LPA_DFR_FLOOR_MAX;w_jj++)
	{
		w_csValue.Format(_T("%d"), w_jj);
		options4.Add(w_csValue);
	}
	CGridCellCombo *pCell4 = (CGridCellCombo*) m_GridDev.GetCell(a_Row, w_nCol);
	pCell4->SetOptions(options4);
	pCell4->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE

	w_nCol = GRID_IDX_NUM;
	w_Cnt = GetLowPA()->GetDfrCount(a_Addr, a_Row);
	w_csStr.Format(_T("%d"), w_Cnt);
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);

	//Delete
	w_nCol = GRID_IDX_MODYN;
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
	m_GridDev.SetCellType(a_Row, w_nCol, RUNTIME_CLASS(CGridCellCheck));
	CGridCellCheck *pCell5 = (CGridCellCheck*) m_GridDev.GetCell(a_Row, w_nCol);
	if(pCell5) {
		pCell5->SetCheck(BST_UNCHECKED);
	}

	m_GridDev.Invalidate();
}

void CDLDfrSetDlg::DrawDfrData(int a_Data)
{
	int	w_nCol = 0;
	int w_Row = 0;
	int w_Type;
	int w_jj;
	CString w_csStr;
	CString w_csValue;
	int	w_Addr;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	w_Addr = m_SelAddr;
	if(w_Addr < 1) {
		return;
	}
	w_Type = wp_PAData->m_DfrData[w_Addr-1].m_PRFlag;	// P/R
	if(w_Type == 0) {
		return;
	}

	for(w_Row = 1;w_Row < m_GridDev.GetRowCount();w_Row++) {
		w_nCol = GRID_IDX_ZONE;
		if(a_Data == 1) {
			wp_PAData->m_DfrData[w_Addr-1].m_Item[w_Row-1].m_Pos.m_Zone = 0;

			m_GridDev.SetItemText(w_Row, w_nCol, _T("-"));
			m_GridDev.SetItemState(w_Row, w_nCol, m_GridDev.GetItemState(w_Row, w_nCol) | GVIS_READONLY);
			CGridCell *pCell3 = (CGridCell*) m_GridDev.GetCell(w_Row, w_nCol);
			if(pCell3) {
				pCell3->SetBackClr(RGB(125,125,125));
			}
		} else {
			m_GridDev.SetItemState(w_Row, w_nCol, m_GridDev.GetItemState(w_Row, w_nCol) & (~GVIS_READONLY));
			m_GridDev.SetCellType(w_Row, w_nCol, RUNTIME_CLASS(CGridCellCombo));
			CStringArray options3;

			for(w_jj=0;w_jj< LPA_DFR_ZONE_MAX;w_jj++)
			{
				options3.Add(gp_Zone[w_jj]);
			}
			CGridCellCombo *pCell5 = (CGridCellCombo*) m_GridDev.GetCell(w_Row, w_nCol);
			pCell5->SetBackClr(RGB(255,255,255));
			pCell5->SetOptions(options3);
			pCell5->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE

			//DL_LOG("ZONE_B:%d", wp_PAData->m_DfrData[w_Addr-1].m_Item[w_Row-1].m_Pos.m_Zone );
			if(wp_PAData->m_DfrData[w_Addr-1].m_Item[w_Row-1].m_Pos.m_Zone  < LPA_DFR_ZONE_MAX) {
				w_csStr.Format(_T("%s"), gp_Zone[wp_PAData->m_DfrData[w_Addr-1].m_Item[w_Row-1].m_Pos.m_Zone]);
			} else {
				w_csStr.Format(_T("%s"), gp_Zone[0]);
			}
			m_GridDev.SetItemText(w_Row, w_nCol, w_csStr);

		}
	}
	m_GridDev.Invalidate();
}


void CDLDfrSetDlg::OnBnClickedButtonAdd()
{
	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	if(m_SelAddr <= 0) {
		return;
	}
	if(wp_PAData->m_DevCnt[LPA_DFR_IDX] < 1) {
		MessageBox(_T("추가할수 없습니다. (DFR 장치수가  0입니다.)"), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	int w_jj = m_SelAddr-1;
	int	w_Item = 0;

	if(wp_PAData->m_DfrData[w_jj].m_PRFlag  == 0) { // P형 ==> LPA_DFR_PITEM으로 제한함
		if(m_GridDev.GetRowCount() > LPA_DFR_PITEM) {
			MessageBox(_T("더 이상 추가할수 없습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
			return;
		}
	} else {
		if(m_GridDev.GetRowCount() > LPA_DFR_ITEM) {
			MessageBox(_T("더 이상 추가할수 없습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
			return;
		}
	}

	wp_PAData->m_DfrData[w_jj].m_Cnt++;
	w_Item = wp_PAData->m_DfrData[w_jj].m_Cnt-1;

	memset(&wp_PAData->m_DfrData[w_jj].m_Item[w_Item], 0x00, sizeof(LPA_ITEM_T));
	memset(&wp_PAData->m_DfrTerm[w_jj][w_Item], 0x00, sizeof(LPA_DFR_TERM_T));

	if(w_Item >= 1) {
		wp_PAData->m_DfrData[w_jj].m_Item[w_Item].m_Pos.m_Building = wp_PAData->m_DfrData[w_jj].m_Item[w_Item-1].m_Pos.m_Building;
		wp_PAData->m_DfrData[w_jj].m_Item[w_Item].m_Pos.m_Zone = wp_PAData->m_DfrData[w_jj].m_Item[w_Item-1].m_Pos.m_Zone;
		wp_PAData->m_DfrData[w_jj].m_Item[w_Item].m_Pos.m_Stair = wp_PAData->m_DfrData[w_jj].m_Item[w_Item-1].m_Pos.m_Stair;
		wp_PAData->m_DfrData[w_jj].m_Item[w_Item].m_Pos.m_Floor = wp_PAData->m_DfrData[w_jj].m_Item[w_Item-1].m_Pos.m_Floor+1;
		if(wp_PAData->m_DfrData[w_jj].m_Item[w_Item].m_Pos.m_Floor >= LPA_DFR_FLOOR_MAX) {
			wp_PAData->m_DfrData[w_jj].m_Item[w_Item].m_Pos.m_Floor = LPA_DFR_FLOOR_MAX-1;
		}
	}
	DrawDfrItem(w_jj+1, w_Item+1);

	m_GridDev.SetCurSel(w_Item+1);
	m_SelItem = w_Item+1;

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}

	GetLowPA()->SetModified(1);
}


void CDLDfrSetDlg::OnBnClickedButtonDel()
{
	int		w_ii;
	int		w_Cnt = 0;
	int		w_OKCnt = 0;
	CString w_csStr;
	int		w_jj;
	int		w_Col;


	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	if(wp_PAData->m_DevCnt[LPA_DFR_IDX] < 1) {
		MessageBox(_T("삭제 할 수 없습니다. (DFR 장치수가  0입니다.)"), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}

	if(m_SelAddr < 1) {
		return;
	}
	if(m_GridDev.GetRowCount() <= 0) {
		MessageBox(_T("더이상 삭제할 장치가 없습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	w_Col = GRID_IDX_MODYN;

	for(w_ii=m_GridDev.GetRowCount()-1;w_ii>=1;w_ii--) {
		CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, w_Col);
		if(pCell) {
			if(pCell->GetCheck()) {
				w_Cnt++;
			}
		}
	}
	if(w_Cnt == 0) {
		MessageBox(_T("삭제할 장치를 선택해주십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	} 

	w_Cnt = 0;
	w_jj = m_SelAddr-1;


	LPA_ITEM_T			w_Item[LPA_ITEM_MAX];
	LPA_DFR_TERM_T		w_DfrTerm[LPA_DFR_ITEM];

	memset(w_Item, 0x00, sizeof(w_Item));
	memset(w_DfrTerm, 0x00, sizeof(w_DfrTerm));

	// DATA부터 정리
	for(w_ii=1; w_ii<m_GridDev.GetRowCount();w_ii++) {
		CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, w_Col);
		if(pCell) {
			if(!pCell->GetCheck()) {
				memcpy(&w_Item[w_OKCnt], &wp_PAData->m_DfrData[w_jj].m_Item[w_ii-1], sizeof(LPA_ITEM_T));
				memcpy(&w_DfrTerm[w_OKCnt], &wp_PAData->m_DfrTerm[w_jj][w_ii-1], sizeof(LPA_DFR_TERM_T));

				w_OKCnt++;
			}
		}
	}

	memset(&wp_PAData->m_DfrData[w_jj].m_Item[0], 0x00, sizeof(LPA_ITEM_T) * LPA_ITEM_MAX);
	memset(&wp_PAData->m_DfrTerm[w_jj][0], 0x00, sizeof(LPA_DFR_TERM_T) * LPA_DFR_ITEM);

	if(w_OKCnt > 0) {
		memcpy(&wp_PAData->m_DfrData[w_jj].m_Item[0], &w_Item[0], sizeof(w_Item));
		memcpy(&wp_PAData->m_DfrTerm[w_jj][0], &w_DfrTerm[0], sizeof(w_DfrTerm));
	}


	for(w_ii=m_GridDev.GetRowCount()-1;w_ii>=0;w_ii--) {
		CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, w_Col);
		if(pCell) {
			if(pCell->GetCheck()) {
				if(w_ii == 0) {
					pCell->SetCheck(BST_UNCHECKED);
				} else {
					m_GridDev.DeleteRow(w_ii);
					w_Cnt++;
				}
			}
		}
	}
	wp_PAData->m_DfrData[w_jj].m_Cnt = w_OKCnt;
	DrawDfrItem(m_SelAddr);

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}

	GetLowPA()->SetModified(1);

}

void CDLDfrSetDlg::OnCbnSelchangeComboDfr()
{
	int	w_Addr;

	// 현재 선택되어진 아이템
	int nIndex = m_comboDfr.GetCurSel();
	if(nIndex < 0) {
		return;
	}
	w_Addr = m_SelAddr = nIndex + 1;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	SetBtnType(wp_PAData->m_DfrData[w_Addr-1].m_PRFlag);	// P/R
	SetBtnData(wp_PAData->m_DfrData[w_Addr-1].m_BFlag);		// 10/12


	InitGridHeader();
	DrawDfrItem(m_SelAddr);

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}
}



LRESULT CDLDfrSetDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_SELPAGE:
			m_SelAddr = 1;
			m_SelItem = 1;

			if(m_comboDfr.GetCount() > 0) {
				if(m_SelAddr > 0) {
					m_comboDfr.SetCurSel(m_SelAddr-1);
				} else {
					m_comboDfr.SetCurSel(0);
				}
				DrawDfrItem(m_SelAddr);
			}
			break;
		case WM_INVALIDATE:
		case WM_CHANGEDEQUIP:
			m_SelAddr = 1;
			m_SelItem = 1;
			DrawDfrCombo();
			break;

	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}

void CDLDfrSetDlg::OnBnClickedRadioP()
{
	int			w_Addr;
	LPA_DATA_T *wp_PAData = GetPAData();

	w_Addr = m_SelAddr;
	if(w_Addr < 1) {
		return;
	}
	if(wp_PAData->m_DfrData[w_Addr-1].m_PRFlag == 0) {
		return;
	}
	if(MessageBox(_T("형변경시 기존 설정정보는 모드삭제됩니다.\r\n계속하시겠습니까?"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
		SetBtnType(1);
		return;
	}
	if(!wp_PAData) {
		return;
	}
	m_btnData10.EnableWindow(FALSE);
	m_btnData12.EnableWindow(FALSE);

	memset(&wp_PAData->m_DfrData[w_Addr-1].m_Item[0], 0x00, sizeof(LPA_ITEM_T) * LPA_DFR_ITEM);
	memset(&wp_PAData->m_DfrTerm[w_Addr-1][0], 0x00, sizeof(LPA_DFR_TERM_T) * LPA_DFR_ITEM);

	wp_PAData->m_DfrData[w_Addr-1].m_PRFlag = 0;	// P
	wp_PAData->m_DfrData[w_Addr-1].m_BFlag = 0;
	wp_PAData->m_DfrData[w_Addr-1].m_Cnt = 0;

	InitGridHeader();
	DrawDfrItem(m_SelAddr);
}

void CDLDfrSetDlg::OnBnClickedRadioR()
{
	int			w_Addr;
	LPA_DATA_T *wp_PAData = GetPAData();

	w_Addr = m_SelAddr;
	if(w_Addr < 1) {
		return;
	}
	if(wp_PAData->m_DfrData[w_Addr-1].m_PRFlag == 1) {
		return;
	}
	if(MessageBox(_T("형변경시 기존 설정정보는 모드삭제됩니다.\r\n계속하시겠습니까?"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
		SetBtnType(0);
		return;
	}
	if(!wp_PAData) {
		return;
	}
	m_btnData10.EnableWindow(TRUE);
	m_btnData12.EnableWindow(TRUE);

	memset(&wp_PAData->m_DfrData[w_Addr-1].m_Item[0], 0x00, sizeof(LPA_ITEM_T) * LPA_ITEM_MAX);
	memset(&wp_PAData->m_DfrTerm[w_Addr-1][0], 0x00, sizeof(LPA_DFR_TERM_T) * LPA_DFR_ITEM);

	wp_PAData->m_DfrData[w_Addr-1].m_PRFlag = 1;	// R
	wp_PAData->m_DfrData[w_Addr-1].m_BFlag = 0;
	wp_PAData->m_DfrData[w_Addr-1].m_Cnt = 0;

	SetBtnData(wp_PAData->m_DfrData[w_Addr-1].m_BFlag);

	InitGridHeader();
	DrawDfrItem(m_SelAddr);
}

void CDLDfrSetDlg::OnBnClickedRadio10()
{
	int			w_Addr;
	LPA_DATA_T *wp_PAData = GetPAData();

	w_Addr = m_SelAddr;
	if(!wp_PAData || w_Addr < 1) {
		return;
	}
	wp_PAData->m_DfrData[w_Addr-1].m_BFlag = 0;	// 10 Byte

	DrawDfrData(wp_PAData->m_DfrData[w_Addr-1].m_BFlag);

}

void CDLDfrSetDlg::OnBnClickedRadio12()
{
	int			w_Addr;
	LPA_DATA_T *wp_PAData = GetPAData();

	w_Addr = m_SelAddr;
	if(!wp_PAData || w_Addr < 1) {
		return;
	}

	wp_PAData->m_DfrData[w_Addr-1].m_BFlag = 1;	// 12 Byte

	DrawDfrData(wp_PAData->m_DfrData[w_Addr-1].m_BFlag);
}


void CDLDfrSetDlg::SetBtnType(int a_Type)
{
	if(a_Type == 0) {
		m_btnTypeP.SetCheck(TRUE);
		m_btnTypeR.SetCheck(FALSE);

		m_btnData10.EnableWindow(FALSE);
		m_btnData12.EnableWindow(FALSE);
	} else {
		m_btnTypeP.SetCheck(FALSE);
		m_btnTypeR.SetCheck(TRUE);

		m_btnData10.EnableWindow(TRUE);
		m_btnData12.EnableWindow(TRUE);
	}
}



void CDLDfrSetDlg::SetBtnData(int a_Data)
{
	if(a_Data == 0) {
		m_btnData10.SetCheck(TRUE);
		m_btnData12.SetCheck(FALSE);
	} else {
		m_btnData10.SetCheck(FALSE);
		m_btnData12.SetCheck(TRUE);
	}
}

void CDLDfrSetDlg::OnBnClickedButtonCheck()
{
	if(m_SelAddr < 1) {
		return;
	}
	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	int		w_ii;
	int		w_jj;
	TCHAR	w_Msg[256];

	for(w_ii = 0;w_ii<wp_PAData->m_DfrData[m_SelAddr-1].m_Cnt;w_ii++) {
		for(w_jj = w_ii+1;w_jj<wp_PAData->m_DfrData[m_SelAddr-1].m_Cnt;w_jj++) {
			if(wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Building == wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_jj].m_Pos.m_Building
				&& wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Stair == wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_jj].m_Pos.m_Stair
				&& wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Floor == wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_jj].m_Pos.m_Floor
				&& wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Zone == wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_jj].m_Pos.m_Zone) {

				if(wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Zone  >= LPA_DFR_ZONE_MAX) {
					wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Zone = 0;
				} 
				
				wsprintf(w_Msg, _T("%d항과 %d항이 같습니다.(%d동 %d라인 %s %d층)\r\n맨 첫 오류만 표시했습니다.\r\n수정 후 다시 체크해주세요."), w_ii+1, w_jj+1, 
					wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Building+1,
					wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Stair+1,
					gp_Zone[wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Zone],
					wp_PAData->m_DfrData[m_SelAddr-1].m_Item[w_ii].m_Pos.m_Floor+1);

				MessageBox(w_Msg, _T("실행오류"), MB_OK | MB_ICONWARNING);
				return;

			}
		}
	}
	MessageBox(_T("중복된 내용이 없습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
}
void CDLDfrSetDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;
	CRect w_Rect;
	GetClientRect(&w_Rect);

	if(m_btnAdd.GetSafeHwnd())
	{	
		w_X = w_Rect.Width()/3 - 110;
		w_Y = w_Rect.bottom - 40;
		w_W = 100;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_btnAdd.MoveWindow(&w_ButtonRc);
	}

	if(m_btnDel.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/3 + 15;
		w_Y = w_Rect.bottom - 40;
		w_W = 100;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_btnDel.MoveWindow(&w_ButtonRc);
	}

	if(m_btnCheck.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/3 + 140;
		w_Y = w_Rect.bottom - 40;
		w_W = 100;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_btnCheck.MoveWindow(&w_ButtonRc);
	}

	if(m_comboDfr.GetSafeHwnd())
	{	
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 5;
		w_W = 350;
		w_H = 300;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_comboDfr.MoveWindow(&w_ButtonRc);
	}

	if(GetDlgItem(IDC_RADIO_P)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 10;
		w_Y = 30;
		w_W = 80;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_RADIO_P)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_RADIO_P)->Invalidate();
	}

	if(GetDlgItem(IDC_STATIC)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 58;
		w_W = 350;
		w_H = 35;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_STATIC)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_STATIC)->Invalidate();
	}
	if(GetDlgItem(IDC_RADIO_R)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 10;
		w_Y = 62;
		w_W = 80;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_RADIO_R)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_RADIO_R)->Invalidate();
	}

	if(GetDlgItem(IDC_RADIO_10)->GetSafeHwnd())
	{
		w_X = w_X+100;
		w_Y = 62;
		w_W = 80;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_RADIO_10)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_RADIO_10)->Invalidate();
	}
	if(GetDlgItem(IDC_RADIO_12)->GetSafeHwnd())
	{
		w_X =w_X+90;
		w_Y = 62;
		w_W = 80;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_RADIO_12)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_RADIO_12)->Invalidate();
	}

	if(m_GridDev.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 100;
		w_W = 350;
		w_H = w_Rect.bottom - 50 - 100;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_GridDev.MoveWindow(&w_ButtonRc);
	}
}
