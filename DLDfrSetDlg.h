#pragma once

#include "GridCtrl_src/GridCtrl.h"
#include "NewCellTypes/GridURLCell.h"
#include "NewCellTypes/GridCellCombo.h"
#include "NewCellTypes/GridCellCheck.h"
#include "NewCellTypes/GridCellNumeric.h"
#include "NewCellTypes/GridCellDateTime.h"
#include "afxwin.h"


// CDLDfrSetDlg 대화 상자입니다.

class CDLDfrSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLDfrSetDlg)

public:
	CDLDfrSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLDfrSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLDFR_DIALOG };

	int			m_SelAddr;					// 선택 Devide 주소
	int			m_SelItem;					// 선택 Device Item(Output)

public:
	void			initScreen();
	void			InitGridHeader();

	void			OnModYnClick(int a_Row);

	VOID			DrawDfrCombo();
	void			DrawDfrItem(int a_Addr);
	void			DrawDfrItem(int a_Addr, int a_Row);

	void			RedrawCount(int a_Row);

	void			SetBtnType(int a_Type);
	void			SetBtnData(int a_Data);

	void			DrawDfrData(int a_Data);
public:

	CGridCtrl		m_GridDev;
	CComboBox		m_comboDfr;

	CButton			m_btnTypeP;
	CButton			m_btnTypeR;
	CButton			m_btnData10;
	CButton			m_btnData12;
	CButton			m_btnAdd;
	CButton			m_btnDel;
	CButton			m_btnCheck;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnCbnSelchangeComboDfr();
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedRadioP();
	afx_msg void OnBnClickedRadioR();
	afx_msg void OnBnClickedRadio10();
	afx_msg void OnBnClickedRadio12();
	afx_msg void OnBnClickedButtonCheck();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
