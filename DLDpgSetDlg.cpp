// DLDpgSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLDpgSetDlg.h"
#include "DLLowPAPopupDlg.h"

#define	GRID_IDX_ID				0
#define	GRID_IDX_NAME			1
#define GRID_IDX_NUM			2

// CDLDpgSetDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLDpgSetDlg, CDialog)

CDLDpgSetDlg::CDLDpgSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLDpgSetDlg::IDD, pParent)
{

	m_SelAddr = 1;		// 선택 Devide 주소
	m_SelItem = 1;		// 선택 Device Item(Output)

}

CDLDpgSetDlg::~CDLDpgSetDlg()
{
}

void CDLDpgSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_DPG, m_comboDpg);
}


BEGIN_MESSAGE_MAP(CDLDpgSetDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_NOTIFY(NM_CLICK, IDC_LST_DPG_LIST, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_LST_DPG_LIST, OnGridDblClick)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_LST_DPG_LIST, OnGridEndEdit)
	ON_NOTIFY(GVN_SELCHANGED, IDC_LST_DPG_LIST, OnGridEndSelChange)
	ON_CBN_SELCHANGE(IDC_COMBO_DPG, &CDLDpgSetDlg::OnCbnSelchangeComboDpg)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CDLDpgSetDlg 메시지 처리기입니다.

BOOL CDLDpgSetDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//return FALSE;
	return CDialog::OnEraseBkgnd(pDC);
}

void CDLDpgSetDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

BOOL CDLDpgSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initScreen();
	DrawDpg();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDLDpgSetDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CDLDpgSetDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch(message)
	{
		case WM_SELPAGE:
			m_SelAddr = 1;
			m_SelItem = 1;
			if(m_comboDpg.GetCount() > 0) {
				if(m_SelAddr > 0) {
					m_comboDpg.SetCurSel(m_SelAddr-1);
				} else {
					m_comboDpg.SetCurSel(0);
				}
				DrawDpgItem(m_SelAddr);
			}
			break;
		case WM_INVALIDATE:
		case WM_CHANGEDEQUIP:
			m_SelAddr = 1;
			m_SelItem = 1;
			InitGridHeader();
			DrawDpg();
			break;

	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}

void CDLDpgSetDlg::initScreen()
{
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;

	w_X = 5;
	w_Y = 5;
	w_W = 350;
	w_H = 300;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_comboDpg.MoveWindow(&w_ButtonRc);

	w_X = 5;
	w_Y = 30;
	w_W = 350;
	w_H = 630;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_GridDev.Create(w_ButtonRc, this, IDC_LST_DPG_LIST);
	m_GridDev.SetListMode(FALSE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.SetHeaderSort(FALSE);
	m_GridDev.SetSingleRowSelection(TRUE);
	m_GridDev.SetSingleColSelection(TRUE);
	//m_GridDev.SetEditable(m_bEditable);
	m_GridDev.EnableSelection(TRUE);
	m_GridDev.SetRowResize(TRUE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.EnableTitleTips(FALSE);
	m_GridDev.GetDefaultCell(FALSE, FALSE)->SetFormat(DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_NOPREFIX|DT_END_ELLIPSIS);	


	InitGridHeader();
}

void CDLDpgSetDlg::InitGridHeader()
{
	int nRowNum =0;
	int	i;
	int w_nW = 0;

	char *Llistcolumn[]={"번호", "설정지역명", "설정갯수"};
	int Lwidth[]       ={40, 200, 100};  

	m_GridDev.DeleteAllItems();
	m_GridDev.SetColumnCount(3);
	m_GridDev.SetRowCount(1);

	for (i = 0; i < 3; i++) 
	{		
		m_GridDev.SetItemText(nRowNum, i, CA2T(Llistcolumn[i]));
		m_GridDev.SetColumnWidth(i, Lwidth[i]);
	}

	m_GridDev.SetFixedRowCount(1);
	m_GridDev.SetRowHeight(0, 30);
	m_GridDev.SetSingleRowSelection(1);
	m_GridDev.SetHeaderSort(0);
	m_GridDev.SetListMode(1);
	m_GridDev.SetFixedColumnSelection(0);


}

void CDLDpgSetDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	//DL_LOG_SUB("OnGridClick:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
}

void CDLDpgSetDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}


void CDLDpgSetDlg::OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

//DL_LOG("OnGridEndSelChange:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	m_SelItem = pItem->iRow;
	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}
	RedrawCount(pItem->iRow);
}



void CDLDpgSetDlg::OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
//DL_LOG("OnGridEndEdit:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	if( pItem->iColumn == GRID_IDX_NAME && pItem->iRow > 0) {
		CString w_csStr;

		w_csStr = m_GridDev.GetItemText(pItem->iRow, pItem->iColumn);

		m_SelItem = pItem->iRow;
		GetLowPA()->SetDpgName(CT2A(w_csStr), m_SelAddr, m_SelItem);

	}
}


void CDLDpgSetDlg::RedrawCount(int a_Row)
{
	int w_Cnt;
	int w_nCol;
	CString w_csStr;

	if(a_Row > 0) {
		w_nCol = GRID_IDX_NUM;
		w_Cnt = GetLowPA()->GetDpgCount(m_SelAddr, m_SelItem);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
		m_GridDev.Invalidate();
	}

}

void CDLDpgSetDlg::DrawDpg()
{
	CString		w_Str;
	int			w_ii;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	m_comboDpg.ResetContent();
	for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DPG_IDX];w_ii++) {
		w_Str.Format(_T("DPG %d"), w_ii+1);
		
		m_comboDpg.AddString(w_Str);
	}

	if(m_comboDpg.GetCount() > 0) {
		m_GridDev.SetRowCount(LPA_DPG_ITEM+1);
		m_comboDpg.SetCurSel(0);

		DrawDpgItem(m_SelAddr);
	} else {
		m_GridDev.SetRowCount(1);
	}
}


void CDLDpgSetDlg::DrawDpgItem(int a_Addr)
{
	int		w_ii;
	int		w_nCol;
	int		nRowNum;
	CString w_csStr;
	int		w_Cnt = 0;

	CString CA2TTemp;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData || a_Addr < 1) {
		return;
	}

	for(w_ii=0;w_ii<LPA_DPG_ITEM;w_ii++) {
		nRowNum = w_ii+1;
		w_nCol = GRID_IDX_ID;
		w_csStr.Format(_T("%d"), nRowNum);
		m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

		//Name
		w_nCol = GRID_IDX_NAME;
		if(wp_PAData->m_DpgData[a_Addr-1].m_Item[w_ii].m_Name[0]) {
			CA2TTemp = CA2T(wp_PAData->m_DpgData[a_Addr - 1].m_Item[w_ii].m_Name);
			w_csStr.Format(_T("%s"), CA2TTemp);
		} else {
			w_csStr.Format(_T("AMP %d"), ((a_Addr-1) * LPA_DPG_ITEM)+w_ii+1);
		}
		m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol));
		m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

		//Delete
		w_nCol = GRID_IDX_NUM;
		w_Cnt = GetLowPA()->GetDpgCount(a_Addr, w_ii+1);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);
	}

	m_GridDev.SetFocusCell(-1,-1);
	m_GridDev.SetCurSel(0);
	m_GridDev.Invalidate();
}

void CDLDpgSetDlg::OnCbnSelchangeComboDpg()
{
	// 현재 선택되어진 아이템
	int nIndex = m_comboDpg.GetCurSel();
	if(nIndex == -1) {
		return;
	}
	m_SelAddr = nIndex + 1;
	DrawDpgItem(m_SelAddr);

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}
}

void CDLDpgSetDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;
	CRect w_Rect;
	GetClientRect(&w_Rect);

	if(m_comboDpg.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 5;
		w_W = 350;
		w_H = 300;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_comboDpg.MoveWindow(&w_ButtonRc);
	}

	if(m_GridDev.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 20+10;
		w_W = 350;
		w_H = w_Rect.Height()-35;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_GridDev.MoveWindow(&w_ButtonRc);
	}
}
