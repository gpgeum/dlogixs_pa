#pragma once
#include "afxwin.h"

#include "GridCtrl_src/GridCtrl.h"
#include "NewCellTypes/GridURLCell.h"
#include "NewCellTypes/GridCellCombo.h"
#include "NewCellTypes/GridCellCheck.h"
#include "NewCellTypes/GridCellNumeric.h"
#include "NewCellTypes/GridCellDateTime.h"


// CDLDpgSetDlg 대화 상자입니다.

class CDLDpgSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLDpgSetDlg)

public:
	CDLDpgSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLDpgSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLDPGSET_DIALOG };

	int		m_SelAddr;					// 선택 Devide 주소
	int		m_SelItem;					// 선택 Device Item(Output)
public:
	void	initScreen();
	void	InitGridHeader();

	void	DrawDpg();
	void	DrawDpgItem(int a_Addr);
	void	RedrawCount(int a_Row);
public:
	CGridCtrl	m_GridDev;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboDpg;
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnCbnSelchangeComboDpg();
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
