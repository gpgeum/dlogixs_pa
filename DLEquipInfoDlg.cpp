// DLEquipInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLEquipInfoDlg.h"


// CDLEquipInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLEquipInfoDlg, CDialog)

CDLEquipInfoDlg::CDLEquipInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLEquipInfoDlg::IDD, pParent)
{
	m_EquipDisplay = _T("");
}

CDLEquipInfoDlg::~CDLEquipInfoDlg()
{
}

void CDLEquipInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_EQUIP, m_EquipLabel);
}


BEGIN_MESSAGE_MAP(CDLEquipInfoDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_NOTIFY(NM_CLICK, IDC_LST_EQUIPINFO_LIST, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_LST_EQUIPINFO_LIST, OnGridDblClick)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_LST_EQUIPINFO_LIST, OnGridEndEdit)
	ON_NOTIFY(GVN_SELCHANGED, IDC_LST_EQUIPINFO_LIST, OnGridEndSelChange)
END_MESSAGE_MAP()


// CDLEquipInfoDlg 메시지 처리기입니다.

BOOL CDLEquipInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	MoveWindow(0,0,800,600);
	CenterWindow();

	initScreen();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDLEquipInfoDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return FALSE;
	return CDialog::OnEraseBkgnd(pDC);
}

void CDLEquipInfoDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

BOOL CDLEquipInfoDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CDLEquipInfoDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch(message)
	{
		case WM_UPDATE_INFO:
			DrawEquipInfo();
			break;
		case WM_UPDATE_DATA:
			DrawEquipData();
			break;
	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}

void CDLEquipInfoDlg::initScreen()
{
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;
	CString strFormat = _T("");

	int w_DmtCnt=1;
	int w_DmtCon=0;
	int w_DpgCnt=0;
	int w_DpgCon=0;
	int w_DreCnt=0;
	int w_DreCon=0;
	int w_DrmCnt=0;
	int w_DrmCon=0;
	int w_DpaCnt=0;
	int w_DpaCon=0;
	int w_DmpCnt=0;
	int w_DmpCon=0;
	int w_DssCnt=0;
	int w_DssCon=0;
	int w_DfrCnt=0;
	int w_DfrCon=0;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(wp_PAData) {
		w_DpgCnt = wp_PAData->m_DevCnt[LPA_DPG_IDX];
		w_DreCnt = wp_PAData->m_DevCnt[LPA_DRE_IDX];
		w_DrmCnt = wp_PAData->m_DevCnt[LPA_DRM_IDX];
		w_DpaCnt = 0;
		w_DmpCnt = wp_PAData->m_DevCnt[LPA_DMP_IDX];
		w_DssCnt = wp_PAData->m_DevCnt[LPA_DSS_IDX];
		w_DfrCnt = wp_PAData->m_DevCnt[LPA_DFR_IDX];
	}


	w_X = 4;
	w_Y = 2;
	w_W = 788;
	w_H = 90;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_EquipLabel.MoveWindow(&w_ButtonRc);
	strFormat.LoadString(IDS_STRING_EQUIPINFO);
	m_EquipDisplay.Format(strFormat,w_DmtCnt,w_DmtCon,w_DmtCnt-w_DmtCon,
									w_DpgCnt,w_DpgCon,w_DpgCnt-w_DpgCon,
									w_DreCnt,w_DreCon,w_DreCnt-w_DreCon,
									w_DrmCnt,w_DrmCon,w_DrmCnt-w_DrmCon,
									w_DpaCnt,w_DpaCon,w_DpaCnt-w_DpaCon,
									w_DmpCnt,w_DmpCon,w_DmpCnt-w_DmpCon,
									w_DssCnt,w_DssCon,w_DssCnt-w_DssCon,
									w_DfrCnt,w_DfrCon,w_DfrCnt-w_DfrCon	);
	m_EquipLabel.SetFont(&g_titleFont);
	m_EquipLabel.SetWindowText(m_EquipDisplay);
	

	w_X = 2;
	w_Y = 32+60;
	w_W = 790;
	w_H = 478;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_GridDev.Create(w_ButtonRc, this, IDC_LST_EQUIPINFO_LIST);
	m_GridDev.SetListMode(FALSE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.SetHeaderSort(FALSE);
	m_GridDev.SetSingleRowSelection(TRUE);
	m_GridDev.SetSingleColSelection(TRUE);
	//m_GridDev.SetEditable(m_bEditable);
	m_GridDev.EnableSelection(TRUE);
	m_GridDev.SetRowResize(TRUE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.EnableTitleTips(FALSE);
	m_GridDev.GetDefaultCell(FALSE, FALSE)->SetFormat(DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_NOPREFIX|DT_END_ELLIPSIS);	

	InitGridHeader();
}

void CDLEquipInfoDlg::InitGridHeader()
{
	int nRowNum =0;
	int	i;
	int w_nW = 0;

	char *Llistcolumn[]={"번호", "장비타입", "장비번호", "부트버전", "APP버전", "기타", "비고" };
	int Lwidth[]       ={40, 100, 100, 100, 100, 200, 144 };

	m_GridDev.DeleteAllItems();
	m_GridDev.SetColumnCount(7);
	m_GridDev.SetRowCount(1);

	for (i = 0; i < 7; i++) 
	{		
		m_GridDev.SetItemText(nRowNum, i, CA2T(Llistcolumn[i]));
		m_GridDev.SetColumnWidth(i, Lwidth[i]);
	}

	m_GridDev.SetFixedRowCount(1);
	m_GridDev.SetRowHeight(0, 30);
	m_GridDev.SetSingleRowSelection(1);
	m_GridDev.SetHeaderSort(0);
	m_GridDev.SetListMode(1);
	m_GridDev.SetFixedColumnSelection(0);
}

void CDLEquipInfoDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	//DL_LOG_SUB("OnGridClick:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
}

void CDLEquipInfoDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}


void CDLEquipInfoDlg::OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	//DL_LOG("OnGridEndSelChange:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	//m_SelItem = pItem->iRow;
	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}
	RedrawCount(pItem->iRow);
}



void CDLEquipInfoDlg::OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//DL_LOG("OnGridEndEdit:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	//if( pItem->iColumn == GRID_IDX_NAME && pItem->iRow > 0) {
	//	CString w_csStr;

	//	w_csStr = m_GridDev.GetItemText(pItem->iRow, pItem->iColumn);

	//	//m_SelItem = pItem->iRow;
	//	//GetLowPA()->SetDpgName(CT2A(w_csStr), m_SelAddr, m_SelItem);

	//}
}

void CDLEquipInfoDlg::RedrawCount(int a_Row)
{
#if 0
	int w_Cnt;
	int w_nCol;
	CString w_csStr;

	if(a_Row > 0) 
	{
		w_nCol = GRID_IDX_NUM;
		w_Cnt = GetLowPA()->GetDpgCount(m_SelAddr, m_SelItem);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
		m_GridDev.Invalidate();
	}
#endif
}

void CDLEquipInfoDlg::DrawEquipData()
{
	int			w_ii=0;
	int			w_Total=0;
	int			w_nCol;
	int			nRowNum=0;
	int			w_Cnt = 0;
	int			w_DevCnt=0;
	CString		w_csStr = _T("");
	CString		strFormat = _T("");

	int			w_DmtCnt=1;
	int			w_DmtCon=0;
	int			w_DpgCnt=0;
	int			w_DpgCon=0;
	int			w_DreCnt=0;
	int			w_DreCon=0;
	int			w_DrmCnt=0;
	int			w_DrmCon=0;
	int			w_DpaCnt=0;
	int			w_DpaCon=0;
	int			w_DmpCnt=0;
	int			w_DmpCon=0;
	int			w_DssCnt=0;
	int			w_DssCon=0;
	int			w_DfrCnt=0;
	int			w_DfrCon=0;


	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	w_Total =	1 + 
				wp_PAData->m_DevCnt[LPA_DSS_IDX] + 
				wp_PAData->m_DevCnt[LPA_DPG_IDX] + 
				wp_PAData->m_DevCnt[LPA_DFR_IDX] + 
				wp_PAData->m_DevCnt[LPA_DRM_IDX] + 
				wp_PAData->m_DevCnt[LPA_DRE_IDX] + 
				wp_PAData->m_DevCnt[LPA_DMP_IDX] + 1;

	w_DpgCnt = wp_PAData->m_DevCnt[LPA_DPG_IDX];
	w_DreCnt = wp_PAData->m_DevCnt[LPA_DRE_IDX];
	w_DrmCnt = wp_PAData->m_DevCnt[LPA_DRM_IDX];
	w_DpaCnt = 0;
	w_DmpCnt = wp_PAData->m_DevCnt[LPA_DMP_IDX];
	w_DssCnt = wp_PAData->m_DevCnt[LPA_DSS_IDX];
	w_DfrCnt = wp_PAData->m_DevCnt[LPA_DFR_IDX];

	InitGridHeader();
	if(w_Total > 0) 
	{
		m_GridDev.SetRowCount(w_Total+1);
		//DMT
		for(w_ii=0;w_ii<1;w_ii++) 
		{
			if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0)
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DMT");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY );
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY );
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol) | GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DmtCon++;
			}
		}

		//DPG
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DPG_IDX];w_ii++) 
		{
			if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nEquipNo>0)
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DPG");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DpgCon++;
			}
		}

		//DRE
		if(wp_PAData->m_DevCnt[LPA_DRE_IDX]>0)
		{
			w_DevCnt = 7;
		}
		for(w_ii=0;w_ii<w_DevCnt;w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nEquipNo>=0 && 
				(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ||
				 GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED ) )
			{
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DRE");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				w_csStr = _T("0");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nEquipNo>=0)
				{
					w_csStr.Format(_T("[DPG출력번호:%d]"), GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("[DMT]");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//if(GetLowPA()->m_PAData.m_DevCnt[LPA_DPG_IDX]==0)
				//{
				//	DL_LOG("[DRE-BREAK-DPG : 0]")
				//	break;
				//}

				w_DreCon++;
			}
		}

		//DRM
		w_DevCnt = 63;
		for(w_ii=0;w_ii<w_DevCnt;w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nEquipNo>=0 && 
				(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ||
				 GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED ) )
			{
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DRM");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[1]);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[0]>=0)
				{
					w_csStr.Format(_T("[DPG출력번호:%d] [DRE출력번호:%d]") ,GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[0]
																		   ,GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr.Format(_T("[DPG출력번호:%d] [DMT]") ,GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[0]);
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//if(GetLowPA()->m_PAData.m_DevCnt[LPA_DPG_IDX]==0)
				//{
				//	DL_LOG("[DRM-BREAK-DPG : 0]")
				//	break;
				//}

				w_DrmCon++;
			}
		}

#if 0	//5000인경우
		//DPA
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DMA_IDX];w_ii++) 
		{
			if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0)
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DPA");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DpaCon++;
			}
		}
#endif

		//DMP
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DMP_IDX];w_ii++) 
		{
			if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nEquipNo>0)
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DMP");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DmpCon++;
			}
		}

		//DSS
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DSS_IDX];w_ii++) 
		{
			if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nEquipNo>0)
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DSS");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DssCon++;
			}
		}

		//DFR
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DFR_IDX];w_ii++) 
		{
			if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nEquipNo>0)
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DFR");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DfrCon++;
			}
		}

		//DMX
		//for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DFR_IDX];w_ii++) 
		{
			if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nEquipNo>0)
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr.Format(_T("DMX"));
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] == LPA_DATA_FAILED )
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED)
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);
			}
		}
		m_GridDev.Invalidate();
	} else {
		m_GridDev.SetRowCount(1);
	}

	strFormat.LoadString(IDS_STRING_EQUIPINFO);
	m_EquipDisplay.Format(strFormat,w_DmtCnt,w_DmtCon,w_DmtCnt-w_DmtCon,
									w_DpgCnt,w_DpgCon,w_DpgCnt-w_DpgCon,
									w_DreCnt,w_DreCon,w_DreCnt-w_DreCon,
									w_DrmCnt,w_DrmCon,w_DrmCnt-w_DrmCon,
									w_DpaCnt,w_DpaCon,w_DpaCnt-w_DpaCon,
									w_DmpCnt,w_DmpCon,w_DmpCnt-w_DmpCon,
									w_DssCnt,w_DssCon,w_DssCnt-w_DssCon,
									w_DfrCnt,w_DfrCon,w_DfrCnt-w_DfrCon	);
	m_EquipLabel.SetWindowText(m_EquipDisplay);
}

void CDLEquipInfoDlg::DrawEquipInfo()
{
	int			w_ii=0;
	int			w_Total=0;
	int			w_nCol;
	int			nRowNum=0;
	int			w_Cnt = 0;
	int			w_DevCnt=0;
	CString		w_csStr = _T("");
	CString		strFormat = _T("");

	int			w_DmtCnt=1;
	int			w_DmtCon=0;
	int			w_DpgCnt=0;
	int			w_DpgCon=0;
	int			w_DreCnt=0;
	int			w_DreCon=0;
	int			w_DrmCnt=0;
	int			w_DrmCon=0;
	int			w_DpaCnt=0;
	int			w_DpaCon=0;
	int			w_DmpCnt=0;
	int			w_DmpCon=0;
	int			w_DssCnt=0;
	int			w_DssCon=0;
	int			w_DfrCnt=0;
	int			w_DfrCon=0;


	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	w_Total = 1 + 
			  wp_PAData->m_DevCnt[LPA_DSS_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DPG_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DFR_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DRM_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DRE_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DMP_IDX] + 1;

	w_DpgCnt = wp_PAData->m_DevCnt[LPA_DPG_IDX];
	w_DreCnt = wp_PAData->m_DevCnt[LPA_DRE_IDX];
	w_DrmCnt = wp_PAData->m_DevCnt[LPA_DRM_IDX];
	w_DpaCnt = 0;
	w_DmpCnt = wp_PAData->m_DevCnt[LPA_DMP_IDX];
	w_DssCnt = wp_PAData->m_DevCnt[LPA_DSS_IDX];
	w_DfrCnt = wp_PAData->m_DevCnt[LPA_DFR_IDX];

	InitGridHeader();
	if(w_Total > 0) 
	{
		m_GridDev.SetRowCount(w_Total+1);
		//DMT
		for(w_ii=0;w_ii<1;w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0 && 
				(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DMT");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY );
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY );
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol) | GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DmtCon++;
			}
		}

		//DPG
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DPG_IDX];w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nEquipNo>0 && 
				(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DPG");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}				
				else if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DpgCon++;
			}
		}

		//DRE
		//if(wp_PAData->m_DevCnt[LPA_DPG_IDX]>0)
		//{
			if(wp_PAData->m_DevCnt[LPA_DRE_IDX]>0)
			{
				w_DevCnt = 7;
			}
		//}else{
		//	if(wp_PAData->m_DevCnt[LPA_DRE_IDX]>0)
		//	{
		//		w_DevCnt = 1;
		//	}
		//}
		for(w_ii=0;w_ii<w_DevCnt;w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nEquipNo>=0 && 
				(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DRE");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				w_csStr = _T("0");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nEquipNo>=0)
				{
					w_csStr.Format(_T("[DPG출력번호:%d]"), GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("[DMT]");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//if(GetLowPA()->m_PAData.m_DevCnt[LPA_DPG_IDX]==0)
				//{
				//	DL_LOG("[DRE-BREAK-DPG : 0]")
				//	break;
				//}

				w_DreCon++;
			}
		}

		//DRM
		w_DevCnt = 63;
		for(w_ii=0;w_ii<w_DevCnt;w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nEquipNo>=0 && 
				(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DRM");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[1]);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nEquipNo>=0)
				{
					w_csStr.Format(_T("[DPG출력번호:%d] [DRE출력번호:%d]") ,GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[0]
																		   ,GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr.Format(_T("[DPG출력번호:%d] [DMT]") ,GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[0]);
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//if(GetLowPA()->m_PAData.m_DevCnt[LPA_DPG_IDX]==0)
				//{
				//	DL_LOG("[DRM-BREAK-DPG : 0]")
				//	break;
				//}
				w_DrmCon++;
			}
		}

#if 0	//5000인경우
		//DPA
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DMA_IDX];w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0 && 
				(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DPA");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DpaCon++;
			}
		}
#endif

		//DMP
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DMP_IDX];w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nEquipNo>0 && 
				(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DMP");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DmpCon++;
			}
		}

		//DSS
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DSS_IDX];w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nEquipNo>0 && 
				(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DSS");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DssCon++;
			}
		}

		//DFR
		for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DFR_IDX];w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nEquipNo>0 && 
				(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr = _T("DFR");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 3)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 2)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 1)
													, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				w_DfrCon++;
			}
		}

		//DMX
		//for(w_ii=0;w_ii<wp_PAData->m_DevCnt[LPA_DMX_IDX];w_ii++) 
		{
			if( GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nEquipNo>0 && 
				(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] == LPA_INFO_SUCESS || 
				 GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] == LPA_DATA_SUCESS ) )
			{			
				nRowNum++;
				//INDEX
				w_nCol = GRID_IDX_UPD_ID;
				w_csStr.Format(_T("%d"), nRowNum);
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//TYPE
				w_nCol = GRID_IDX_UPD_TYPE;
				w_csStr.Format(_T("DMX"));
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ADDRESS
				w_nCol = GRID_IDX_UPD_ADDR;
				if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nEquipNo>0)
				{
					w_csStr.Format(_T("%d"), GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nEquipNo);
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//BOOT VERSION
				w_nCol = GRID_IDX_UPD_BOOTV;
				if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nBootVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//APP VERSION
				w_nCol = GRID_IDX_UPD_APPV;
				if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer>0)
				{
					w_csStr.Format(_T("%d.%d.%d.%d"), *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 3)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 2)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 1)
						, *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].nApplVer) + 0));
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//ETC
				w_nCol = GRID_IDX_UPD_ETC;
				w_csStr = _T("");
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

				//CONTENT
				w_nCol = GRID_IDX_UPD_CONTENT;
				if( GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] == LPA_INFO_FAILED )
				{
					w_csStr = _T("정보조회 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] == LPA_INFO_SUCESS)
				{
					w_csStr = _T("정보조회 성공");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ii].arbyReserved[2] == LPA_DATA_FAILED)
				{
					w_csStr = _T("업데이트 실패");
				}
				else if(GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ii].arbyReserved[2] == LPA_DATA_SUCESS)
				{
					w_csStr = _T("업데이트 성공");
				}
				else
				{
					w_csStr = _T("");
				}
				m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol)| GVIS_READONLY);
				m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);
			}
		}
		m_GridDev.Invalidate();
	} else {
		m_GridDev.SetRowCount(1);
	}

	strFormat.LoadString(IDS_STRING_EQUIPINFO);
	m_EquipDisplay.Format(strFormat,w_DmtCnt,w_DmtCon,w_DmtCnt-w_DmtCon,
									w_DpgCnt,w_DpgCon,w_DpgCnt-w_DpgCon,
									w_DreCnt,w_DreCon,w_DreCnt-w_DreCon,
									w_DrmCnt,w_DrmCon,w_DrmCnt-w_DrmCon,
									w_DpaCnt,w_DpaCon,w_DpaCnt-w_DpaCon,
									w_DmpCnt,w_DmpCon,w_DmpCnt-w_DmpCon,
									w_DssCnt,w_DssCon,w_DssCnt-w_DssCon,
									w_DfrCnt,w_DfrCon,w_DfrCnt-w_DfrCon	);
	m_EquipLabel.SetWindowText(m_EquipDisplay);
}
