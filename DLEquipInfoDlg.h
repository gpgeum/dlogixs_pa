#pragma once
#include "afxwin.h"

#include "GridCtrl_src/GridCtrl.h"
#include "NewCellTypes/GridURLCell.h"
#include "NewCellTypes/GridCellCombo.h"
#include "NewCellTypes/GridCellCheck.h"
#include "NewCellTypes/GridCellNumeric.h"
#include "NewCellTypes/GridCellDateTime.h"

//#include "DLUpdateDevice.h"
#define	GRID_IDX_UPD_ID				0
#define	GRID_IDX_UPD_TYPE			1
#define	GRID_IDX_UPD_ADDR			2
#define	GRID_IDX_UPD_BOOTV			3
#define	GRID_IDX_UPD_APPV			4
#define	GRID_IDX_UPD_ETC			5
#define	GRID_IDX_UPD_CONTENT		6


// CDLEquipInfoDlg 대화 상자입니다.

class CDLEquipInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLEquipInfoDlg)

public:
	CDLEquipInfoDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLEquipInfoDlg();

public:
	void				initScreen();
	void				InitGridHeader();

	void				DrawEquipData();
	void				DrawEquipInfo();
	void				RedrawCount(int a_Row);

public:
	CGridCtrl			m_GridDev;

	CString				m_EquipDisplay;
	CStatic				m_EquipLabel;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLEQUIPINFO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};
