// DLEquipSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLEquipSetDlg.h"

#if defined(DEV_5000)
enum {
	ITEM_DMT_ROW = 0,
	ITEM_DPA_ROW,
	ITEM_DSS_ROW,
	ITEM_DPG_ROW,
	ITEM_DFR_ROW,
	ITEM_DRM_ROW,
	ITEM_DRE_ROW,
	ITEM_DMP_ROW
};
#else
enum {
	ITEM_DMT_ROW = 0,
	ITEM_DSS_ROW,
	ITEM_DPG_ROW,
	ITEM_DFR_ROW,
	ITEM_DRM_ROW,
	ITEM_DRE_ROW,
	ITEM_DMP_ROW
};
#endif

// CDLEquipSetDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLEquipSetDlg, CDialog)

CDLEquipSetDlg::CDLEquipSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLEquipSetDlg::IDD, pParent)
{
}

CDLEquipSetDlg::~CDLEquipSetDlg()
{
}

void CDLEquipSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STC_MSG, m_StcMsg);
}

BEGIN_MESSAGE_MAP(CDLEquipSetDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_NOTIFY(NM_CLICK, IDC_LST_EQUIP_LIST, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_LST_EQUIP_LIST, OnGridDblClick)
	ON_NOTIFY(GVN_SELCHANGED, IDC_LST_EQUIP_LIST, OnGridEndSelChange)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_LST_EQUIP_LIST, OnGridEndEdit)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CDLEquipSetDlg 메시지 처리기입니다.

void CDLEquipSetDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

BOOL CDLEquipSetDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//return FALSE;
	return CDialog::OnEraseBkgnd(pDC);
}

BOOL CDLEquipSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_StcMsg.SetBkColor(COLOR_WHITE);
	m_StcMsg.SetTextColor(COLOR_RED);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initScreen();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDLEquipSetDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


void CDLEquipSetDlg::initScreen()
{
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;

	w_X = 5;
	w_Y = 10;
	w_W = 350;
#if defined(DEV_5000)
	w_H = 220;
#else
	w_H = 190;
#endif

	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_GridDev.Create(w_ButtonRc, this, IDC_LST_EQUIP_LIST);
	m_GridDev.SetListMode(FALSE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.SetHeaderSort(FALSE);
	m_GridDev.SetSingleRowSelection(TRUE);
	m_GridDev.SetSingleColSelection(TRUE);
	//m_GridDev.SetEditable(m_bEditable);
	m_GridDev.EnableSelection(TRUE);
	m_GridDev.SetRowResize(TRUE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.EnableTitleTips(FALSE);
	m_GridDev.GetDefaultCell(FALSE, FALSE)->SetFormat(DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_NOPREFIX|DT_END_ELLIPSIS);	
	InitGrid();

#if defined(DEV_5000)
	RedrawGate();
#endif

	DrawEquip();
}

void CDLEquipSetDlg::InitGrid()
{
	int nRowNum;
	int	i;
	int w_nW = 0;

	char *Llistcolumn[]={"ID", "Name", "Count"};
	int Lwidth[]       ={40, 150, 150};  

	m_GridDev.DeleteAllItems();
	m_GridDev.SetColumnCount(3);
	nRowNum = m_GridDev.InsertRow(_T(""));
	for (i = 0; i < 3; i++) 
	{		
		m_GridDev.SetItemText(nRowNum, i, CA2T(Llistcolumn[i]));
		m_GridDev.SetColumnWidth(i, Lwidth[i]);
	}
	m_GridDev.SetFixedRowCount(1);
	m_GridDev.SetRowHeight(0, 30);
	m_GridDev.SetSingleRowSelection(1);
	m_GridDev.SetHeaderSort(0);
	m_GridDev.SetListMode(1);
	m_GridDev.SetFixedColumnSelection(0);
}

void CDLEquipSetDlg::InsertRow(int a_idx, char *ap_Name, int a_Max, int a_Cnt)
{
	int		w_jj=0;
	CString w_csStr;
	CString w_csValue;

	CString CA2TTemp;

	int nRowNum = m_GridDev.InsertRow(_T(""));

	//ID
	int w_nCol = GRID_IDX_ID;
	w_csStr.Format(_T("%d"), a_idx);
	m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol) | GVIS_READONLY);
	m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

	//Name
	w_nCol = GRID_IDX_NAME;
	CA2TTemp = CA2T(ap_Name);
	w_csStr.Format(_T("%s"), CA2TTemp);
	m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol) | GVIS_READONLY);
	m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);

	//Count
	w_nCol = GRID_IDX_COUNT;

	if(a_Max > 0) {
		w_csStr.Format(_T("%d"), a_Cnt);

		m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol));
		m_GridDev.SetCellType(nRowNum, w_nCol, RUNTIME_CLASS(CGridCellCombo));
		m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);
		CStringArray options;
		for(w_jj=0;w_jj<=a_Max;w_jj++)
		{
			w_csValue.Format(_T("%d"), w_jj);
			options.Add(w_csValue);
		}
		CGridCellCombo *pCell = (CGridCellCombo*) m_GridDev.GetCell(nRowNum, w_nCol);
		pCell->SetOptions(options);
		pCell->SetStyle(CBS_DROPDOWNLIST); //CBS_DROPDOWN, CBS_DROPDOWNLIST, CBS_SIMPLE
	}else{
		w_csStr.Format(_T("%d"), a_Cnt);
		m_GridDev.SetItemState(nRowNum, w_nCol, m_GridDev.GetItemState(nRowNum, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(nRowNum, w_nCol, w_csStr);
	}
}


void CDLEquipSetDlg::DrawEquip()
{
	int		w_kk = 1;
	char *Llistcolumn[]={"DSS","DPG","DFR","DRM","DRE","DMP"};

	LPA_DATA_T *wp_LPAData = GetPAData();
	if(!wp_LPAData){
		return;
	}

#if defined(DEV_5000)
	InsertRow(w_kk++, "DMT", 0, 1);

	InsertRow(w_kk++, "DPA", wp_LPAData->m_DevMax[LPA_DMA_IDX], wp_LPAData->m_DevCnt[LPA_DMA_IDX]);
#else
	InsertRow(w_kk++, "DMT", 0, wp_LPAData->m_DevCnt[LPA_DMA_IDX]);
#endif
	InsertRow(w_kk++, "DSS", wp_LPAData->m_DevMax[LPA_DSS_IDX], wp_LPAData->m_DevCnt[LPA_DSS_IDX]);
	InsertRow(w_kk++, "DPG", wp_LPAData->m_DevMax[LPA_DPG_IDX], wp_LPAData->m_DevCnt[LPA_DPG_IDX]);
	InsertRow(w_kk++, "DFR", wp_LPAData->m_DevMax[LPA_DFR_IDX], wp_LPAData->m_DevCnt[LPA_DFR_IDX]);
	InsertRow(w_kk++, "DRM", wp_LPAData->m_DevMax[LPA_DRM_IDX], wp_LPAData->m_DevCnt[LPA_DRM_IDX]);
	InsertRow(w_kk++, "DRE", wp_LPAData->m_DevMax[LPA_DRE_IDX], wp_LPAData->m_DevCnt[LPA_DRE_IDX]);
	InsertRow(w_kk++, "DMP", wp_LPAData->m_DevMax[LPA_DMP_IDX], wp_LPAData->m_DevCnt[LPA_DMP_IDX]);

	m_GridDev.Invalidate();
}


void CDLEquipSetDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;


//	DL_LOG("OnGridClick:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
}

void CDLEquipSetDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}


void CDLEquipSetDlg::OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	CString strValue=_T("");
	int m_nRow,m_nCol;
	int w_DevCnt=0;
	int	w_ii;
	int	w_jj;
	int	w_mm;
	int w_nn;

	m_nRow = pItem->iRow;
	m_nCol = pItem->iColumn;

//DL_LOG("OnGridEndEdit:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	if (m_nRow != 0) {
		strValue = m_GridDev.GetItemText(pItem->iRow, GRID_IDX_COUNT);
		w_DevCnt = dl_atoi1((CT2A)strValue);

		// 같은수로 바꿀경우 그냥 리턴
#if defined(DEV_5000)
		int	w_CurRow = m_nRow-1;
		if( (w_CurRow == ITEM_DPA_ROW && wp_PAData->m_DevCnt[LPA_DMA_IDX] == w_DevCnt)
			|| (w_CurRow == ITEM_DSS_ROW && wp_PAData->m_DevCnt[LPA_DSS_IDX] == w_DevCnt)
			|| (w_CurRow == ITEM_DPG_ROW && wp_PAData->m_DevCnt[LPA_DPG_IDX] == w_DevCnt)
			|| (w_CurRow == ITEM_DFR_ROW && wp_PAData->m_DevCnt[LPA_DFR_IDX] == w_DevCnt)
			|| (w_CurRow == ITEM_DRM_ROW && wp_PAData->m_DevCnt[LPA_DRM_IDX] == w_DevCnt)
			|| (w_CurRow == ITEM_DRE_ROW && wp_PAData->m_DevCnt[LPA_DRE_IDX] == w_DevCnt)
			|| (w_CurRow == ITEM_DMP_ROW && wp_PAData->m_DevCnt[LPA_DMP_IDX] == w_DevCnt)
			) {

			return;
		}
#else
		if(wp_PAData->m_DevCnt[m_nRow-1] == w_DevCnt) {
			return;
		}
#endif




#if defined(DEV_5000)
		if(m_nRow <= LPA_EQUIP_MAX+1) {
#else
		if(m_nRow <= LPA_EQUIP_MAX) {
#endif

#if defined(DEV_5000)
			if(m_nRow-1 == ITEM_DSS_ROW) {		// DPA

				CString strDpaValue = m_GridDev.GetItemText(ITEM_DPA_ROW+1, GRID_IDX_COUNT);
				int w_DpaDevCnt = dl_atoi1((CT2A)strDpaValue);

				if(w_DpaDevCnt + w_DevCnt > LPA_DSS_MAX) {
					//MessageBox(_T("DPA+DSS수가 최대수(20)을 초과합니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
					strValue.Format(_T("%d"), wp_PAData->m_DevCnt[LPA_DSS_IDX]);
					m_GridDev.SetItemText(m_nRow, GRID_IDX_COUNT, strValue); 

					PostMessage(WM_DPADSS_ERROR);
					return;
				}
				wp_PAData->m_DevCnt[LPA_DSS_IDX] = w_DevCnt;
			} else if(m_nRow-1 == ITEM_DPA_ROW) {
				CString strDssValue = m_GridDev.GetItemText(ITEM_DSS_ROW+1, GRID_IDX_COUNT);
				int w_DssDevCnt = dl_atoi1((CT2A)strDssValue);
				if(w_DssDevCnt + w_DevCnt > LPA_DSS_MAX) {
					//MessageBox(_T("DPA+DSS수가 최대수(20)을 초과합니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);

					strValue.Format(_T("%d"), wp_PAData->m_DevCnt[LPA_DMA_IDX]);
					m_GridDev.SetItemText(m_nRow, GRID_IDX_COUNT, strValue); 

					PostMessage(WM_DPADSS_ERROR);
					return;
				}
				wp_PAData->m_DevCnt[LPA_DMA_IDX] = w_DevCnt;
			} else {
	 			wp_PAData->m_DevCnt[m_nRow-2] = w_DevCnt;
			}
#else
			wp_PAData->m_DevCnt[m_nRow-1] = w_DevCnt;
#endif

#if defined(DEV_5000)
			if(GetDpgWnd() && (m_nRow-1 == ITEM_DPA_ROW)) {

				int w_DssCnt = wp_PAData->m_DevCnt[LPA_DSS_IDX];
				memset(&wp_PAData->m_DmtData[w_DevCnt], 0x00, sizeof(LPA_DEVICE_T) * (LPA_DMA_MAX-w_DevCnt));
				for(w_jj=w_DevCnt+w_DssCnt;w_jj<(LPA_DMA_MAX+LPA_DSS_MAX);w_jj++) {
					for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = 0;
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = 0;
					}
				}

			} else if(GetDpgWnd() && (m_nRow-1 == ITEM_DSS_ROW)) {

				int w_DmaCnt = wp_PAData->m_DevCnt[LPA_DMA_IDX];
				memset(&wp_PAData->m_DssData[w_DevCnt], 0x00, sizeof(LPA_DEVICE_T) * (LPA_DSS_MAX-w_DevCnt));
				for(w_jj=w_DmaCnt+w_DevCnt;w_jj<(LPA_DMA_MAX+LPA_DSS_MAX);w_jj++) {
					for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = 0;
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = 0;
					}
				}
#else
			if(GetDpgWnd() && (m_nRow-1 == LPA_DSS_IDX)) {
				memset(&wp_PAData->m_DssData[w_DevCnt], 0x00, sizeof(LPA_DEVICE_T) * (LPA_DSS_MAX-w_DevCnt));

				for(w_jj=LPA_DMA_MAX+w_DevCnt;w_jj<(LPA_DMA_MAX+LPA_DSS_MAX);w_jj++) {
					for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = 0;
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = 0;
					}
				}
#endif
			} else if(GetDpgWnd() && m_nRow-1 == ITEM_DPG_ROW) {

				memset(&wp_PAData->m_DpgData[w_DevCnt], 0x00, sizeof(LPA_DEVICE_T) * (LPA_DPG_MAX-w_DevCnt));
				for(w_jj=0;w_jj<(LPA_DMA_MAX+LPA_DSS_MAX);w_jj++) {
					for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
						if( wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr > w_DevCnt) {
							wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = 0;
							wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = 0;
						}
					}
				}
				GetDpgWnd()->PostMessage(WM_CHANGEDEQUIP);
			} else if(GetMMacroWnd() && m_nRow-1 == ITEM_DRM_ROW) {
				if(w_DevCnt == 0) {		// DRM이 0인 경우만 삭제
					memset(&wp_PAData->m_MMacroData[w_DevCnt], 0x00, sizeof(LPA_DEVICE_T) * (LPA_MMACRO_MAX-w_DevCnt));
					memset(&wp_PAData->m_MMacroTerm[w_DevCnt], 0x00, sizeof(LPA_MMACRO_TERM_T) * (LPA_MMACRO_MAX-w_DevCnt));

					GetMMacroWnd()->PostMessage(WM_CHANGEDEQUIP);
				}
			} else if(GetDfrWnd() && m_nRow-1 == ITEM_DFR_ROW) {
				memset(&wp_PAData->m_DfrData[w_DevCnt], 0x00, sizeof(LPA_DEVICE_T) * (LPA_DFR_MAX-w_DevCnt));
				memset(&wp_PAData->m_DfrTerm[w_DevCnt], 0x00, sizeof(LPA_DFR_TERM_T) * (LPA_DFR_MAX-w_DevCnt));

				GetDfrWnd()->PostMessage(WM_CHANGEDEQUIP);
			}

			// 
#if defined(DEV_5000)
			if((m_nRow-1 == ITEM_DSS_ROW || m_nRow-1 == ITEM_DPA_ROW || m_nRow-1 == ITEM_DPG_ROW) && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0 ) {	
#else
			if((m_nRow-1 == LPA_DSS_IDX || m_nRow-1 == ITEM_DPG_ROW) && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0 ) {	
#endif
																						// DSS 또는 DPG 변경시 DSS/DPG에 맞게 관련 MACRO값을 재 설정해야 함
																						// 단 DPG가 0일때는 제외
				for(w_jj=0;w_jj<(LPA_DMA_MAX+LPA_DSS_MAX);w_jj++) {
					for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
						if(wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0  && wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput  == 0) {
							for(w_nn=0;w_nn<LPA_PCMACRO_MAX;w_nn++) {
								for(w_mm=0;w_mm<LPA_PCMACRO_ITEM;w_mm++) {
									GetLowPA()->SetBit(wp_PAData->m_PCMacroTerm[w_nn][w_mm].m_SSFlag,w_jj, w_ii, 0);
								}
							}
							for(w_nn=0;w_nn<LPA_TIMERMACRO_MAX;w_nn++) {
								for(w_mm=0;w_mm<LPA_TIMERMACRO_ITEM;w_mm++) {
									GetLowPA()->SetBit(wp_PAData->m_TimerMacroTerm[w_nn][w_mm].m_SSFlag,w_jj, w_ii, 0);
								}
							}
#if defined(DEV_5000)
							for(w_nn=0;w_nn<LPA_EMMACRO_MAX;w_nn++) {
								for(w_mm=0;w_mm<LPA_EMMACRO_ITEM;w_mm++) {
									GetLowPA()->SetBit(wp_PAData->m_EmMacroTerm[w_nn][w_mm].m_SSFlag,w_jj, w_ii, 0);
								}
							}
#endif
							for(w_nn=0;w_nn<LPA_MMACRO_MAX;w_nn++) {
								for(w_mm=0;w_mm<LPA_MMACRO_ITEM;w_mm++) {
									GetLowPA()->SetBit(wp_PAData->m_MMacroTerm[w_nn][w_mm].m_SSFlag,w_jj, w_ii, 0);
								}
							}
							for(w_nn=0;w_nn<LPA_DFR_MAX;w_nn++) {
								for(w_mm=0;w_mm<LPA_DFR_ITEM;w_mm++) {
									GetLowPA()->SetBit(wp_PAData->m_DfrTerm[w_nn][w_mm].m_SSFlag,w_jj, w_ii, 0);
								}
							}
						}
					}
				}

				GetPCMacroWnd()->PostMessage(WM_CHANGEDEQUIP);
				GetTimerMacroWnd()->PostMessage(WM_CHANGEDEQUIP);
				GetMMacroWnd()->PostMessage(WM_CHANGEDEQUIP);
				GetDfrWnd()->PostMessage(WM_CHANGEDEQUIP);
			}


			GetLowPA()->SetModified(1);
			if(GetTermWnd()) {
				GetTermWnd()->Invalidate();
			}
		}
	}

}

// GVN_SELCHANGED
void CDLEquipSetDlg::OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	//DL_LOG("OnGridEndSelChange:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
}

LRESULT CDLEquipSetDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch(message)
	{
		case WM_SELPAGE:
#if defined(DEV_500)
			RedrawGate();
#endif
			break;
		case WM_DPADSS_ERROR:
			MessageBox(_T("DPA+DSS수가 최대수(20)을 초과합니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
			break;
		case WM_INVALIDATE:
			InitGrid();
			DrawEquip();
#if defined(DEV_500)
			RedrawGate();
#endif
			break;
	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}


#if defined(DEV_5000)

void CDLEquipSetDlg::RedrawGate()
{
	int		w_X, w_Y, w_W, w_H;

	CRect	w_ButtonRc;
	CRect	w_Rect;
	GetClientRect(&w_Rect);

	w_X = w_Rect.Width()/2 - 340/2;
	w_Y = 310;
	w_W = 340;
	w_H = (GATE_LINE_YGAP*(GATE_LINE_NUM-1))+1;

	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);


	if(!::IsWindow(m_GateStatic.GetSafeHwnd())) {
		m_GateStatic.Create(_T("Gate"), WS_CHILD | WS_VISIBLE, w_ButtonRc, this);
	} else {
		m_GateStatic.MoveWindow(&w_ButtonRc);
	}
	m_GateStatic.Invalidate();
}
#endif


void CDLEquipSetDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;
	CRect w_Rect;
	GetClientRect(&w_Rect);

	if(m_GridDev.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 10;
		w_W = 350;
	#if defined(DEV_5000)
		w_H = 220;
	#else
		w_H = 190;
	#endif

		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_GridDev.MoveWindow(&w_ButtonRc);
	}

	if(m_StcMsg.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = w_H+20;
		w_W = 350;
		w_H = 40;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_StcMsg.MoveWindow(&w_ButtonRc);
	}
#if defined(DEV_5000)
	RedrawGate();
#endif

	Invalidate();
}
