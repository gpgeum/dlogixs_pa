#pragma once
#include "afxwin.h"

#include "GridCtrl_src/GridCtrl.h"
#include "NewCellTypes/GridURLCell.h"
#include "NewCellTypes/GridCellCombo.h"
#include "NewCellTypes/GridCellCheck.h"
#include "NewCellTypes/GridCellNumeric.h"
#include "NewCellTypes/GridCellDateTime.h"

#define	GRID_IDX_ID				0
#define	GRID_IDX_NAME			1
#define	GRID_IDX_COUNT			2

// CDLEquipSetDlg 대화 상자입니다.

class CDLEquipSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLEquipSetDlg)

public:
	CDLEquipSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLEquipSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLEQUIPSET_DIALOG };

public:
	void			initScreen();
	void			InitGrid();

	void			DrawEquip();
	void			InsertRow(int a_idx, char *ap_Name, int a_Max, int a_Cnt);

#if defined(DEV_5000)
	void			RedrawGate();
#endif

public:
	CGridCtrl		m_GridDev;
#if defined(DEV_5000)
	CGateStatic		m_GateStatic;
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	CColorStatic m_StcMsg;
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
