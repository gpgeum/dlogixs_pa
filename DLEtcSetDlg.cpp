// DLEtcSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLEtcSetDlg.h"

#include "DLLPADevice.h"

// CDLEtcSetDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLEtcSetDlg, CDialog)

CDLEtcSetDlg::CDLEtcSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLEtcSetDlg::IDD, pParent)
	, m_nPort(0)
{

}

CDLEtcSetDlg::~CDLEtcSetDlg()
{
}

void CDLEtcSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDV_MinMaxInt(pDX, m_nPort, 1, 255);
	DDX_Control(pDX, IDC_STC_MSG, m_StcMsg1);
	DDX_Control(pDX, IDC_STC_MSG2, m_StcMsg2);
	DDX_Control(pDX, IDC_BTN_CLEAR, m_btnInitDevice);
	DDX_Control(pDX, IDC_BTN_UPGRADE, m_btnUpgrade);
}


BEGIN_MESSAGE_MAP(CDLEtcSetDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_TO_DEVICE, &CDLEtcSetDlg::OnBnClickedBtnToDevice)
	ON_BN_CLICKED(IDC_BTN_FROM_DEVICE, &CDLEtcSetDlg::OnBnClickedBtnFromDevice)
	ON_BN_CLICKED(IDC_BTN_TO_FILE, &CDLEtcSetDlg::OnBnClickedBtnToFile)
	ON_BN_CLICKED(IDC_BTN_FROM_FILE, &CDLEtcSetDlg::OnBnClickedBtnFromFile)
	ON_BN_CLICKED(IDC_BTN_CONNECT, &CDLEtcSetDlg::OnBnClickedBtnConnect)
	ON_BN_CLICKED(IDC_BTN_CLEAR, &CDLEtcSetDlg::OnBnClickedBtnClear)
	ON_BN_CLICKED(IDC_BTN_DISCONNECT, &CDLEtcSetDlg::OnBnClickedBtnDisconnect)
	ON_BN_CLICKED(IDC_BTN_CALSIZE, &CDLEtcSetDlg::OnBnClickedBtnCalsize)
	ON_MESSAGE(WM_LOCALFILE_SAVE, &CDLEtcSetDlg::OnNotifyToFile)
	ON_MESSAGE(WM_LOCALFILE_CHECK, &CDLEtcSetDlg::OnNotifyToFileCheck)
	
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BTN_UPGRADE, &CDLEtcSetDlg::OnBnClickedBtnUpgrade)
END_MESSAGE_MAP()


// CDLEtcSetDlg 메시지 처리기입니다.

BOOL CDLEtcSetDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CDLEtcSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	GetLowPA()->m_SerialPort = g_LowPADevPort;
	m_nPort = g_LowPADevPort;
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDLEtcSetDlg::OnBnClickedBtnToDevice()
{
	char	w_FileName[MAX_PATH];
	char	w_FilePath[MAX_PATH];
	char	w_GUID[MAX_PATH];
//	char	w_Value[MAX_PATH];
	CString szValue=_T("");

	memset(&w_FileName,0x0,sizeof(w_FileName));
	memset(&w_FilePath,0x0,sizeof(w_FilePath));
	memset(&w_GUID,0x0,sizeof(w_GUID));

	if(!GetLowPA()->m_PAData.m_DevCode)
	{
		GetLowPA()->m_PAData.m_DevCode = 0;
	}

	if(!GetLowPA()->IsSerialOpened()) {
		MessageBox(_T("COM 포트가 정상적으로 OPEN되지 않았습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	if(!GetLowPA()->GetDeviceStatus()) {
		MessageBox(_T("DEVICE와 연결이 되지 않았습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	if(MessageBox(_T("DEVICE에 현 설정정보를 보내시겠습니까?"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
		return;
	}

	//설정파일 저장시 파일명 인덱스 증가
	//sprintf(w_GUID,"%08x",GetLowPA()->m_PAData.m_DevCode);
	//GetLowPA()->GetSearchConfigureNewFileName(w_FilePath,w_FileName,w_GUID);
	//szValue.Format(_T("%s"),CA2T(w_GUID));
	/*
	dl_strcpy(w_Value,CT2A(szValue.Mid(0,2)));
	GetLowPA()->m_PAData.m_GUID[0] = dl_stoi(w_Value);
	dl_strcpy(w_Value,CT2A(szValue.Mid(2,2)));
	GetLowPA()->m_PAData.m_GUID[1] = dl_stoi(w_Value);
	dl_strcpy(w_Value,CT2A(szValue.Mid(4,2)));
	GetLowPA()->m_PAData.m_GUID[2] = dl_stoi(w_Value);
	dl_strcpy(w_Value,CT2A(szValue.Mid(6,2)));
	GetLowPA()->m_PAData.m_GUID[3] = dl_stoi(w_Value);
	*/
	//DL_LOG("=>SAVE DEV GUID[%08x]",GetLowPA()->m_PAData.m_DevCode);

	int w_Len1 = GetLowPA()->MakeRom1Data(&GetLowPA()->m_PAData, GetLowPA()->m_Rom1);
	int w_Len2 = GetLowPA()->MakeRom2Data(&GetLowPA()->m_PAData, GetLowPA()->m_Rom2);


	if(w_Len1 > LPA_ROM_SIZE || w_Len1 > LPA_ROM_SIZE) {
		CString	w_Str;
		w_Str.Format(_T("생성된 데이타 사용량 SIZE가 너무 큽니다.(ROM1:(%d) BYTE ROM2:(%d) BYTE"), GetLowPA()->m_Rom1Len, GetLowPA()->m_Rom2Len);

		MessageBox(w_Str, _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	GetLowPA()->m_Rom1Len = (WORD)w_Len1;
	GetLowPA()->m_Rom2Len = (WORD)w_Len2;


#if defined(DEV_SIMUL_TEST)
//	GetLowPA()->m_Rom1Len = 4096;
//	GetLowPA()->m_Rom2Len = 4096;
#endif

	GetLowPA()->m_Rom1Page = (GetLowPA()->m_Rom1Len/LPA_PACKET_LEN) + ((GetLowPA()->m_Rom1Len%LPA_PACKET_LEN) ? 1 : 0);
	GetLowPA()->m_Rom2Page = (GetLowPA()->m_Rom2Len/LPA_PACKET_LEN) + ((GetLowPA()->m_Rom2Len%LPA_PACKET_LEN) ? 1 : 0);

//	DL_LOG("GetLowPA()->m_Rom1Len : %d", GetLowPA()->m_Rom1Len);
//	DL_LOG("GetLowPA()->m_Rom2Len : %d", GetLowPA()->m_Rom2Len);
//	DL_LOG("GetLowPA()->m_Rom1Page : %d", GetLowPA()->m_Rom1Page);
//	DL_LOG("GetLowPA()->m_Rom2Page : %d", GetLowPA()->m_Rom2Page);

	GetLowPA()->m_SendPage = 0;
	GetLowPA()->m_DevStat = DEV_STAT_SEND;
	if(GetLowPA()->GetMainHwnd()) {
		::PostMessage(GetLowPA()->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_START, GetLowPA()->m_Rom1Page+GetLowPA()->m_Rom2Page);
	}
}



void CDLEtcSetDlg::OnBnClickedBtnFromDevice()
{

	if(!GetLowPA()->IsSerialOpened()) {
		MessageBox(_T("COM 포트가 정상적으로 OPEN되지 않았습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	if(!GetLowPA()->GetDeviceStatus()) {
		MessageBox(_T("DEVICE와 연결이 되지 않았습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}

	if(MessageBox(_T("DEVICE로부터 설정정보를 읽겠습니까?"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
		return;
	}


#if defined(DEV_SIMUL_TEST)
	if(GetLowPA()->mp_Device) {
		GetLowPA()->mp_Device->m_Rom1Len = GetLowPA()->MakeRom1Data(&GetLowPA()->m_PAData, GetLowPA()->mp_Device->m_Rom1);
		GetLowPA()->mp_Device->m_Rom2Len = GetLowPA()->MakeRom2Data(&GetLowPA()->m_PAData, GetLowPA()->mp_Device->m_Rom2);

//	DL_LOG("GetLowPA()->mp_Device->m_Rom1Len : %d", GetLowPA()->mp_Device->m_Rom1Len);
//	DL_LOG("GetLowPA()->mp_Device->m_Rom2Len : %d", GetLowPA()->mp_Device->m_Rom2Len);

		GetLowPA()->mp_Device->m_Rom1Page = (GetLowPA()->mp_Device->m_Rom1Len/LPA_PACKET_LEN) + ((GetLowPA()->mp_Device->m_Rom1Len%LPA_PACKET_LEN) ? 1 : 0);
		GetLowPA()->mp_Device->m_Rom2Page = (GetLowPA()->mp_Device->m_Rom2Len/LPA_PACKET_LEN) + ((GetLowPA()->mp_Device->m_Rom2Len%LPA_PACKET_LEN) ? 1 : 0);

//	DL_LOG("GetLowPA()->mp_Device->m_Rom1Page : %d", GetLowPA()->mp_Device->m_Rom1Page);
//	DL_LOG("GetLowPA()->mp_Device->m_Rom2Page : %d", GetLowPA()->mp_Device->m_Rom2Page);

#if 1 // TEST
		memset(&GetLowPA()->m_PAData, 0x00, sizeof(LPA_DATA_T));
		int w_Rtn = GetLowPA()->ParseRomData(&GetLowPA()->m_PAData, GetLowPA()->mp_Device->m_Rom1, 
				GetLowPA()->mp_Device->m_Rom1Len, GetLowPA()->mp_Device->m_Rom2, GetLowPA()->mp_Device->m_Rom2Len);
#endif
	}
#endif

#if !defined(DEV_SIMUL_TEST)
	GetLowPA()->m_RecvPage = 0;
	GetLowPA()->m_DevStat = DEV_STAT_RECV;
	if(GetLowPA()->GetMainHwnd()) {
		::PostMessage(GetLowPA()->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_START, LPA_PACKET_NUM);
	}
#endif
}

void CDLEtcSetDlg::OnBnClickedBtnToFile()
{
#if 1
	TCHAR	szFilter[] = _T("conf File(*.conf)|*.conf All Files (*.*)|*.*||");
	char	szPath[1024];

	char	w_FileName[MAX_PATH];
	char	w_FilePath[MAX_PATH];
	char	w_GUID[MAX_PATH];

	CString w_FindName=_T("");
	CString w_Display=_T("");

	CString CA2TTemp;

	memset(&szPath,0x0,sizeof(szPath));
	memset(&w_FileName,0x0,sizeof(w_FileName));
	memset(&w_FilePath,0x0,sizeof(w_FilePath));
	memset(&w_GUID,0x0,sizeof(w_GUID));

	//설정파일 저장시 파일명 인덱스 증가
	sprintf(w_GUID,"%08x",GetLowPA()->m_PAData.m_DevCode);
	GetLowPA()->GetSearchConfigureNewFileName(w_FilePath,w_FileName,w_GUID);

	sprintf(szPath,"%s\\SETUPFILES\\config.conf",w_FilePath);
	CA2TTemp = CA2T(szPath);
	w_FindName.Format(_T("%s"), CA2TTemp);

	w_Display.Format(_T("저장될 설정 파일명 : [config.conf]\r\n저장하시겠습니까?"));
	if(MessageBox(w_Display, _T("설정저장확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) 
	{
		return;
	}

	if(GetLowPA()->Save(szPath,NULL) < 0) 
	{
		MessageBox(_T("해당 파일을 저장할 수 없습니다. "), _T("처리오류"), MB_OK|MB_ICONWARNING);
		return;
	}
#else
	GetLowPA()->Save("config.conf");
#endif

}

void CDLEtcSetDlg::OnBnClickedBtnFromFile()
{
	LPA_DATA_T *wp_PAData;

	CString path = _T("");
	CString szFilePath = _T("");

	TCHAR	pszPathName[_MAX_PATH];
	int		w_Rtn=0;

#if 1
	TCHAR szFilter[] = _T("conf File (*.conf)|*.conf|");
	CFileDialog	openDlg(TRUE, _T("config"), _T(""), OFN_HIDEREADONLY, szFilter, this);

	::GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH); 
	PathRemoveFileSpec(pszPathName);
	szFilePath.Format(_T("%s"),pszPathName);
	path.Format(_T("%s\\SETUPFILES"),szFilePath);

	openDlg.GetOFN().lpstrInitialDir = (LPCTSTR)path;

	if(openDlg.DoModal() != IDOK) {
		return;
	}

	wp_PAData = new LPA_DATA_T;

	GetLowPA()->InitLPAData(wp_PAData);
	if((w_Rtn = GetLowPA()->Load((CT2A)openDlg.GetPathName(), wp_PAData)) < 0) {

		if(w_Rtn == -2) {
#if defined(DEV_5000)	
			MessageBox(_T("해당 파일을 읽을 수 없습니다.\r\n(전관(1000) 데이타입니다.)"), _T("처리오류"), MB_OK|MB_ICONWARNING);
#else
			MessageBox(_T("해당 파일을 읽을 수 없습니다.\r\n(전관(5000) 데이타입니다.)"), _T("처리오류"), MB_OK|MB_ICONWARNING);
#endif
		}// else {
		//	MessageBox(_T("해당 파일을 읽을 수 없습니다."), _T("처리오류"), MB_OK|MB_ICONWARNING);
		//}
		delete wp_PAData;
		return;
	}
#else
	GetLowPA()->Load("test.conf", wp_PAData);
#endif
	memcpy(GetPAData(), wp_PAData, sizeof(LPA_DATA_T));
	GetLowPA()->Invalidate();

	delete wp_PAData;
}

void CDLEtcSetDlg::DrawStatus()
{
	CString	w_Str;

	if(GetLowPA()->IsSerialOpened()) {
		w_Str = _T("COM 포트상태 : Opened");
		m_StcMsg1.SetBkColor(COLOR_WHITE);
		m_StcMsg1.SetTextColor(COLOR_BLUE);
		m_StcMsg1.SetWindowText(w_Str);
	} else {
		w_Str = _T("COM 포트상태 : Closed");
		m_StcMsg1.SetBkColor(COLOR_WHITE);
		m_StcMsg1.SetTextColor(COLOR_RED);
		m_StcMsg1.SetWindowText(w_Str);
	}
	if(GetLowPA()->GetDeviceStatus()) {
		w_Str = _T("DEVICE 상태 : Connected");
		m_StcMsg2.SetBkColor(COLOR_WHITE);
		m_StcMsg2.SetTextColor(COLOR_BLUE);
		m_StcMsg2.SetWindowText(w_Str);
	} else {
		w_Str = _T("DEVICE 상태 : Disconnected");
		m_StcMsg2.SetBkColor(COLOR_WHITE);
		m_StcMsg2.SetTextColor(COLOR_RED);
		m_StcMsg2.SetWindowText(w_Str);
	}
	UpdateData(FALSE);
}


void CDLEtcSetDlg::OnBnClickedBtnConnect()
{

	if(MessageBox(_T("지정포트로 연결하시겠습니까??"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
		return;
	}
		
	UpdateData(TRUE);
	GetLowPA()->StopSerial();
	GetLowPA()->StartSerial(m_nPort, DEF_SERIAL_BAUD);

	DrawStatus();
}

LRESULT CDLEtcSetDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_CHANGEDSTATUS:
			DrawStatus();
			break;

	}

	return CDialog::DefWindowProc(message, wParam, lParam);
}

void CDLEtcSetDlg::OnBnClickedBtnClear()
{
	if(MessageBox(_T("설정한 모든 데이타가 삭제됩니다. 계속하시겠습니까??"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
		return;
	}
	GetLowPA()->Init();
	GetLowPA()->Invalidate();
}

void CDLEtcSetDlg::OnBnClickedBtnUpgrade()
{
	m_UpdateDlg.DoModal();
}

void CDLEtcSetDlg::OnBnClickedBtnDisconnect()
{
	if(!GetLowPA()->IsSerialOpened()) {
		MessageBox(_T("DEVICE와 연결이 되지 않았습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	if(MessageBox(_T("연결을 종료하시겠습니까??"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
		return;
	}	
	GetLowPA()->StopSerial();
	DrawStatus();
}


void CDLEtcSetDlg::OnBnClickedBtnCalsize()
{
	int w_Len1 = GetLowPA()->MakeRom1Data(&GetLowPA()->m_PAData, GetLowPA()->m_Rom1);
	int w_Len2 = GetLowPA()->MakeRom2Data(&GetLowPA()->m_PAData, GetLowPA()->m_Rom2);

	CString	w_Str;
	w_Str.Format(_T("(사용량 계산 결과)\r\n\r\n사용량1: %d BYTE\r\n사용량2: %d BYTE"), w_Len1, w_Len2);

	MessageBox(w_Str, _T("실행결과"), MB_OK | MB_ICONINFORMATION);
}

//장비로 설정파일 저장후, 로컬 디스크에 파일저장관련 통지 메시지 처리함수.
LRESULT CDLEtcSetDlg::OnNotifyToFile(WPARAM wParam, LPARAM lParam)
{
	OnBnClickedBtnToFile();
	return(0);
}

// 장비로부터 설정내용을 읽은후, 로컬 디스크에 저장되어 있는 설정파일과 자료구조가 일치하는지 여부를 판별하여
// 장비 / 지역정보 별칭 및 매크로 별칭을 표시할 수 있도록 구조체 재설정 처리 함수

LRESULT CDLEtcSetDlg::OnNotifyToFileCheck(WPARAM wParam, LPARAM lParam)
{
	int			w_ii=0;
	int			w_jj=0;
	int			w_Check=0;
	LPA_DATA_T *wp_LocalPAData;

	CString path = _T("");
	CString szFilePath = _T("");

	TCHAR	pszPathName[_MAX_PATH];

	::GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH); 
	PathRemoveFileSpec(pszPathName);
	szFilePath.Format(_T("%s"),pszPathName);
	path.Format(_T("%s\\SETUPFILES\\config.conf"),szFilePath);

	wp_LocalPAData = new LPA_DATA_T;
	GetLowPA()->InitLPAData(wp_LocalPAData);
	if(GetLowPA()->Load((CT2A)path, wp_LocalPAData) < 0) 
	{
		MessageBox(_T("저장되어 있는 설정파일을 읽을 수 없습니다. "), _T("처리오류"), MB_OK|MB_ICONWARNING);
		delete wp_LocalPAData;
		return (0);
	}

	//자료구조 체크(장치코드/장비명/지역정보/매크로 이름을 제외한 모든 항목 검사)
	if ( memcmp(&wp_LocalPAData->m_DevCnt,&GetLowPA()->m_PAData.m_DevCnt,sizeof(wp_LocalPAData->m_DevCnt)) != 0)
	{
		w_Check = TRUE;
	}
	if ( memcmp(&wp_LocalPAData->m_DevMax,&GetLowPA()->m_PAData.m_DevMax,sizeof(wp_LocalPAData->m_DevMax)) != 0)
	{
		w_Check = TRUE;
	}

	//지역정보
	//DMT
	for(w_ii=0;w_ii<LPA_DMA_MAX;w_ii++)
	{
		for(w_jj=0;w_jj<wp_LocalPAData->m_DmtData[w_ii].m_Cnt;w_jj++)
		{
			if ( memcmp(&wp_LocalPAData->m_DmtData[w_ii].m_Item[w_jj].m_Pos,&GetLowPA()->m_PAData.m_DmtData[w_ii].m_Item[w_jj].m_Pos,sizeof(wp_LocalPAData->m_DmtData[w_ii].m_Item[w_jj].m_Pos)) != 0)
			{
				w_Check = TRUE;
			}
		}
	}
	//DSS
	for(w_ii=0;w_ii<LPA_DSS_MAX;w_ii++)
	{
		for(w_jj=0;w_jj<wp_LocalPAData->m_DssData[w_ii].m_Cnt;w_jj++)
		{
			if ( memcmp(&wp_LocalPAData->m_DssData[w_ii].m_Item[w_jj].m_Pos,&GetLowPA()->m_PAData.m_DssData[w_ii].m_Item[w_jj].m_Pos,sizeof(wp_LocalPAData->m_DssData[w_ii].m_Item[w_jj].m_Pos)) != 0)
			{
				w_Check = TRUE;
			}
		}
	}

//	LPA_DEVICE_T	m_DfrData[LPA_DFR_MAX];

	//DPG
	for(w_ii=0;w_ii<LPA_DPG_MAX;w_ii++)
	{
		for(w_jj=0;w_jj<wp_LocalPAData->m_DpgData[w_ii].m_Cnt;w_jj++)
		{
			if ( memcmp(&wp_LocalPAData->m_DpgData[w_ii].m_Item[w_jj].m_Pos,&GetLowPA()->m_PAData.m_DpgData[w_ii].m_Item[w_jj].m_Pos,sizeof(wp_LocalPAData->m_DpgData[w_ii].m_Item[w_jj].m_Pos)) != 0)
			{
				w_Check = TRUE;
			}
		}
	}
	//DFR
	for(w_ii=0;w_ii<LPA_DFR_MAX;w_ii++)
	{
		for(w_jj=0;w_jj<wp_LocalPAData->m_DfrData[w_ii].m_Cnt;w_jj++)
		{
			if ( memcmp(&wp_LocalPAData->m_DfrData[w_ii].m_Item[w_jj].m_Pos,&GetLowPA()->m_PAData.m_DfrData[w_ii].m_Item[w_jj].m_Pos,sizeof(wp_LocalPAData->m_DfrData[w_ii].m_Item[w_jj].m_Pos)) != 0)
			{
				w_Check = TRUE;
			}
		}
	}

	//메인 매크로
	if(wp_LocalPAData->m_MMacroCnt != GetLowPA()->m_PAData.m_MMacroCnt)
	{
		w_Check = TRUE;
	}
	if(wp_LocalPAData->m_MMacroMax != GetLowPA()->m_PAData.m_MMacroMax)
	{
		w_Check = TRUE;
	}
	if ( memcmp(&wp_LocalPAData->m_MMacroData,GetLowPA()->m_PAData.m_MMacroData,sizeof(wp_LocalPAData->m_MMacroData)) != 0)
	{
		w_Check = TRUE;
	}

	//PC 매크로
	if(wp_LocalPAData->m_PCMacroCnt != GetLowPA()->m_PAData.m_PCMacroCnt)
	{
		w_Check = TRUE;
	}
	if(wp_LocalPAData->m_PCMacroMax != GetLowPA()->m_PAData.m_PCMacroMax)
	{
		w_Check = TRUE;
	}
	for(w_ii=0;w_ii<LPA_PCMACRO_MAX;w_ii++)
	{
		for(w_jj=0;w_jj<wp_LocalPAData->m_PCMacroData[w_ii].m_Cnt;w_jj++)
		{
			if ( memcmp(&wp_LocalPAData->m_PCMacroData[w_ii].m_Item[w_jj].m_Pos,&GetLowPA()->m_PAData.m_PCMacroData[w_ii].m_Item[w_jj].m_Pos,sizeof(wp_LocalPAData->m_PCMacroData[w_ii].m_Item[w_jj].m_Pos)) != 0)
			{
				w_Check = TRUE;
			}
		}
	}

	if( memcmp(&wp_LocalPAData->m_TermDpg,&GetLowPA()->m_PAData.m_TermDpg,sizeof(wp_LocalPAData->m_TermDpg)) != 0)
	{
		w_Check = TRUE;
	}
	if( memcmp(&wp_LocalPAData->m_DfrTerm,&GetLowPA()->m_PAData.m_DfrTerm,sizeof(wp_LocalPAData->m_DfrTerm)) != 0)
	{
		w_Check = TRUE;
	}

	if( memcmp(&wp_LocalPAData->m_PCMacroTerm,&GetLowPA()->m_PAData.m_PCMacroTerm,sizeof(wp_LocalPAData->m_PCMacroTerm)) != 0)
	{
		w_Check = TRUE;
	}
	if( memcmp(&wp_LocalPAData->m_MMacroTerm,&GetLowPA()->m_PAData.m_MMacroTerm,sizeof(wp_LocalPAData->m_MMacroTerm)) != 0)
	{
		w_Check = TRUE;
	}

	// 타이머 매크로 비교
	if(wp_LocalPAData->m_TimerMacroCnt != GetLowPA()->m_PAData.m_TimerMacroCnt)
	{
		w_Check = TRUE;
	}
	if(wp_LocalPAData->m_TimerMacroMax != GetLowPA()->m_PAData.m_TimerMacroMax)
	{
		w_Check = TRUE;
	}
	if( memcmp(&wp_LocalPAData->m_TimerMacroData,&GetLowPA()->m_PAData.m_TimerMacroData,sizeof(wp_LocalPAData->m_TimerMacroData)) != 0)
	{
		w_Check = TRUE;
	}
	if( memcmp(&wp_LocalPAData->m_TimerMacroTerm,&GetLowPA()->m_PAData.m_TimerMacroTerm,sizeof(wp_LocalPAData->m_TimerMacroTerm)) != 0)
	{
		w_Check = TRUE;
	}

	if(!w_Check)
	{
		memcpy(&GetLowPA()->m_PAData, wp_LocalPAData, sizeof(LPA_DATA_T));
		GetLowPA()->Invalidate();
	}
	delete wp_LocalPAData;

	return(0);
}
void CDLEtcSetDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;
	CRect w_Rect;
	GetClientRect(&w_Rect);


	if(m_btnInitDevice.GetSafeHwnd())
	{	
		w_X = w_Rect.Width()/2 - 170;
		w_Y = 10;
		w_W = 150;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_btnInitDevice.MoveWindow(&w_ButtonRc);
		m_btnInitDevice.Invalidate();
	}

	if(m_btnUpgrade.GetSafeHwnd())
	{	
		w_X = w_Rect.Width()/2 + 30;
		w_Y = 10;
		w_W = 150;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_btnUpgrade.MoveWindow(&w_ButtonRc);
		m_btnUpgrade.Invalidate();
	}

	if(GetDlgItem(IDC_STATIC)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 40;
		w_W = 60;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_STATIC)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_STATIC)->Invalidate();
	}
	if(GetDlgItem(IDC_EDIT_PORT)->GetSafeHwnd())
	{
		w_X = w_X+65;
		w_Y = 40;
		w_W = 110;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_EDIT_PORT)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_EDIT_PORT)->Invalidate();
	}
	if(GetDlgItem(IDC_BTN_CONNECT)->GetSafeHwnd())
	{
		w_X = w_X+115;
		w_Y = 40;
		w_W = 80;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_BTN_CONNECT)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_BTN_CONNECT)->Invalidate();
	}
	if(GetDlgItem(IDC_BTN_DISCONNECT)->GetSafeHwnd())
	{
		w_X =w_X+85;
		w_Y = 40;
		w_W = 80;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_BTN_DISCONNECT)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_BTN_DISCONNECT)->Invalidate();
	}

	if(GetDlgItem(IDC_STC_MSG)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 80;
		w_W = 350;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_STC_MSG)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_STC_MSG)->Invalidate();
	}
	if(GetDlgItem(IDC_STC_MSG)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 115;
		w_W = 350;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_STC_MSG2)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_STC_MSG2)->Invalidate();
	}

	if(GetDlgItem(IDC_BTN_TO_DEVICE)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 150;
		w_W = 150;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_BTN_TO_DEVICE)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_BTN_TO_DEVICE)->Invalidate();
	}
	if(GetDlgItem(IDC_BTN_TO_FILE)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 185;
		w_W = 150;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_BTN_TO_FILE)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_BTN_TO_FILE)->Invalidate();
	}
	if(GetDlgItem(IDC_BTN_FROM_DEVICE)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 + 25 + 5;
		w_Y = 150;
		w_W = 150;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_BTN_FROM_DEVICE)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_BTN_FROM_DEVICE)->Invalidate();
	}
	if(GetDlgItem(IDC_BTN_FROM_FILE)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 + 25 + 5;
		w_Y = 185;
		w_W = 150;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_BTN_FROM_FILE)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_BTN_FROM_FILE)->Invalidate();
	}

	if(GetDlgItem(IDC_BTN_CALSIZE)->GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 220;
		w_W = 150;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		GetDlgItem(IDC_BTN_CALSIZE)->MoveWindow(&w_ButtonRc);
		GetDlgItem(IDC_BTN_CALSIZE)->Invalidate();
	}
}
