#pragma once
#include "afxwin.h"
#include "DLUpdateDlg.h"

// CDLEtcSetDlg 대화 상자입니다.

class CDLEtcSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLEtcSetDlg)

public:
	CDLEtcSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLEtcSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLETCSET_DIALOG };

	void	DrawStatus();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

public:
	CDLUpdateDlg		m_UpdateDlg;					// 장치업그레이드 팝업

	CButton				m_btnInitDevice;
	CButton				m_btnUpgrade;

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnToDevice();
	afx_msg void OnBnClickedBtnFromDevice();
	afx_msg void OnBnClickedBtnToFile();
	afx_msg void OnBnClickedBtnFromFile();
	int m_nPort;
	afx_msg void OnBnClickedBtnConnect();
	CColorStatic m_StcMsg1;
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	CColorStatic m_StcMsg2;
	afx_msg void OnBnClickedBtnClear();
	afx_msg void OnBnClickedBtnDisconnect();
	afx_msg void OnBnClickedBtnCalsize();
	afx_msg LRESULT OnNotifyToFile(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNotifyToFileCheck(WPARAM wParam, LPARAM lParam);
	
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedBtnUpgrade();
	
};
