#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLLowPAData.h"
#include "DLOs.h"
#include "DLLPADevice.h"
#include "DLUpdateDevice.h"
#include "DLMixerDevice.h"


CDLLowPA g_LowPA;

CDLLowPA *GetLowPA()
{
	return(&g_LowPA);
}


LPA_DATA_T *GetPAData()
{
	return(&GetLowPA()->m_PAData);
}



CDLLowPASetupDlg *GetMainWnd()
{
	return(GetLowPA()->GetMainWnd());
}

CNodeListView	 *GetTermWnd()
{
	return(GetLowPA()->GetTermWnd());
}

CDLEquipSetDlg	*GetEquipWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_EqiipDlg);
	}
	return NULL;
}

CDLDpgSetDlg	*GetDpgWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_DpgDlg);
	}
	return NULL;
}


CDLPCMacroSetDlg	*GetPCMacroWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_PCMacroDlg);
	}
	return NULL;
}

#if defined(DEV_5000)

CDLDMTMacroSetDlg	*GetDMTMacroWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_DMTMacroDlg);
	}
	return NULL;
}
#endif

CDLTimerMacroSetDlg	*GetTimerMacroWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_TimerMacroDlg);
	}
	return NULL;
}

CDLDfrSetDlg	*GetDfrWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_DfrDlg);
	}
	return NULL;
}

CDLMMacroSetDlg	*GetMMacroWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_MMacroDlg);
	}
	return NULL;
}

CDLDLCSetDlg	*GetDLCWnd()
{
	if (GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_DLCDlg);
	}
	return NULL;
}

CDLEtcSetDlg	*GetEtcWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_EtcDlg);
	}
	return NULL;
}

CDLUpdateDlg	*GetUpdateWnd()
{
	if(GetLowPA()->GetMainWnd()) {
		return(&GetLowPA()->GetMainWnd()->m_EtcDlg.m_UpdateDlg);
	}
	return NULL;
}

CDLLowPA::CDLLowPA()
{
#if defined(_DEBUG)
#if defined(LOG_TO_CONSOLE)
	DL_LOG_TO(1);
#else
	DL_LOG_TO(2);
#endif
#else
	DL_LOG_TO(1);
#endif
	mp_TermWnd = NULL;
	mp_MainWnd = NULL;
	m_MainHwnd = NULL;

	m_Modified = 0;

	m_SelPage = 0;
//	m_SelAddr = 1;		// 선택 Devide 주소
//	m_SelItem = 1;		// 선택 Device Item(Output)

	
	m_SerialPort = 1;

	mp_Device = NULL;				//DMT 시리얼 장치 클래스
	mp_UpdateDevice = NULL;			//DMT 펌웨어 시리얼 장치 클래스

	m_DevStat = DEV_STAT_NORMAL;
	m_UpdateDevStat = DEV_STAT_NORMAL;
	m_UpdateFlag = DEV_UPDATE_NORMAL;

	m_SendPage = 0;
	m_RecvPage = 0;

	m_Rom1Len = 0;
	m_Rom2Len = 0;

	m_Rom1Page = 0;
	m_Rom2Page = 0;

	//장치업데이트
	m_RomUpdateLen = 0;
	m_RomUpdatePage = 0;

#if defined(DEV_5000)
	m_TimerMacroMode = 0;
#endif

	Init();
}

CDLLowPA::~CDLLowPA()
{

}


void CDLLowPA::Lock()
{
	m_mutex.Lock();
}

void CDLLowPA::UnLock()
{
	m_mutex.UnLock();
}


void CDLLowPA::Init()
{
	memset(m_Rom1, 0x00, LPA_ROM_MAX);
	memset(m_Rom2, 0x00, LPA_ROM_MAX);
	memset(m_RomUpdate, 0x00, LPA_UPDATE_ROM_MAX);

	memset(m_RecvRom1, 0x00, LPA_ROM_MAX);
	memset(m_RecvRom2, 0x00, LPA_ROM_MAX);
	memset(m_RecvRomUpdate, 0x00, LPA_UPDATE_ROM_MAX);

	//펌웨어 업그레이드 장치정보 구조체
	memset(m_EquipInfo, 0x00, sizeof(m_EquipInfo));

	InitLPAData(&m_PAData);
}

void CDLLowPA::InitLPAData(LPA_DATA_T *ap_PAData)
{
	int w_ii;

	memset(ap_PAData, 0x00, sizeof(LPA_DATA_T));

	ap_PAData->m_DevMax[LPA_DMA_IDX] = LPA_DMA_MAX;
	ap_PAData->m_DevMax[LPA_DSS_IDX] = LPA_DSS_MAX;
	ap_PAData->m_DevMax[LPA_DPG_IDX] = LPA_DPG_MAX;
	ap_PAData->m_DevMax[LPA_DRM_IDX] = LPA_DRM_MAX;
	ap_PAData->m_DevMax[LPA_DMP_IDX] = LPA_DMP_MAX;
	ap_PAData->m_DevMax[LPA_DRE_IDX] = LPA_DRE_MAX;
	ap_PAData->m_DevMax[LPA_DFR_IDX] = LPA_DFR_MAX;

	ap_PAData->m_PCMacroCnt = 1;
	ap_PAData->m_PCMacroMax = LPA_PCMACRO_MAX;

#if defined(DEV_5000)
	ap_PAData->m_DMTMacroCnt = 1;
	ap_PAData->m_DMTMacroMax = LPA_DMTMACRO_MAX;
#endif

	ap_PAData->m_TimerMacroCnt = 1;
	ap_PAData->m_TimerMacroMax = LPA_TIMERMACRO_MAX;

#if defined(DEV_5000)
	ap_PAData->m_EmMacroCnt = 1;
	ap_PAData->m_EmMacroMax = LPA_EMMACRO_MAX;
#endif

	ap_PAData->m_MMacroCnt = LPA_MMACRO_MAX;
	ap_PAData->m_MMacroMax = LPA_MMACRO_MAX;

#if !defined(DEV_5000)		// 1000일경우는 DMT를 1로 고정
	ap_PAData->m_DevCnt[LPA_DMA_IDX] = 1;
#endif

	// DPG ITEM 숫자는 무조건 8
	for(w_ii=0;w_ii<m_PAData.m_DevCnt[LPA_DPG_IDX];w_ii++) {
		m_PAData.m_DpgData[w_ii].m_Cnt = LPA_DPG_ITEM;
	}

	ap_PAData->m_DevCode = 0;		// 식별자 코드 

	ap_PAData->m_DLCCnt = LPA_DLC_MAX;
	ap_PAData->m_DLCMax = LPA_DLC_MAX;
}




void CDLLowPA::SetMainWnd(void	*ap_MainWnd)	
{	
	mp_MainWnd = (CDLLowPASetupDlg *)ap_MainWnd;	
												
	if(mp_MainWnd) {		
		m_MainHwnd = mp_MainWnd->GetSafeHwnd();									
	} else {							
		m_MainHwnd = NULL;											
	}											
}



int CDLLowPA::Save(char *ap_fname, LPA_DATA_T *ap_PAData)
{
	FILE 	*wp_wfp;
	FHEAD_T w_Header;
	MARK_T	w_Marker;
	LPA_DATA_T *wp_PAData;
	char	w_Path[MAX_PATH];

	memset(&w_Path,0x00,sizeof(w_Path));
	dl_strcpy(w_Path,ap_fname);

	Lock();
	if(ap_PAData) {
		wp_PAData = ap_PAData;
	} else {
		wp_PAData = &m_PAData;
	}
	if ((wp_wfp = fopen(w_Path, "wb")) == NULL) {   // when open error 
		DL_LOG("Load(%s) open error\n", ap_fname);
		UnLock();
		return -1;
	}
	memset(&w_Header, 0x00, sizeof(FHEAD_T));
#if defined(DEV_5000)	
	w_Header.m_Cheker = LPA_FILE_CHECKER_5000;
#else
	w_Header.m_Cheker = LPA_FILE_CHECKER_1000;
#endif
	fwrite(&w_Header, sizeof(FHEAD_T), 1, wp_wfp);

	memset(&w_Marker, 0x00, sizeof(MARK_T));
	w_Marker.m_Marker = MARK_LPA_DATA;

	fwrite(&w_Marker, sizeof(MARK_T), 1, wp_wfp);
	fwrite(wp_PAData, sizeof(LPA_DATA_T), 1, wp_wfp);

	fclose(wp_wfp);
	m_Modified = 0;
	UnLock();
	return 1;
}

//#define	TEST_LPA_DATA_T

#if defined(TEST_LPA_DATA_T)
#define		DL_FREAD(x, y, z, w)	fread((x), (z), (y), (w));w_cnt+=(z); if((z) != 2) { DL_LOG("OFFSET:%d", w_cnt); }
#define		DL_FSEEK(x, y, w)		fseek((x), (y), (w));w_cnt+=(y); DL_LOG("OFFSET:%d", w_cnt);
#define		DL_FLOG()				DL_LOG("OFFSET:%d", w_cnt);
#else
#define		DL_FREAD(x, y, z, w)	fread((x), (z), (y), (w))
#define		DL_FSEEK(x, y, w)		fseek((x), (y), (w))
#define		DL_FLOG()
#endif

int CDLLowPA::Load(char *ap_fname, LPA_DATA_T *ap_PAData)
{
	FILE 	*wp_rfp;
	LPA_DATA_T *wp_PAData;
	FHEAD_T w_Header;
	MARK_T	w_Marker;

	int w_ii, w_jj, w_kk;
	int	w_dss_max = 0;
	int w_dmx_max = 0;
	int	w_cnt = 0;

	Lock();
	if(ap_PAData) {
		wp_PAData = ap_PAData;
	} else {
		wp_PAData = &m_PAData;
	}
	if ((wp_rfp = fopen(ap_fname, "rb")) == NULL) {   // when open error 
		DL_LOG("Load(%s) open error\n", ap_fname);
		UnLock();
		return -1;
	}
	
	if(fread(&w_Header, 1, sizeof(FHEAD_T), wp_rfp) !=  sizeof(FHEAD_T)) {
		DL_LOG("Load(%s) FHEAD_H read error\n", ap_fname);
		fclose(wp_rfp);
		UnLock();
		return -1;
	}
#if 0
#if defined(DEV_5000)
	if(w_Header.m_Cheker != LPA_FILE_CHECKER_5000) {

#else
	if(w_Header.m_Cheker != LPA_FILE_CHECKER_1000) {
#endif
		DL_LOG("%s: FHEAD_H check error\n", ap_fname);
		fclose(wp_rfp);
		UnLock();
		return -1;
	}
#endif

	if(fread(&w_Marker, 1, sizeof(MARK_T), wp_rfp) !=  sizeof(MARK_T)) {
		DL_LOG("%s: Load End-Marker\n", ap_fname);
		fclose(wp_rfp);
		UnLock();
		return -1;
	}

#if 0
	if(w_Marker.m_Marker == MARK_LPA_DATA) {
		fread(wp_PAData, 1, sizeof(LPA_DATA_T), wp_rfp);
	}
#else

	if(w_Marker.m_Marker == MARK_LPA_DATA) 
	{
//#if defined(TEST_LPA_DATA_T)
//		int	w_cnt = 0;

		DL_LOG("m_DevCnt : %d", (char *)&wp_PAData->m_DevCnt - (char *)wp_PAData);
		DL_LOG("m_DevMax : %d", (char *)&wp_PAData->m_DevMax - (char *)wp_PAData);
		DL_LOG("m_DmtData : %d", (char *)&wp_PAData->m_DmtData - (char *)wp_PAData);
		DL_LOG("m_DssData : %d", (char *)&wp_PAData->m_DssData - (char *)wp_PAData);
		DL_LOG("m_DpgData : %d", (char *)&wp_PAData->m_DpgData - (char *)wp_PAData);
		DL_LOG("m_DfrData : %d", (char *)&wp_PAData->m_DfrData - (char *)wp_PAData);

		DL_LOG("m_MMacroCnt : %d", (char *)&wp_PAData->m_MMacroCnt - (char *)wp_PAData);
		DL_LOG("m_MMacroMax : %d", (char *)&wp_PAData->m_MMacroMax - (char *)wp_PAData);
		DL_LOG("m_MMacroData : %d", (char *)&wp_PAData->m_MMacroData - (char *)wp_PAData);

		DL_LOG("m_PCMacroCnt : %d", (char *)&wp_PAData->m_PCMacroCnt - (char *)wp_PAData);
		DL_LOG("m_PCMacroMax : %d", (char *)&wp_PAData->m_PCMacroMax - (char *)wp_PAData);
		DL_LOG("m_PCMacroData : %d", (char *)&wp_PAData->m_PCMacroData - (char *)wp_PAData);

		DL_LOG("m_TermDpg : %d", (char *)&wp_PAData->m_TermDpg - (char *)wp_PAData);
		DL_LOG("m_DfrTerm : %d", (char *)&wp_PAData->m_DfrTerm - (char *)wp_PAData);
		DL_LOG("m_PCMacroTerm : %d", (char *)&wp_PAData->m_PCMacroTerm - (char *)wp_PAData);
		DL_LOG("m_MMacroTerm : %d", (char *)&wp_PAData->m_MMacroTerm - (char *)wp_PAData);

		DL_LOG("m_TimerMacroCnt : %d", (char *)&wp_PAData->m_TimerMacroCnt - (char *)wp_PAData);
		DL_LOG("m_TimerMacroMax : %d", (char *)&wp_PAData->m_TimerMacroMax - (char *)wp_PAData);
		DL_LOG("m_TimerMacroData : %d", (char *)&wp_PAData->m_TimerMacroData - (char *)wp_PAData);
		DL_LOG("m_TimerMacroTerm : %d", (char *)&wp_PAData->m_TimerMacroTerm - (char *)wp_PAData);	

#if defined(DEV_5000)
		DL_LOG("m_DMTMacroCnt : %d", (char *)&wp_PAData->m_DMTMacroCnt - (char *)wp_PAData);
		DL_LOG("m_DMTMacroMax : %d", (char *)&wp_PAData->m_DMTMacroMax - (char *)wp_PAData);
		DL_LOG("m_DMTMacroData : %d", (char *)&wp_PAData->m_DMTMacroData - (char *)wp_PAData);
		DL_LOG("m_DMTMacroTerm : %d", (char *)&wp_PAData->m_DMTMacroTerm - (char *)wp_PAData);

		DL_LOG("m_EmMacroCnt : %d", (char *)&wp_PAData->m_EmMacroCnt - (char *)wp_PAData);
		DL_LOG("m_EmMacroMax : %d", (char *)&wp_PAData->m_EmMacroMax - (char *)wp_PAData);
		DL_LOG("m_EmMacroData : %d", (char *)&wp_PAData->m_EmMacroData - (char *)wp_PAData);
		DL_LOG("m_EmMacroTerm : %d", (char *)&wp_PAData->m_EmMacroTerm - (char *)wp_PAData);
#endif
		DL_LOG("m_DevCode : %d", (char *)&wp_PAData->m_DevCode - (char *)wp_PAData);

		DL_FREAD(&wp_PAData->m_DevCode, 1, sizeof(DWORD), wp_rfp);

		DL_FREAD(&wp_PAData->m_DevCnt[0], 1, sizeof(int) * LPA_EQUIP_MAX, wp_rfp);
		DL_FREAD(&wp_PAData->m_DevMax[0], 1, sizeof(int) * LPA_EQUIP_MAX, wp_rfp);

		wp_PAData->m_DevMax[LPA_DMA_IDX] = LPA_DMA_MAX;
		wp_PAData->m_DevMax[LPA_DSS_IDX] = LPA_DSS_MAX;

#if defined(DEV_5000)
//		if(w_Header.m_Cheker == LPA_FILE_CHECKER_5000_VER01) {
//			DL_FREAD(&wp_PAData->m_DmtData[0], 1, sizeof(LPA_DEVICE_T) * LPA_DMA_MAX_10, wp_rfp);
//		} else {
			DL_FREAD(&wp_PAData->m_DmtData[0], 1, sizeof(LPA_DEVICE_T) * LPA_DMA_MAX, wp_rfp);
//		}
#else
		DL_FREAD(&wp_PAData->m_DmtData[0], 1, sizeof(LPA_DEVICE_T) * LPA_DMA_MAX, wp_rfp);
#endif
		w_dss_max = LPA_DSS_MAX;
		w_dmx_max = LPA_DMA_MAX;

		/*
		if(		w_Header.m_Cheker == LPA_FILE_CHECKER_1000_VER01 
			||  w_Header.m_Cheker == LPA_FILE_CHECKER_1000_VER02
			||  w_Header.m_Cheker == LPA_FILE_CHECKER_5000_VER01
			) {
			w_dss_max = LPA_DSS_MAX_10;
		}*/
#if defined(DEV_5000)
//		if(w_Header.m_Cheker == LPA_FILE_CHECKER_5000_VER01) {
//			w_dmx_max = LPA_DMA_MAX_10;
//		}
#endif

		DL_FREAD(&wp_PAData->m_DssData[0], 1, sizeof(LPA_DEVICE_T) * w_dss_max, wp_rfp);	
		DL_FREAD(&wp_PAData->m_DpgData[0], 1, sizeof(LPA_DEVICE_T) * LPA_DPG_MAX, wp_rfp);	
		DL_FREAD(&wp_PAData->m_DfrData[0], 1, sizeof(LPA_DEVICE_T) * LPA_DFR_MAX, wp_rfp);	

		DL_FREAD(&wp_PAData->m_MMacroCnt, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_MMacroMax, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_MMacroData[0], 1, sizeof(LPA_DEVICE_T) * LPA_MMACRO_MAX , wp_rfp);	

		DL_FREAD(&wp_PAData->m_PCMacroCnt, 1, sizeof(int), wp_rfp);
		DL_FREAD(&wp_PAData->m_PCMacroMax, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_PCMacroData[0], 1, sizeof(LPA_DEVICE_T) * LPA_PCMACRO_MAX , wp_rfp);

		for(w_jj=0;w_jj<(w_dmx_max+w_dss_max);w_jj++) 
		{
			for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
				DL_FREAD(&wp_PAData->m_TermDpg[w_jj][w_ii], 1, sizeof(LPA_TERM_DPG_T), wp_rfp);
			}
		}
//		DL_FREAD(&wp_PAData->m_TermDpg[0][0], 1, sizeof(wp_PAData->m_TermDpg), wp_rfp);
		DL_FLOG();

		for(w_jj=0;w_jj<LPA_DFR_MAX;w_jj++) {
			for(w_ii=0;w_ii<LPA_DFR_ITEM;w_ii++) {
				for(w_kk=0;w_kk<(w_dmx_max+w_dss_max);w_kk++) {
					DL_FREAD(&wp_PAData->m_DfrTerm[w_jj][w_ii].m_SSFlag[w_kk], 1, sizeof(WORD), wp_rfp);
				}
			}
		}
//		DL_FREAD(&wp_PAData->m_DfrTerm[0][0], 1, sizeof(wp_PAData->m_DfrTerm), wp_rfp);
		DL_FLOG();

		for(w_ii=0;w_ii<LPA_PCMACRO_ITEM;w_ii++) {
			for(w_jj=0;w_jj<LPA_PCMACRO_MAX;w_jj++) {
				for(w_kk=0;w_kk<(w_dmx_max+w_dss_max);w_kk++) {
					DL_FREAD(&wp_PAData->m_PCMacroTerm[w_jj][w_ii].m_SSFlag[w_kk], 1, sizeof(WORD), wp_rfp);
				}
			}
		}
//		DL_FREAD(&wp_PAData->m_PCMacroTerm[0][0], 1, sizeof(wp_PAData->m_PCMacroTerm), wp_rfp);
		DL_FLOG();

		for(w_jj=0;w_jj<LPA_MMACRO_MAX;w_jj++) {
			for(w_ii=0;w_ii<LPA_MMACRO_ITEM;w_ii++) {
				for(w_kk=0;w_kk<(w_dmx_max+w_dss_max);w_kk++) {
					DL_FREAD(&wp_PAData->m_MMacroTerm[w_jj][w_ii].m_SSFlag[w_kk], 1, sizeof(WORD), wp_rfp);
				}
			}
		}
//		DL_FREAD(&wp_PAData->m_MMacroTerm[0][0], 1, sizeof(wp_PAData->m_MMacroTerm), wp_rfp);
		DL_FLOG();
		//if(w_Header.m_Cheker == LPA_FILE_CHECKER_1000) {
		//	DL_FSEEK(wp_rfp, 2, SEEK_CUR);
		//}

		DL_FREAD(&wp_PAData->m_TimerMacroCnt, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_TimerMacroMax, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_TimerMacroData[0], 1, sizeof(LPA_DEVICE_T) * LPA_TIMERMACRO_MAX , wp_rfp);	

		for(w_ii=0;w_ii<LPA_TIMERMACRO_MAX;w_ii++) {
			for(w_jj=0;w_jj<LPA_TIMERMACRO_ITEM;w_jj++) {
				for(w_kk=0;w_kk<(w_dmx_max+w_dss_max);w_kk++) {
					DL_FREAD(&wp_PAData->m_TimerMacroTerm[w_jj][w_ii].m_SSFlag[w_kk], 1, sizeof(WORD), wp_rfp);
				}
			}
		}	
//		DL_FREAD(&wp_PAData->m_TimerMacroTerm[0][0], 1, sizeof(wp_PAData->m_TimerMacroTerm), wp_rfp);
		DL_FLOG();

		if(w_Header.m_Cheker == LPA_FILE_CHECKER_1000) {
			DL_FSEEK(wp_rfp, 2, SEEK_CUR);
		}

#if defined(DEV_1000)
		DL_FREAD(&wp_PAData->m_DevCode, 1, sizeof(DWORD), wp_rfp);
#endif

#if defined(DEV_5000)
		DL_FREAD(&wp_PAData->m_DMTMacroCnt, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_DMTMacroMax, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_DMTMacroData[0], 1, sizeof(LPA_DEVICE_T) * LPA_DMTMACRO_MAX , wp_rfp);	
		for(w_jj=0;w_jj<LPA_DMTMACRO_MAX;w_jj++) {
			for(w_ii=0;w_ii<LPA_DMTMACRO_ITEM;w_ii++) {
				for(w_kk=0;w_kk<(w_dmx_max+w_dss_max);w_kk++) {
					DL_FREAD(&wp_PAData->m_DMTMacroTerm[w_jj][w_ii].m_SSFlag[w_kk], 1, sizeof(WORD), wp_rfp);
				}
			}
		}	
		DL_FLOG();

		DL_FREAD(&wp_PAData->m_EmMacroCnt, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_EmMacroMax, 1, sizeof(int), wp_rfp);	
		DL_FREAD(&wp_PAData->m_EmMacroData[0], 1, sizeof(LPA_DEVICE_T) * LPA_EMMACRO_MAX , wp_rfp);	
		for(w_jj=0;w_jj<LPA_EMMACRO_MAX;w_jj++) {
			for(w_ii=0;w_ii<LPA_EMMACRO_ITEM;w_ii++) {
				for(w_kk=0;w_kk<(w_dmx_max+w_dss_max);w_kk++) {
					DL_FREAD(&wp_PAData->m_EmMacroTerm[w_jj][w_ii].m_SSFlag[w_kk], 1, sizeof(WORD), wp_rfp);
				}
			}
		}	
		DL_FLOG();

#if defined(DEV_5000)
		DL_FREAD(&wp_PAData->m_Gate, 1, sizeof(WORD), wp_rfp);		// GATE : 2015.03.31
#endif

#endif
	}
#endif

	fpos_t w_pos;
	fgetpos( wp_rfp, &w_pos );

//	DL_LOG("OFFSET:%d", w_pos);

	fclose(wp_rfp);
	m_Modified = 0;
	UnLock();
	return 1;
}

int CDLLowPA::GetDpgCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] +  m_PAData.m_DevCnt[LPA_DSS_IDX];

	for(w_jj=0;w_jj<w_DevCnt;w_jj++) {
		for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
			if( m_PAData.m_TermDpg[w_jj][w_ii].m_DpgAddr == a_Addr 
					&&  m_PAData.m_TermDpg[w_jj][w_ii].m_DpgOutput == a_Item) {
				w_Cnt++;
			}
		}
	}
	return(w_Cnt);
}


int CDLLowPA::GetPCMacroCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	if(a_Addr < 1 || a_Item < 1) {
		return(0);
	}

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] +  m_PAData.m_DevCnt[LPA_DSS_IDX];

	for(w_jj=0;w_jj<w_DevCnt;w_jj++) {
		for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
			if(IsBitOn(m_PAData.m_PCMacroTerm[a_Addr-1][a_Item-1].m_SSFlag, w_jj, w_ii)) {
				w_Cnt++;
			}
		}
	}

	return(w_Cnt);
}

#if defined(DEV_5000)
int CDLLowPA::GetDMTMacroCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	if(a_Addr < 1 || a_Item < 1) {
		return(0);
	}

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] +  m_PAData.m_DevCnt[LPA_DSS_IDX];

	for(w_jj=0;w_jj<w_DevCnt;w_jj++) {
		for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
			if(IsBitOn(m_PAData.m_DMTMacroTerm[a_Addr-1][a_Item-1].m_SSFlag, w_jj, w_ii)) {
				w_Cnt++;
			}
		}
	}

	return(w_Cnt);
}
#endif

int CDLLowPA::GetDLCCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	if (a_Addr < 1 || a_Item < 1) {
		return(0);
	}

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] + m_PAData.m_DevCnt[LPA_DSS_IDX];

	for (w_jj = 0; w_jj < w_DevCnt; w_jj++) {
		for (w_ii = 0; w_ii < LPA_TERMINAL_ITEM; w_ii++) {
			if (IsBitOn(m_PAData.m_DLCTerm[a_Addr - 1][a_Item - 1].m_SSFlag, w_jj, w_ii)) {
				w_Cnt++;
			}
		}
	}

	return(w_Cnt);
}


int CDLLowPA::GetTimerMacroCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	if(a_Addr < 1 || a_Item < 1) {
		return(0);
	}

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] +  m_PAData.m_DevCnt[LPA_DSS_IDX];

	for(w_jj=0;w_jj<w_DevCnt;w_jj++) {
		for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
			if(IsBitOn(m_PAData.m_TimerMacroTerm[a_Addr-1][a_Item-1].m_SSFlag, w_jj, w_ii)) {
				w_Cnt++;
			}
		}
	}

	return(w_Cnt);
}

#if defined(DEV_5000)
int CDLLowPA::GetEmMacroCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	if(a_Addr < 1 || a_Item < 1) {
		return(0);
	}

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] +  m_PAData.m_DevCnt[LPA_DSS_IDX];

	for(w_jj=0;w_jj<w_DevCnt;w_jj++) {
		for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
			if(IsBitOn(m_PAData.m_EmMacroTerm[a_Addr-1][a_Item-1].m_SSFlag, w_jj, w_ii)) {
				w_Cnt++;
			}
		}
	}

	return(w_Cnt);
}
#endif



int CDLLowPA::GetMMacroCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	if(a_Addr < 1 || a_Item < 1) {
		return(0);
	}

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] +  m_PAData.m_DevCnt[LPA_DSS_IDX];

	for(w_jj=0;w_jj<w_DevCnt;w_jj++) {
		for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
			if(IsBitOn(m_PAData.m_MMacroTerm[a_Addr-1][a_Item-1].m_SSFlag, w_jj, w_ii)) {
				w_Cnt++;
			}
		}
	}

	return(w_Cnt);
}


int CDLLowPA::GetAddrMMacroCount(int a_Addr)
{
	if(a_Addr < 1) {
		return(0);
	}
	return(m_PAData.m_MMacroData[a_Addr-1].m_Cnt);
}


int CDLLowPA::GetAddrMMacroChannelCount(int a_Addr)
{
	int w_ii;
	if(a_Addr < 1) {
		return(0);
	}
	int w_Cnt = 0;

	for(w_ii=0;w_ii<m_PAData.m_MMacroData[a_Addr-1].m_Cnt;w_ii++) {
		w_Cnt += GetMMacroCount(a_Addr, w_ii);
	}
	return(w_Cnt);
}


int CDLLowPA::GetDfrCount(int a_Addr, int a_Item)
{
	int	w_ii, w_jj;
	int w_Cnt = 0;
	int w_DevCnt;

	if(a_Addr < 1 || a_Item < 1) {
		return(0);
	}

	w_DevCnt = m_PAData.m_DevCnt[LPA_DMA_IDX] +  m_PAData.m_DevCnt[LPA_DSS_IDX];

	for(w_jj=0;w_jj<w_DevCnt;w_jj++) {
		for(w_ii=0;w_ii<LPA_TERMINAL_ITEM;w_ii++) {
			if(IsBitOn(m_PAData.m_DfrTerm[a_Addr-1][a_Item-1].m_SSFlag, w_jj, w_ii)) {
				w_Cnt++;
			}
		}
	}

	return(w_Cnt);
}

void CDLLowPA::SetDmtName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_DevCnt[LPA_DMA_IDX] && a_Item > 0 && a_Item <= LPA_DMA_MAX) {

		strcpy(m_PAData.m_DmtData[a_Addr-1].m_DevName, ap_Name);
		m_Modified = 1;
	}
}

void CDLLowPA::SetDssName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_DevCnt[LPA_DSS_IDX] && a_Item > 0 && a_Item <= LPA_DSS_MAX) {

		strcpy(m_PAData.m_DssData[a_Addr-1].m_DevName, ap_Name);
		m_Modified = 1;
	}
}

void CDLLowPA::SetDpgName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_DevCnt[LPA_DPG_IDX]
		&& a_Item > 0 && a_Item <= LPA_DPG_ITEM) {

		strcpy(m_PAData.m_DpgData[a_Addr-1].m_Item[a_Item-1].m_Name, ap_Name);
		m_Modified = 1;
	}
}


void CDLLowPA::SetPCMacroName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_PCMacroCnt
		&& a_Item > 0 && a_Item <= LPA_PCMACRO_ITEM) {

		strcpy(m_PAData.m_PCMacroData[a_Addr-1].m_Item[a_Item-1].m_Name, ap_Name);
	}
}


#if defined(DEV_5000)
void CDLLowPA::SetDMTMacroName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_DMTMacroCnt
		&& a_Item > 0 && a_Item <= LPA_DMTMACRO_ITEM) {

		strcpy(m_PAData.m_DMTMacroData[a_Addr-1].m_Item[a_Item-1].m_Name, ap_Name);
	}
}
#endif


void CDLLowPA::SetMMacroName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_MMacroCnt
		&& a_Item > 0 && a_Item <= LPA_MMACRO_ITEM) {

		strcpy(m_PAData.m_MMacroData[a_Addr-1].m_Item[a_Item-1].m_Name, ap_Name);
		m_Modified = 1;
	}
}


void CDLLowPA::SetTimerMacroName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_TimerMacroCnt
		&& a_Item > 0 && a_Item <= LPA_TIMERMACRO_ITEM) {

		strcpy(m_PAData.m_TimerMacroData[a_Addr-1].m_Item[a_Item-1].m_Name, ap_Name);
		m_Modified = 1;
	}
}

#if defined(DEV_5000)
void CDLLowPA::SetEmMacroName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_EmMacroCnt
		&& a_Item > 0 && a_Item <= LPA_EMMACRO_ITEM) {

		strcpy(m_PAData.m_EmMacroData[a_Addr-1].m_Item[a_Item-1].m_Name, ap_Name);
		m_Modified = 1;
	}
}
#endif

void CDLLowPA::SetDfrName(char *ap_Name, int a_Addr, int a_Item)
{
	if(ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_DevCnt[LPA_DFR_IDX]
		&& a_Item > 0 && a_Item <= LPA_DFR_ITEM) {

		strcpy(m_PAData.m_DfrData[a_Addr-1].m_Item[a_Item-1].m_Name, ap_Name);
		m_Modified = 1;
	}
}

void CDLLowPA::SetDlcName(char *ap_Name, int a_Addr, int a_Item)
{
	if (ap_Name && a_Addr > 0 && a_Addr <= m_PAData.m_DLCCnt
		&& a_Item > 0 && a_Item <= LPA_DLC_ITEM) {

		strcpy(m_PAData.m_DLCData[a_Addr - 1].m_Item[a_Item - 1].m_Name, ap_Name);
		m_Modified = 1;
	}
}



bool CDLLowPA::IsBitOn(WORD *ap_Data, int a_Y, int a_X)
{
	return( ((ap_Data[a_Y] >> a_X) & 0x01) ? true : false);
}



void CDLLowPA::ToggleBit(WORD *ap_Data, int a_Y, int a_X)
{
	ap_Data[a_Y] ^= (1 << a_X);
	m_Modified = 1;
}

void CDLLowPA::SetBit(WORD *ap_Data, int a_Y, int a_X, int a_OnOff)
{
	if(a_OnOff) {
		ap_Data[a_Y] |= (1 << a_X);
	} else {
		ap_Data[a_Y] &= ~(1 << a_X);
	}
	m_Modified = 1;
}



void CDLLowPA::Invalidate()
{
	if(GetEquipWnd()) {
		GetEquipWnd()->PostMessage(WM_INVALIDATE,0,0);
	}
	if(GetDpgWnd()) {
		GetDpgWnd()->PostMessage(WM_INVALIDATE,0,0);
	}
	if(GetPCMacroWnd()) {
		GetPCMacroWnd()->PostMessage(WM_INVALIDATE,0,0);
	}
#if defined(DEV_5000)
	if(GetDMTMacroWnd()) {
		GetDMTMacroWnd()->PostMessage(WM_INVALIDATE,0,0);
	}
#endif
	if(GetDfrWnd()) {
		GetDfrWnd()->PostMessage(WM_INVALIDATE,0,0);
	}
	if(GetMMacroWnd()) {
		GetMMacroWnd()->PostMessage(WM_INVALIDATE,0,0);
	}
	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}
}


int	 CDLLowPA::ToDevice()
{
	int w_Cnt = 0;

	return(w_Cnt);
}

int	 CDLLowPA::FromDevice()
{
	int w_Cnt = 0;

	return(w_Cnt);
}

int	 CDLLowPA::MakeRom1Data(LPA_DATA_T *ap_PAData, BYTE *ap_Rom1)
{
	int	w_ii;
	int	w_jj;
	int	w_kk;
	int	w_mm;
	int w_Cnt = 0;
	int	w_SSTotCnt;
	int	w_SSCnt;
	int	w_DpgCnt;
	int	w_DfrCnt;

	BYTE w_DevType=0x0;
	BYTE w_Zone=0x0;

	memset(ap_Rom1, 0x00, LPA_ROM_MAX);

	SET_DWORD(ap_Rom1,  w_Cnt, ap_PAData->m_DevCode);	// 식별자 코드

	SET_BYTE(ap_Rom1,  w_Cnt, 0);
	SET_BYTE(ap_Rom1,  w_Cnt, 1);
#if defined(DEV_5000)
	SET_BYTE(ap_Rom1,  w_Cnt, 1);	// DMT
#else
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DMA_IDX]);	// DMT
#endif
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DSS_IDX]);	// DSS
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DPG_IDX]);	// DPG
	SET_BYTE(ap_Rom1,  w_Cnt, 0);
	SET_BYTE(ap_Rom1,  w_Cnt, 0);
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DRM_IDX]);	// DRM
#if defined(DEV_5000)
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DMA_IDX]);	// DPA
#else
	SET_BYTE(ap_Rom1,  w_Cnt, 0);	// DPA
#endif
	SET_BYTE(ap_Rom1,  w_Cnt, 0);
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DMP_IDX]);	// DMP
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DRE_IDX]);	// DRE
	SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DFR_IDX]);	// DFR
	SET_BYTE(ap_Rom1,  w_Cnt, 0);
	SET_BYTE(ap_Rom1,  w_Cnt, 0);
#if defined(DEV_5000)
	SET_BYTE(ap_Rom1,  w_Cnt, 1);									// 5000 SERISE
#else
	SET_BYTE(ap_Rom1,  w_Cnt, 0);									// 1000 SERISE
#endif

#if defined(DEV_5000)
	SET_WORD(ap_Rom1,  w_Cnt, ap_PAData->m_Gate);					// GATE 정보 2015.03.31
#endif

	if(ap_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {					// DPG가 0인경우 PCMACRO/DRM/DFR에서 사용한 SSFlag를 
																// PDG가 할당되었다고 보고 TermDlg로 설정해서 보낸다.]
		LPA_TERM_DPG_T	w_TermDpg[LPA_DMA_MAX+LPA_DSS_MAX][LPA_TERMINAL_ITEM];
		memset(&w_TermDpg, 0x00, sizeof(w_TermDpg));

		// PC MACRO에 설정된값 임시 DPG설정으로..만든다.
		w_SSTotCnt = ap_PAData->m_DevCnt[LPA_DMA_IDX] +  ap_PAData->m_DevCnt[LPA_DSS_IDX];
		for(w_jj=0;w_jj<ap_PAData->m_PCMacroCnt;w_jj++) {
			for(w_ii=0;w_ii<ap_PAData->m_PCMacroData[w_jj].m_Cnt;w_ii++) {
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					for(w_mm=0;w_mm<LPA_TERMINAL_ITEM;w_mm++) {
						if(IsBitOn(ap_PAData->m_PCMacroTerm[w_jj][w_ii].m_SSFlag, w_kk, w_mm)) {

							w_TermDpg[w_kk][w_mm].m_DpgAddr = 1;
							w_TermDpg[w_kk][w_mm].m_DpgOutput = 1;
						}
					}
				}
			}
		}
#if defined(DEV_5000)
	// DMT MACRO MACRO에 설정된값 임시 DPG설정으로..만든다.
		w_SSTotCnt = ap_PAData->m_DevCnt[LPA_DMA_IDX] +  ap_PAData->m_DevCnt[LPA_DSS_IDX];
		for(w_jj=0;w_jj<ap_PAData->m_DMTMacroCnt;w_jj++) {
			for(w_ii=0;w_ii<ap_PAData->m_DMTMacroData[w_jj].m_Cnt;w_ii++) {
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					for(w_mm=0;w_mm<LPA_TERMINAL_ITEM;w_mm++) {
						if(IsBitOn(ap_PAData->m_DMTMacroTerm[w_jj][w_ii].m_SSFlag, w_kk, w_mm)) {

							w_TermDpg[w_kk][w_mm].m_DpgAddr = 1;
							w_TermDpg[w_kk][w_mm].m_DpgOutput = 1;
						}
					}
				}
			}
		}
#endif
		// DRM에 설정된값 임시 DPG설정으로..만든다.
		for(w_jj=0;w_jj<ap_PAData->m_MMacroCnt;w_jj++) {
			for(w_ii=0;w_ii<ap_PAData->m_MMacroData[w_jj].m_Cnt;w_ii++) {
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					for(w_mm=0;w_mm<LPA_TERMINAL_ITEM;w_mm++) {
						if(IsBitOn(ap_PAData->m_MMacroTerm[w_jj][w_ii].m_SSFlag, w_kk, w_mm)) {
							w_TermDpg[w_kk][w_mm].m_DpgAddr = 1;
							w_TermDpg[w_kk][w_mm].m_DpgOutput = 1;
						}
					}
				}
			}
		}
		// TIMER MACRO에 설정된값 임시 DPG설정으로..만든다.
		for(w_jj=0;w_jj<ap_PAData->m_TimerMacroCnt;w_jj++) {
			for(w_ii=0;w_ii<ap_PAData->m_TimerMacroData[w_jj].m_Cnt;w_ii++) {
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					for(w_mm=0;w_mm<LPA_TERMINAL_ITEM;w_mm++) {
						if(IsBitOn(ap_PAData->m_TimerMacroTerm[w_jj][w_ii].m_SSFlag, w_kk, w_mm)) {
							w_TermDpg[w_kk][w_mm].m_DpgAddr = 1;
							w_TermDpg[w_kk][w_mm].m_DpgOutput = 1;
						}
					}
				}
			}
		}
#if defined(DEV_5000)
		// EM MACRO에 설정된값 임시 DPG설정으로..만든다.
		for(w_jj=0;w_jj<ap_PAData->m_EmMacroCnt;w_jj++) {
			for(w_ii=0;w_ii<ap_PAData->m_EmMacroData[w_jj].m_Cnt;w_ii++) {
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					for(w_mm=0;w_mm<LPA_TERMINAL_ITEM;w_mm++) {
						if(IsBitOn(ap_PAData->m_EmMacroTerm[w_jj][w_ii].m_SSFlag, w_kk, w_mm)) {
							w_TermDpg[w_kk][w_mm].m_DpgAddr = 1;
							w_TermDpg[w_kk][w_mm].m_DpgOutput = 1;
						}
					}
				}
			}
		}
#endif
		// DFR에 설정된값 임시 DPG설정으로..만든다.
		for(w_jj=0;w_jj<ap_PAData->m_DevCnt[LPA_DFR_IDX];w_jj++) {
			for(w_ii=0;w_ii<ap_PAData->m_DfrData[w_jj].m_Cnt;w_ii++) {
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					for(w_mm=0;w_mm<LPA_TERMINAL_ITEM;w_mm++) {
						if(IsBitOn(ap_PAData->m_DfrTerm[w_jj][w_ii].m_SSFlag, w_kk, w_mm)) {
							w_TermDpg[w_kk][w_mm].m_DpgAddr = 1;
							w_TermDpg[w_kk][w_mm].m_DpgOutput = 1;
						}
					}
				}
			}
		}
		for(w_jj=0;w_jj<w_SSTotCnt;w_jj++) {

// 2015.06.15
//#if defined(DEV_5000)
//			SET_BYTE(ap_Rom1,  w_Cnt, DEV_TYPE_DSS);				// DMT or DSS
//			SET_BYTE(ap_Rom1,  w_Cnt, w_jj+1);						// DMT or DSS
//#else 
			if(w_jj >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
				w_DevType = DEV_TYPE_DSS;
				SET_BYTE(ap_Rom1,  w_Cnt, w_DevType);				// DMT or DSS
				SET_BYTE(ap_Rom1,  w_Cnt, w_jj-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);					// DMT or DSS
			} else {
#if defined(DEV_5000)
				w_DevType = DEV_TYPE_DPA;
#else
				w_DevType = DEV_TYPE_DMT;
#endif
				SET_BYTE(ap_Rom1,  w_Cnt, w_DevType);				// DMT or DSS
				SET_BYTE(ap_Rom1,  w_Cnt, w_jj+1);					// DMT or DSS
			}
//#endif

			w_DpgCnt = 0;
			for(w_ii=LPA_TERMINAL_ITEM-1;w_ii>=0;w_ii--) {
				if(w_TermDpg[w_jj][w_ii].m_DpgAddr != 0 && w_TermDpg[w_jj][w_ii].m_DpgOutput != 0) {
					w_DpgCnt = w_ii+1;
					break;
				}
			}
			SET_BYTE(ap_Rom1,  w_Cnt, w_DpgCnt);					// 사용되는 채널갯수
			for(w_ii=0;w_ii<w_DpgCnt;w_ii++) {
				SET_BYTE(ap_Rom1,  w_Cnt, 0);						// DPG Addr ==> 무조건 0로 보낸다.
				SET_BYTE(ap_Rom1,  w_Cnt, 0);						// DPG Output ==> 무조건 0로 보낸다.
			}
		}
	} else {
		w_SSTotCnt = ap_PAData->m_DevCnt[LPA_DMA_IDX] +  ap_PAData->m_DevCnt[LPA_DSS_IDX];
		for(w_jj=0;w_jj<w_SSTotCnt;w_jj++) {
// 2015.06.15
//#if defined(DEV_5000)
//			SET_BYTE(ap_Rom1,  w_Cnt, DEV_TYPE_DSS);				// DMT or DSS
//			SET_BYTE(ap_Rom1,  w_Cnt, w_jj+1);						// DMT or DSS
//#else 
			if(w_jj >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
				w_DevType = DEV_TYPE_DSS;
				SET_BYTE(ap_Rom1,  w_Cnt, w_DevType);				// DMT or DSS
				SET_BYTE(ap_Rom1,  w_Cnt, w_jj-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);					// DMT or DSS
			} else {
#if defined(DEV_5000)
				w_DevType = DEV_TYPE_DPA;
#else
				w_DevType = DEV_TYPE_DMT;
#endif

				SET_BYTE(ap_Rom1,  w_Cnt, w_DevType);				// DMT or DSS
				SET_BYTE(ap_Rom1,  w_Cnt, w_jj+1);					// DMT or DSS
			}
//#endif
			w_DpgCnt = 0;
			for(w_ii=LPA_TERMINAL_ITEM-1;w_ii>=0;w_ii--) {
				if(ap_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0 && ap_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0) {
					w_DpgCnt = w_ii+1;
					break;
				}
			}
			SET_BYTE(ap_Rom1,  w_Cnt, w_DpgCnt);													// 사용되는 채널갯수
			for(w_ii=0;w_ii<w_DpgCnt;w_ii++) {
				SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr);				// DPG Addr
				SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput);			// DPG Output
			}
		}
	}

	for(w_jj=0;w_jj<ap_PAData->m_DevCnt[LPA_DFR_IDX];w_jj++) {
		// 실제 데이타가 설정되었는지 체크
		w_DfrCnt = 0;
		w_SSCnt = 0;
		for(w_ii=0;w_ii<ap_PAData->m_DfrData[w_jj].m_Cnt;w_ii++) {
			w_SSCnt = 0;
			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
				if(ap_PAData->m_DfrTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
					w_SSCnt++;
				}
			}
			if(w_SSCnt > 0) {
				w_DfrCnt++;
			}
		}
		if(w_DfrCnt == 0) {
			continue;
		}
		SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DfrData[w_jj].m_PRFlag ? DEV_TYPE_DDE : DEV_TYPE_DFE);		// R : DDE P:DFE
		SET_BYTE(ap_Rom1,  w_Cnt, w_jj+1);														// ADDRESS
		SET_WORD(ap_Rom1,  w_Cnt, w_DfrCnt);													// 화재 총 갯수

		for(w_ii=0;w_ii<ap_PAData->m_DfrData[w_jj].m_Cnt;w_ii++) {
			w_SSCnt = 0;
			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
				if(ap_PAData->m_DfrTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
					w_SSCnt++;
				}
			}
			if(w_SSCnt == 0) {
				continue;
			} 
			SET_BYTE(ap_Rom1,  w_Cnt, w_ii+1);													// 접점번호
			
			if(ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Building == 255) {	// 2016-04-14
				SET_BYTE(ap_Rom1,  w_Cnt, 0);// 동
			} else {
				SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Building+1);// 동
			}
			if(ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Stair == 255) {	// 2016-04-14
				SET_BYTE(ap_Rom1,  w_Cnt, 0);// 동
			} else {
				SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Stair+1);	// 라인
			}
			if(ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Floor == 255) {	// 2016-04-14
				SET_BYTE(ap_Rom1,  w_Cnt, 0);// 동
			} else {
				SET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Floor+1);	// 층
			}

// 2015.06.25 P형 ==> R형 10BYTE와 같게 수정
//			if(	ap_PAData->m_DfrData[w_jj].m_PRFlag == 0 ||				
//				ap_PAData->m_DfrData[w_jj].m_BFlag == 1) {						// P형, R형의 12BYTE 
			if(	ap_PAData->m_DfrData[w_jj].m_PRFlag == 1 &&				
				ap_PAData->m_DfrData[w_jj].m_BFlag == 1) {						// R형의 12BYTE 만 무조건 0
				w_Zone = 0;
			} else if(ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Zone == 0) {
				w_Zone = 0x20;
			} else if(ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Zone == 1) {
				w_Zone = 0x42;
			} else if(ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Zone == 2) {
				w_Zone = 0x50;
			} else if(ap_PAData->m_DfrData[w_jj].m_Item[w_ii].m_Pos.m_Zone == 3) {
				w_Zone = 0x4B;
			} else {
				w_Zone = 0;
			}
			SET_BYTE(ap_Rom1,  w_Cnt, w_Zone);														// 구역
			SET_BYTE(ap_Rom1,  w_Cnt, w_SSCnt);														// SS 총갯수

			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
				if(ap_PAData->m_DfrTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
//2015.06.15 
//#if defined(DEV_5000)
//					SET_BYTE(ap_Rom1,  w_Cnt, DEV_TYPE_DSS);										// DMT or DSS
//					SET_BYTE(ap_Rom1,  w_Cnt, w_kk+1);												// DMT or DSS
//2015.06.15 
//#else 

					if(w_kk >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
						w_DevType = DEV_TYPE_DSS;
						SET_BYTE(ap_Rom1,  w_Cnt, w_DevType);										// eType : DSS
						SET_BYTE(ap_Rom1,  w_Cnt, w_kk-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);			// ADDR
					} else {
//2015.06.15 
#if defined(DEV_5000)
						w_DevType = DEV_TYPE_DPA;
#else
						w_DevType = DEV_TYPE_DMT;
#endif
						SET_BYTE(ap_Rom1,  w_Cnt, w_DevType);										// eType :DMT 
						SET_BYTE(ap_Rom1,  w_Cnt, w_kk+1);											// ADDR
					}
//2015.06.15 
//#endif
					SET_WORD(ap_Rom1, w_Cnt, ap_PAData->m_DfrTerm[w_jj][w_ii].m_SSFlag[w_kk]);	// SS Flag
				}
			}
		}
	}

#if 0
	DL_LOG("ROM1:%d", w_Cnt);
	DL_LOG_HEXA((char *)ap_Rom1, LPA_ROM_MAX);
#endif

	return(w_Cnt);
}

int	 CDLLowPA::MakeRom2Data(LPA_DATA_T *ap_PAData, BYTE *ap_Rom2)
{
	int	w_ii;
	int	w_jj;
	int	w_kk;
	int w_Cnt = 0;
	int	w_SSTotCnt;
	int	w_SSCnt;

	BYTE w_DevType=0x0;
	int	w_TotMacroCnt;
	int	w_MacroCnt;

	memset(ap_Rom2, 0x00, LPA_ROM_MAX);

	w_TotMacroCnt = 0;

	// PCMACRO 총갯수
	w_SSTotCnt = ap_PAData->m_DevCnt[LPA_DMA_IDX] +  ap_PAData->m_DevCnt[LPA_DSS_IDX];

	// 실제 데이타가 설정되었는지 체크 ==> PC MACRO
	for(w_jj=0;w_jj<ap_PAData->m_PCMacroCnt;w_jj++) 
	{
		w_MacroCnt = 0;
		for(w_ii=0;w_ii<ap_PAData->m_PCMacroData[w_jj].m_Cnt;w_ii++) {
			w_SSCnt = 0;
			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
				if(ap_PAData->m_PCMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
					w_SSCnt++;
				}
			}
			if(w_SSCnt > 0) {
				w_MacroCnt++;
			}
		}
		w_TotMacroCnt += w_MacroCnt;
	}

#if defined(DEV_5000)
	// 실제 데이타가 설정되었는지 체크 ==> DMT MACRO
	for(w_jj=0;w_jj<ap_PAData->m_DMTMacroCnt;w_jj++) {
		w_MacroCnt = 0;
		for(w_ii=0;w_ii<ap_PAData->m_DMTMacroData[w_jj].m_Cnt;w_ii++) {
			w_SSCnt = 0;
			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
				if(ap_PAData->m_DMTMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
					w_SSCnt++;
				}
			}
			if(w_SSCnt > 0) {
				w_MacroCnt++;
			}
		}
		w_TotMacroCnt += w_MacroCnt;
	}
#endif

	// 실제 데이타가 설정되었는지 체크 ==> DRM MACRO
	for(w_jj=0;w_jj<LPA_MMACRO_MAX;w_jj++) 
	{
		w_MacroCnt = 0;
		for(w_ii=0;w_ii<ap_PAData->m_MMacroData[w_jj].m_Cnt;w_ii++) 
		{
			w_SSCnt = 0;
			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
				if(ap_PAData->m_MMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
					w_SSCnt++;
				}
			}
			if(w_SSCnt > 0) {
				w_MacroCnt++;
			}
		}
		w_TotMacroCnt += w_MacroCnt;
	}

	// 실제 데이타가 설정되었는지 체크 ==> TIMER MACRO
	for(w_jj=0;w_jj<LPA_TIMERMACRO_MAX;w_jj++) 
	{
		w_MacroCnt = 0;
		for(w_ii=0;w_ii<ap_PAData->m_TimerMacroData[w_jj].m_Cnt;w_ii++) 
		{
			w_SSCnt = 0;
			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) 
			{
				if(ap_PAData->m_TimerMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) 
				{
					w_SSCnt++;
				}
			}
			if(w_SSCnt > 0) {
				w_MacroCnt++;
			}
		}
		w_TotMacroCnt += w_MacroCnt;
	}

#if defined(DEV_5000)
	// 실제 데이타가 설정되었는지 체크 ==> DRM MACRO
	for(w_jj=0;w_jj<LPA_EMMACRO_MAX;w_jj++) 
	{
		w_MacroCnt = 0;
		for(w_ii=0;w_ii<ap_PAData->m_EmMacroData[w_jj].m_Cnt;w_ii++) {
			w_SSCnt = 0;
			for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
				if(ap_PAData->m_EmMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
					w_SSCnt++;
				}
			}
			if(w_SSCnt > 0) {
				w_MacroCnt++;
			}
		}
		w_TotMacroCnt += w_MacroCnt;
	}
#endif

	SET_WORD(ap_Rom2,  w_Cnt, w_TotMacroCnt);	
	for(w_jj=0;w_jj<ap_PAData->m_PCMacroCnt;w_jj++) 
	{
		if(ap_PAData->m_PCMacroData[w_jj].m_Cnt > 0) 
		{
			// 실제 데이타가 설정되었는지 체크
			w_SSCnt = 0;
			w_MacroCnt = 0;
			for(w_ii=0;w_ii<ap_PAData->m_PCMacroData[w_jj].m_Cnt;w_ii++) 
			{
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) 
				{
					if(ap_PAData->m_PCMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) 
					{
						w_SSCnt++;
					}
				}
				if(w_SSCnt > 0) {
					w_MacroCnt++;
				}
			}
			if(w_MacroCnt == 0) {
				continue;
			}

			SET_BYTE(ap_Rom2,  w_Cnt, LPA_PCMACRO_CODE);							// PCMACRO GROUP NUMBER
			SET_BYTE(ap_Rom2,  w_Cnt, w_MacroCnt);									// 해당 그룹 PCMACRO 갯수

			for(w_ii=0;w_ii<ap_PAData->m_PCMacroData[w_jj].m_Cnt;w_ii++) 
			{
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) 
				{
					if(ap_PAData->m_PCMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt == 0) {
					continue;
				}
				SET_BYTE(ap_Rom2,  w_Cnt, w_ii+1);									// PCMACRO NO
				SET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_PCMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
// 2016.06.15
//#if defined(DEV_5000)
//						SET_BYTE(ap_Rom2,  w_Cnt, DEV_TYPE_DSS);					// DMT or DSS
//						SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);							// DMT or DSS
//#else 
						if(w_kk >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
							w_DevType = DEV_TYPE_DSS;
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);										// eType : DSS
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);			// ADDR
						} else {
// 2016.06.15
#if defined(DEV_5000)
							w_DevType = DEV_TYPE_DPA;
#else
							w_DevType = DEV_TYPE_DMT;
#endif
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);										// eType : DMT	
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);											// ADDR
						}
//#endif
						SET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_PCMacroTerm[w_jj][w_ii].m_SSFlag[w_kk]);	// SSFlag
					}
				}
			}
		}
	}

#if defined(DEV_5000)
	for(w_jj=0;w_jj<ap_PAData->m_DMTMacroCnt;w_jj++) {
		if(ap_PAData->m_DMTMacroData[w_jj].m_Cnt > 0) {

			// 실제 데이타가 설정되었는지 체크
			w_SSCnt = 0;
			w_MacroCnt = 0;
			for(w_ii=0;w_ii<ap_PAData->m_DMTMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_DMTMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt > 0) {
					w_MacroCnt++;
				}
			}
			if(w_MacroCnt == 0) {
				continue;
			}

			SET_BYTE(ap_Rom2,  w_Cnt, LPA_DMTMACRO_CODE);							// DMTMACRO GROUP NUMBER
			SET_BYTE(ap_Rom2,  w_Cnt, w_MacroCnt);									// 해당 그룹 DMTMACRO 갯수

			for(w_ii=0;w_ii<ap_PAData->m_DMTMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_DMTMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt == 0) {
					continue;
				}
				SET_BYTE(ap_Rom2,  w_Cnt, w_ii+1);									// DMTMACRO NO
				SET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_DMTMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
// 2015.06.15
//#if defined(DEV_5000)
//						SET_BYTE(ap_Rom2,  w_Cnt, DEV_TYPE_DSS);					// DMT or DSS
//						SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);							// DMT or DSS
//#else 
						if(w_kk >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
							w_DevType = DEV_TYPE_DSS;
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);											// eType : DSS
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);				// ADDR
						} else {
#if defined(DEV_5000)
							w_DevType = DEV_TYPE_DPA;
#else
							w_DevType = DEV_TYPE_DMT;
#endif
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);											// eType : DMT	
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);												// ADDR
						}
//#endif
						SET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_DMTMacroTerm[w_jj][w_ii].m_SSFlag[w_kk]);	// SSFlag

					}
				}
			}
		}
	}
#endif

	for(w_jj=0;w_jj<LPA_MMACRO_MAX;w_jj++) {
		if(ap_PAData->m_MMacroData[w_jj].m_Cnt > 0) {
			
			// 실제 데이타가 설정되었는지 체크
			w_SSCnt = 0;
			w_MacroCnt = 0;
			for(w_ii=0;w_ii<ap_PAData->m_MMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_MMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt > 0) {
					w_MacroCnt++;
				}
			}
			if(w_MacroCnt == 0) {
				continue;
			}

			SET_BYTE(ap_Rom2,  w_Cnt, LPA_MMACRO_CODE+w_jj);					// PCMACRO GROUP NUMBER
			SET_BYTE(ap_Rom2,  w_Cnt, w_MacroCnt);								// 해당 그룹 PCMACRO 갯수

			for(w_ii=0;w_ii<ap_PAData->m_MMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_MMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt == 0) {
					continue;
				}
				SET_BYTE(ap_Rom2,  w_Cnt, w_ii+1);							// PCMACRO NO
				SET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);							// SS 총갯수
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if( ap_PAData->m_MMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
// 2015.06.15
//#if defined(DEV_5000)
//						SET_BYTE(ap_Rom2,  w_Cnt, DEV_TYPE_DSS);					// DMT or DSS
//						SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);							// DMT or DSS
//#else 
						if(w_kk >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
							w_DevType = DEV_TYPE_DSS;
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);											// eType : DSS
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);				// ADDR
						} else {
#if defined(DEV_5000)
							w_DevType = DEV_TYPE_DPA;
#else
							w_DevType = DEV_TYPE_DMT;
#endif
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);											// eType : DMT	
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);												// ADDR
						}
//#endif
						SET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_MMacroTerm[w_jj][w_ii].m_SSFlag[w_kk]);	// SSFlag
					}
				}
			}
		}
	}

	for(w_jj=0;w_jj<ap_PAData->m_TimerMacroCnt;w_jj++) {
		if(ap_PAData->m_TimerMacroData[w_jj].m_Cnt > 0) {

			// 실제 데이타가 설정되었는지 체크
			w_SSCnt = 0;
			w_MacroCnt = 0;
			for(w_ii=0;w_ii<ap_PAData->m_TimerMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_TimerMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt > 0) {
					w_MacroCnt++;
				}
			}
			if(w_MacroCnt == 0) {
				continue;
			}

			SET_BYTE(ap_Rom2,  w_Cnt, LPA_TIMERMACRO_CODE);							// TIMER GROUP NUMBER
			SET_BYTE(ap_Rom2,  w_Cnt, w_MacroCnt);									// 해당 그룹 PCMACRO 갯수

			for(w_ii=0;w_ii<ap_PAData->m_TimerMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_TimerMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt == 0) {
					continue;
				}
				SET_BYTE(ap_Rom2,  w_Cnt, w_ii+1);									// PCMACRO NO
				SET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_TimerMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
// 2015.06.15
//#if defined(DEV_5000)
//						SET_BYTE(ap_Rom2,  w_Cnt, DEV_TYPE_DSS);					// DMT or DSS
//						SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);							// DMT or DSS
//#else 
						if(w_kk >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
							w_DevType = DEV_TYPE_DSS;
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);											// eType : DSS
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);				// ADDR
						} else {
#if defined(DEV_5000)
							w_DevType = DEV_TYPE_DPA;
#else
							w_DevType = DEV_TYPE_DMT;
#endif
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);											// eType : DMT	
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);												// ADDR
						}
//#endif
						SET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_TimerMacroTerm[w_jj][w_ii].m_SSFlag[w_kk]);	// SSFlag
					}
				}
			}
		}
	}
#if defined(DEV_5000)

	for(w_jj=0;w_jj<ap_PAData->m_EmMacroCnt;w_jj++) {
		if(ap_PAData->m_EmMacroData[w_jj].m_Cnt > 0) {

			// 실제 데이타가 설정되었는지 체크
			w_SSCnt = 0;
			w_MacroCnt = 0;
			for(w_ii=0;w_ii<ap_PAData->m_EmMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_EmMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt > 0) {
					w_MacroCnt++;
				}
			}
			if(w_MacroCnt == 0) {
				continue;
			}

			SET_BYTE(ap_Rom2,  w_Cnt, LPA_EMMACRO_CODE);							// EM GROUP NUMBER
			SET_BYTE(ap_Rom2,  w_Cnt, w_MacroCnt);									// 해당 그룹 EM 갯수

			for(w_ii=0;w_ii<ap_PAData->m_EmMacroData[w_jj].m_Cnt;w_ii++) {
				w_SSCnt = 0;
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_EmMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
						w_SSCnt++;
					}
				}
				if(w_SSCnt == 0) {
					continue;
				}
				SET_BYTE(ap_Rom2,  w_Cnt, w_ii+1);									// PCMACRO NO
				SET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수
				for(w_kk = 0;w_kk<w_SSTotCnt;w_kk++) {
					if(ap_PAData->m_EmMacroTerm[w_jj][w_ii].m_SSFlag[w_kk] != 0) {
// 2015.06.15
//#if defined(DEV_5000)
//						SET_BYTE(ap_Rom2,  w_Cnt, DEV_TYPE_DSS);					// DMT or DSS
//						SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);							// DMT or DSS
//#else 
						if(w_kk >= ap_PAData->m_DevCnt[LPA_DMA_IDX]) {
							w_DevType = DEV_TYPE_DSS;
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);										// eType : DSS
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk-ap_PAData->m_DevCnt[LPA_DMA_IDX]+1);			// ADDR
						} else {
// 2015.06.15
#if defined(DEV_5000)
							w_DevType = DEV_TYPE_DPA;
#else
							w_DevType = DEV_TYPE_DMT;
#endif
							SET_BYTE(ap_Rom2,  w_Cnt, w_DevType);										// eType : DMT	
							SET_BYTE(ap_Rom2,  w_Cnt, w_kk+1);											// ADDR
						}
//#endif
						SET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_EmMacroTerm[w_jj][w_ii].m_SSFlag[w_kk]);	// SSFlag
					}
				}
			}
		}
	}
#endif

#if 0
	DL_LOG("ROM2 : %d", w_Cnt);
	DL_LOG_HEXA((char *)ap_Rom2, LPA_ROM_MAX);
#endif

	return(w_Cnt);
}

int CDLLowPA::MakeRomUpdateData(BYTE *ap_PAData, BYTE *ap_RomUpdate, int a_Len)
{
	int w_Cnt = 0;
	w_Cnt = a_Len;
	
	if(w_Cnt==0)
	{
		return(0);
	}
	memset(ap_RomUpdate, 0x00, LPA_UPDATE_ROM_MAX);
	memcpy(ap_RomUpdate,ap_PAData,w_Cnt);

	return(w_Cnt);
}


int	 CDLLowPA::ParseRomData(LPA_DATA_T *ap_PAData, BYTE *ap_Rom1, int a_Rom1Len, BYTE *ap_Rom2, int a_Rom2Len)
{
	int		w_ii;
	int		w_jj;
	int		w_kk;
	int		w_Cnt = 0;
	BYTE	w_Value=0x0;

	InitLPAData(ap_PAData);

	GET_DWORD(ap_Rom1,  w_Cnt, ap_PAData->m_DevCode);

	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);
	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);
#if defined(DEV_5000) 
	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);								// DMT
#else
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DMA_IDX]);	// DMT
#endif
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DSS_IDX]);	// DSS
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DPG_IDX]);	// DPG

	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);
	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DRM_IDX]);	// DRM

#if defined(DEV_5000) 
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DMA_IDX]);	// DPA
#else
	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);								// DPA
#endif

	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DMP_IDX]);	// DMP
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DRE_IDX]);	// DRE
	GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_DevCnt[LPA_DFR_IDX]);	// DFR
	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);								// 0
	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);								// 0
	GET_BYTE(ap_Rom1,  w_Cnt, w_Value);								// 1000 SERISE/5000시리즈

#if defined(DEV_5000)
	GET_WORD(ap_Rom1,  w_Cnt, ap_PAData->m_Gate);					// GATE 정보  2015.03.31
#endif

	if(ap_PAData->m_DevCnt[LPA_DMA_IDX] > LPA_DMA_MAX) {
		DL_LOG("LPA_DMA_MAX error");		// ERROR
		return(-1);
	}
	if(ap_PAData->m_DevCnt[LPA_DSS_IDX] > LPA_DSS_MAX) {
		DL_LOG("LPA_DSS_MAX error");		// ERROR
		return(-1);
	}
	if(ap_PAData->m_DevCnt[LPA_DPG_IDX] > LPA_DPG_MAX) {
		DL_LOG("LPA_DPG_MAX error");		// ERROR
		return(-1);
	}
	if(ap_PAData->m_DevCnt[LPA_DRM_IDX] > LPA_DRM_MAX) {
		DL_LOG("LPA_DRM_MAX error");		// ERROR
		return(-1);
	}
	if(ap_PAData->m_DevCnt[LPA_DMP_IDX] > LPA_DMP_MAX) {
		DL_LOG("LPA_DMP_MAX error");		// ERROR
		return(-1);
	}
	if(ap_PAData->m_DevCnt[LPA_DRE_IDX] > LPA_DRE_MAX) {
		DL_LOG("LPA_DRE_MAX error");		// ERROR
		return(-1);
	}
	if(ap_PAData->m_DevCnt[LPA_DFR_IDX] > LPA_DFR_MAX) {
		DL_LOG("LPA_DFR_MAX error");		// ERROR
		return(-1);
	}

	int		w_SSTotCnt;
	BYTE	w_DevType;
	BYTE	w_DpgCnt;
	BYTE	w_DpgAddr;

	w_SSTotCnt = ap_PAData->m_DevCnt[LPA_DMA_IDX] +  ap_PAData->m_DevCnt[LPA_DSS_IDX];
	for(w_jj=0;w_jj<w_SSTotCnt;w_jj++) {
		GET_BYTE(ap_Rom1,  w_Cnt, w_DevType);								// TYPE(DMT/DSS)
		GET_BYTE(ap_Rom1,  w_Cnt, w_DpgAddr);								
#if defined(DEV_5000)
		// 어떻게 체크해야 할지 모르겠슴... ==> 일단 오류체크 안함

#else
		if(     (w_DevType == DEV_TYPE_DMT && w_DpgAddr > ap_PAData->m_DevCnt[LPA_DMA_IDX])
			||  (w_DevType == DEV_TYPE_DSS && w_DpgAddr > ap_PAData->m_DevCnt[LPA_DSS_IDX])
			||  (w_DpgAddr < 1) ) {
			DL_LOG("DEV_TYPE_DMT/DEV_TYPE_DSS error");						// ERROR
			return(-1);
		}
#endif
		GET_BYTE(ap_Rom1,  w_Cnt, w_DpgCnt);								// Cnt	 
		if(w_DpgCnt > LPA_TERMINAL_ITEM) {
			DL_LOG("LPA_TERMINAL_ITEM error");								// ERROR
			return(-1);
		}

		for(w_ii=0;w_ii<w_DpgCnt;w_ii++) {
//#if defined(DEV_5000)
//			if(0) {	// 다 순서대로 읽어 넣으면 됨
//#else
			if(w_DevType == DEV_TYPE_DSS) {
//#endif
				GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_TermDpg[ap_PAData->m_DevCnt[LPA_DMA_IDX]+w_DpgAddr-1][w_ii].m_DpgAddr);		// DPG Addr
				GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_TermDpg[ap_PAData->m_DevCnt[LPA_DMA_IDX]+w_DpgAddr-1][w_ii].m_DpgOutput);	// DPG Output
			} else {
				GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_TermDpg[w_DpgAddr-1][w_ii].m_DpgAddr);		// DPG Addr
				GET_BYTE(ap_Rom1,  w_Cnt, ap_PAData->m_TermDpg[w_DpgAddr-1][w_ii].m_DpgOutput);		// DPG Output
			}
		}
	}



	BYTE	w_PRFlag;
	BYTE	w_DfrAddr;
	WORD	w_DfrCnt;
	BYTE	w_DfrItem;
	BYTE	w_SSCnt;
	BYTE	w_SSAddr;

	for(w_jj=0;w_jj<ap_PAData->m_DevCnt[LPA_DFR_IDX];w_jj++) {
		GET_BYTE(ap_Rom1,  w_Cnt, w_PRFlag);					// P or R
		GET_BYTE(ap_Rom1,  w_Cnt, w_DfrAddr);					// ADDRESS
		if(w_DfrAddr > ap_PAData->m_DevMax[LPA_DFR_IDX]) {
			DL_LOG("m_DevMax[LPA_DFR_IDX] error");				// ERROR
			return(-1);
		}
		if(w_DfrAddr == 0) {	// END...
			break;
		}
		if(w_PRFlag  == DEV_TYPE_DDE) {
			ap_PAData->m_DfrData[w_DfrAddr-1].m_PRFlag  = 1;
		} else {
			ap_PAData->m_DfrData[w_DfrAddr-1].m_PRFlag  = 0;
		}

		GET_WORD(ap_Rom1,  w_Cnt, w_DfrCnt);					// 화재 총 갯수
		if(w_DfrCnt > LPA_DFR_ITEM) {
			DL_LOG("LPA_DFR_ITEM1 error");						// ERROR
			return(-1);
		}
		for(w_ii=0;w_ii<w_DfrCnt;w_ii++) {
			GET_BYTE(ap_Rom1,  w_Cnt, w_DfrItem);				// 접점번호
			if(w_DfrItem > LPA_DFR_ITEM) {
				DL_LOG("LPA_DFR_ITEM2 error");					// ERROR
				return(-1);
			}
			if(ap_PAData->m_DfrData[w_DfrAddr-1].m_Cnt <= w_DfrItem) {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Cnt = w_DfrItem;
			}

			GET_BYTE(ap_Rom1,  w_Cnt, w_Value);	// 동
			if(w_Value <= 0) {					// 2016-04-14
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Building = 255;
			} else {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Building = w_Value -1;
			}
			GET_BYTE(ap_Rom1,  w_Cnt, w_Value);// 라인
			if(w_Value <= 0) {					// 2016-04-14
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Stair = 255;
			} else {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Stair = w_Value -1;
			}
			GET_BYTE(ap_Rom1,  w_Cnt, w_Value);// 층
			if(w_Value <= 0) {					// 2016-04-14
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Floor = 255;
			} else {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Floor = w_Value -1;
			}
			GET_BYTE(ap_Rom1,  w_Cnt, w_Value);// 층
			if(w_Value == 0x20) {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Zone = 0;
			} else if(w_Value == 0x42) {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Zone = 1;
			} else if(w_Value == 0x50) {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Zone = 2;
			} else if(w_Value == 0x4B) {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Zone = 3;
			} else {
				ap_PAData->m_DfrData[w_DfrAddr-1].m_Item[w_DfrItem-1].m_Pos.m_Zone = 0;
				if(ap_PAData->m_DfrData[w_DfrAddr-1].m_PRFlag != 0) {		// P형이 아니면 m_BFlag=1임
					ap_PAData->m_DfrData[w_DfrAddr-1].m_BFlag = 1;
				}
			}

			GET_BYTE(ap_Rom1,  w_Cnt, w_SSCnt);													// SS총갯수
			if(w_SSCnt > w_SSTotCnt) {
				DL_LOG("w_SSTotCnt 1 error");			// ERROR
				return(-1);
			}
			for(w_kk = 0;w_kk<w_SSCnt;w_kk++) {
				GET_BYTE(ap_Rom1,  w_Cnt, w_DevType);
				GET_BYTE(ap_Rom1,  w_Cnt, w_SSAddr);											// ADDR
				if(w_SSAddr < 1) {
					DL_LOG("LPA_DFR_ITEM3 error");		// ERROR
					return(-1);
				}
// 2015.06.15
//#if defined(DEV_5000)
//				if(0) {
//#else
				if(w_DevType == DEV_TYPE_DSS) {
//#endif
					GET_WORD(ap_Rom1, w_Cnt, ap_PAData->m_DfrTerm[w_DfrAddr-1][w_DfrItem-1].m_SSFlag[w_SSAddr+ap_PAData->m_DevCnt[LPA_DMA_IDX]-1]);	// SS Flag
				} else {
					GET_WORD(ap_Rom1, w_Cnt, ap_PAData->m_DfrTerm[w_DfrAddr-1][w_DfrItem-1].m_SSFlag[w_SSAddr-1]);	// SS Flag
				}
			}
		}
	}

	WORD	w_TotMacroNum;
	BYTE	w_MacroGroupNumber;
	BYTE	w_MacroGroupCnt;
	BYTE	w_MacroNumber;
	BYTE	w_MacroAddr;

	w_Cnt = 0;
	GET_WORD(ap_Rom2, w_Cnt, w_TotMacroNum);										// MACRO 총갯수
	for(w_jj=0;w_jj<w_TotMacroNum;w_jj++) {
		GET_BYTE(ap_Rom2,  w_Cnt, w_MacroGroupNumber);								// GROUP NUMBER
		GET_BYTE(ap_Rom2,  w_Cnt, w_MacroGroupCnt);									// 해당 그룹 PCMACRO 갯수
		
		for(w_ii=0;w_ii<w_MacroGroupCnt;w_ii++) {
			if(w_MacroGroupNumber == LPA_PCMACRO_CODE) {								// PC PCMACRO
				w_MacroAddr = 0;
				GET_BYTE(ap_Rom2,  w_Cnt, w_MacroNumber);								// PC MACRO ADDR

				if(w_MacroNumber < 1 || w_MacroNumber > LPA_PCMACRO_ITEM) {
					DL_LOG("LPA_PCMACRO_ITEM1 error");									// ERROR
					return(-1);
				}
				if(ap_PAData->m_PCMacroData[w_MacroAddr].m_Cnt < w_MacroNumber) {
					ap_PAData->m_PCMacroData[w_MacroAddr].m_Cnt = w_MacroNumber;
				}
				GET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수

				if(w_SSCnt > w_SSTotCnt) {
					DL_LOG("w_SSTotCnt 2 error");									// ERROR
					return(-1);
				}
				for(w_kk = 0;w_kk<w_SSCnt;w_kk++) {
					GET_BYTE(ap_Rom2,  w_Cnt, w_DevType);
					GET_BYTE(ap_Rom2,  w_Cnt, w_SSAddr);							// ADDR
					if(w_SSAddr < 1) {
						DL_LOG("LPA_PCMACRO_ITEM2 error");							// ERROR
						return(-1);
					}
// 2015.06.15
//#if defined(DEV_5000)
//					if(0) {
//#else
					if(w_DevType == DEV_TYPE_DSS) {
//#endif
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_PCMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr+ap_PAData->m_DevCnt[LPA_DMA_IDX]-1]);	// SS Flag
					} else {
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_PCMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr-1]);	// SS Flag
					}
				}
#if defined(DEV_5000)
			} else if(w_MacroGroupNumber == LPA_DMTMACRO_CODE) {						// DMT MACRO
				w_MacroAddr = 0;
				GET_BYTE(ap_Rom2,  w_Cnt, w_MacroNumber);								// DMT MACRO ADDR

				if(w_MacroNumber < 1 || w_MacroNumber > LPA_DMTMACRO_ITEM) {
					DL_LOG("LPA_PCMACRO_ITEM1 error");									// ERROR
					return(-1);
				}
				if(ap_PAData->m_DMTMacroData[w_MacroAddr].m_Cnt < w_MacroNumber) {
					ap_PAData->m_DMTMacroData[w_MacroAddr].m_Cnt = w_MacroNumber;
				}
				GET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수

				if(w_SSCnt > w_SSTotCnt) {
					DL_LOG("w_SSTotCnt 2 error");									// ERROR
					return(-1);
				}
				for(w_kk = 0;w_kk<w_SSCnt;w_kk++) {
					GET_BYTE(ap_Rom2,  w_Cnt, w_DevType);
					GET_BYTE(ap_Rom2,  w_Cnt, w_SSAddr);							// ADDR
					if(w_SSAddr < 1) {
						DL_LOG("LPA_PCMACRO_ITEM2 error");							// ERROR
						return(-1);
					}
// 2015.06.15
//#if defined(DEV_5000)
//					if(0) {
//#else
					if(w_DevType == DEV_TYPE_DSS) {
//#endif
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_DMTMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr+ap_PAData->m_DevCnt[LPA_DMA_IDX]-1]);	// SS Flag
					} else {
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_DMTMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr-1]);	// SS Flag
					}
				}
#endif
			} else if(w_MacroGroupNumber == LPA_TIMERMACRO_CODE) {						// TIMER MACRO
				w_MacroAddr = 0;
				GET_BYTE(ap_Rom2,  w_Cnt, w_MacroNumber);								// TIMER MACRO ADDR

				if(w_MacroNumber < 1 || w_MacroNumber > LPA_TIMERMACRO_ITEM) {
					DL_LOG("LPA_TIMERMACRO_ITEM1 error");									// ERROR
					return(-1);
				}
				if(ap_PAData->m_TimerMacroData[w_MacroAddr].m_Cnt < w_MacroNumber) {
					ap_PAData->m_TimerMacroData[w_MacroAddr].m_Cnt = w_MacroNumber;
				}
				GET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수

				if(w_SSCnt > w_SSTotCnt) {
					DL_LOG("w_SSTotCnt 2 error");									// ERROR
					return(-1);
				}
				for(w_kk = 0;w_kk<w_SSCnt;w_kk++) {
					GET_BYTE(ap_Rom2,  w_Cnt, w_DevType);
					GET_BYTE(ap_Rom2,  w_Cnt, w_SSAddr);							// ADDR
					if(w_SSAddr < 1) {
						DL_LOG("LPA_TIMERMACRO_ITEM2 error");							// ERROR
						return(-1);
					}
// 2015.06.15
//#if defined(DEV_5000)
//					if(0) {
//#else
					if(w_DevType == DEV_TYPE_DSS) {
//#endif
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_TimerMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr+ap_PAData->m_DevCnt[LPA_DMA_IDX]-1]);	// SS Flag
					} else {
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_TimerMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr-1]);	// SS Flag
					}
				}
#if defined(DEV_5000)
			} else if(w_MacroGroupNumber == LPA_EMMACRO_CODE) {							// EM MACRO
				w_MacroAddr = 0;
				GET_BYTE(ap_Rom2,  w_Cnt, w_MacroNumber);								// EM MACRO ADDR

				if(w_MacroNumber < 1 || w_MacroNumber > LPA_EMMACRO_ITEM) {
					DL_LOG("LPA_EMMACRO_ITEM1 error");									// ERROR
					return(-1);
				}
				if(ap_PAData->m_EmMacroData[w_MacroAddr].m_Cnt < w_MacroNumber) {
					ap_PAData->m_EmMacroData[w_MacroAddr].m_Cnt = w_MacroNumber;
				}
				GET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수

				if(w_SSCnt > w_SSTotCnt) {
					DL_LOG("w_SSTotCnt 2 error");									// ERROR
					return(-1);
				}
				for(w_kk = 0;w_kk<w_SSCnt;w_kk++) {
					GET_BYTE(ap_Rom2,  w_Cnt, w_DevType);
					GET_BYTE(ap_Rom2,  w_Cnt, w_SSAddr);							// ADDR
					if(w_SSAddr < 1) {
						DL_LOG("LPA_EMMACRO_ITEM2 error");							// ERROR
						return(-1);
					}
// 2015.06.15
//#if defined(DEV_5000)
//					if(0) {
//#else
					if(w_DevType == DEV_TYPE_DSS) {
//#endif
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_EmMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr+ap_PAData->m_DevCnt[LPA_DMA_IDX]-1]);	// SS Flag
					} else {
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_EmMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr-1]);	// SS Flag
					}
				}
#endif
			} else if(1 <= w_MacroGroupNumber && w_MacroGroupNumber <= 6) {	
				w_MacroAddr = w_MacroGroupNumber-1;
				GET_BYTE(ap_Rom2,  w_Cnt, w_MacroNumber);							// 해당 그룹 PCMACRO 갯수

				if(w_MacroNumber < 1 || w_MacroNumber > LPA_MMACRO_ITEM) {
					DL_LOG("LPA_PCMACRO_ITEM1 error");								// ERROR
					return(-1);
				}
				if(ap_PAData->m_MMacroData[w_MacroAddr].m_Cnt < w_MacroNumber) {
					ap_PAData->m_MMacroData[w_MacroAddr].m_Cnt = w_MacroNumber;
				}

				GET_BYTE(ap_Rom2,  w_Cnt, w_SSCnt);									// SS 총갯수

				if(w_SSCnt > w_SSTotCnt) {
					DL_LOG("w_SSTotCnt 2 error");									// ERROR
					return(-1);
				}
				for(w_kk = 0;w_kk<w_SSCnt;w_kk++) {
					GET_BYTE(ap_Rom2,  w_Cnt, w_DevType);
					GET_BYTE(ap_Rom2,  w_Cnt, w_SSAddr);							// ADDR
					if(w_SSAddr < 1) {
						DL_LOG("LPA_MMACRO_ITEM2 error");							// ERROR
						return(-1);
					}
// 2015.06.15
//#if defined(DEV_5000)
//					if(0) {
//#else
					if(w_DevType == DEV_TYPE_DSS) {
//#endif
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_MMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr+ap_PAData->m_DevCnt[LPA_DMA_IDX]-1]);	// SS Flag
					} else {
						GET_WORD(ap_Rom2, w_Cnt, ap_PAData->m_MMacroTerm[w_MacroAddr][w_MacroNumber-1].m_SSFlag[w_SSAddr-1]);	// SS Flag
					}
				}
			}
		}
	}
	return(w_Cnt);
}


void CDLLowPA::SendCmd(BYTE a_Cmd, BYTE a_PRM1, BYTE a_PRM2, BYTE a_PRM3, BYTE a_PRM4)
{
	int		w_Len;
	char	w_Buff[MAX_DATA_LEN];

	if(!mp_Device) {
		return;
	}

	w_Len = mp_Device->MakePacket(a_Cmd, NULL, 0, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4);

	if(w_Len > 0) {
		//DL_LOG("SendCmd");
		//DL_LOG_HEXA(w_Buff, w_Len);
		mp_Device->SendToQueue(w_Buff, w_Len);
	}
}

void CDLLowPA::SendUpdateCmd(BYTE a_Cmd, BYTE a_PRM1, BYTE a_PRM2, BYTE a_PRM3, BYTE a_PRM4, BYTE a_TYPE, BYTE a_ENO, BYTE* apVersion)
{
	int		w_Len;
	char	w_Buff[MAX_DATA_LEN];

	if(!mp_UpdateDevice) {
		return;
	}

	memset(&w_Buff,0x00,sizeof(char)*MAX_DATA_LEN);
	
	if(a_Cmd == CMD_UPDATE_KEEP || a_Cmd == CMD_UPDATE_CLEAR)
	{
		//업데이트 유지 명령은 w_Len = 13으로 고정
		w_Len = mp_UpdateDevice->MakeBootPacket(a_Cmd, NULL, 0, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4);
	}
	else if(a_Cmd == CMD_UPDATE_INFO)
	{
		w_Len = mp_UpdateDevice->MakeInfoPacket(a_Cmd, NULL, 0, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4, a_TYPE, a_ENO);
	}
	else if(a_Cmd == CMD_UPDATE_VERSION)
	{
		w_Len = mp_UpdateDevice->MakeVersionPacket(a_Cmd, apVersion, 4, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4, a_TYPE, a_ENO);
	}
	else
	{
		w_Len = mp_UpdateDevice->MakePacket(a_Cmd, NULL, 0, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4);
	}

	if(w_Len > 0) 
	{
		DL_LOG("SendUpdateCmd");
		DL_LOG_HEXA(w_Buff, w_Len);
		mp_UpdateDevice->SendToQueue(w_Buff, w_Len);
	}
}


void CDLLowPA::SendMixerCmd(BYTE a_Cmd, BYTE a_PRM1, BYTE a_PRM2, BYTE a_PRM3, BYTE a_PRM4, BYTE a_TYPE, BYTE a_ENO, BYTE* apVersion)
{
	int		w_Len;
	char	w_Buff[MAX_DATA_LEN];

	if(!mp_MixerDevice) {
		return;
	}

	memset(&w_Buff,0x00,sizeof(char)*MAX_DATA_LEN);

	if(a_Cmd == CMD_MIXER_KEEP || a_Cmd == CMD_MIXER_CLEAR)
	{
		//업데이트 유지 명령은 w_Len = 13으로 고정
		w_Len = mp_MixerDevice->MakeBootPacket(a_Cmd, NULL, 0, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4);
	}
	else if(a_Cmd == CMD_MIXER_INFO)
	{
		w_Len = mp_MixerDevice->MakeInfoPacket(a_Cmd, NULL, 0, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4, a_TYPE, a_ENO);
	}
	else if(a_Cmd == CMD_MIXER_VERSION)
	{
		w_Len = mp_MixerDevice->MakeVersionPacket(a_Cmd, apVersion, 4, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4, a_TYPE, a_ENO);
	}
	else
	{
		w_Len = mp_MixerDevice->MakePacket(a_Cmd, NULL, 0, w_Buff, MAX_DATA_LEN, a_PRM1, a_PRM2, a_PRM3, a_PRM4);
	}

	if(w_Len > 0) 
	{
		DL_LOG("SendMixerCmd");
		DL_LOG_HEXA(w_Buff, w_Len);
		mp_MixerDevice->SendToQueue(w_Buff, w_Len);
	}
}

int CDLLowPA::SendData(WORD a_Page)
{
	int				w_Len;
	unsigned char	w_TempBuff[MAX_DATA_LEN];
	unsigned char	w_Buff[MAX_DATA_LEN];
	BYTE			w_Prm2 = 0;

	if(!mp_Device || a_Page >= (m_Rom1Page + m_Rom2Page) ) {
		return(0);
	}

	if(a_Page < m_Rom1Page) {
		w_TempBuff[0] = LOW_WORD(a_Page+1);
		w_TempBuff[1] = HIGH_WORD(a_Page+1);

		w_TempBuff[2] = LOW_WORD((WORD)LPA_PACKET_LEN);
		w_TempBuff[3] = HIGH_WORD((WORD)LPA_PACKET_LEN);

		memcpy(&w_TempBuff[4], &m_Rom1[a_Page*LPA_PACKET_LEN], LPA_PACKET_LEN);
		w_Prm2 = 0;

	} else {
		w_TempBuff[0] = LOW_WORD(a_Page-m_Rom1Page+1);
		w_TempBuff[1] = HIGH_WORD(a_Page-m_Rom1Page+1);
		w_TempBuff[2] = LOW_WORD((WORD)LPA_PACKET_LEN);
		w_TempBuff[3] = HIGH_WORD((WORD)LPA_PACKET_LEN);

		memcpy(&w_TempBuff[4], &m_Rom2[(a_Page-m_Rom1Page)*LPA_PACKET_LEN], LPA_PACKET_LEN);
		w_Prm2 = 1;
	}
	w_Len = mp_Device->MakePacket(CMD_LOW_DATA, (char*)&w_TempBuff, LPA_PACKET_LEN+4, (char*)&w_Buff, MAX_DATA_LEN, PRM1_SEND, w_Prm2);	// 전송

	if(w_Len > 0) {
		DL_LOG("SendData : PAGE(%x)(%d) TOTPAGE(%d)", a_Page, a_Page, m_Rom1Page + m_Rom2Page);
		//DL_LOG_HEXA((char*)&w_Buff, w_Len);
		mp_Device->SendToQueue((char*)&w_Buff, w_Len);
	}

	return(w_Len);
}

int CDLLowPA::UpdateSetDevInfo(BYTE a_TYPE, BYTE a_ENO, BYTE a_PRM3)
{
	if(!mp_UpdateDevice) 
	{
		return(0);
	}

	mp_UpdateDevice->m_DstType = a_TYPE;
	mp_UpdateDevice->m_DstENo = a_ENO;
	mp_UpdateDevice->m_DstSubENo = a_PRM3;

	return(0);
}

int CDLLowPA::MixerSetDevInfo(BYTE a_TYPE, BYTE a_ENO, BYTE a_PRM3)
{
	if(!mp_MixerDevice) 
	{
		return(0);
	}

	mp_MixerDevice->m_DstType = a_TYPE;
	mp_MixerDevice->m_DstENo = a_ENO;
	mp_MixerDevice->m_DstSubENo = a_PRM3;

	return(0);
}

int CDLLowPA::SendMixerData(WORD a_Page)
{
	int				w_Len = 0;
	int				w_SendLen = 0;
	int				w_PRM1 = 0;
	int				w_PRM2 = 0;
	int				w_PRM3 = 0;
	int				w_PRM4 = 0;
	unsigned char	w_TempBuff[MAX_DATA_LEN];
	unsigned char	w_Buff[MAX_DATA_LEN];

	if(!mp_MixerDevice || a_Page >= m_RomUpdatePage ) 
	{
		return(0);
	}

	w_SendLen = LPA_PACKET_256LEN;
	memcpy(&w_TempBuff[0], &m_RomUpdate[a_Page*w_SendLen], w_SendLen);
	if(a_Page>255)
	{
		w_PRM2 = a_Page;//+1;
	}
	else
	{
		w_PRM1 = a_Page;//+1;
	}

	w_Len = mp_MixerDevice->MakeFlashPacket(CMD_MIXER_FLASH, (char*)&w_TempBuff, w_SendLen, (char*)&w_Buff, MAX_DATA_LEN, w_PRM1, w_PRM2, w_PRM3, w_PRM4);	// 전송
	if(w_Len > 0) 
	{
		DL_LOG("SendMixerData : PAGE(%x)(%d) TOTPAGE(%d)", a_Page, a_Page, m_RomUpdatePage);
		DL_LOG_HEXA((char*)&w_Buff, w_Len);
		mp_MixerDevice->SendToQueue((char*)&w_Buff, w_Len);
	}

	return(w_Len);
}

int CDLLowPA::SendUpdateData(int a_Page)
{
	int				w_Len = 0;
	int				w_SendLen = 0;
	int				w_PRM1 = 0;
	int				w_PRM2 = 0;
	int				w_PRM3 = 0;
	int				w_PRM4 = 0;
	unsigned char	w_TempBuff[MAX_DATA_LEN];
	unsigned char	w_Buff[MAX_DATA_LEN];

	if(!mp_UpdateDevice || a_Page >= m_RomUpdatePage ) 
	{
		return(0);
	}

	if(mp_UpdateDevice->m_DstType == EEQUIP_TYPE_DMP)
	{
		w_SendLen = LPA_PACKET_128LEN;
	}
	else
	{
		w_SendLen = LPA_PACKET_256LEN;
	}
	memcpy(&w_TempBuff[0], &m_RomUpdate[a_Page*w_SendLen], w_SendLen);
	if(a_Page>255)
	{
		w_PRM1 = a_Page&0xff;//+1;
		w_PRM2 = (a_Page>>8)&0x0fff;//+1;
	}
	else
	{
		w_PRM1 = a_Page&0xff;//+1;
	}
	if(mp_UpdateDevice->m_DstType == EEQUIP_TYPE_DRM)
	{
		w_PRM3 = (mp_UpdateDevice->m_DstSubENo<<1);
	}
	w_Len = mp_UpdateDevice->MakeFlashPacket(CMD_UPDATE_FLASH, (char*)&w_TempBuff, w_SendLen, (char*)&w_Buff, MAX_DATA_LEN, w_PRM1, w_PRM2, w_PRM3, w_PRM4);	// 전송
	if(w_Len > 0) 
	{
		DL_LOG("SendUpdateData : PAGE(%x)(%d) TOTPAGE(%d)", a_Page, a_Page, m_RomUpdatePage);
		//DL_LOG_HEXA((char*)&w_Buff, w_Len);
		mp_UpdateDevice->SendToQueue((char*)&w_Buff, w_Len);
	}

	return(w_Len);
}


int CDLLowPA::ParseRecvData(char *ap_Buff, int a_Len)
{
	WORD		w_Page;
	BYTE		w_Prm1;
	BYTE		w_Prm2;
	BYTE		w_Prm3;
	BYTE		w_Prm4;

	WORD		w_DLen;
	BYTE		w_Data[MAX_DATA_LEN];
	WORD		w_Len;


	w_Prm1 = GET_PRM1(ap_Buff);
	w_Prm2 = GET_PRM2(ap_Buff);
	w_Prm3 = GET_PRM3(ap_Buff);
	w_Prm4 = GET_PRM4(ap_Buff);
	w_DLen = GET_DLEN(ap_Buff);
	if(w_DLen < 4) {
		return(0);
	}

	if(w_DLen <= MAX_DATA_LEN && w_DLen > 0) {
		memcpy(w_Data, GET_DATA(ap_Buff), w_DLen);
	}

	w_Page = MAKE_WORD(w_Data);
	w_Len = MAKE_WORD(&w_Data[2]);
//	DL_LOG("ParseRecvData : PAGE(%d):Len(%d)", w_Page, w_Len);

	if(w_Len > LPA_PACKET_LEN || w_Page > LPA_PACKET_NUM) {
		return(0);
	}

	if(w_Prm2 == 0) {
		memcpy(&m_RecvRom1[LPA_PACKET_LEN * (w_Page-1)], &w_Data[4], w_Len);
		m_RecvRom1Len = LPA_PACKET_LEN * w_Page;
	} else {
		memcpy(&m_RecvRom2[LPA_PACKET_LEN * (w_Page-1)], &w_Data[4], w_Len);
		m_RecvRom2Len = LPA_PACKET_LEN * w_Page;
	}

	return(w_Page);
}



int CDLLowPA::SerialRecv(void *ap_Void, char *ap_Buff, int a_Len)
{
	CDLLowPA	*wp_LowPA;
	CLPADevice	*wp_Device;

//	BYTE		w_DstEType;		// EType of Destination Device
//	BYTE		w_DstENo;		// ENo of Destination Device
//	BYTE		w_SrcEType;		// EType of Source Device
//	BYTE		w_SrcENo;		// ENo of Source Device

	WORD		w_PID;

	BYTE		w_Prm1;
	BYTE		w_Prm2;
	BYTE		w_Prm3;
	BYTE		w_Prm4;

	WORD		w_DLen;
	BYTE		w_Data[MAX_DATA_LEN];

	memset(w_Data, 0x00, MAX_DATA_LEN);
	wp_Device = (CLPADevice *)ap_Void;
	if(wp_Device) {
		wp_LowPA = (CDLLowPA *)wp_Device->GetParent();
		if(wp_LowPA) {
			wp_LowPA->Lock();
//DL_LOG("SerialRecv");
//DL_LOG_HEXA(ap_Buff, a_Len);

			w_PID = GET_PID(ap_Buff);

			w_Prm1 = GET_PRM1(ap_Buff);
			w_Prm2 = GET_PRM2(ap_Buff);
			w_Prm3 = GET_PRM3(ap_Buff);
			w_Prm4 = GET_PRM4(ap_Buff);

			w_DLen = GET_DLEN(ap_Buff);
			if(w_DLen <= MAX_DATA_LEN && w_DLen > 0) {
				memcpy(w_Data, GET_DATA(ap_Buff), w_DLen);
			}


			switch(w_PID) {
				case CMD_LOW_POLL				:								// Poll : 101	
//DL_LOG("CMD_LOW_POLL-m_DevStat(%d)", wp_LowPA->m_DevStat);
//DL_LOG_HEXA(ap_Buff, a_Len);

					if(wp_LowPA->m_DevStat == DEV_STAT_NORMAL) {				
						wp_Device->m_DstSetENo = GET_SRC_SETNO(ap_Buff);
						wp_Device->m_DstType = GET_SRC_ETYPE(ap_Buff);
						wp_Device->m_DstENo = GET_SRC_ENO(ap_Buff);

						wp_Device->m_SrcSetENo = GET_DST_SETNO(ap_Buff);
						wp_Device->m_SrcType = GET_DST_ETYPE(ap_Buff);
						wp_Device->m_SrcENo = GET_DST_ENO(ap_Buff);

						wp_LowPA->SendCmd(CMD_LOW_POLL);
					} else if(wp_LowPA->m_DevStat == DEV_STAT_SEND) {		// SEND REQUEST

						if(wp_LowPA->GetMainHwnd()) {
							::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendPage);
						}
//DL_LOG("DEV_STAT_SEND : %d", wp_LowPA->m_SendPage);

						wp_LowPA->SendData(wp_LowPA->m_SendPage);

					} else if(wp_LowPA->m_DevStat == DEV_STAT_RECV) {		// RECV REQUEST

//DL_LOG("DEV_STAT_RECV : %d", wp_LowPA->m_RecvPage);
						if(wp_LowPA->GetMainHwnd()) {
							::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_RecvPage);
						}
						wp_LowPA->SendCmd(CMD_LOW_DATA, PRM1_REQUEST);
					}	
					break;
				case CMD_LOW_DATA				:								// Data : 103
//DL_LOG("CMD_LOW_DATA:m_DevStat(%d)", wp_LowPA->m_DevStat);
//DL_LOG_HEXA(ap_Buff, a_Len);
					if(wp_LowPA->m_DevStat == DEV_STAT_SEND && w_Prm1 == PRM1_ACK) {				// SEND
//DL_LOG("DEV_STAT_SEND : PRM1(%d): PRM2(%d) PRM4(%d), PAGE(%d)", w_Prm1, w_Prm2, w_Prm4, wp_LowPA->m_SendPage);


						if(w_Prm4 == 1) {	// 재전송요구 ==> 페이지 조정
							wp_LowPA->m_SendPage = MAKE_WORD(w_Data);
							if(wp_LowPA->m_SendPage > 0) {
								wp_LowPA->m_SendPage -= 1;
							}
							if(w_Prm2 == 1) {
								wp_LowPA->m_SendPage += wp_LowPA->m_Rom1Page;
							}
						} else {
							wp_LowPA->m_SendPage++;
						}

						if(wp_LowPA->m_SendPage >= (wp_LowPA->m_Rom1Page + wp_LowPA->m_Rom2Page) || (wp_LowPA->m_SendPage < 0)) {

							wp_LowPA->m_SendPage = 0;
							wp_LowPA->m_DevStat = DEV_STAT_NORMAL;
							if(wp_LowPA->GetMainHwnd()) {
								::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_END, EV_SEND_OK);		// 송신 OK
							}
						} else {														// Data : 103
							if(wp_LowPA->GetMainHwnd()) {
								::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendPage);	
							}
						}
					} else if(wp_LowPA->m_DevStat == DEV_STAT_RECV && w_Prm1 == PRM1_SEND) {		// RECV

//DL_LOG("DEV_STAT_RECV : PRM1(%d): PRM2(%d) PRM4(%d), PAGE(%d)", w_Prm1, w_Prm2, w_Prm4, wp_LowPA->m_RecvPage);

							int w_Rtn = wp_LowPA->ParseRecvData(ap_Buff, a_Len);
//DL_LOG("DEV_STAT_RECV : wp_LowPA->ParseRecvData: %d", w_Rtn);
							if(w_Rtn >= 0) {
								wp_LowPA->m_RecvPage = w_Rtn;
								wp_LowPA->SendCmd(CMD_LOW_DATA, PRM1_ACK, w_Prm2);					// ACK 전송

								if(wp_LowPA->GetMainHwnd()) {
									::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_RecvPage);
								}
							} else {	// ERROR ==> 해당페이지를 재요청할지 아니면 ERROR처리할지(일단 무조건 에러처리함) 
								wp_LowPA->m_RecvPage = 0;
								wp_LowPA->m_DevStat = DEV_STAT_NORMAL;
	
								if(wp_LowPA->GetMainHwnd()) {
									::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS,  EV_PROGRESS_END, EV_RECV_ERROR);	// 수신 에러
								}

							}

					} else if(wp_LowPA->m_DevStat == DEV_STAT_RECV && w_Prm1 == PRM1_END) {				// RECV-END
						wp_LowPA->m_RecvPage = 0;
						wp_LowPA->m_DevStat = DEV_STAT_NORMAL;
						if(wp_LowPA->GetMainHwnd()) {
							::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS,  EV_PROGRESS_END, EV_RECV_OK);			// 수신 OK
						}
						// 

						memset(&wp_LowPA->m_PAData, 0x00, sizeof(LPA_DATA_T));				
						int w_Rtn = wp_LowPA->ParseRomData(&wp_LowPA->m_PAData, wp_LowPA->m_RecvRom1, wp_LowPA->m_RecvRom1Len, wp_LowPA->m_RecvRom2, wp_LowPA->m_RecvRom2Len);
						if(w_Rtn < 0) {
							wp_LowPA->InitLPAData(&wp_LowPA->m_PAData);
						}
						wp_LowPA->Invalidate();

					}

					break;
			}

			wp_LowPA->UnLock();
		}
	}
	return(1);
}

int CDLLowPA::SerialCallback(void *ap_Void, unsigned long a_msg, unsigned long a_param)
{

	CDevice		*wp_Device;

	wp_Device = (CDevice *)ap_Void;
	if(wp_Device) {
		if(a_msg == CB_STATUS_CHANGED) 
		{
			if(GetEtcWnd()) {
				::PostMessage(GetEtcWnd()->GetSafeHwnd(), WM_CHANGEDSTATUS, a_param, 0);
			}
		}
	}
	return(1);
}

int CDLLowPA::UpdateSerialRecv(void *ap_Void, char *ap_Buff, int a_Len)
{
	CDLLowPA		*wp_LowPA;
	CLUpdateDevice	*wp_Device;

	WORD		w_PID;

	BYTE		w_EType=0;
	BYTE		w_ENo=0;
	BYTE		w_DpgNo=0;
	BYTE		w_ENoIndex=0;
	BYTE		w_SendNum=0;
	BYTE		w_DevMax=0;
	int			w_CheckDreCnt=0;
	int			w_CheckDrmCnt=0;
	int			w_CheckDmpCnt=0;
	int			w_CheckDssCnt=0;
	int			w_CheckDfrCnt=0;
	int			w_CheckDrmMax=0;

	//일괄업그레이드시, 사용자가 선택한 장치의 갯수 체크변수
	int			w_CheckDmt=0;
	int			w_CheckDpg=0;
	int			w_CheckDre=0;
	int			w_CheckDrm=0;
	int			w_CheckDpa=0;
	int			w_CheckDmp=0;
	int			w_CheckDss=0;
	int			w_CheckDfr=0;

	BYTE		w_Prm1;
	BYTE		w_Prm2;
	BYTE		w_Prm3;
	BYTE		w_Prm4;

	WORD		w_DLen;
	BYTE		w_Data[MAX_DATA_LEN];
	BYTE		w_Version[MAX_DATA_LEN];

	int			w_ii=0;
	int			w_jj=0;

	memset(w_Data, 0x00, MAX_DATA_LEN);
	memset(w_Version, 0x00, MAX_DATA_LEN);
	wp_Device = (CLUpdateDevice *)ap_Void;
	if(wp_Device) 
	{
		wp_LowPA = (CDLLowPA *)wp_Device->GetParent();
		if(wp_LowPA) 
		{
			wp_LowPA->Lock();
			DL_LOG("[SerialRecv: LEN:%d]",a_Len);
			//DL_LOG_HEXA(ap_Buff, a_Len);

			if(a_Len == LPA_UPD_CMD_LEN)
			{
				w_PID = GET_UPD_PID(ap_Buff);

				w_Prm1 = GET_UPD_PRM1(ap_Buff);
				w_Prm2 = GET_UPD_PRM2(ap_Buff);
				w_Prm3 = GET_UPD_PRM3(ap_Buff);
				w_Prm4 = GET_UPD_PRM4(ap_Buff);

				w_DLen = GET_UPD_DLEN(ap_Buff);
				if(w_DLen <= MAX_DATA_LEN && w_DLen > 0) 
				{
					memcpy(w_Data, GET_UPD_DATA(ap_Buff), w_DLen);
				}
			}
			else
			{
				w_PID = GET_RST_PID(ap_Buff);

				w_EType = GET_RST_ETYPE(ap_Buff);
				w_ENo = GET_RST_ENO(ap_Buff);

				w_Prm1 = GET_RST_PRM1(ap_Buff);
				w_Prm2 = GET_RST_PRM2(ap_Buff);
				w_Prm3 = GET_RST_PRM3(ap_Buff);
				w_Prm4 = GET_RST_PRM4(ap_Buff);

				w_DLen = GET_RST_DLEN(ap_Buff);
				if(w_DLen <= MAX_DATA_LEN && w_DLen > 0) 
				{
					memcpy(w_Data, GET_RST_DATA(ap_Buff), w_DLen);
				}
			}

			switch(w_PID) 
			{

				case CMD_UPDATE_RESET:											// Reset : 11
					DL_LOG("[RECERVE RESET]")
					break;
				case CMD_UPDATE_FLASH:											// Flash Write : 20
					DL_LOG("[RECERVE FLASH]")
					if(wp_LowPA->m_UpdateDevStat == DEV_STAT_SEND) 
					{
						//FLASH전송타이머 클리어
						GetUpdateWnd()->KillTimer(WM_TIMER_SENDFLASHCMD);

						DL_LOG("DEV_STAT_SEND : PRM1(%d): PRM2(%d) PRM3(%d) PRM4(%d), PAGE(%d)", w_Prm1, w_Prm2, w_Prm3, w_Prm4, wp_LowPA->m_SendUpdatePage);
						if( !(w_Prm3 & 0x1) ) {							// 재전송요구 ==> 페이지 조정
							wp_LowPA->m_SendUpdatePage = MAKE_WORD(w_Data);
							if(wp_LowPA->m_SendUpdatePage > 0) {
								wp_LowPA->m_SendUpdatePage -= 1;
								if(GetUpdateWnd()) 
								{
									GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
								}
							}
						} else {
							wp_LowPA->m_SendUpdatePage++;
							if(GetUpdateWnd()) 
							{
								GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
							}
						}

						w_DpgNo = (BYTE)(w_Prm3 >> 1);
						if( (wp_LowPA->m_SendUpdatePage >= (wp_LowPA->m_RomUpdatePage) ) || (wp_LowPA->m_SendUpdatePage < 0)) 
						{
							wp_LowPA->m_SendUpdatePage = 0;
							wp_LowPA->m_UpdateDevStat = DEV_STAT_NORMAL;
							if(GetUpdateWnd()) 
							{
								GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
								::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, wp_LowPA->m_SendUpdatePage);
							}

							w_EType = GetUpdateWnd()->stSendCurrent.nEquipType;
							w_ENo = GetUpdateWnd()->stSendCurrent.nEquipNo;
							memcpy(w_Version, GetUpdateWnd()->stSendCurrent.nVersion,sizeof(char)*4);

							GetLowPA()->SendUpdateCmd(CMD_UPDATE_VERSION,0x0,0x0,(w_DpgNo<<1),0x0,w_EType,w_ENo, w_Version);
						} 
						else 
						{												// FLASH Data : 20
							if(GetUpdateWnd()) 
							{
								GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nReTry = 1;
								GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
								::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdatePage);
							}
							wp_LowPA->SendUpdateData(wp_LowPA->m_SendUpdatePage);
							//FLASH전송타이머 설정
							GetUpdateWnd()->SetTimer(WM_TIMER_SENDFLASHCMD,UPDATE_INFO_TIMEVALUE,NULL);
						}
					}
					break;
				case CMD_UPDATE_VERSION:
					if( !(w_Prm3 & 0x1) )		//정보조회 실패
					{
						DL_LOG("[EQUIP VERSION ERROR]")
						break;
					}
					else
					{
						w_EType = GetUpdateWnd()->stSendCurrent.nEquipType;
						w_ENo = GetUpdateWnd()->stSendCurrent.nEquipNo;
						memcpy(w_Version,GetUpdateWnd()->stSendCurrent.nVersion,sizeof(char)*4);

						w_DpgNo = (BYTE)(w_Prm3 >> 1);
						if(w_EType == EEQUIP_TYPE_DRM)
						{
							w_ENoIndex = w_ENo+(w_Prm3>>1)*9;
						} else {
							if(w_ENo>0)
							{
								w_ENoIndex = w_ENo - 1;
							}else{
								w_ENoIndex = w_ENo;
							}
						}

						switch(w_EType)
						{
							case EEQUIP_TYPE_DMT:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DSS:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DPG:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DRM:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DPA:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DMP:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DRE:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DFR:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
							case EEQUIP_TYPE_DMX:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
								wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_SUCESS;
								break;
						}

						//일괄업데이트일경우
						if(GetUpdateWnd()->m_bBatchAll)
						{
							for(w_ii=0;w_ii<GetUpdateWnd()->m_nBatchAllCount;w_ii++)
							{
								if( GetUpdateWnd()->stAllSendCurrent[w_ii].nEquipType == w_EType &&
									GetUpdateWnd()->stAllSendCurrent[w_ii].nEquipNo == w_ENo &&
									GetUpdateWnd()->stAllSendCurrent[w_ii].nDpgNo == w_DpgNo )
								{
									GetUpdateWnd()->stAllSendCurrent[w_ii].nSendOk = TRUE;
									wp_LowPA->m_SendUpdateData++;
									GetUpdateWnd()->m_nSendCur++;
									break;
								}
							}

							if( GetLowPA()->m_SendUpdateData >= GetUpdateWnd()->m_nBatchAllCount )
							{
								wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
								wp_LowPA->m_SendUpdateData = 0;
								PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_DATA, DEV_UPDATE_END, 0);
							}
							else
							{
								if(GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nEquipType == EEQUIP_TYPE_DRM)
								{
									w_SendNum = GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nEquipNo+(GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nDpgNo)*9;
								}else{
									w_SendNum = GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nEquipNo;
								}
								GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nReTry++;
								::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_DATA, GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nEquipType, w_SendNum);
							}
						} 
						else 
						{
							//개별 업그레이드시
							wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
							wp_LowPA->m_SendUpdateData = 0;
							//GetUpdateWnd()->KillTimer(WM_TIMER_SENDCMD);
							PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_DATA, DEV_UPDATE_END, 0);
						}

						//
						if(GetUpdateWnd()) 
						{
							if(GetUpdateWnd()->m_pEquipInfoDlg)
							{
								::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_DATA, 0, 0);
							}
						}
					}
					break;
				case CMD_UPDATE_INFO:												// 정보조회 : 14
					DL_LOG("[RECERVE INFO] DATA[%d]",sizeof(ST_EQUIP_INFO));

					//개별 정보조회 일경우
					if(w_EType == EEQUIP_TYPE_DRM)
					{
						w_ENoIndex = w_ENo+(w_Prm3>>1)*9;
					} else {
						if(w_ENo>0)
						{
							w_ENoIndex = w_ENo - 1;
						}else{
							w_ENoIndex = w_ENo;
						}
					}

					if( !(w_Prm3 & 0x1) )		//정보조회 실패
					{
						DL_LOG("[EQUIP INFO ERROR]");

						switch(w_EType)
						{
							case EEQUIP_TYPE_DMT:
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DSS:
								wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DPG:
								wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DRM:
								wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DPA:
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DMP:
								wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DRE:
								wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DFR:
								wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
							case EEQUIP_TYPE_DMX:
								wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nSetNo = 1;
								wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].eEquipType = w_EType;
								wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
								break;
						}

						if(GetUpdateWnd()) 
						{
							if(GetUpdateWnd()->m_pEquipInfoDlg)
							{
								::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
							}
						}
						if(wp_LowPA->m_UpdateDevStat == DEV_STAT_NEXT)
						{
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DMT)
							{
								DL_LOG("[DMT ALL EQUIPINFO]");
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DmtCnt;w_ii++)
								{
									if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType && 
										GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == w_ENo)
									{
										if(wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
										{
											GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult = TRUE;
										}
										wp_LowPA->m_SendUpdateInfo++;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo>0)
								{
									wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPG;
								}
							}

							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DPG)
							{
								DL_LOG("[DPG ALL EQUIPINFO]");
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DpgCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo == w_ENo)
										{
											if(wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
											{
												GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].bResult = TRUE;
											}
											wp_LowPA->m_SendUpdateInfo++;
											break;
										}else{
											w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo;
											break;
										}
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=  GetUpdateWnd()->m_DmtCnt + GetUpdateWnd()->m_DpgCnt)
								{
									if(GetUpdateWnd()->m_DreCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DRE;
										w_CheckDpg = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DPG);
										//w_CheckDre = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DRE);
										if(w_CheckDpg>0)
										{
											GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][0].bUseFlag = TRUE;
										}
									}
									else
									{
										if(GetUpdateWnd()->m_DrmCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DRM;
										}else{
											if(GetUpdateWnd()->m_DpaCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPA;
											}else{
												if(GetUpdateWnd()->m_DmpCnt>0)
												{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
												}else{
													if(GetUpdateWnd()->m_DssCnt>0)
													{
														wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
													}else{
														if(GetUpdateWnd()->m_DfrCnt>0)
														{
															wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
														}else{
															wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
															wp_LowPA->m_SendUpdateInfo = 0;
															::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
															::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
														}
													}
												}
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DpgCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DPG, w_SendNum);
									}
								}
							}

							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DRE)
							{
								DL_LOG("[DRE ALL EQUIPINFO]");
								w_DevMax = 7;
								for(w_ii=0;w_ii<w_DevMax;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bError = TRUE;
											if(wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
											{
												GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult = TRUE;
											}
											//GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = FALSE;
											//wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo == w_ENo)
												{
													GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bError = TRUE;
													if(wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
													{
														GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult = TRUE;
													}
													//GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = FALSE;
													//wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDreCnt++;
								}

								if(w_DevMax == w_CheckDreCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo < GetUpdateWnd()->m_DmtCnt+
																	GetUpdateWnd()->m_DpgCnt+
																	GetUpdateWnd()->m_DreCnt )
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+GetUpdateWnd()->m_DreCnt;
									}
								}

								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt )
								{
									if(GetUpdateWnd()->m_DrmCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DRM;
										//DRM을 찾기위한 구조체 플래그 계산
										//초기 DRE/DPG의 번호가 0인경우는 제외
										w_CheckDpg = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DPG);
										w_CheckDre = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DRE);
										if(w_CheckDpg>0 || w_CheckDre>0)
										{
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][0].bUseFlag = TRUE;

											for(w_ii=0;w_ii<7;w_ii++)
											{
												w_CheckDrmMax = w_ii*9 + 9;
												if(GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult)
												{
													if(!GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bError)
													{
														GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii*9].bUseFlag = TRUE;
													}else{
														for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
														{
															GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
														}
													}
												} else {
													for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
													{
														GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
													}
												}
											}
										} else {
											for(w_ii=0;w_ii<7;w_ii++)
											{
												w_CheckDrmMax = w_ii*9 + 9;
												for(w_jj=w_ii*9;w_jj<w_CheckDrmMax;w_jj++)
												{
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
												}
											}
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][0].bUseFlag = FALSE;
										}
									}else{
										if(GetUpdateWnd()->m_DpaCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPA;
										}else{
											if(GetUpdateWnd()->m_DmpCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
											}else{
												if(GetUpdateWnd()->m_DssCnt>0)
												{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
												}else{
													if(GetUpdateWnd()->m_DfrCnt>0)
													{
														wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
													}else{
														wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
														wp_LowPA->m_SendUpdateInfo = 0;
														::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
														::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
													}
												}
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DreCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DRE, w_SendNum);
									}
								}
							}

							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DRM)
							{
								DL_LOG("[DRM ALL EQUIPINFO]");
								w_DevMax = 63;
								for(w_ii=0;w_ii<w_DevMax;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo == w_ENo &&
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nDpgNo == (w_Prm3>>1) )
										{
											if(wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
											{
												GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bResult = TRUE;
											}
											//GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = FALSE;
											//wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo == w_ENo &&
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nDpgNo == (w_Prm3>>1) )
												{
													if(wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
													{
														GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bResult = TRUE;
													}
													//GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = FALSE;
													//wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = w_ii;//GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDrmCnt++;
								}

								if(w_CheckDrmCnt == w_DevMax)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt )
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt;
									}
								}

								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt )
								{
									if(GetUpdateWnd()->m_DpaCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPA;
									}else{
										if(GetUpdateWnd()->m_DmpCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
										}else{
											if(GetUpdateWnd()->m_DssCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
											}else{
												if(GetUpdateWnd()->m_DfrCnt>0)
												{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
												}else{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
													wp_LowPA->m_SendUpdateInfo = 0;
													::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
													::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
												}
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DrmCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DRM, w_SendNum);
									}
								}
							}

							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DPA)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DpaCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = FALSE;
											if(wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
											{
												GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult = TRUE;
											}
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == w_ENo)
												{
													if(wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
													{
														GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult = TRUE;
													}
													GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt )
								{
									if(GetUpdateWnd()->m_DmpCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
									}else{
										if(GetUpdateWnd()->m_DssCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
										}else{
											if(GetUpdateWnd()->m_DfrCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
											}else{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
												wp_LowPA->m_SendUpdateInfo = 0;
												::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
												::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DpaCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DPA, w_SendNum);
									}
								}
							}

							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DMP)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DmpCnt;w_ii++)
								{
									if( !GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bResult )
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = FALSE;
											if(wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
											{
												GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bResult = TRUE;
											}
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo == w_ENo)
												{
													if(wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
													{
														GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bResult = TRUE;
													}
													GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDmpCnt++;
								}

								if(w_CheckDrmCnt == GetUpdateWnd()->m_DmpCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt )
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+
																	 GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt+
																	 GetUpdateWnd()->m_DpaCnt+GetUpdateWnd()->m_DmpCnt;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt )
								{
									if(GetUpdateWnd()->m_DssCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
									}else{
										if(GetUpdateWnd()->m_DfrCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
										}else{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
											wp_LowPA->m_SendUpdateInfo = 0;
											::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
											::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DmpCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DMP, w_SendNum);
									}
								}
							}

							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DSS)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DssCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bResult )
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo == w_ENo)
										{
											//GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = FALSE;
											if(wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
											{
												GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bResult = TRUE;
											}
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo == w_ENo)
												{
													if(wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
													{
														GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bResult = TRUE;
													}
													//GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDssCnt++;
								}
								if(w_CheckDssCnt == GetUpdateWnd()->m_DssCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt )
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+
																	GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt+
																	GetUpdateWnd()->m_DpaCnt+GetUpdateWnd()->m_DmpCnt+
																	GetUpdateWnd()->m_DssCnt;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt )
								{
									if(GetUpdateWnd()->m_DfrCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
									}else{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
										wp_LowPA->m_SendUpdateInfo = 0;
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DssCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DSS, w_SendNum);
									}
								}
							}

							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DFR)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DfrCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bResult )
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = FALSE;
											if(wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
											{
												GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bResult = TRUE;
											}
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo == w_ENo)
												{
													if(wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] != LPA_INFO_FAILED)
													{
														GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bResult = TRUE;
													}
													GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDfrCnt++;
								}
								if(w_CheckDfrCnt == GetUpdateWnd()->m_DfrCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt +
																	GetUpdateWnd()->m_DfrCnt)
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+
																	GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt+
																	GetUpdateWnd()->m_DpaCnt+GetUpdateWnd()->m_DmpCnt+
																	GetUpdateWnd()->m_DssCnt+GetUpdateWnd()->m_DfrCnt;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt +
																	GetUpdateWnd()->m_DfrCnt)
								{
									wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
									wp_LowPA->m_SendUpdateInfo = 0;
									::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
									::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DfrCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DFR, w_SendNum);
									}
								}
							}
						}
						else
						{
							//개별조회인 경우
							wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
							wp_LowPA->m_SendUpdateInfo = 0;
							if(GetUpdateWnd()->GetSafeHwnd())
							{
								if(GetUpdateWnd()) 
								{
									::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, wp_LowPA->m_SendUpdateInfo);
								}

								GetUpdateWnd()->KillTimer(WM_TIMER_SENDCMD);
								::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
							}
						}

						break;
					}
					else
					{
						switch(w_EType)
						{
							case EEQUIP_TYPE_DMT:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DSS:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DPG:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nEquipNo = w_ENo;
								wp_LowPA->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DRM:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DPA:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DMP:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DRE:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DRE_IDX][w_ENo].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DFR:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
							case EEQUIP_TYPE_DMX:
								memcpy(&wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
								wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
								break;
						}

						if(GetUpdateWnd()) 
						{
							if(GetUpdateWnd()->m_pEquipInfoDlg)
							{
								::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
							}
						}

						if(wp_LowPA->m_UpdateDevStat == DEV_STAT_NEXT)
						{
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DMT)
							{
								DL_LOG("[DMT ALL OK EQUIPINFO]");
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DmtCnt;w_ii++)
								{
									if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType && 
										GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == w_ENo)
									{
										GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult = TRUE;
										wp_LowPA->m_SendUpdateInfo++;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo>0)
								{
									wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPG;
								}
							}
							
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DPG)
							{
								DL_LOG("[DPG ALL OK EQUIPINFO]");
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DpgCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].bResult = TRUE;
											GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].bUseFlag = FALSE;
											wp_LowPA->m_SendUpdateInfo++;
											break;
										}else{
											GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].bUseFlag = TRUE;
											w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo;
											break;
										}
									}
								}

								if(wp_LowPA->m_SendUpdateInfo >=  GetUpdateWnd()->m_DmtCnt + GetUpdateWnd()->m_DpgCnt)
								{
									if(GetUpdateWnd()->m_DreCnt>0)
									{
										w_CheckDreCnt = 0;
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DRE;
										w_CheckDpg = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DPG);
										if(w_CheckDpg>0)
										{
											GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][0].bUseFlag = TRUE;
										}
									}
									else
									{
										if(GetUpdateWnd()->m_DrmCnt>0)
										{
											w_CheckDrmCnt = 0;
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DRM;
											//DRM을 찾기위한 구조체 플래그 계산
											//초기 DRE/DPG의 번호가 0인경우는 제외
											w_CheckDpg = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DPG);
											w_CheckDre = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DRE);
											if(w_CheckDpg>0 || w_CheckDre>0)
											{
												GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][0].bUseFlag = TRUE;

												for(w_ii=0;w_ii<7;w_ii++)
												{
													w_CheckDrmMax = w_ii*9 + 9;
													if(GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult)
													{
														if(!GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bError)
														{
															GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii*9].bUseFlag = TRUE;
														}else{
															for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
															{
																GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
															}
														}
													} 
													else 
													{
														for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
														{
															GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
														}
													}
												}
											}
											else
											{
												for(w_ii=1;w_ii<63;w_ii++)
												{
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = TRUE;
												}
											}

										}else{
											if(GetUpdateWnd()->m_DpaCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPA;
											}else{
												if(GetUpdateWnd()->m_DmpCnt>0)
												{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
												}else{
													if(GetUpdateWnd()->m_DssCnt>0)
													{
														wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
													}else{
														if(GetUpdateWnd()->m_DfrCnt>0)
														{
															wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
														}else{
															wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
															wp_LowPA->m_SendUpdateInfo = 0;
															::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
															::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
														}
													}
												}
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DpgCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DPG, w_SendNum);
									}
								}
							}
							
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DRE)
							{
								DL_LOG("[DRE ALL OK EQUIPINFO]");
								w_DevMax = 7;
								for(w_ii=0;w_ii<w_DevMax;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult = TRUE;
											GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = FALSE;
											wp_LowPA->m_SendUpdateInfo++;
										}
										else
										{
											if(GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo == w_ENo)
												{
													GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult = TRUE;
													GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												//if( GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == w_EType )
												//{
												GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = TRUE;
												//}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDreCnt++;
								}

								if(w_DevMax == w_CheckDreCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo < GetUpdateWnd()->m_DmtCnt+
																	GetUpdateWnd()->m_DpgCnt+
																	GetUpdateWnd()->m_DreCnt )
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+GetUpdateWnd()->m_DreCnt;
									}
								}

								if(wp_LowPA->m_SendUpdateInfo >=  GetUpdateWnd()->m_DmtCnt + 
																  GetUpdateWnd()->m_DpgCnt + 
																  GetUpdateWnd()->m_DreCnt )
								{
									if(GetUpdateWnd()->m_DrmCnt>0)
									{
										w_CheckDrmCnt = 0;
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DRM;
										//DRM을 찾기위한 구조체 플래그 계산
										//초기 DRE/DPG의 번호가 0인경우는 제외
										w_CheckDpg = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DPG);
										w_CheckDre = GetUpdateWnd()->GetCheckEquipCount(EEQUIP_TYPE_DRE);
										if(w_CheckDpg>0 || w_CheckDre>0)
										{
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][0].bUseFlag = TRUE;

											for(w_ii=0;w_ii<7;w_ii++)
											{
												w_CheckDrmMax = w_ii*9 + 9;
												if(GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bResult)
												{
													if(!GetUpdateWnd()->stBatchAll[LPA_DRE_IDX][w_ii].bError)
													{
														GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii*9].bUseFlag = TRUE;
													}else{
														for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
														{
															GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
														}
													}
												} 
												else 
												{
													for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
													{
														GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
													}
												}
											}
										}else{
											for(w_ii=1;w_ii<63;w_ii++)
											{
												GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = TRUE;
											}
										}

									}else{
										if(GetUpdateWnd()->m_DpaCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPA;
										}else{
											if(GetUpdateWnd()->m_DmpCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
											}else{
												if(GetUpdateWnd()->m_DssCnt>0)
												{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
												}else{
													if(GetUpdateWnd()->m_DfrCnt>0)
													{
														wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
													}else{
														wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
														wp_LowPA->m_SendUpdateInfo = 0;
														::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
														::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
													}
												}
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DreCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DRE, w_SendNum);
									}
								}
							}
							
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DRM)
							{
								DL_LOG("[DRM ALL EQUIPINFO]");
								w_DevMax = 63;
								for(w_ii=0;w_ii<w_DevMax;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo == w_ENo &&
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nDpgNo == (w_Prm3>>1) )
										{
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bResult = TRUE;
											GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = FALSE;
											wp_LowPA->m_SendUpdateInfo++;
										}
										else
										{
											if(GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo == w_ENo &&
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nDpgNo == (w_Prm3>>1) )
												{
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bResult = TRUE;
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = w_ii;//GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDrmCnt++;
								}

								if(w_CheckDrmCnt == w_DevMax)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt )
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt;
									}
								}

								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt )
								{
									if(GetUpdateWnd()->m_DpaCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DPA;
									}else{
										if(GetUpdateWnd()->m_DmpCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
										}else{
											if(GetUpdateWnd()->m_DssCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
											}else{
												if(GetUpdateWnd()->m_DfrCnt>0)
												{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
												}else{
													wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
													wp_LowPA->m_SendUpdateInfo = 0;
													::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
													::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
												}
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DrmCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DRM, w_SendNum);
									}
								}
							}
							
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DPA)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DpaCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult)
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = FALSE;
											GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult = TRUE;
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == w_ENo)
												{
													GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bResult = TRUE;
													GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt )
								{
									if(GetUpdateWnd()->m_DmpCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DMP;
									}else{
										if(GetUpdateWnd()->m_DssCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
										}else{
											if(GetUpdateWnd()->m_DfrCnt>0)
											{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
											}else{
												wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
												wp_LowPA->m_SendUpdateInfo = 0;
												::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
												::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
											}
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DpaCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DPA, w_SendNum);
									}
								}
							}
							
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DMP)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DmpCnt;w_ii++)
								{
									if( !GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bResult )
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = FALSE;
											GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bResult = TRUE;
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo == w_ENo)
												{
													GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bResult = TRUE;
													GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDmpCnt++;
								}
								if(w_CheckDmpCnt == GetUpdateWnd()->m_DmpCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt )
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+
																	GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt+
																	GetUpdateWnd()->m_DpaCnt+GetUpdateWnd()->m_DmpCnt;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt )
								{
									if(GetUpdateWnd()->m_DssCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DSS;
									}else{
										if(GetUpdateWnd()->m_DfrCnt>0)
										{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
										}else{
											wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
											wp_LowPA->m_SendUpdateInfo = 0;
											::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
											::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
										}
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DmpCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DMP, w_SendNum);
									}
								}
							}
							
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DSS)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DssCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bResult )
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = FALSE;
											GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bResult = TRUE;
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo == w_ENo)
												{
													GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bResult = TRUE;
													GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDssCnt++;
								}
								if(w_CheckDssCnt == GetUpdateWnd()->m_DssCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt)
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+
																	GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt+
																	GetUpdateWnd()->m_DpaCnt+GetUpdateWnd()->m_DmpCnt+
																	GetUpdateWnd()->m_DssCnt;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt )
								{
									if(GetUpdateWnd()->m_DfrCnt>0)
									{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_DFR;
									}else{
										wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
										wp_LowPA->m_SendUpdateInfo = 0;
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
									}
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DssCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DSS, w_SendNum);
									}
								}
							}
							
							if(wp_LowPA->m_UpdateFlag == DEV_UPDATE_DFR)
							{
								for(w_ii=0;w_ii<GetUpdateWnd()->m_DfrCnt;w_ii++)
								{
									if(!GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bResult )
									{
										if( GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == w_EType && 
											GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo == w_ENo)
										{
											GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = FALSE;
											GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bResult = TRUE;
											wp_LowPA->m_SendUpdateInfo++;
										}else{
											if(GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag)
											{
												if( GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == w_EType && 
													GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo == w_ENo)
												{
													GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bResult = TRUE;
													GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = FALSE;
													wp_LowPA->m_SendUpdateInfo++;
												}
											} else {
												if( GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == w_EType )
												{
													GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = TRUE;
												}
												w_SendNum = GetUpdateWnd()->stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo;
												break;
											}
										}
									}
									w_CheckDfrCnt++;
								}
								if(w_CheckDfrCnt == GetUpdateWnd()->m_DfrCnt)
								{
									if(wp_LowPA->m_SendUpdateInfo <	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt +
																	GetUpdateWnd()->m_DfrCnt)
									{
										wp_LowPA->m_SendUpdateInfo = GetUpdateWnd()->m_DmtCnt+GetUpdateWnd()->m_DpgCnt+
																	GetUpdateWnd()->m_DreCnt+GetUpdateWnd()->m_DrmCnt+
																	GetUpdateWnd()->m_DpaCnt+GetUpdateWnd()->m_DmpCnt+
																	GetUpdateWnd()->m_DssCnt+GetUpdateWnd()->m_DfrCnt;
									}
								}
								if(wp_LowPA->m_SendUpdateInfo >=	GetUpdateWnd()->m_DmtCnt + 
																	GetUpdateWnd()->m_DpgCnt + 
																	GetUpdateWnd()->m_DreCnt +
																	GetUpdateWnd()->m_DrmCnt +
																	GetUpdateWnd()->m_DpaCnt +
																	GetUpdateWnd()->m_DmpCnt +
																	GetUpdateWnd()->m_DssCnt +
																	GetUpdateWnd()->m_DfrCnt)
								{
									wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
									wp_LowPA->m_SendUpdateInfo = 0;
									::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, 0);
									::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
								}
								else
								{
									if(GetUpdateWnd()) 
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdateInfo);
									}

									if(GetUpdateWnd()->m_DfrCnt>0)
									{
										::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, EEQUIP_TYPE_DFR, w_SendNum);
									}
								}
							}
						}
						else
						{
							//개별조회인 경우
							wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
							wp_LowPA->m_SendUpdateInfo = 0;
							if(GetUpdateWnd()->GetSafeHwnd())
							{
								if(GetUpdateWnd()) 
								{
									::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, wp_LowPA->m_SendUpdateInfo);
								}

								GetUpdateWnd()->KillTimer(WM_TIMER_SENDCMD);
								::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
							}
						}
					}
					break;
				case CMD_UPDATE_KEEP:											// Update Mode Keep : 15
					DL_LOG("[RECERVE KEEP]")
					GetLowPA()->SendUpdateCmd(CMD_UPDATE_KEEP,0x1);
					break;
				case CMD_LOW_POLL				:								// Poll : 101	
					//DL_LOG("CMD_LOW_POLL-m_UpdateDevStat(%d)", wp_LowPA->m_UpdateDevStat);
					//DL_LOG_HEXA(ap_Buff, a_Len);

					if(wp_LowPA->m_UpdateDevStat == DEV_STAT_NORMAL) {				
						wp_Device->m_DstSetENo = GET_SRC_SETNO(ap_Buff);
						wp_Device->m_DstType = GET_SRC_ETYPE(ap_Buff);
						wp_Device->m_DstENo = GET_SRC_ENO(ap_Buff);

						wp_Device->m_SrcSetENo = GET_DST_SETNO(ap_Buff);
						wp_Device->m_SrcType = GET_DST_ETYPE(ap_Buff);
						wp_Device->m_SrcENo = GET_DST_ENO(ap_Buff);

						//임시
						//wp_LowPA->SendCmd(CMD_LOW_POLL);

					} else if(wp_LowPA->m_UpdateDevStat == DEV_STAT_SEND) {		// SEND REQUEST

						if(wp_LowPA->GetMainHwnd()) {
							::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdatePage);
						}
						DL_LOG("DEV_STAT_SEND : %d", wp_LowPA->m_SendUpdatePage);

						wp_LowPA->SendData(wp_LowPA->m_SendUpdatePage);

					} else if(wp_LowPA->m_UpdateDevStat == DEV_STAT_RECV) {		// RECV REQUEST

						DL_LOG("DEV_STAT_RECV : %d", wp_LowPA->m_RecvUpdatePage);
						if(wp_LowPA->GetMainHwnd()) {
							::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_RecvUpdatePage);
						}
						wp_LowPA->SendCmd(CMD_LOW_DATA, PRM1_REQUEST);
					}	
					break;
				case CMD_LOW_DATA				:								// Data : 103
					//DL_LOG("CMD_LOW_DATA:m_DevStat(%d)", wp_LowPA->m_DevStat);
					//DL_LOG_HEXA(ap_Buff, a_Len);
					if(wp_LowPA->m_UpdateDevStat == DEV_STAT_SEND && w_Prm1 == PRM1_ACK) {				// SEND
						//DL_LOG("DEV_STAT_SEND : PRM1(%d): PRM2(%d) PRM4(%d), PAGE(%d)", w_Prm1, w_Prm2, w_Prm4, wp_LowPA->m_SendPage);

						if(w_Prm4 == 1) {	// 재전송요구 ==> 페이지 조정
							wp_LowPA->m_SendPage = MAKE_WORD(w_Data);
							if(wp_LowPA->m_SendPage > 0) {
								wp_LowPA->m_SendPage -= 1;
							}
							if(w_Prm2 == 1) {
								wp_LowPA->m_SendPage += wp_LowPA->m_Rom1Page;
							}
						} else {
							wp_LowPA->m_SendPage++;
						}

						if(wp_LowPA->m_SendPage >= (wp_LowPA->m_Rom1Page + wp_LowPA->m_Rom2Page) || (wp_LowPA->m_SendPage < 0)) {

							wp_LowPA->m_SendPage = 0;
							wp_LowPA->m_DevStat = DEV_STAT_NORMAL;
							if(wp_LowPA->GetMainHwnd()) {
								::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_END, EV_SEND_OK);		// 송신 OK
							}
						} else {														// Data : 103
							if(wp_LowPA->GetMainHwnd()) {
								::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendPage);	
							}
						}
					} else if(wp_LowPA->m_UpdateDevStat == DEV_STAT_RECV && w_Prm1 == PRM1_SEND) {		// RECV
						//DL_LOG("DEV_STAT_RECV : PRM1(%d): PRM2(%d) PRM4(%d), PAGE(%d)", w_Prm1, w_Prm2, w_Prm4, wp_LowPA->m_RecvPage);
						int w_Rtn = wp_LowPA->ParseRecvData(ap_Buff, a_Len);
						//DL_LOG("DEV_STAT_RECV : wp_LowPA->ParseRecvData: %d", w_Rtn);
						if(w_Rtn >= 0) {
							wp_LowPA->m_RecvPage = w_Rtn;
							wp_LowPA->SendCmd(CMD_LOW_DATA, PRM1_ACK, w_Prm2);					// ACK 전송

							if(wp_LowPA->GetMainHwnd()) {
								::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_RecvPage);
							}
						} else {	// ERROR ==> 해당페이지를 재요청할지 아니면 ERROR처리할지(일단 무조건 에러처리함) 
							wp_LowPA->m_RecvPage = 0;
							wp_LowPA->m_DevStat = DEV_STAT_NORMAL;

							if(wp_LowPA->GetMainHwnd()) {
								::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS,  EV_PROGRESS_END, EV_RECV_ERROR);	// 수신 에러
							}
						}
					} else if(wp_LowPA->m_UpdateDevStat == DEV_STAT_RECV && w_Prm1 == PRM1_END) {				// RECV-END
						wp_LowPA->m_RecvPage = 0;
						wp_LowPA->m_DevStat = DEV_STAT_NORMAL;
						if(wp_LowPA->GetMainHwnd()) {
							::PostMessage(wp_LowPA->GetMainHwnd(), WM_PROGRESS,  EV_PROGRESS_END, EV_RECV_OK);			// 수신 OK
						}
						// 

						memset(&wp_LowPA->m_PAData, 0x00, sizeof(LPA_DATA_T));				
						int w_Rtn = wp_LowPA->ParseRomData(&wp_LowPA->m_PAData, wp_LowPA->m_RecvRom1, wp_LowPA->m_RecvRom1Len, wp_LowPA->m_RecvRom2, wp_LowPA->m_RecvRom2Len);
						if(w_Rtn < 0) {
							wp_LowPA->InitLPAData(&wp_LowPA->m_PAData);
						}
						wp_LowPA->Invalidate();
					}
					break;
			}

			wp_LowPA->UnLock();
		}
	}
	return(1);
}

int CDLLowPA::UpdateSerialCallback(void *ap_Void, unsigned long a_msg, unsigned long a_param)
{
	CDevice		*wp_Device;

	wp_Device = (CDevice *)ap_Void;
	if(wp_Device) 
	{
		if(a_msg == CB_STATUS_CHANGED) 
		{
			if(GetUpdateWnd()) 
			{
				::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_CHANGEDSTATUS, a_param, 0);
			}
		}
	}
	return(1);
}

int CDLLowPA::MixerSerialRecv(void *ap_Void, char *ap_Buff, int a_Len)
{
	CDLLowPA		*wp_LowPA;
	CLMixerDevice	*wp_Device;

	//	BYTE		w_DstEType;		// EType of Destination Device
	//	BYTE		w_DstENo;		// ENo of Destination Device
	//	BYTE		w_SrcEType;		// EType of Source Device
	//	BYTE		w_SrcENo;		// ENo of Source Device

	WORD		w_PID;

	BYTE		w_EType=0;
	BYTE		w_ENo=0;
	BYTE		w_DpgNo=0;
	BYTE		w_ENoIndex=0;
	BYTE		w_SendNum=0;
	BYTE		w_DevMax=0;
	int			w_CheckDreCnt=0;
	int			w_CheckDrmMax=0;

	//일괄업그레이드시, 사용자가 선택한 장치의 갯수 체크변수
	int			w_CheckDmt=0;
	int			w_CheckDpg=0;
	int			w_CheckDre=0;
	int			w_CheckDrm=0;
	int			w_CheckDpa=0;
	int			w_CheckDmp=0;
	int			w_CheckDss=0;
	int			w_CheckDfr=0;

	BYTE		w_Prm1;
	BYTE		w_Prm2;
	BYTE		w_Prm3;
	BYTE		w_Prm4;

	WORD		w_DLen;
	BYTE		w_Data[MAX_DATA_LEN];
	BYTE		w_Version[MAX_DATA_LEN];

	int			w_ii=0;
	int			w_jj=0;

	memset(w_Data, 0x00, MAX_DATA_LEN);
	memset(w_Version, 0x00, MAX_DATA_LEN);
	wp_Device = (CLMixerDevice *)ap_Void;
	if(wp_Device) 
	{
		wp_LowPA = (CDLLowPA *)wp_Device->GetParent();
		if(wp_LowPA) 
		{
			wp_LowPA->Lock();
			DL_LOG("[MixerSerialRecv: LEN:%d]",a_Len);
			DL_LOG_HEXA(ap_Buff, a_Len);

			if(a_Len == LPA_MIX_CMD_LEN)
			{
				w_PID = GET_MIX_PID(ap_Buff);

				w_Prm1 = GET_MIX_PRM1(ap_Buff);
				w_Prm2 = GET_MIX_PRM2(ap_Buff);
				w_Prm3 = GET_MIX_PRM3(ap_Buff);
				w_Prm4 = GET_MIX_PRM4(ap_Buff);

				w_DLen = GET_MIX_DLEN(ap_Buff);
				if(w_DLen <= MAX_DATA_LEN && w_DLen > 0) 
				{
					memcpy(w_Data, GET_MIX_DATA(ap_Buff), w_DLen);
				}
			}
			else
			{
				w_PID = GET_MIX_RST_PID(ap_Buff);

				w_EType = GET_MIX_RST_ETYPE(ap_Buff);
				w_ENo = GET_MIX_RST_ENO(ap_Buff);

				w_Prm1 = GET_MIX_RST_PRM1(ap_Buff);
				w_Prm2 = GET_MIX_RST_PRM2(ap_Buff);
				w_Prm3 = GET_MIX_RST_PRM3(ap_Buff);
				w_Prm4 = GET_MIX_RST_PRM4(ap_Buff);

				w_DLen = GET_MIX_RST_DLEN(ap_Buff);
				if(w_DLen <= MAX_DATA_LEN && w_DLen > 0) 
				{
					memcpy(w_Data, GET_MIX_RST_DATA(ap_Buff), w_DLen);
				}
			}

			switch(w_PID) 
			{

			case CMD_MIXER_RESET:											// Reset : 11
				DL_LOG("[RECERVE RESET]")
				break;
			case CMD_MIXER_FLASH:											// Flash Write : 20
				DL_LOG("[RECERVE FLASH]")
					if(wp_LowPA->m_UpdateDevStat == DEV_STAT_SEND) 
					{
						DL_LOG("DEV_STAT_SEND : PRM1(%d): PRM2(%d) PRM3(%d) PRM4(%d), PAGE(%d)", w_Prm1, w_Prm2, w_Prm3, w_Prm4, wp_LowPA->m_SendUpdatePage);
						if( !(w_Prm3 & 0x1) ) {							// 재전송요구 ==> 페이지 조정
							wp_LowPA->m_SendUpdatePage = MAKE_WORD(w_Data);
							if(wp_LowPA->m_SendUpdatePage > 0) {
								wp_LowPA->m_SendUpdatePage -= 1;
								if(GetUpdateWnd()) 
								{
									GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
								}
							}
						} else {
							wp_LowPA->m_SendUpdatePage++;
							if(GetUpdateWnd()) 
							{
								GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
							}
						}

						w_DpgNo = (BYTE)(w_Prm3 >> 1);
						if( (wp_LowPA->m_SendUpdatePage >= (wp_LowPA->m_RomUpdatePage) ) || (wp_LowPA->m_SendUpdatePage < 0)) 
						{
							wp_LowPA->m_SendUpdatePage = 0;
							wp_LowPA->m_UpdateDevStat = DEV_STAT_NORMAL;

							if(GetUpdateWnd()) 
							{
								GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
								::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, wp_LowPA->m_SendUpdatePage);		// 송신 OK
							}

							w_EType = GetUpdateWnd()->stSendCurrent.nEquipType;
							w_ENo = GetUpdateWnd()->stSendCurrent.nEquipNo;
							memcpy(w_Version, GetUpdateWnd()->stSendCurrent.nVersion,sizeof(char)*4);

							GetLowPA()->SendMixerCmd(CMD_UPDATE_VERSION,0x0,0x0,(w_DpgNo<<1),0x0,w_EType,w_ENo, w_Version);
						} 
						else 
						{												// FLASH Data : 20
							if(GetUpdateWnd()) 
							{
								GetUpdateWnd()->stAllSendCurrent[GetUpdateWnd()->m_nSendCur].nReTry = 1;
								GetUpdateWnd()->stSendCurrent.nSendPage = wp_LowPA->m_SendUpdatePage;
								::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_POS, wp_LowPA->m_SendUpdatePage);
							}
							wp_LowPA->SendMixerData(wp_LowPA->m_SendUpdatePage);
							//FLASH전송타이머 설정
							GetUpdateWnd()->SetTimer(WM_TIMER_SENDFLASHCMD,UPDATE_INFO_TIMEVALUE,NULL);
						}
					}
					break;
			case CMD_MIXER_VERSION:
				if( !(w_Prm3 & 0x1) )		//정보조회 실패
				{
					DL_LOG("[EQUIP VERSION ERROR]")
					break;
				}
				else
				{
					w_EType = GetUpdateWnd()->stSendCurrent.nEquipType;
					w_ENo = GetUpdateWnd()->stSendCurrent.nEquipNo;
					memcpy(w_Version,GetUpdateWnd()->stSendCurrent.nVersion,sizeof(char)*4);

					w_DpgNo = (BYTE)(w_Prm3 >> 1);
					if(w_ENo>0)
					{
						w_ENoIndex = w_ENo - 1;
					}else{
						w_ENoIndex = w_ENo;
					}

					switch(w_EType)
					{
						case EEQUIP_TYPE_DMX:
							memcpy(&wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nApplVer,w_Version,sizeof(char)*4);
							wp_LowPA->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] = LPA_DATA_SUCESS;
							break;
					}

					//개별 업그레이드시
					wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
					wp_LowPA->m_SendUpdateData = 0;
					PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_DATA, DEV_UPDATE_END, 0);

					//
					if(GetUpdateWnd()) 
					{
						if(GetUpdateWnd()->m_pEquipInfoDlg)
						{
							::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_DATA, 0, 0);
						}
					}
				}
				break;
			case CMD_MIXER_INFO:												// 정보조회 : 14
				DL_LOG("[RECERVE INFO] DATA[%d]",sizeof(ST_EQUIP_INFO));

				if( !(w_Prm3 & 0x1) )		//정보조회 실패
				{
					DL_LOG("[EQUIP INFO ERROR]")
					break;
				}
				else
				{
					//개별 정보조회 일경우
					if(w_ENo>0)
					{
						w_ENoIndex = w_ENo - 1;
					}else{
						w_ENoIndex = w_ENo;
					}

					switch(w_EType)
					{
						case EEQUIP_TYPE_DMX:
							memcpy(&wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex],(ST_EQUIP_INFO*)&w_Data,sizeof(ST_EQUIP_INFO));
							wp_LowPA->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_SUCESS;
							break;
					}

					if(GetUpdateWnd()) 
					{
						if(GetUpdateWnd()->m_pEquipInfoDlg)
						{
							::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
						}
					}

					//개별조회인 경우
					wp_LowPA->m_UpdateFlag = DEV_UPDATE_END;
					wp_LowPA->m_SendUpdateInfo = 0;
					if(GetUpdateWnd()->GetSafeHwnd())
					{
						if(GetUpdateWnd()) 
						{
							::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_PROGRESS, EV_PROGRESS_END, wp_LowPA->m_SendUpdateInfo);
						}

						GetUpdateWnd()->stBatchAll[LPA_DMX_IDX][0].bUseFlag = FALSE;
						GetUpdateWnd()->stBatchAll[LPA_DMX_IDX][0].bResult = TRUE;

						GetUpdateWnd()->KillTimer(WM_TIMER_SENDCMD);
						::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_UPDATE_INFO, DEV_UPDATE_END, 0);
					}

				}
				break;
			case CMD_MIXER_KEEP:											// Update Mode Keep : 15
				DL_LOG("[RECERVE KEEP]")
				GetLowPA()->SendMixerCmd(CMD_MIXER_KEEP,0x1);
				break;
			}

			wp_LowPA->UnLock();
		}
	}
	return(1);
}

int CDLLowPA::MixerSerialCallback(void *ap_Void, unsigned long a_msg, unsigned long a_param)
{
	CDevice		*wp_Device;

	wp_Device = (CDevice *)ap_Void;
	if(wp_Device) 
	{
		if(a_msg == CB_STATUS_CHANGED) 
		{
			if(GetUpdateWnd()) 
			{
				::PostMessage(GetUpdateWnd()->GetSafeHwnd(), WM_CHANGEDSTATUS, a_param, 1);
			}
		}
	}
	return(1);
}

int CDLLowPA::StartSerial(int a_Port, int a_Baud)
{
	if(!mp_Device) {
		mp_Device = new CLPADevice;

		mp_Device->SetDevDataFunc(SerialRecv, this);
		mp_Device->SetCallbackFunc(SerialCallback);
		mp_Device->RunTaskThread(1);
		if(mp_Device->Start(a_Port, a_Baud) < 0) 
		{
			StopSerial();
			return(-1);
		}
	}
	return(1);
}

void CDLLowPA::StopSerial()
{
	if(mp_Device) {
		SendCmd(CMD_LOW_RESET);
		Sleep(200);
		
		mp_Device->SetDevDataFunc(NULL, this);
		mp_Device->SetCallbackFunc(NULL);
		mp_Device->RunTaskThread(0);
		mp_Device->Stop();

		Sleep(800);

		delete mp_Device;
		mp_Device = NULL;
	}
}

int CDLLowPA::UpdateStartSerial(int a_Port, int a_Baud)
{
	if(!mp_UpdateDevice) 
	{
		mp_UpdateDevice = new CLUpdateDevice;

		mp_UpdateDevice->SetDevDataFunc(UpdateSerialRecv, this);
		mp_UpdateDevice->SetCallbackFunc(UpdateSerialCallback);
		mp_UpdateDevice->RunTaskThread(1);
		if(mp_UpdateDevice->Start(a_Port, a_Baud) < 0) 
		{
			UpdateStopSerial();
			return(-1);
		}
	}
	return(1);
}

void CDLLowPA::UpdateStopSerial()
{
	if(mp_UpdateDevice) 
	{
		mp_UpdateDevice->SetDevDataFunc(NULL, this);
		mp_UpdateDevice->SetCallbackFunc(NULL);
		mp_UpdateDevice->RunTaskThread(0);
		mp_UpdateDevice->Stop();

		delete mp_UpdateDevice;
		mp_UpdateDevice = NULL;
	}
}

int CDLLowPA::MixerStartSerial(int a_Port, int a_Baud)
{
	if(!mp_MixerDevice) 
	{
		mp_MixerDevice = new CLMixerDevice;

		mp_MixerDevice->SetDevDataFunc(MixerSerialRecv, this);
		mp_MixerDevice->SetCallbackFunc(MixerSerialCallback);
		mp_MixerDevice->RunTaskThread(1);
		if(mp_MixerDevice->Start(a_Port, a_Baud) < 0) 
		{
			MixerStopSerial();
			return(-1);
		}
	}
	return(1);
}

void CDLLowPA::MixerStopSerial()
{
	if(mp_MixerDevice) 
	{
		mp_MixerDevice->SetDevDataFunc(NULL, this);
		mp_MixerDevice->SetCallbackFunc(NULL);
		mp_MixerDevice->RunTaskThread(0);
		mp_MixerDevice->Stop();

		delete mp_MixerDevice;
		mp_MixerDevice = NULL;
	}
}

int CDLLowPA::IsSerialOpened()
{
	if(mp_Device) 
	{
		return(mp_Device->IsOpened());
	}
	return(0);
}

int CDLLowPA::IsUpdateSerialOpened()
{
	if(mp_UpdateDevice) 
	{
		return(mp_UpdateDevice->IsOpened());
	}
	return(0);
}

int CDLLowPA::IsMixerSerialOpened()
{
	if(mp_MixerDevice) 
	{
		return(mp_MixerDevice->IsOpened());
	}
	return(0);
}

int	CDLLowPA::GetDeviceStatus()
{
	if(mp_Device) 
	{
		if(mp_Device->IsOpened()) 
		{
			return(mp_Device->m_Status);
		}
	}
	return(0);
}

int	CDLLowPA::GetUpdateDeviceStatus()
{
	if(mp_UpdateDevice) 
	{
		if(mp_UpdateDevice->IsOpened()) 
		{
			return(mp_UpdateDevice->m_Status);
		}
	}
	return(0);
}


void CDLLowPA::GetSearchConfigureNewFileName(char* ap_FilePath, char* ap_FileName, char* ap_GUID)
{
	int		w_Idx=0;
	int		w_FindFlag=0;
	CString path = _T("");
	CString fileName = _T("");
	CString szFilePath = _T("");
	CString szNewFileName=_T("");
	CString szFindFileName=_T("");

	char	w_Mac[MAX_PATH];
	char	w_GUID[MAX_PATH];
	TCHAR	pszPathName[_MAX_PATH];

	memset(&w_Mac,0x0,sizeof(w_Mac));
	memset(&w_GUID,0x0,sizeof(w_GUID));

	if(ap_GUID)
	{
		//memcpy(&w_GUID,ap_GUID,sizeof(BYTE)*LPA_GUID_MAX);
		//sprintf(w_GUID,"%02x%02x%02x%02x",(char*)&ap_GUID[0],(char*)&ap_GUID[1],(char*)&ap_GUID[2],(char*)&ap_GUID[3]);
		dl_strcpy(w_GUID,ap_GUID);
		szFindFileName = _T("config.conf");
	}
	DL_LOG("FILE GUID[%s]",w_GUID);
	
	// 검색 클래스 
	CFileFind finder; 

	::GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH); 
	PathRemoveFileSpec(pszPathName);
	szFilePath.Format(_T("%s"),pszPathName);
	
	dl_getmacaddr(w_Mac,0);
	// 폴더나 파일의 경로 적음 
	// 모든 파일을 검색할경우 *.*으로 특정확장자만 검색할 경우 *.jpg 이런식의 파일명 쓰기 
	sprintf(w_Mac,"%02x%02x%02x",(w_Mac[3]&0x0ff),(w_Mac[4]&0x0ff),(w_Mac[5]&0x0ff));

	{	
		path.Format(_T("%s\\SETUPFILES\\*.conf"),szFilePath);
		// path 위치에 파일이 존재하는지 확인 
		BOOL bol = finder.FindFile(path);
		// 존재 한다면.. 
		while(bol)
		{
			// path 위치의 파일로 이동
			bol = finder.FindNextFile();
			// 이동이 성공 했다면..
			if(finder.IsArchived())
			{
				//파일의 이름 
				fileName.Format(_T("%s"),  finder.GetFileName());

				// 현재폴더 상위폴더 썸네일파일은 제외
				if( fileName == _T(".") || 	fileName == _T("..")|| fileName == _T("Thumbs.db") ) continue;
				char* CT2ATemp = new char[strlen(CT2A(fileName)) + 1];
				strcpy(CT2ATemp, CT2A(fileName));
				DL_LOG("=>FILE[%s]", CT2ATemp);
				delete[] CT2ATemp;
			}
		}
		sprintf(w_GUID,"%s%02d",w_Mac,w_Idx);
		szNewFileName = _T("config.conf");
	}
	dl_strcpy(ap_GUID,w_GUID);
	dl_strcpy(ap_FileName, CT2A(szNewFileName));
	dl_strcpy(ap_FilePath,CT2A(szFilePath));
}

void CDLLowPA::TimerProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	timeKillEvent(m_uTimeoutID);

	if(uID == m_uTimeoutID)
	{
#if 0
		if(m_stBatch.nTry >= MAX_TRY_SEND_PACKET)
			::PostMessage(m_hwndDest, WM_UPDATEDLG_BATCH_PROGRESS, (WPARAM)&m_stBatch, LPARAM(FALSE));
		else
			SendBatchPacket(FALSE);
		//
#endif
	}
}

void CALLBACK TimerEntry(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);

static void CALLBACK TimerEntry(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	((CDLLowPA*)dwUser)->TimerProc(uID, uMsg, dwUser, dw1, dw2);
}

void CDLLowPA::StartTimer()
{
	timeKillEvent(m_uTimeoutID);
	m_uTimeoutID = timeSetEvent(m_secResponseTimeout,
								100,
								TimerEntry,
								(DWORD)this,
								NULL);
}

void CDLLowPA::StopTimer()
{
	timeKillEvent(m_uTimeoutID);
	m_uTimeoutID = 0;
}