#ifndef	DLLOWPADATA_DEF
#define	DLLOWPADATA_DEF

#include "DLOs.h"
#include "DLUtils.h"
#include "DLThreadMutex.h"
#include "DLUpdateDevice.h"
#include "DLMixerDevice.h"

#define		DEF_FONT_NAME			_T("굴림")

#define		DEF_SERIAL_BAUD			57600//19200
#define		DEF_SERIAL_MIX_BAUD		19200

enum DL_DEV_TYPE 
{
	DEV_TYPE_ALL =		0,	// 전체
	DEV_TYPE_PC =		1,	// PC
	DEV_TYPE_MAIN =		2,
	DEV_TYPE_DMT =		2,
	DEV_TYPE_DSS =		3,	
	DEV_TYPE_DPG =		4,	
	DEV_TYPE_DFE =		5,	
	DEV_TYPE_DDE =		6,	
	DEV_TYPE_DRM =		7,	
	DEV_TYPE_DPA =		8,		// 2015.0615 추가
	DEV_TYPE_DMP =		10,	
	DEV_TYPE_DRE =		11,
	DEV_TYPE_DFR =		12
};

// 설정을 위해 재 정의하여 사용
//장치 최대수량
#define LPA_EQUIP_MAX			7

// DMA ==> 1000에서는 DMT 5000에서는 DPA를 지침함..
#if defined(DEV_5000)
#define LPA_DMA_MAX				10
#define LPA_DMA_MAX_10			10
#else 
#define LPA_DMA_MAX				1
#endif
#define LPA_DSS_MAX_10			10

#define LPA_DSS_MAX				20
#define LPA_DPG_MAX				1
#define LPA_DFR_MAX				10
#define	LPA_DRM_MAX				48
#define LPA_DRE_MAX				6
#define LPA_DMP_MAX				1

#define	LPA_PCMACRO_MAX			1	

#if defined(DEV_5000)
#define	LPA_DMTMACRO_MAX		1	
#define	LPA_EMMACRO_MAX			1	
#endif

#define	LPA_TIMERMACRO_MAX		1	
#define LPA_MMACRO_MAX			6
#define LPA_DLC_MAX				1


enum {
	 LPA_DMA_IDX=0,
	 LPA_DSS_IDX,
	 LPA_DPG_IDX,
	 LPA_DFR_IDX,
	 LPA_DRM_IDX,
	 LPA_DRE_IDX,
	 LPA_DMP_IDX,
	 LPA_DLC_IDX,
	 LPA_DMX_IDX
};

enum {
	LPA_DATA_NORMAL=0,
	LPA_DATA_SUCESS,
	LPA_DATA_FAILED,
	LPA_INFO_SUCESS,
	LPA_INFO_FAILED,
};



#define	LPA_ITEM_MAX			255
#define	LPA_NAME_LEN			64
#define	LPA_NAME_MAX			255

#define	LPA_MMACRO_ITEM			20
#define	LPA_PCMACRO_ITEM		100			// 255
#define	LPA_TIMERMACRO_ITEM		1
#define	LPA_DLC_ITEM			2

#if defined(DEV_5000)
#define	LPA_EMMACRO_ITEM		1
#endif

#if defined(DEV_5000)
#define	LPA_DMTMACRO_ITEM		16
#endif

#define	LPA_TERMINAL_ITEM		16			// DMT/DSS
#define	LPA_DPG_ITEM			8			// 1 DPG당 숫자

#define	LPA_DFR_PITEM			64			// P형
#define	LPA_DFR_ITEM			255			// R형(P/R중 큰수지정)

#define	LPA_DFR_BUILDING_MAX	99
#define	LPA_DFR_STAIR_MAX		99
#define	LPA_DFR_FLOOR_MAX		99
#define	LPA_DFR_ZONE_MAX		4

#define LPA_MMACRO_CODE			1			// 1~6 : A~F
#define	LPA_PCMACRO_CODE		10			// 10
#define LPA_TIMERMACRO_CODE		20			// 20

#if defined(DEV_5000)
#define	LPA_DMTMACRO_CODE		11			// 11
#define	LPA_EMMACRO_CODE		21			// 21 => 비상접점 매크로
#endif

struct LPA_POS_S
{
	BYTE			m_Building;					// 빌딩 번호
	BYTE			m_Stair;					// 계단 번호
	BYTE			m_Floor;					// 층     번호 (0 : 지하층, 0: 옥외, ELV, 0: 지상층)
	BYTE			m_Zone;						// 구역 번호
};
typedef struct LPA_POS_S LPA_POS_T;


struct LPA_ITEM_S
{
	char			m_Name[LPA_NAME_LEN];		// 별칭
	LPA_POS_T		m_Pos;
};
typedef struct LPA_ITEM_S LPA_ITEM_T;


struct LPA_DEVICE_S
{
	BYTE			m_PRFlag;					// P/R형
	BYTE			m_BFlag;					// 10BYTE 12 BYTE
	WORD			m_Cnt;						// ITEM 숫자
	
	LPA_ITEM_T		m_Item[LPA_ITEM_MAX];
	char			m_DevName[LPA_NAME_MAX];	// 장비 타이틀 별칭
};
typedef struct LPA_DEVICE_S LPA_DEVICE_T;


// Terminal ==> DPG 관계데이타
struct LPA_TERM_DPG_S
{
	BYTE			m_DpgAddr;					// 장치주소 (DPG)
	BYTE			m_DpgOutput;				// Output
};
typedef struct LPA_TERM_DPG_S LPA_TERM_DPG_T;


// DFR ==> TERMINAL 관계데이타
struct LPA_DFR_TERM_S
{
	WORD			m_SSFlag[LPA_DMA_MAX+LPA_DSS_MAX];		// 설정값

};
typedef struct LPA_DFR_TERM_S LPA_DFR_TERM_T;

// PCMACRO ==> TERMINAL 관계데이타
struct LPA_PCMACRO_TERM_S
{
	WORD			m_SSFlag[LPA_DMA_MAX+LPA_DSS_MAX];		// 설정값
};
typedef struct LPA_PCMACRO_TERM_S LPA_PCMACRO_TERM_T;

// MMACRO ==> TERMINAL 관계데이타
struct LPA_MMACRO_TERM_S
{
	WORD			m_SSFlag[LPA_DMA_MAX+LPA_DSS_MAX];		// 설정값
};
typedef struct LPA_MMACRO_TERM_S LPA_MMACRO_TERM_T;

// TIMERMACRO ==> TERMINAL 관계데이타
struct LPA_TIMERMACRO_TERM_S
{
	WORD			m_SSFlag[LPA_DMA_MAX+LPA_DSS_MAX];		// 설정값
};
typedef struct LPA_TIMERMACRO_TERM_S LPA_TIMERMACRO_TERM_T;

struct LPA_DLC_TERM_S
{
	WORD			m_SSFlag[LPA_DMA_MAX + LPA_DSS_MAX];		// 설정값
};
typedef struct LPA_DLC_TERM_S LPA_DLC_TERM_T;

#if defined(DEV_5000)
// DMTMACRO ==> TERMINAL 관계데이타
struct LPA_DMTMACRO_TERM_S
{
	WORD			m_SSFlag[LPA_DMA_MAX+LPA_DSS_MAX];		// 설정값
};
typedef struct LPA_DMTMACRO_TERM_S LPA_DMTMACRO_TERM_T;

// EMMACRO ==> TERMINAL 관계데이타
struct LPA_EMMACRO_TERM_S
{
	WORD			m_SSFlag[LPA_DMA_MAX+LPA_DSS_MAX];		// 설정값
};
typedef struct LPA_EMMACRO_TERM_S LPA_EMMACRO_TERM_T;

#endif

#if defined(DEV_5000) 
// 선택 화면 페이지
#define		LPA_SEL_EQUIP			0
#define		LPA_SEL_DPG				1
#define		LPA_SEL_DMTMACRO		2
#define		LPA_SEL_PCMACRO			3
#define		LPA_SEL_TIMERMACRO		4
#define		LPA_SEL_MMACRO			5
#define		LPA_SEL_DFR				6
#define		LPA_SEL_DLC				7
#define		LPA_SEL_ETC				8
#define		LPA_SEL_UPDATE			9				//펌웨어 업데이트

// FILE CHECKER
#define		LPA_FILE_CHECKER		0x7e04

#else		// 1000

// 선택 화면 페이지
#define		LPA_SEL_EQUIP			0
#define		LPA_SEL_DPG				1
#define		LPA_SEL_PCMACRO			2
#define		LPA_SEL_TIMERMACRO		3
#define		LPA_SEL_MMACRO			4
#define		LPA_SEL_DFR				5
#define		LPA_SEL_DLC				6
#define		LPA_SEL_ETC				7
#define		LPA_SEL_UPDATE			8				//펌웨어 업데이트

#endif


// FILE CHECKER
//#define		LPA_FILE_CHECKER_5000_VER01	0x7e04		// ==> 이전버전...
#define		LPA_FILE_CHECKER_5000		0x7e05

// FILE CHECKER
//#define		LPA_FILE_CHECKER_1000_VER01	0x4e03		// TEMP VERSION

//#define		LPA_FILE_CHECKER_1000_VER02	0x4e04		//  ==> 이전버전..
#define		LPA_FILE_CHECKER_1000		0x4e05

struct FHEAD_S
{
	WORD	m_Cheker;					// File Validation Checker
	char	m_Dummy[6];					// 
};
typedef struct FHEAD_S FHEAD_T;

#define		MARK_LPA_DATA			0x13a1

struct MARK_S
{
	unsigned short	m_Marker;			// Structure Marker
	unsigned short	m_Option;
};
typedef struct MARK_S MARK_T;

// 1000/5000 Series 설정정보
struct LPA_DATA_S
{
	DWORD			m_DevCode;		// New Append

	int				m_DevCnt[LPA_EQUIP_MAX];
	int				m_DevMax[LPA_EQUIP_MAX];

	LPA_DEVICE_T	m_DmtData[LPA_DMA_MAX];
	LPA_DEVICE_T	m_DssData[LPA_DSS_MAX];
	LPA_DEVICE_T	m_DpgData[LPA_DPG_MAX];
	LPA_DEVICE_T	m_DfrData[LPA_DFR_MAX];


	int				m_MMacroCnt;
	int				m_MMacroMax;
	LPA_DEVICE_T	m_MMacroData[LPA_MMACRO_MAX];

	int				m_PCMacroCnt;
	int				m_PCMacroMax;
	LPA_DEVICE_T	m_PCMacroData[LPA_PCMACRO_MAX];

	LPA_TERM_DPG_T	m_TermDpg[LPA_DMA_MAX+LPA_DSS_MAX][LPA_TERMINAL_ITEM];
	LPA_DFR_TERM_T	m_DfrTerm[LPA_DFR_MAX][LPA_DFR_ITEM];

	LPA_PCMACRO_TERM_T m_PCMacroTerm[LPA_PCMACRO_MAX][LPA_PCMACRO_ITEM];
	LPA_MMACRO_TERM_T m_MMacroTerm[LPA_MMACRO_MAX][LPA_MMACRO_ITEM];

// Timer CNT ==> 이전버전의 호환성을 위해 뒤에 추가함
	int				m_TimerMacroCnt;
	int				m_TimerMacroMax;
	LPA_DEVICE_T	m_TimerMacroData[LPA_TIMERMACRO_MAX];
	LPA_TIMERMACRO_TERM_T m_TimerMacroTerm[LPA_TIMERMACRO_MAX][LPA_TIMERMACRO_ITEM];

// DLC 추가
	int				m_DLCCnt;
	int				m_DLCMax;
	LPA_DEVICE_T	m_DLCData[LPA_DLC_MAX];
	LPA_DLC_TERM_T	m_DLCTerm[LPA_DLC_MAX][LPA_DLC_ITEM];

#if defined(DEV_5000)
	int				m_DMTMacroCnt;
	int				m_DMTMacroMax;
	LPA_DEVICE_T	m_DMTMacroData[LPA_DMTMACRO_MAX];
	LPA_DMTMACRO_TERM_T m_DMTMacroTerm[LPA_DMTMACRO_MAX][LPA_DMTMACRO_ITEM];

	int				m_EmMacroCnt;
	int				m_EmMacroMax;
	LPA_DEVICE_T	m_EmMacroData[LPA_EMMACRO_MAX];
	LPA_EMMACRO_TERM_T m_EmMacroTerm[LPA_EMMACRO_MAX][LPA_EMMACRO_ITEM];
#endif

#if defined(DEV_5000)
	WORD			m_Gate;
#endif
};
typedef struct LPA_DATA_S LPA_DATA_T;


#undef	LPA_ROM_SIZE
#undef	LPA_ROM_MAX

#define	LPA_ROM_SIZE		(1024 * 20)
#define	LPA_ROM_MAX			(1024 * 200)
#define	LPA_UPDATE_ROM_MAX	(1024 * 512)

#define	LPA_PACKET_LEN		128
#define	LPA_PACKET_128LEN	128
#define	LPA_PACKET_256LEN	256

#define	LPA_PACKET_NUM	(LPA_ROM_SIZE/LPA_PACKET_LEN)

class	CNodeListView;
class	CDLLowPASetupDlg;

class 	CDLEquipSetDlg;
class 	CDLDpgSetDlg;
class 	CDLPCMacroSetDlg;
class 	CDLTimerMacroSetDlg;
class 	CDLDfrSetDlg;
class 	CDLMMacroSetDlg;
class 	CDLEtcSetDlg;
class	CDLUpdateDlg;

class	CDLDLCSetDlg;

#if defined(DEV_5000)
class 	CDLDMTMacroSetDlg;
class	CLMixerDevice;
#endif

class 	CLPADevice;
class	CLUpdateDevice;

#define		SET_BYTE(ROM, IDX, VALUE)		ROM[IDX] = ((BYTE)(VALUE));(IDX)++
#define		SET_WORD(ROM, IDX, VALUE)		ROM[IDX] = (BYTE)((VALUE)&0x0ff);ROM[(IDX)+1] = (BYTE)(((VALUE)>>8)&0x0ff);(IDX) += 2
#define		SET_DWORD(ROM, IDX, VALUE)		ROM[IDX] = (BYTE)((VALUE)&0x0ff);ROM[(IDX)+1] = (BYTE)(((VALUE)>>8)&0x0ff);ROM[(IDX)+2] = (BYTE)(((VALUE)>>16)&0x0ff);ROM[(IDX)+3] = (BYTE)(((VALUE)>>24)&0x0ff);(IDX) += 4

#define		GET_BYTE(ROM, IDX, VALUE)		VALUE = (BYTE)ROM[IDX];(IDX)++
#define		GET_WORD(ROM, IDX, VALUE)		VALUE = (WORD)(ROM[IDX]&0x0ff) | ((ROM[IDX+1]&0x0ff) << 8);(IDX) += 2
#define		GET_DWORD(ROM, IDX, VALUE)		VALUE = (DWORD)(ROM[IDX]&0x0ff) | ((ROM[IDX+1]&0x0ff) << 8) || ((ROM[IDX+2]&0x0ff) << 16) || ((ROM[IDX+3]&0x0ff) << 24);(IDX) += 4


enum {
	DEV_STAT_NORMAL=0,
	DEV_STAT_SEND=1,
	DEV_STAT_RECV=2,
	DEV_STAT_NEXT=3
};

enum {
	DEV_UPDATE_NORMAL=0x100,
	DEV_UPDATE_DMT=0x101,
	DEV_UPDATE_DPG=0x102,
	DEV_UPDATE_DRE=0x103,
	DEV_UPDATE_DRM=0x104,
	DEV_UPDATE_DPA=0x105,
	DEV_UPDATE_DMP=0x106,
	DEV_UPDATE_DSS=0x107,
	DEV_UPDATE_DFR=0x108,
	DEV_UPDATE_DMX=0x109,
	DEV_UPDATE_END,
	DEV_UPDATE_ENDERROR
};

class CDLLowPA
{
public:
	CNodeListView		*mp_TermWnd;
	CDLLowPASetupDlg    *mp_MainWnd;
	HWND				m_MainHwnd;

	CLPADevice			*mp_Device;
	CLUpdateDevice		*mp_UpdateDevice;
	CLMixerDevice		*mp_MixerDevice;

	//펌웨어 업그레이드용 장치정보 구조체
	ST_EQUIP_INFO		m_EquipInfo[LPA_MMACRO_ITEM][LPA_DRM_MAX+15];

	CThreadMutex m_mutex;

	int			m_Modified;

	int			m_SelPage;					// 선택 화면 페이지 ==> 위 define참조(LPA_SEL_EQUIP등)

//	int			m_SelAddr;					// 선택 Devide 주소
//	int			m_SelItem;					// 선택 Device Item(Output)

	int			m_SerialPort;

	LPA_DATA_T	m_PAData;

	int			m_DevStat;					// 설정 시리얼 드라이버 상태변수
	int			m_UpdateDevStat;			// 업데이트 시리얼 드라이버 상태변수
	int			m_UpdateFlag;				// 장치조회 및 일괄업데이트시 장치 상태변수

	// 만약을 위해 LPA_ROM_MAX로 메모리는 확보함..
	WORD		m_Rom1Len;
	WORD		m_Rom1Page;
	BYTE		m_Rom1[LPA_ROM_MAX];

	WORD		m_Rom2Len;
	WORD		m_Rom2Page;
	BYTE		m_Rom2[LPA_ROM_MAX];

	int			m_RomUpdateLen;
	int			m_RomUpdatePage;
	BYTE		m_RomUpdate[LPA_UPDATE_ROM_MAX];

	WORD		m_RomMixerLen;
	WORD		m_RomMixerPage;
	BYTE		m_RomMixer[LPA_UPDATE_ROM_MAX];

	WORD		m_RecvRom1Len;
	BYTE		m_RecvRom1[LPA_ROM_MAX];

	WORD		m_RecvRom2Len;
	BYTE		m_RecvRom2[LPA_ROM_MAX];

	WORD		m_RecvRomUpdateLen;
	BYTE		m_RecvRomUpdate[LPA_UPDATE_ROM_MAX];

#if defined(DEV_5000)
	int			m_TimerMacroMode;			//  0: TIMER 1:EM
#endif
public :
	CDLLowPA();
	~CDLLowPA();

	void	Lock();
	void	UnLock();

	void SetTermWnd(void	*ap_TermWnd)	{	mp_TermWnd = (CNodeListView *)ap_TermWnd;	};
	CNodeListView	*GetTermWnd()			{   return(mp_TermWnd);		};

	void SetMainWnd(void	*ap_MainWnd);
	CDLLowPASetupDlg	*GetMainWnd()		{   return(mp_MainWnd);		};
	HWND				GetMainHwnd()		{	return(m_MainHwnd);		};

	int			IsModified()				{	return(m_Modified);		};
	void		SetModified(int a_flag)		{	m_Modified = a_flag;	};

	int			GetDpgCount(int a_Addr, int a_Item);					// 설정된 DPG ITEM 수를 COUNT
	int			GetPCMacroCount(int a_Addr, int a_Item);				// 설정된 PC Macro ITEM 수를 COUNT
#if defined(DEV_5000)
	int			GetDMTMacroCount(int a_Addr, int a_Item);				// 설정된 DMT Macro ITEM 수를 COUNT
#endif
	int			GetTimerMacroCount(int a_Addr, int a_Item);				// 설정된 Timer Macro ITEM 수를 COUNT
#if defined(DEV_5000)
	int			GetEmMacroCount(int a_Addr, int a_Item);				// 설정된 Em Macro ITEM 수를 COUNT
#endif
	int			GetMMacroCount(int a_Addr, int a_Item);					// 설정된 DRM ADDR수를 COUNT
	int			GetAddrMMacroCount(int a_Addr);							// 설정된 DRM ITEM 수를 COUNT
	int			GetAddrMMacroChannelCount(int a_Addr);					// 설정된 DRM의 총 DPG 채널 갯수
	int			GetDfrCount(int a_Addr, int a_Item);					// 설정된 DFR ITEM 수를 COUNT

	int			GetDLCCount(int a_Addr, int a_Item);					// 설정된 DLC ITEM 수를 COUNT

	void		SetDmtName(char *ap_Name, int a_Addr, int a_Item);			// DMT 별칭을 설정
	void		SetDssName(char *ap_Name, int a_Addr, int a_Item);			// DSS 별칭을 설정
	void		SetDpgName(char *ap_Name, int a_Addr, int a_Item);			// DPG 별칭을 설정
	void		SetPCMacroName(char *ap_Name, int a_Addr, int a_Item);		// PC Macro 별칭을 설정
#if defined(DEV_5000)
	void		SetDMTMacroName(char *ap_Name, int a_Addr, int a_Item);	// DMT Macro 별칭을 설정
#endif
	void		SetMMacroName(char *ap_Name, int a_Addr, int a_Item);		// DRM 별칭을 설정
	void		SetTimerMacroName(char *ap_Name, int a_Addr, int a_Item);	// Timer Macro 별칭을 설정
#if defined(DEV_5000)
	void		SetEmMacroName(char *ap_Name, int a_Addr, int a_Item);	// Em Macro 별칭을 설정
#endif
	void		SetDfrName(char *ap_Name, int a_Addr, int a_Item);		// DFR 별칭을 설정

	void		SetDlcName(char *ap_Name, int a_Addr, int a_Item);		// DLC 별칭을 설정

	bool		IsBitOn(WORD *ap_Data, int a_Y, int a_X);				// 해당 SS flag 체크
	void		ToggleBit(WORD *ap_Data, int a_Y, int a_X);				// 해당 SS flag를 ON => OFF, OFF=>ON처리함
	void		SetBit(WORD *ap_Data, int a_Y, int a_X, int a_OnOff);	// 해당 SS flag를 ON/OFF

	void		Init();													// 전체를 초기화함
	void		InitLPAData(LPA_DATA_T *ap_PAData);						// 전관장비 설정 데이타를 초기화함
	int			Save(char *ap_fname, LPA_DATA_T *ap_PAData=NULL);		// 파일에서 읽기
	int			Load(char *ap_fname, LPA_DATA_T *ap_PAData=NULL);		// 파일에 저장

	int			ToDevice();
	int			FromDevice();

	int			MakeRom1Data(LPA_DATA_T *ap_PAData, BYTE *ap_Rom1);		// 장치로 보내기위한 데이타 만듬
	int			MakeRom2Data(LPA_DATA_T *ap_PAData, BYTE *ap_Rom2);
	int			MakeRomUpdateData(BYTE *ap_PAData, BYTE *ap_RomUpdate, int a_Len);	// 각 장치로 보낼 장치 업그레이드 데이타 만듬

																		// 장치로부터 받은 데이타를 파싱함
	int			ParseRomData(LPA_DATA_T *ap_PAData, BYTE *ap_Rom1, int a_Rom1Len, BYTE *ap_Rom2, int a_Rom2Len);

	void		Invalidate();						// 전체 재표시

	void		GetSearchConfigureNewFileName(char* ap_FilePath=NULL, char* ap_FileName=NULL, char* ap_GUID=NULL);

public :	// FOR SERIAL

	static	int SerialRecv(void *ap_Void, char *ap_Buff, int a_Len);
	static	int SerialCallback(void *ap_Void, unsigned long a_msg, unsigned long a_param);

	int			StartSerial(int a_Port, int a_Baud);	
	void		StopSerial();
	int			IsSerialOpened();					// COM PORT Open상태
	int			GetDeviceStatus();					// 전관장치와 통신상태

	WORD		m_SendPage;							// 보내고 있는 페이지
	WORD		m_RecvPage;							// 받고 있는 페이지

	void		SendCmd(BYTE a_Cmd, BYTE a_PRM1=0, BYTE a_PRM2=0, BYTE a_PRM3=0, BYTE a_PRM4=0);
													// CMD 를 보냄
	int			SendData(WORD a_Page);				// 103 DaTA보냄
	
	int			ParseRecvData(char *ap_Data, int a_Len);	// 수신데이타를 Parsing함

public :	// For UpdateDevice Serial
	int			UpdateSetDevInfo(BYTE a_TYPE=0, BYTE a_ENO=0, BYTE a_PRM3=0);
	static	int UpdateSerialRecv(void *ap_Void, char *ap_Buff, int a_Len);
	static	int UpdateSerialCallback(void *ap_Void, unsigned long a_msg, unsigned long a_param);

	int			UpdateStartSerial(int a_Port, int a_Baud);
	void		UpdateStopSerial();
	int			IsUpdateSerialOpened();				// COM PORT Open상태
	int			GetUpdateDeviceStatus();			// 전관장치와 통신상태

	WORD		m_SendUpdateInfo;					// 보내고 있는 전체 정보조회
	int			m_SendUpdateData;					// 보내고 있는 전체 펌웨어 데이타
	int			m_SendUpdatePage;					// 보내고 있는 페이지
	WORD		m_RecvUpdatePage;					// 받고 있는 페이지

	void		SendUpdateCmd(BYTE a_Cmd, BYTE a_PRM1=0, BYTE a_PRM2=0, BYTE a_PRM3=0, BYTE a_PRM4=0, BYTE a_TYPE=0, BYTE a_ENO=0, BYTE* apVersion=NULL);
	int			SendUpdateData(int a_Page);
	int			ParseUpdateRecvData(char *ap_Data, int a_Len);	// 수신데이타를 Parsing함

public :	// For MixerDevice Serial
	int			MixerSetDevInfo(BYTE a_TYPE=0, BYTE a_ENO=0, BYTE a_PRM3=0);
	static	int MixerSerialRecv(void *ap_Void, char *ap_Buff, int a_Len);
	static	int MixerSerialCallback(void *ap_Void, unsigned long a_msg, unsigned long a_param);

	int			MixerStartSerial(int a_Port, int a_Baud);
	void		MixerStopSerial();
	int			IsMixerSerialOpened();				// COM PORT Open상태
	int			GetMixerDeviceStatus();				// 전관장치와 통신상태

	WORD		m_SendMixerInfo;					// 보내고 있는 전체 정보조회
	WORD		m_SendMixerData;					// 보내고 있는 전체 펌웨어 데이타
	WORD		m_SendMixerPage;					// 보내고 있는 페이지
	WORD		m_RecvMixerPage;					// 받고 있는 페이지

	void		SendMixerCmd(BYTE a_Cmd, BYTE a_PRM1=0, BYTE a_PRM2=0, BYTE a_PRM3=0, BYTE a_PRM4=0, BYTE a_TYPE=0, BYTE a_ENO=0, BYTE* apVersion=NULL);
	int			SendMixerData(WORD a_Page);
	int			ParseMixerRecvData(char *ap_Data, int a_Len);	// 수신데이타를 Parsing함

	UINT		m_uTimeoutID;
	DWORD		m_secResponseTimeout;

	void		StartTimer();
	void		StopTimer();
	void		TimerProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);

};



#ifdef	__cplusplus
extern "C" {
#endif

CDLLowPA		*GetLowPA();
LPA_DATA_T		*GetPAData();

CDLLowPASetupDlg *GetMainWnd();
CNodeListView	 *GetTermWnd();

CDLEquipSetDlg	*GetEquipWnd();
CDLDpgSetDlg	*GetDpgWnd();
CDLPCMacroSetDlg	*GetPCMacroWnd();
#if defined(DEV_5000) 
CDLDMTMacroSetDlg	*GetDMTMacroWnd();
#endif

CDLTimerMacroSetDlg	*GetTimerMacroWnd();
CDLDfrSetDlg	*GetDfrWnd();
CDLMMacroSetDlg	*GetMMacroWnd();
CDLDLCSetDlg	*GetDLCWnd(); // 20191105추가
CDLEtcSetDlg	*GetEtcWnd();

CDLUpdateDlg	*GetUpdateWnd();

#ifdef	__cplusplus
}
#endif



#endif