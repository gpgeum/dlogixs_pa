// DLLowPAPopupDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLLowPAPopupDlg.h"


// CDLLowPAPopupDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLLowPAPopupDlg, CDialog)

CDLLowPAPopupDlg::CDLLowPAPopupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLLowPAPopupDlg::IDD, pParent)
	, m_StrName(_T(""))
	, m_StrMsg(_T(""))
{
}

CDLLowPAPopupDlg::~CDLLowPAPopupDlg()
{
}

void CDLLowPAPopupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_NAME, m_StrName);
	DDX_Text(pDX, IDC_EDIT_MSG, m_StrMsg);
	DDX_Control(pDX, IDC_EDIT_MSG, m_StcMsg);
}


BEGIN_MESSAGE_MAP(CDLLowPAPopupDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDLLowPAPopupDlg::OnOK)
END_MESSAGE_MAP()


// CDLLowPAPopupDlg 메시지 처리기입니다.

void CDLLowPAPopupDlg::OnOK()
{
	UpdateData(TRUE);

//	if(m_StrName.GetLength() <= 0) {
//		MessageBox(_T("별칭을 입력하십시요."), _T("입력오류"), MB_OK | MB_ICONWARNING);
//		return;
//	}

	CDialog::OnOK();
}

BOOL CDLLowPAPopupDlg::PreTranslateMessage(MSG* pMsg)
{
	CString strDisplay=_T("");
	CString strName=_T("");
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN && pMsg->hwnd == GetDlgItem(IDC_EDIT_NAME)->GetSafeHwnd())
		{
			PostMessage(WM_COMMAND, IDOK);
			return TRUE;
		}

		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}

	if (pMsg->message == WM_CHAR && pMsg->hwnd == GetDlgItem(IDC_EDIT_NAME)->GetSafeHwnd() )
	{
		//문자열 길이가 12이상, 15글자미만 체크
		int		w_Len=0;
		TCHAR	str[32];
		char	w_Buff[32];

		//((CEdit*)GetDlgItem(IDC_EDIT_NAME)->GetSafeHwnd())->SendMessage(WM_CHAR, pMsg->wParam, pMsg->lParam);
		((CEdit*)GetDlgItem(IDC_EDIT_NAME))->GetWindowText(strName);

		memset(&str,0x00,sizeof(str));
		memset(&w_Buff,0x00,sizeof(w_Buff));
		wsprintf(str,_T("%s"),strName);
		dl_strcpy(w_Buff, CT2A(strName));
		//sprintf(w_Buff,"%s", CA2TTemp);
		w_Len = lstrlenA(w_Buff);
		if(w_Len > 20)
		{
			MessageBox(_T("지역 별칭명이 너무깁니다.\n글자수를 한글 10자, 영문 및 숫자 20자 이내로 조절하십시요."), _T("입력오류"), MB_OK | MB_ICONWARNING);
			strDisplay.Format(_T("%s"),strName.Mid(0,strName.GetLength()-2)); 
			((CEdit*)GetDlgItem(IDC_EDIT_NAME))->SetWindowText(strDisplay);
			return TRUE;
		}
//		DL_LOG("%s",CT2A(strName));
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CDLLowPAPopupDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_StcMsg.SetBkColor(COLOR_WHITE);
	m_StcMsg.SetTextColor(COLOR_BLUE);

	((CEdit*)GetDlgItem(IDC_EDIT_NAME))->SetLimitText(20);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
