#pragma once
#include "afxwin.h"


// CDLLowPAPopupDlg 대화 상자입니다.

class CDLLowPAPopupDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLLowPAPopupDlg)

public:
	CDLLowPAPopupDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLLowPAPopupDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_POPUP_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnOK();
	CString m_StrName;
	CString m_StrMsg;
	CColorStatic m_StcMsg;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
};
