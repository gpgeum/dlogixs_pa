
// DLLowPASetup.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLLowPASetupDlg.h"
#include "shlwapi.h"

#pragma pack(push,8) /* Work around a bug in tlhelp32.h in WIN64, which generates the wrong structure if packing has been changed */
#include <tlhelp32.h>
#pragma pack(pop)

int g_LowPADevPort=0;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDLLowPASetupApp

BEGIN_MESSAGE_MAP(CDLLowPASetupApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CDLLowPASetupApp 생성

CDLLowPASetupApp::CDLLowPASetupApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CDLLowPASetupApp 개체입니다.

CDLLowPASetupApp theApp;


// CDLLowPASetupApp 초기화

BOOL CDLLowPASetupApp::InitInstance()
{

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();
	//afxAmbientActCtx = FALSE;

	int iCnt = 0;
	LPWSTR* pStr = NULL;
	pStr = CommandLineToArgvW( GetCommandLine(), &iCnt);

	for(int i=0; i<iCnt; i++)
	{
		CString str;
		str.Format(_T("%s"), pStr[i]);		//배열 처럼 쓸수있다. // pStr[0]은 실행파일. 1번부터가 인자
		char* CT2ATemp = new char[strlen(CT2A(str)) + 1];;
		strcpy(CT2ATemp, CT2A(str));
		DL_LOG("=>PORT[%s]", CT2ATemp);

//		AfxMessageBox(str);
	}

	if(iCnt>1){
		CString str;
		str.Format(_T("%s"), pStr[1]);
		g_LowPADevPort = (int)_wtof(str);
		if(!g_LowPADevPort)
		{
			g_LowPADevPort = 1;
		}
	}

	LocalFree(pStr);
	ExitTerminateProcess(DL_LOWPA_PROCESSNAME);

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(MAIN_APP_TITLE);
	if(!IsRunningApp())
	{
		CDLLowPASetupDlg dlg;
		m_pMainWnd = &dlg;
		INT_PTR nResponse = dlg.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
			//  코드를 배치합니다.
		}
		else if (nResponse == IDCANCEL)
		{
			// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
			//  코드를 배치합니다.
		}
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

int CDLLowPASetupApp::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CWinAppEx::ExitInstance();
}

BOOL CDLLowPASetupApp::IsRunningApp(void)
{
	CString appName = _T("");
	appName.LoadString(IDS_APP_NAME);

	HANDLE hMuxtex = CreateMutex(NULL, TRUE, (LPCTSTR)appName);
	if(GetLastError() == ERROR_ALREADY_EXISTS)
	{
		ReleaseMutex(hMuxtex);
		CWnd *pWndPre = NULL, *pWndChild = NULL;

		return TRUE;
	}

	ReleaseMutex(hMuxtex);

	return FALSE;
}

BOOL CDLLowPASetupApp::ExitTerminateProcess(CString szProcessName)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 sEntry32;
	sEntry32.dwSize = sizeof(PROCESSENTRY32); // 이거 빠지면 검색 잘 안됨

	if((hSnapshot == INVALID_HANDLE_VALUE) || (hSnapshot == NULL)) return FALSE;

	BOOL bRemain = Process32First(hSnapshot, &sEntry32);
	while(bRemain)
	{
		CString szExeName;
		szExeName.Format(_T("%s"), sEntry32.szExeFile);

		if(szExeName.CompareNoCase(szProcessName) == 0)
		{
			HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, sEntry32.th32ProcessID);
			if((hProcess != INVALID_HANDLE_VALUE) && (hProcess != NULL))
			{
				BOOL bRtn = ::TerminateProcess(hProcess, 0);
				CloseHandle(hProcess);
				CloseHandle(hSnapshot);
				return bRtn;
			}
		}

		bRemain = Process32Next(hSnapshot, &sEntry32);
	}

	CloseHandle(hSnapshot);

	return FALSE;
}