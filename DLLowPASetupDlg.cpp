
// DLLowPASetupDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLLowPASetupDlg.h"
#include "DLLPADevice.h"

#include <crtdbg.h> 

#ifdef _DEBUG

#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW

#endif

CFont			g_titleFont;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDLLowPASetupDlg 대화 상자




CDLLowPASetupDlg::CDLLowPASetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLLowPASetupDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDLLowPASetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDLLowPASetupDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CDLLowPASetupDlg 메시지 처리기

BOOL CDLLowPASetupDlg::OnInitDialog()
{

	CDialog::OnInitDialog();
	GetDlgItem(IDC_TAB)->ShowWindow(SW_HIDE);
	SetTabsPosition();
	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	MoveWindow(0,0,DEF_SCR_WIDTH,DEF_SCR_HEIGHT);
	SetWindowText(MAIN_APP_TITLE);


//	GetGlobal()->mp_MainDlg = this;

	CenterWindow();

	InitControls();

	if(GetEtcWnd()) {
		GetEtcWnd()->DrawStatus();
	}

//	DLTaskInit();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CDLLowPASetupDlg::InitControls()
{
	CFont font;
	font.CreatePointFont(85,_T("Tahoma"));
	m_MPCC.SetFont(&font);
	m_MPCC.SetFontSelect(&font);
	// 
	// 
	m_MPCC.InstallStyle(&m_MPCC.styleVS2008_client_silver);

	m_MPCC.SetDockMarkers(MarkersLayoutA(),DockingMarkers::Params(25,true,14),61);
	// 
	m_MPCC.SetAbilityManager(this);
	m_MPCC.SetNotifyManager(this);
	// 
	m_MPCC.SetCursors(IDC_CURSOR1,IDC_CURSOR2,IDC_CURSOR3,IDC_CURSOR4,IDC_CURSOR5);
	m_MPCC.RemoveTabEnable(false);
	m_MPCC.DragTabEnable(false);
	m_MPCC.SetMinSize(CSize(376,600));
	// 
	m_MPCC.Update();

	// 
	m_MPCC.SetTabLayout(TAB_LAYOUT_TOP);
	m_MPCC.SetTabBehavior(TAB_BEHAVIOR_SCALE);
	m_MPCC.SetSplitterDraggingMode(SPLITTER_DRAGGING_DYNAMIC);

	// 
	LOGFONT logfont;
	logfont.lfWeight=FW_BOLD;
	m_MPCC.GetFontSelect()->GetLogFont(&logfont);

	// 
	m_MPCC.ShowTabBorder(TRUE);
	m_MPCC.HideSingleTab(TRUE);
	m_MPCC.ShowTabCloseButton(FALSE);
	m_MPCC.ShowTabMenuButton(FALSE);
	//m_MPCC.ShowTabScrollButtons(FALSE);
	m_MPCC.Update();

	g_titleFont.CreateFont(20,0,0,0,FW_NORMAL,FALSE,FALSE,FALSE,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH|FF_DONTCARE,_T("Tahoma"));
}

void CDLLowPASetupDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	} else if(nID == SC_CLOSE) {
		if(GetLowPA()->IsModified()) {
			int w_Rtn;

			w_Rtn = MessageBox(_T("값이 변경되었습니다. 저장후 종료하시겠습니까?"), _T("실행확인"), MB_YESNOCANCEL|MB_ICONQUESTION);
			if(w_Rtn == IDYES) {
				TCHAR szFilter[] = _T("config File(*.conf)|*.conf|All Files (*.*)|*.*||");
				CFileDialog openDlg(FALSE, _T("config"), _T("noname"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, this);

				if(openDlg.DoModal() != IDOK) {
					return;
				}
				if(GetLowPA()->Save((CT2A)openDlg.GetPathName()) < 0) {
					MessageBox(_T("해당 파일을 저장할 수 없습니다. "), _T("처리오류"), MB_OK|MB_ICONWARNING);
					return;
				}
			} else if(w_Rtn == IDCANCEL) {
				return;
			}
			else 
			{
				if(MessageBox(_T("설정 프로그램을 종료후, 전관운영프로그램이\n실행 되도록 하시겠습니까?"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) 
				{
					::PostQuitMessage(0);
					return;
				}
			}
		} else {
			if(MessageBox(_T("설정 프로그램을 종료후, 전관운영프로그램이\n실행 되도록 하시겠습니까?"), _T("실행확인"), MB_YESNO|MB_ICONQUESTION) != IDYES) 
			{
				::PostQuitMessage(0);
				return;
			}
		}
		OnExitSetupApp();
	} 
	CDialog::OnSysCommand(nID, lParam);
}

void CDLLowPASetupDlg::OnExitSetupApp()
{
	int ret=0;

	ret = run_application(DL_LOWPA_PROCESSNAME, NULL, FALSE );

	Sleep(100);
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CDLLowPASetupDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CDLLowPASetupDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


int CDLLowPASetupDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	ModifyStyle(0,WS_CLIPCHILDREN);	// to avoid flicks of child controls.
	// 
	// 
	// Creation of MultiPaneCtrl object.
	// 
	if(m_MPCC.Create(this,WS_CHILD | WS_VISIBLE,CRect(0,0,0,0),MULTI_PAIN_CONTROL)==false) return -1;
	// 
	// 
	// Creation of child windows.
	// 
	// 
	m_MainPosDlg.Create(CDLMainPosDlg::IDD,this);
	m_MainPosDlg.SetDlgCtrlID(IDD_DLMAINPOS_DIALOG);

	m_EqiipDlg.Create(CDLEquipSetDlg::IDD,this);
	m_EqiipDlg.SetDlgCtrlID(IDD_DLEQUIPSET_DIALOG);

	m_DpgDlg.Create(CDLDpgSetDlg::IDD,this);
	m_DpgDlg.SetDlgCtrlID(IDD_DLDPGSET_DIALOG);

	m_PCMacroDlg.Create(CDLPCMacroSetDlg::IDD,this);
	m_PCMacroDlg.SetDlgCtrlID(IDD_DLMACROSET_DIALOG);

#if defined(DEV_5000)
	m_DMTMacroDlg.Create(CDLDMTMacroSetDlg::IDD,this);
	m_DMTMacroDlg.SetDlgCtrlID(IDD_DLDMTSET_DIALOG);
#endif

	m_TimerMacroDlg.Create(CDLTimerMacroSetDlg::IDD,this);
	m_TimerMacroDlg.SetDlgCtrlID(IDD_DLTMACROSET_DIALOG);

	m_MMacroDlg.Create(CDLMMacroSetDlg::IDD,this);
	m_MMacroDlg.SetDlgCtrlID(IDD_DLDRMSET_DIALOG);

	m_DfrDlg.Create(CDLDfrSetDlg::IDD,this);
	m_DfrDlg.SetDlgCtrlID(IDD_DLDFR_DIALOG);

	m_DLCDlg.Create(CDLDLCSetDlg::IDD, this);
	m_DLCDlg.SetDlgCtrlID(IDD_DLDLCSET_DIALOG);

	m_EtcDlg.Create(CDLEtcSetDlg::IDD,this);
	m_EtcDlg.SetDlgCtrlID(IDD_DLETCSET_DIALOG);

	GetLowPA()->SetMainWnd(this);

	if(!::IsWindow(m_WaitDialog.GetSafeHwnd())) {
		m_WaitDialog.Create(CWaitDialog::IDD, this);
		m_WaitDialog.ShowWindow(SW_HIDE);

		m_WaitDialog.SetCloseCB(WaitCloseCB);
	}


	// Loading state or creation default state.
	// 
	try
	{	

		MultiPaneCtrl::Tabs tabs;
#if defined(DEV_5000)
		tabs.Add(m_MainPosDlg,_T("지역선택"),0);
		tabs.Add(m_EqiipDlg,_T("장비설정"),1);
		tabs.Add(m_DpgDlg,_T("DPG설정"),2);
		tabs.Add(m_DMTMacroDlg,_T("DMT설정"),3);
		tabs.Add(m_PCMacroDlg,_T("매크로설정"),4);
		tabs.Add(m_TimerMacroDlg,_T("외부 접점 설정"),5);
		tabs.Add(m_MMacroDlg,_T("DRM설정"),6);
		tabs.Add(m_DfrDlg,_T("화재설정"),7);
		tabs.Add(m_DLCDlg, _T("DLC설정"), 8);
		tabs.Add(m_EtcDlg,_T("기타"),9);
#else
		tabs.Add(m_MainPosDlg,_T("지역선택"),0);
		tabs.Add(m_EqiipDlg,_T("장비설정"),1);
		tabs.Add(m_DpgDlg,_T("DPG설정"),2);
		tabs.Add(m_PCMacroDlg,_T("매크로설정"),3);
		tabs.Add(m_TimerMacroDlg,_T("Timer설정"),4);
		tabs.Add(m_MMacroDlg,_T("DRM설정"),5);
		tabs.Add(m_DfrDlg,_T("화재설정"),6);
		tabs.Add(m_DLCDlg,_T("DLC설정"),7);
		tabs.Add(m_EtcDlg,_T("기타"),8);
#endif

		if(m_MPCC.LoadState(AfxGetApp(),MAIN_MPCC_STATE,_T("State"),&tabs,false) == false)
		{		
			// create default state.
			HPANE h1 = m_MPCC.ConvertToLine(NULL,true);
			HPANE h2 = m_MPCC.ConvertToLine(h1,false);
			HPANE h3 = m_MPCC.ConvertToLine(h2,true);

			m_MPCC.AddTab(h3,tabs[0]);
			HPANE h4 = m_MPCC.Add(h2);
			m_MPCC.AddTab(h4,tabs[1]);
			m_MPCC.AddTab(h4,tabs[2]);
			m_MPCC.AddTab(h4,tabs[3]);
			m_MPCC.AddTab(h4,tabs[4]);
			m_MPCC.AddTab(h4,tabs[5]);
			m_MPCC.AddTab(h4,tabs[6]);
			m_MPCC.AddTab(h4,tabs[7]);
			m_MPCC.AddTab(h4,tabs[8]);
#if defined(DEV_5000)
			m_MPCC.AddTab(h4,tabs[9]);
#endif
		} else {
			GetLowPA()->Invalidate();
		}
	}
	catch(std::bad_alloc &)
	{	
		return -1;
	}


	return 0;
}



/////////////////////////////////////////////////////////////////////////////
// 
bool CDLLowPASetupDlg::CanDrop(MultiPaneCtrl * /*pCtrl*/, HTAB /*hTabSrc*/, DOCKSIDE side, HPANE /*hPaneDst*/)
{	
	if(side==DOCKSIDE_LEFT) return TRUE;
	else if(side==DOCKSIDE_TOP) return TRUE;
	else if(side==DOCKSIDE_TABS) return TRUE;
	else if(side==DOCKSIDE_BOTTOM) return TRUE;
	else if(side==DOCKSIDE_RIGHT) return TRUE;
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// 
void CDLLowPASetupDlg::OnTabCloseButtonClicked(MultiPaneCtrl * /*pPaneCtrl*/, TabCtrl * /*pTabCtrl*/, CRect const * /*pRect*/, CPoint /*ptScr*/)
{	::MessageBox(m_hWnd,_T("DemoDlg::OnTabCloseButtonClicked"),_T("DemoDlg"),MB_OK);
}
// 
void CDLLowPASetupDlg::OnTabMenuButtonClicked(MultiPaneCtrl * /*pPaneCtrl*/, TabCtrl * /*pTabCtrl*/, CRect const * /*pRect*/, CPoint /*ptScr*/)
{	::MessageBox(m_hWnd,_T("DemoDlg::OnTabMenuButtonClicked"),_T("DemoDlg"),MB_OK);
}

void CDLLowPASetupDlg::OnTabSelected(MultiPaneCtrl *pPaneCtrl, TabCtrl *pTabCtrl, HTAB hTab)
{
	int w_Page=0;

	w_Page = pTabCtrl->GetIndex(hTab);

	if(w_Page >= 0) {
		GetLowPA()->m_SelPage = w_Page;
		
//		m_SelAddr = 1;
//		m_SelItem = 1;

		m_MainPosDlg.Invalidate();
	}

	switch(w_Page)
	{
		case LPA_SEL_EQUIP:
			m_EqiipDlg.PostMessage(WM_SELPAGE,0,0);
			break;
#if defined(DEV_5000)
		case LPA_SEL_DMTMACRO:
			m_DMTMacroDlg.PostMessage(WM_SELPAGE,0,0);
			break;
#endif
		case LPA_SEL_DPG:
			m_DpgDlg.PostMessage(WM_SELPAGE,0,0);
			break;
		case LPA_SEL_PCMACRO:
			m_PCMacroDlg.PostMessage(WM_SELPAGE,0,0);
			break;
		case LPA_SEL_TIMERMACRO:
			m_TimerMacroDlg.PostMessage(WM_SELPAGE,0,0);
			break;
		case LPA_SEL_MMACRO:
			m_MMacroDlg.PostMessage(WM_SELPAGE,0,0);
			break;
		case LPA_SEL_DFR:
			m_DfrDlg.PostMessage(WM_SELPAGE,0,0);
			break;
		case LPA_SEL_DLC:
			m_DLCDlg.PostMessage(WM_SELPAGE, 0, 0);
			break;
		case LPA_SEL_ETC:
			m_EtcDlg.PostMessage(WM_SELPAGE,0,0);
			break;
	}

	TRACE(_T("[%x][%d]\n"),pTabCtrl->GetDlgCtrlID(),pTabCtrl->GetIndex(hTab));	
}
/////////////////////////////////////////////////////////////////////////////


void CDLLowPASetupDlg::SetTabsPosition()
{	
	CWnd *pBaseWnd = GetDlgItem(IDC_TAB);
	// 
	if(pBaseWnd!=NULL)
	{
		CRect rcTab, rcDlg;
		pBaseWnd->GetWindowRect(&rcTab);
		ScreenToClient(&rcTab);
		GetClientRect(&rcDlg);
		// 
		rcTab.right = max(rcTab.left,rcDlg.right-5);
		rcTab.bottom = max(rcTab.top,rcDlg.bottom-4);
		// 
		m_MPCC.MoveWindow(&rcTab);
	}
}

// 
void CDLLowPASetupDlg::AllSplittersActivate(bool active)
{
	for(HPANE h=m_MPCC.GetFirst(); h!=NULL; h=m_MPCC.GetNext(h))
	{
		HPANE parent = m_MPCC.GetParent(h);
		if(parent!=NULL && h!=m_MPCC.GetLastChild(parent))
			m_MPCC.ActiveSplitter(h,active);
	}
}

void CDLLowPASetupDlg::OnDestroy()
{
	m_MPCC.SaveState(AfxGetApp(),MAIN_MPCC_STATE,_T("State"));

	GetLowPA()->StopSerial();


	if(g_titleFont.GetSafeHandle())
	{
		g_titleFont.DeleteObject();
	}

	CDialog::OnDestroy();
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CDLLowPASetupDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	SetTabsPosition();
}


int	CDLLowPASetupDlg::WaitCloseCB(int a_Event)
{
	::GetLowPA()->m_DevStat = DEV_STAT_NORMAL;
	if(a_Event == EV_WAIT_TIMEOUT) {
		if(GetMainWnd()) {
			::GetMainWnd()->MessageBox(_T("장비로부터 응답이 없어 처리가 중단되었습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		}
	} else if(a_Event == EV_WAIT_ERROR) {
		if(GetMainWnd()) {
			::GetMainWnd()->MessageBox(_T("처리중 에러가 발생해 처리가 중단되었습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		}
	}
	return(1);
}


void CDLLowPASetupDlg::ShowWaitDialog(int a_show, int a_timeout)
{

	m_WaitDialog.CenterWindow(this);
	m_WaitDialog.DoWait(a_show, a_timeout);
}


LRESULT CDLLowPASetupDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	int w_Ret=0;
	switch(message) 
	{
		case WM_PROGRESS:
//		DL_LOG("WM_PROGRESS:(%d):(%d)", wParam, lParam);
			switch(wParam) {
				case EV_PROGRESS_START:
					m_WaitDialog.SetRange(0, (short)lParam);
					ShowWaitDialog(1, LPA_TIMOUT_COUNT);
					break;
				case EV_PROGRESS_POS:
					m_WaitDialog.SetPos((short)lParam);
					break;
				case EV_PROGRESS_POLL:
					m_WaitDialog.ResetTimer();
					break;
				case EV_PROGRESS_RANGE:
					m_WaitDialog.SetRange(0, (short)lParam);
					break;
				case EV_PROGRESS_END:
				default:
					CString w_Str;
					ShowWaitDialog(0);
					if(lParam == EV_SEND_OK) {
						w_Str = _T("정상적으로 설정값 저장이 완료되었습니다.");
					} else if(lParam == EV_RECV_OK) {
						w_Str = _T("정상적으로 설정값 읽기가 완료되었습니다.");
					} else if(lParam == EV_SEND_ERROR) {
						w_Str = _T("설정값 저장 중 에러가 발생했습니다. \r\n확인후 재실행해 주시기 바랍니다.");
					} else if(lParam == EV_SEND_ERROR) {
						w_Str = _T("설정값 읽는 중 에러가 발생했습니다. \r\n확인후 재실행해 주시기 바랍니다.");
					} else {
						w_Str = _T("처리중 에러가 발생했습니다. \r\n확인후 재실행해 주시기 바랍니다.");
					}
					if(lParam == EV_SEND_OK) {
						w_Str += _T("\r\n설정값을 파일로 저장하시겠습니까?");
						if(MessageBox(w_Str, _T("실행결과"), MB_YESNO|MB_ICONQUESTION) != IDYES) {
							break;;
						}
						m_EtcDlg.PostMessage(WM_LOCALFILE_SAVE,0,0);
					}else{
						if(lParam == EV_RECV_OK)
						{
							m_EtcDlg.PostMessage(WM_LOCALFILE_CHECK,0,0);
						}
						MessageBox(w_Str, _T("실행결과"), MB_OK | MB_ICONINFORMATION);
					}
					break;
			}
			break;
		case WM_SELPAGE:
			if (m_EqiipDlg.IsWindowVisible())
			{
				m_EqiipDlg.PostMessage(message, wParam, lParam);
			}
#if defined(DEV_5000)
			if (m_PCMacroDlg.IsWindowVisible())
			{
				m_PCMacroDlg.PostMessage(message, wParam, lParam);
			}
#endif
			if (m_DpgDlg.IsWindowVisible())
			{
				m_DpgDlg.PostMessage(message, wParam, lParam);
			}
			if (m_MMacroDlg.IsWindowVisible())
			{
				m_MMacroDlg.PostMessage(message, wParam, lParam);
			}
			if (m_PCMacroDlg.IsWindowVisible())
			{
				m_PCMacroDlg.PostMessage(message, wParam, lParam);
			}
			if (m_TimerMacroDlg.IsWindowVisible())
			{
				m_TimerMacroDlg.PostMessage(message, wParam, lParam);
			}
			if (m_DfrDlg.IsWindowVisible())
			{
				m_DfrDlg.PostMessage(message, wParam, lParam);
			}
			if (m_DLCDlg.IsWindowVisible())
			{
				m_DLCDlg.PostMessage(message, wParam, lParam);
			}
			if (m_EtcDlg.IsWindowVisible())
			{
				m_EtcDlg.PostMessage(message, wParam, lParam);
			}
			break;
	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}

int CDLLowPASetupDlg::run_application( TCHAR* exe, TCHAR* arg, BOOL wait_for_completion )
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	int res;
	int	w_ii = 0;

	TCHAR	fn[1024];
	char	w_Path[1024];

	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;

	if( exe[0] == _T('\\') ) {
		_tcscpy( fn, exe );
	}
	else {
		GetModuleFileName(NULL,fn,1024);
		int i = _tcslen(fn) - 1;
		while( fn[i] != _T('\\') && i > 0 )
			i--;
		fn[i] = 0;

		_tcscat(fn,TEXT("\\"));
		_tcscat(fn,exe);
	}
	WideCharToMultiByte(CP_ACP, 0, fn, 1024, w_Path, 1024, NULL, NULL);

	DL_LOG("=>RUN APP : [%s]", w_Path);

	res = CreateProcess(fn, arg, NULL,	NULL, FALSE, 0, NULL, NULL, &si, &pi);
	if( res ) 
	{
		/*
		if( wait_for_completion ) {
			WaitForSingleObject( pi.hThread,INFINITE );
		}
			
		while(w_ii < 30) 
		{
			Sleep(100);
			w_ii++;
		}

		//ReStart
		OnRestartManager();
		Sleep(100);
		*/

		CloseHandle (pi.hProcess);
		CloseHandle (pi.hThread);
	}

	return res;
}

void CDLLowPASetupDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	lpMMI->ptMinTrackSize.x=1024;
	lpMMI->ptMinTrackSize.y=768;
//	lpMMI->ptMaxTrackSize.x =1000;
//	lpMMI->ptMaxTrackSize.y = 800;

	__super::OnGetMinMaxInfo(lpMMI);
}

BOOL CDLLowPASetupDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//return FALSE;
	return __super::OnEraseBkgnd(pDC);
}
