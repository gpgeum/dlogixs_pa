
// DLLowPASetupDlg.h : 헤더 파일
//

#pragma once

#include "DLMainPosDlg.h"
#include "DLDfrSetDlg.h"
#include "DLPCMacroSetDlg.h"
#include "DLDMTMacroSetDlg.h"
#include "DLDpgSetDlg.h"
#include "DLMMacroSetDlg.h"
#include "DLTimerMacroSetDlg.h"
#include "DLEquipSetDlg.h"
#include "DLEtcSetDlg.h"
#include "DLDLCSetDlg.h"
#include "WaitDialog.h"

#define MULTI_PAIN_CONTROL		3001

class TabDialog : public CDialog
{private:
DECLARE_MESSAGE_MAP()
virtual void OnCancel();
virtual void OnOK();
afx_msg void OnBnClickedButton1();
};
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// 
struct MarkersLayoutA : public DockingMarkers::LayoutPane
{	MarkersLayoutA() : LayoutPane(107,105,		// total size of marker's group.
								  DockingMarkers::MarkerPane( CPoint(0,35),IDB_BITMAP_LEFT,CRect(2,2,34,33) ),		// left marker.
								  DockingMarkers::MarkerPane( CPoint(36,0),IDB_BITMAP_TOP,CRect(2,2,33,34) ),		// top marker.
								  DockingMarkers::MarkerPane( CPoint(71,35),IDB_BITMAP_RIGHT,CRect(2,2,34,33) ),	// right marker.
								  DockingMarkers::MarkerPane( CPoint(36,69),IDB_BITMAP_BOTTOM,CRect(2,2,33,34) ),	// bottom marker.
								  DockingMarkers::MarkerPane( CPoint(15,15),IDB_BITMAP_TABS,CRect(2,2,75,73) ),		// marker of tabs.
								  RGB(255,0,255))	// color of mask (pixels which don't show).
{
}
};
/////////////////////////////////////////////////////////////////////////////
// 
struct MultiPaneCtrlComplex : public MultiPaneCtrl
{	
	MultiPaneCtrlStyle_base styleBase;
	// 
	MultiPaneCtrlStyle_VS2003_client styleVS2003_client;
	MultiPaneCtrlStyle_VS2003_client_custom1 styleVS2003_client_custom1;
	MultiPaneCtrlStyle_VS2003_bars styleVS2003_bars;
	MultiPaneCtrlStyle_VS2003_bars_custom1 styleVS2003_bars_custom1;
	// 
	MultiPaneCtrlStyle_VS2008_client_classic styleVS2008_client_classic;
	MultiPaneCtrlStyle_VS2008_client_blue styleVS2008_client_blue;
	MultiPaneCtrlStyle_VS2008_client_silver styleVS2008_client_silver;
	MultiPaneCtrlStyle_VS2008_client_olive styleVS2008_client_olive;
	MultiPaneCtrlStyle_VS2008_bars_classic styleVS2008_bars_classic;
	MultiPaneCtrlStyle_VS2008_bars_classic_custom1 styleVS2008_bars_classic_custom1;
	MultiPaneCtrlStyle_VS2008_bars_blue styleVS2008_bars_blue;
	MultiPaneCtrlStyle_VS2008_bars_blue_custom1 styleVS2008_bars_blue_custom1;
	MultiPaneCtrlStyle_VS2008_bars_silver styleVS2008_bars_silver;
	MultiPaneCtrlStyle_VS2008_bars_silver_custom1 styleVS2008_bars_silver_custom1;
	MultiPaneCtrlStyle_VS2008_bars_olive styleVS2008_bars_olive;
	MultiPaneCtrlStyle_VS2008_bars_olive_custom1 styleVS2008_bars_olive_custom1;
	// 
	MultiPaneCtrlStyle_VS2010_client styleVS2010_client;
	MultiPaneCtrlStyle_VS2010_bars styleVS2010_bars;
};


// CDLLowPASetupDlg 대화 상자
class CDLLowPASetupDlg : public CDialog,
	public MultiPaneCtrlUserAbility, public MultiPaneCtrlNotify
{
// 생성입니다.
public:
	CDLLowPASetupDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLLOWPASETUP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
	MultiPaneCtrlComplex m_MPCC;

public:
	//메인지역
	CDLMainPosDlg		m_MainPosDlg;


	//설정탭(장치갯수/DFR/DPG/PCMACRO/MMACRO/DLC/ETC)
	CDLDfrSetDlg		m_DfrDlg;
	CDLDpgSetDlg		m_DpgDlg;
	CDLMMacroSetDlg		m_MMacroDlg;
	CDLPCMacroSetDlg	m_PCMacroDlg;
	CDLTimerMacroSetDlg	m_TimerMacroDlg;
	CDLEquipSetDlg		m_EqiipDlg;
	CDLDLCSetDlg		m_DLCDlg;
	CDLEtcSetDlg		m_EtcDlg;
#if defined(DEV_5000)
	CDLDMTMacroSetDlg	m_DMTMacroDlg;
#endif

	CWaitDialog			m_WaitDialog;

	static int			WaitCloseCB(int a_Event);
public:
	void InitControls();

	void	ShowWaitDialog(int a_show, int a_timeout=5000);
	int		run_application( TCHAR* exe, TCHAR* arg, BOOL wait_for_completion );
	void	OnExitSetupApp();

private:
	// MultiPaneCtrlUserAbility.
	virtual bool CanDrop(MultiPaneCtrl *pCtrl, HTAB hTabSrc, DOCKSIDE side, HPANE hPaneDst);
	// 
	// MultiPaneCtrlNotify.
	virtual void OnTabCloseButtonClicked(MultiPaneCtrl *pPaneCtrl, TabCtrl *pTabCtrl, CRect const *pRect, CPoint ptScr);
	virtual void OnTabMenuButtonClicked(MultiPaneCtrl *pPaneCtrl, TabCtrl *pTabCtrl, CRect const *pRect, CPoint ptScr);
	virtual void OnTabSelected(MultiPaneCtrl *pPaneCtrl, TabCtrl *pTabCtrl, HTAB hTab);

private:
	void SetTabsPosition();
	void AllSplittersActivate(bool active);

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};
