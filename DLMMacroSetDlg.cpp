// DLMMacroSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLMMacroSetDlg.h"

#define	GRID_IDX_ADDRNAME			0
#define GRID_IDX_ADDRNUM			1
#define GRID_IDX_ADDRITEMNUM		2


#define	GRID_IDX_ID				0
#define	GRID_IDX_NAME			1
#define GRID_IDX_NUM			2
#define GRID_IDX_MODYN			3


// CDLMMacroSetDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLMMacroSetDlg, CDialog)

CDLMMacroSetDlg::CDLMMacroSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLMMacroSetDlg::IDD, pParent)
{
	m_SelAddr = 1;		// 선택 Devide 주소
	m_SelItem = 1;		// 선택 Device Item(Output)

}

CDLMMacroSetDlg::~CDLMMacroSetDlg()
{
}

void CDLMMacroSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_btnAdd);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_btnDel);
}


BEGIN_MESSAGE_MAP(CDLMMacroSetDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_NOTIFY(NM_CLICK, IDC_LST_DRMDEV_LIST, OnGridAddrClick)
	ON_NOTIFY(NM_CLICK, IDC_LST_DRM_LIST, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_LST_DRM_LIST, OnGridDblClick)
	ON_NOTIFY(GVN_SELCHANGED, IDC_LST_DRM_LIST, OnGridEndSelChange)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_LST_DRM_LIST, OnGridEndEdit)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CDLMMacroSetDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CDLMMacroSetDlg::OnBnClickedButtonDel)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CDLMMacroSetDlg 메시지 처리기입니다.

BOOL CDLMMacroSetDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CDialog::OnEraseBkgnd(pDC);
}

void CDLMMacroSetDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

BOOL CDLMMacroSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	initScreen();
	DrawMMacro();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDLMMacroSetDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDLMMacroSetDlg::initScreen()
{
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;

	w_X = 5;
	w_Y = 5;
	w_W = 350;
	w_H = 170;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_GridAddr.Create(w_ButtonRc, this, IDC_LST_DRMDEV_LIST);
	m_GridAddr.SetListMode(FALSE);
	m_GridAddr.SetColumnResize(TRUE);
	m_GridAddr.SetHeaderSort(FALSE);
	m_GridAddr.SetSingleRowSelection(TRUE);
	m_GridAddr.SetSingleColSelection(TRUE);
	//m_GridDev.SetEditable(m_bEditable);
	m_GridAddr.EnableSelection(TRUE);
	m_GridAddr.SetRowResize(TRUE);
	m_GridAddr.SetColumnResize(TRUE);
	m_GridAddr.EnableTitleTips(FALSE);
	m_GridAddr.GetDefaultCell(FALSE, FALSE)->SetFormat(DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_NOPREFIX|DT_END_ELLIPSIS);	

	w_X = 5;
	w_Y = 180;
	w_W = 350;
	w_H = 460;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_GridDev.Create(w_ButtonRc, this, IDC_LST_DRM_LIST);
	m_GridDev.SetListMode(FALSE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.SetHeaderSort(FALSE);
	m_GridDev.SetSingleRowSelection(TRUE);
	m_GridDev.SetSingleColSelection(TRUE);
	//m_GridDev.SetEditable(m_bEditable);
	m_GridDev.EnableSelection(TRUE);
	m_GridDev.SetRowResize(TRUE);
	m_GridDev.SetColumnResize(TRUE);
	m_GridDev.EnableTitleTips(FALSE);
	m_GridDev.GetDefaultCell(FALSE, FALSE)->SetFormat(DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_NOPREFIX|DT_END_ELLIPSIS);	

	w_X = 50;
	w_Y = 645;
	w_W = 100;
	w_H = 30;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_btnAdd.MoveWindow(&w_ButtonRc);

	w_X = 50+100+30;
	w_Y = 645;
	w_W = 100;
	w_H = 30;
	w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	m_btnDel.MoveWindow(&w_ButtonRc);

	InitAddrGridHeader();
	InitItemGridHeader();
}


void CDLMMacroSetDlg::InitAddrGridHeader()
{
	int nRowNum;
	int	i;
	int w_nW = 0;

	char *Llistcolumn[]={"DRM 주소", "매크로 갯수", "설정갯수"};
	int Lwidth[]       ={100, 120, 120};  

	m_GridAddr.DeleteAllItems();
	m_GridAddr.SetColumnCount(3);
	nRowNum = m_GridAddr.InsertRow(_T(""));
	for (i = 0; i < 3; i++) 
	{		
		m_GridAddr.SetItemText(nRowNum, i, CA2T(Llistcolumn[i]));
		m_GridAddr.SetColumnWidth(i, Lwidth[i]);
	}

	m_GridAddr.SetFixedRowCount(1);
	m_GridAddr.SetRowHeight(0, 30);
	m_GridAddr.SetSingleRowSelection(1);
	m_GridAddr.SetHeaderSort(0);
	m_GridAddr.SetListMode(1);
	m_GridAddr.SetFixedColumnSelection(0);
}

void CDLMMacroSetDlg::InitItemGridHeader()
{
	int nRowNum;
	int	i;
	int w_nW = 0;

	char *Llistcolumn[]={"번호", "설정지역명", "설정갯수", "삭제"};
	int Lwidth[]       ={40, 150, 75, 75};  

	m_GridDev.DeleteAllItems();
	m_GridDev.SetColumnCount(4);
	nRowNum = m_GridDev.InsertRow(_T(""));
	for (i = 0; i < 4; i++) 
	{		
		m_GridDev.SetItemText(nRowNum, i, CA2T(Llistcolumn[i]));
		m_GridDev.SetColumnWidth(i, Lwidth[i]);

		if(i == GRID_IDX_MODYN) {
			m_GridDev.SetCellType(nRowNum, i, RUNTIME_CLASS(CGridCellCheck));
			CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(nRowNum, i);
			if(pCell) {
				pCell->SetCheck(BST_UNCHECKED);
			}
		}
	}

	m_GridDev.SetFixedRowCount(1);
	m_GridDev.SetRowHeight(0, 30);
	m_GridDev.SetSingleRowSelection(1);
	m_GridDev.SetHeaderSort(0);
	m_GridDev.SetListMode(1);
	m_GridDev.SetFixedColumnSelection(0);
}



void CDLMMacroSetDlg::OnModYnClick(int a_Row)
{
	int	w_ii;
	int	w_Flag = 0;
		
	CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(a_Row, GRID_IDX_MODYN);
	if(!pCell) {
		return;
	}
	if(pCell->GetCheck() == BST_CHECKED) {
		pCell->SetCheck(BST_UNCHECKED);
	} else {
		pCell->SetCheck(BST_CHECKED);
		w_Flag = 1;
	}

	if(a_Row == 0) {
		for(w_ii=1;w_ii<m_GridDev.GetRowCount();w_ii++) {
			CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, GRID_IDX_MODYN);
			if(!pCell) {
				continue;
			}
			if(w_Flag) {
				pCell->SetCheck(BST_CHECKED);
			} else {
				pCell->SetCheck(BST_UNCHECKED);
			}
		}
	}
}

void CDLMMacroSetDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	if (pItem->iColumn == GRID_IDX_MODYN)
	{
		OnModYnClick(pItem->iRow);
	}
	
	//DL_LOG("OnGridClick:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
}

void CDLMMacroSetDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}



void CDLMMacroSetDlg::OnGridEndSelChange(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

//DL_LOG("OnGridEndSelChange:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	m_SelItem = pItem->iRow;

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}

	RedrawCount(pItem->iRow);
}


void CDLMMacroSetDlg::OnGridAddrClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;

	//DL_LOG("OnGridAddrClick:row(%d) col(%d)", pItem->iRow, pItem->iColumn);
	if(pItem->iRow > 0) {
		m_SelAddr = pItem->iRow;
	
		DrawMMacroItem(m_SelAddr);

		if(GetTermWnd()) {
			GetTermWnd()->Invalidate();
		}

	}
	
}


void CDLMMacroSetDlg::OnGridEndEdit(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
//DL_LOG("OnGridEndEdit:row(%d) col(%d)", pItem->iRow, pItem->iColumn);

	if( pItem->iColumn == GRID_IDX_NAME && pItem->iRow > 0) {
		CString w_csStr;

		w_csStr = m_GridDev.GetItemText(pItem->iRow, pItem->iColumn);

		m_SelItem = pItem->iRow;
		GetLowPA()->SetMMacroName(CT2A(w_csStr), m_SelAddr, m_SelItem);

	}
}


void CDLMMacroSetDlg::RedrawCount(int a_Row)
{
	int w_Cnt;
	int w_nCol;
	CString w_csStr;

	if(a_Row > 0) {
		w_nCol = GRID_IDX_NUM;
		w_Cnt = GetLowPA()->GetMMacroCount(m_SelAddr, m_SelItem);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);
		m_GridDev.Invalidate();
	}

}


void CDLMMacroSetDlg::RedrawAddrCount(int a_Row)
{
	int w_Cnt;
	int w_nCol;
	CString w_csStr;

	if(a_Row > 0) {
		w_nCol = GRID_IDX_ADDRNUM;
		w_Cnt = GetLowPA()->GetAddrMMacroCount(m_SelAddr);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridAddr.SetItemState(a_Row, w_nCol, m_GridAddr.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		m_GridAddr.SetItemText(a_Row, w_nCol, w_csStr);
		m_GridAddr.Invalidate();

		w_nCol = GRID_IDX_ADDRITEMNUM;
		w_Cnt = GetLowPA()->GetAddrMMacroChannelCount(m_SelAddr);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridAddr.SetItemState(a_Row, w_nCol, m_GridAddr.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
		m_GridAddr.SetItemText(a_Row, w_nCol, w_csStr);
		m_GridAddr.Invalidate();

	}

}


void CDLMMacroSetDlg::DrawMMacro()
{
	CString		w_Str;
	int			w_nCol;
	int			w_Row;
	CString		w_csStr;
	int			w_Cnt = 0;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	m_GridAddr.SetRowCount(LPA_MMACRO_MAX+1);
	for(w_Row = 1; w_Row<=LPA_MMACRO_MAX;w_Row++) {

		w_nCol = GRID_IDX_ADDRNAME;
		w_csStr.Format(_T("DRM %C"), 'A'+ w_Row-1);
		m_GridAddr.SetItemState(w_Row, w_nCol, m_GridAddr.GetItemState(w_Row, w_nCol) | GVIS_READONLY);
		m_GridAddr.SetItemText(w_Row, w_nCol, w_csStr);

		w_nCol = GRID_IDX_ADDRNUM;
		w_Cnt = GetLowPA()->GetAddrMMacroCount(w_Row);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridAddr.SetItemState(w_Row, w_nCol, m_GridAddr.GetItemState(w_Row, w_nCol) | GVIS_READONLY);
		m_GridAddr.SetItemText(w_Row, w_nCol, w_csStr);

		w_nCol = GRID_IDX_ADDRITEMNUM;
		w_Cnt = GetLowPA()->GetAddrMMacroChannelCount(w_Row);
		w_csStr.Format(_T("%d"), w_Cnt);
		m_GridAddr.SetItemState(w_Row, w_nCol, m_GridAddr.GetItemState(w_Row, w_nCol) | GVIS_READONLY);
		m_GridAddr.SetItemText(w_Row, w_nCol, w_csStr);

	}
}


void CDLMMacroSetDlg::DrawMMacroItem(int a_Addr)
{
	int		w_ii;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData || a_Addr < 1) {
		return;
	}

	m_GridDev.SetRowCount(1);
	for(w_ii=0;w_ii<wp_PAData->m_MMacroData[a_Addr-1].m_Cnt;w_ii++) {
		DrawMMacroItem(a_Addr, w_ii+1);
	}
	m_SelItem = 1;
	m_GridDev.SetFocusCell(-1,-1);
	m_GridDev.SetCurSel(0);
	m_GridDev.Invalidate();
}


void CDLMMacroSetDlg::DrawMMacroItem(int a_Addr, int a_Row)
{
	int		w_nCol;
	CString w_csStr;
	int		w_Cnt = 0;

	CString CA2TTemp;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData || a_Addr < 1 || a_Row < 1) {
		return;
	}
	if(m_GridDev.GetRowCount() <= a_Row) {
		m_GridDev.SetRowCount(a_Row+1);
	}

	w_nCol = GRID_IDX_ID;
	w_csStr.Format(_T("%d"), a_Row);
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);

	w_nCol = GRID_IDX_NAME;
	if(wp_PAData->m_MMacroData[a_Addr-1].m_Item[a_Row-1].m_Name[0]) {
		CA2TTemp = CA2T(wp_PAData->m_MMacroData[a_Addr - 1].m_Item[a_Row - 1].m_Name);
		w_csStr.Format(_T("%s"), CA2TTemp);
	} else {
		w_csStr.Format(_T("DRM %C-%d"), 'A'+ a_Addr-1, a_Row);
	}
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);

	w_nCol = GRID_IDX_NUM;
	w_Cnt = GetLowPA()->GetMMacroCount(a_Addr, a_Row);
	w_csStr.Format(_T("%d"), w_Cnt);
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
	m_GridDev.SetItemText(a_Row, w_nCol, w_csStr);

	//Delete
	w_nCol = GRID_IDX_MODYN;
	m_GridDev.SetItemState(a_Row, w_nCol, m_GridDev.GetItemState(a_Row, w_nCol) | GVIS_READONLY);
	m_GridDev.SetCellType(a_Row, w_nCol, RUNTIME_CLASS(CGridCellCheck));
	CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(a_Row, w_nCol);
	if(pCell) {
		pCell->SetCheck(BST_UNCHECKED);
	}
	m_GridDev.Invalidate();
}


void CDLMMacroSetDlg::OnBnClickedButtonAdd()
{
	CString w_Str;
	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	if(m_SelAddr <= 0) {
		return;
	}
	int w_jj = m_SelAddr-1;
	int	w_Item = 0;

	if(wp_PAData->m_DevCnt[LPA_DRM_IDX] <= 0) {
		w_Str.Format(_T("추가할수 없습니다. (DRM 갯수가 0입니다.)"));
		MessageBox(w_Str, _T("실행오류"), MB_OK | MB_ICONWARNING);

		return;
	}

	if(m_GridDev.GetRowCount() > LPA_MMACRO_ITEM) {

		w_Str.Format(_T("더 이상 추가할수 없습니다.(ITEM은 %d개까지만 지정할 수 있습니다."), LPA_MMACRO_ITEM);
		MessageBox(w_Str, _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}

	wp_PAData->m_MMacroData[w_jj].m_Cnt++;
	w_Item = wp_PAData->m_MMacroData[w_jj].m_Cnt-1;

	memset(&wp_PAData->m_MMacroData[w_jj].m_Item[w_Item], 0x00, sizeof(LPA_ITEM_T));
	memset(&wp_PAData->m_MMacroTerm[w_jj][w_Item], 0x00, sizeof(LPA_DFR_TERM_T));

	DrawMMacroItem(w_jj+1, w_Item+1);

	m_GridDev.SetCurSel(w_Item+1);
	m_SelItem = w_Item+1;

	RedrawAddrCount(m_SelAddr);

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}

	GetLowPA()->SetModified(1);

}


void CDLMMacroSetDlg::OnBnClickedButtonDel()
{
	int		w_ii;
	int		w_Cnt = 0;
	int		w_OKCnt = 0;
	CString w_csStr;
	int		w_jj;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	if(m_SelAddr <= 0) {
		return;
	}

	if(m_GridDev.GetRowCount() <= 0) {
		MessageBox(_T("더이상 삭제할 장치가 없습니다."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}

	for(w_ii=m_GridDev.GetRowCount()-1;w_ii>=1;w_ii--) {
		CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, GRID_IDX_MODYN);
		if(pCell) {
			if(pCell->GetCheck()) {
				w_Cnt++;
			}
		}
	}

	if(w_Cnt == 0) {
		MessageBox(_T("삭제할 장치를 선택해주십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	} 

	w_Cnt = 0;
	w_jj = m_SelAddr-1;


	LPA_ITEM_T			w_Item[LPA_ITEM_MAX];
	LPA_MMACRO_TERM_T		w_MMacroTerm[LPA_MMACRO_ITEM];

	memset(w_Item, 0x00, sizeof(w_Item));
	memset(w_MMacroTerm, 0x00, sizeof(w_MMacroTerm));

// DATA부터 정리
	for(w_ii=1; w_ii<m_GridDev.GetRowCount();w_ii++) {
		CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, GRID_IDX_MODYN);
		if(pCell) {
			if(!pCell->GetCheck()) {
				memcpy(&w_Item[w_OKCnt], &wp_PAData->m_MMacroData[w_jj].m_Item[w_ii-1], sizeof(LPA_ITEM_T));
				memcpy(&w_MMacroTerm[w_OKCnt], &wp_PAData->m_MMacroTerm[w_jj][w_ii-1], sizeof(LPA_MMACRO_TERM_T));

				w_OKCnt++;
			}
		}
	}


	memset(&wp_PAData->m_MMacroData[w_jj].m_Item[0], 0x00, sizeof(LPA_ITEM_T) * LPA_ITEM_MAX);
	memset(&wp_PAData->m_MMacroTerm[w_jj][0], 0x00, sizeof(LPA_MMACRO_TERM_T) * LPA_MMACRO_ITEM);

	if(w_OKCnt > 0) {
		memcpy(&wp_PAData->m_MMacroData[w_jj].m_Item[0], &w_Item[0], sizeof(w_Item));
		memcpy(&wp_PAData->m_MMacroTerm[w_jj][0], &w_MMacroTerm[0], sizeof(w_MMacroTerm));
	}


	for(w_ii=m_GridDev.GetRowCount()-1;w_ii>=0;w_ii--) {
		CGridCellCheck *pCell = (CGridCellCheck*) m_GridDev.GetCell(w_ii, GRID_IDX_MODYN);
		if(pCell) {
			if(pCell->GetCheck()) {
				if(w_ii == 0) {
					pCell->SetCheck(BST_UNCHECKED);
				} else {
					m_GridDev.DeleteRow(w_ii);
					w_Cnt++;
				}
			}
		}
	}
	wp_PAData->m_MMacroData[w_jj].m_Cnt = w_OKCnt;
	DrawMMacroItem(m_SelAddr);

	RedrawAddrCount(m_SelAddr);

	if(GetTermWnd()) {
		GetTermWnd()->Invalidate();
	}

	GetLowPA()->SetModified(1);

}

LRESULT CDLMMacroSetDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_SELPAGE:
			m_SelAddr = 1;
			m_SelItem = 1;

			m_GridAddr.SetFocusCell(-1,-1);
			if(m_SelAddr > 0) {
				m_GridAddr.SetCurSel(m_SelAddr);
			} else {
				m_GridAddr.SetCurSel(0);
			}
			DrawMMacroItem(m_SelAddr);
			break;
		case WM_CHANGEDEQUIP:
		case WM_INVALIDATE:
			m_SelAddr = 1;
			m_SelItem = 1;
			DrawMMacro();
			m_GridAddr.SetFocusCell(-1,-1);
			if(m_SelAddr > 0) {
				m_GridAddr.SetCurSel(m_SelAddr);
			} else {
				m_GridAddr.SetCurSel(0);
			}
			DrawMMacroItem(m_SelAddr);
			break;
	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}
void CDLMMacroSetDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	int	  w_X, w_Y, w_W, w_H;
	CRect w_ButtonRc;
	CRect w_Rect;
	GetClientRect(&w_Rect);

	if(m_btnAdd.GetSafeHwnd())
	{	
		w_X = w_Rect.Width()/2 - 120;
		w_Y = w_Rect.bottom - 40;
		w_W = 100;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_btnAdd.MoveWindow(&w_ButtonRc);
	}

	if(m_btnDel.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 + 20;
		w_Y = w_Rect.bottom - 40;
		w_W = 100;
		w_H = 30;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_btnDel.MoveWindow(&w_ButtonRc);
	}

	if(m_GridAddr.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 5;
		w_W = 350;
		w_H = 170;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_GridAddr.MoveWindow(&w_ButtonRc);
	}

	if(m_GridDev.GetSafeHwnd())
	{
		w_X = w_Rect.Width()/2 - 350/2 + 5;
		w_Y = 185;
		w_W = 350;
		w_H = w_Rect.bottom - 50 - 185;
		w_ButtonRc = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		m_GridDev.MoveWindow(&w_ButtonRc);
	}
}
