// DLMainPosDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "DLMainPosDlg.h"


// CDLMainPosDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLMainPosDlg, CDialog)

CDLMainPosDlg::CDLMainPosDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLMainPosDlg::IDD, pParent)
{
}

CDLMainPosDlg::~CDLMainPosDlg()
{
}

void CDLMainPosDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDLMainPosDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDLMainPosDlg 메시지 처리기입니다.

BOOL CDLMainPosDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return FALSE;
	return CDialog::OnEraseBkgnd(pDC);
}

void CDLMainPosDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

BOOL CDLMainPosDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	RedwarControls();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDLMainPosDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDLMainPosDlg::RedwarControls()
{
	CRect w_Rect;

	GetClientRect(w_Rect);

	if(!::IsWindow(m_NodeListView.GetSafeHwnd())) {
		m_NodeListView.Create(NULL, NULL, WS_CHILD | WS_VISIBLE, w_Rect, this, 9999);

	} else {
		m_NodeListView.MoveWindow(w_Rect);
	}
}


BOOL CDLMainPosDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialog::OnCommand(wParam, lParam);
}

int CDLMainPosDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CDLMainPosDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	RedwarControls();
}

void CDLMainPosDlg::OnDestroy()
{
	CDialog::OnDestroy();
}
