#pragma once

#include "NodeListView.h"

// CDLMainPosDlg 대화 상자입니다.

class CDLMainPosDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLMainPosDlg)

public:
	CDLMainPosDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLMainPosDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLMAINPOS_DIALOG };

	CNodeListView	m_NodeListView;

public:
	void RedwarControls();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
};
