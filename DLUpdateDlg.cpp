// DLUpdateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLUpdateDlg.h"

#include "HexDialog.h"
#include "HexConverter.h"

#include "DLUpDateDevice.h"

// CDLUpdateDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDLUpdateDlg, CDialog)

CDLUpdateDlg::CDLUpdateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLUpdateDlg::IDD, pParent)
{
	m_nPort = 0;
	m_nMixerPort = 0;
	m_pEquipInfoDlg = NULL;
	m_pWriteData = NULL;

	m_DmtCnt = 0;
	m_DpgCnt = 0;
	m_DreCnt = 0;
	m_DrmCnt = 0;
	m_DpaCnt = 0;
	m_DmpCnt = 0;
	m_DfrCnt = 0;
	m_DssCnt = 0;
	m_DmxCnt = 0;

	m_bBatchAll = FALSE;
	m_bEquipInfoAll = FALSE;

	//업데이트모드 플래그
	m_bUpdateFlag = FALSE;
	m_bUpdateMixerFlag = FALSE;

	memset(&stBatchAll,0x00, sizeof(stBatchAll));
	memset(&stSendCurrent,0x00, sizeof(stSendCurrent));
	memset(&stAllSendCurrent,0x00,sizeof(stAllSendCurrent));
}

CDLUpdateDlg::~CDLUpdateDlg()
{
	if(m_pEquipInfoDlg->GetSafeHwnd())
	{
		m_pEquipInfoDlg->DestroyWindow();
	}
	m_pEquipInfoDlg = NULL;
}

void CDLUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECKSTATIC_DMT, m_ctrlCSBoxDMT);
	DDX_Control(pDX, IDC_CHECKSTATIC_DSS, m_ctrlCSBoxDSS);
	DDX_Control(pDX, IDC_CHECKSTATIC_DPG, m_ctrlCSBoxDPG);
	DDX_Control(pDX, IDC_CHECKSTATIC_DRM, m_ctrlCSBoxDRM);
	DDX_Control(pDX, IDC_CHECKSTATIC_DPA, m_ctrlCSBoxDPA);
	DDX_Control(pDX, IDC_CHECKSTATIC_DMP, m_ctrlCSBoxDMP);
	DDX_Control(pDX, IDC_CHECKSTATIC_DRE, m_ctrlCSBoxDRE);
	DDX_Control(pDX, IDC_CHECKSTATIC_DFR, m_ctrlCSBoxDFR);
	DDX_Control(pDX, IDC_PROGRESS_UPDATE, m_ctrlProgressUpdate);
	DDX_Text(pDX, IDC_STATIC_STATE, m_szState);
}


BEGIN_MESSAGE_MAP(CDLUpdateDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_UPDATEDLG_BATCH_PROGRESS, OnBatchProgress)
	ON_MESSAGE(WM_UPDATEDLG_BATCH_FINISHED, OnBatchFinished)
	ON_BN_CLICKED(IDC_BUTTON_LOAD_ALL, OnButtonLoadAll)
	ON_BN_CLICKED(IDC_BUTTON_FINISH_BOOT, OnButtonFinishBoot)
	ON_BN_CLICKED(IDC_BUTTON_FINISH_BOOT2, OnButtonFinishBoot2)
	ON_BN_CLICKED(IDC_BUTTON_RESET_BOOT, OnButtonResetBoot)
	ON_BN_CLICKED(IDC_BUTTON_RESET_BOOT2, OnButtonResetBoot2)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE_ALL, OnButtonUpdateAll)
	ON_BN_CLICKED(IDC_BUTTON_RESET_INFO, &CDLUpdateDlg::OnButtonResetInfo)
	ON_WM_SYSCOMMAND()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDLUpdateDlg 메시지 처리기입니다.

BOOL CDLUpdateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	int			w_ii=0;
	CString		strPort=_T("");
	LPA_DATA_T *wp_PAData=NULL;

	if(!m_pEquipInfoDlg->GetSafeHwnd())
	{
		m_pEquipInfoDlg = new CDLEquipInfoDlg;
		m_pEquipInfoDlg->Create(IDD_DLEQUIPINFO_DIALOG,CWnd::GetDesktopWindow());
		//m_pEquipInfoDlg->Create(IDD_DLEQUIPINFO_DIALOG,this);
		m_pEquipInfoDlg->ShowWindow(SW_HIDE);
	}

	if( !::IsWindow(m_WaitingDialog.GetSafeHwnd()) ) 
	{
		m_WaitingDialog.Create(CWaitDialog::IDD, this);
		m_WaitingDialog.ShowWindow(SW_HIDE);
		m_WaitingDialog.SetCloseCB(WaitingCloseCB);
	}

	m_ctrlCSBoxDMT.Init();
	m_ctrlCSBoxDSS.Init();
	m_ctrlCSBoxDPG.Init();
	m_ctrlCSBoxDRM.Init();
	m_ctrlCSBoxDPA.Init();
	m_ctrlCSBoxDMP.Init();
	m_ctrlCSBoxDRE.Init();
	m_ctrlCSBoxDFR.Init();

	m_ctrlCSBoxDPA.EnableWindow(FALSE);

	EnumerateSerialPorts(m_Ports);

	CComboBox *pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_COM_PORT);
	pComboBox->ResetContent();
	
	if(m_Ports.GetSize()>0)
	{
		for (w_ii=0; w_ii<m_Ports.GetSize(); w_ii++)
		{
			strPort.Format(_T("%d"),m_Ports.ElementAt(w_ii));
			pComboBox->AddString(strPort);
			DL_LOG("USE SERAIL PORT : COM%d", m_Ports.ElementAt(w_ii));
		}
	}
	pComboBox->SetCurSel(0);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_COM_PORT2);
	pComboBox->ResetContent();

	EnumerateSerialPorts(m_MixerPorts);

	if(m_MixerPorts.GetSize()>0)
	{
		for (w_ii=0; w_ii<m_MixerPorts.GetSize(); w_ii++)
		{
			strPort.Format(_T("%d"),m_MixerPorts.ElementAt(w_ii));
			pComboBox->AddString(strPort);
			DL_LOG("USE MIXER SERAIL PORT : COM%d", m_MixerPorts.ElementAt(w_ii));
		}
	}
	pComboBox->SetCurSel(0);


	//설정에 저장되어 있는 장치정보 구조체
	wp_PAData = GetPAData();
	if(!wp_PAData) {
		DL_LOG("[CONFIG READ FAILED]");
	}
	else
	{
		m_DmtCnt = 1;
		m_DpgCnt = wp_PAData->m_DevCnt[LPA_DPG_IDX];
		m_DreCnt = wp_PAData->m_DevCnt[LPA_DRE_IDX];
		m_DrmCnt = wp_PAData->m_DevCnt[LPA_DRM_IDX];
		m_DpaCnt = 0;//wp_PAData->m_DevCnt[LPA_DMA_IDX]	<=5000인 경우, 
		m_DmpCnt = wp_PAData->m_DevCnt[LPA_DMP_IDX];
		m_DfrCnt = wp_PAData->m_DevCnt[LPA_DFR_IDX];
		m_DssCnt = wp_PAData->m_DevCnt[LPA_DSS_IDX];
		m_DmxCnt = 1;
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPG);
	pComboBox->ResetContent();
	if(m_DpgCnt>0)
	{
		for (w_ii=0; w_ii<m_DpgCnt; w_ii++)
		{
			strPort.Format(_T("%d"),w_ii+1);
			pComboBox->AddString(strPort);
		}
		DeviceStatusChange(EEQUIP_TYPE_DPG,TRUE);
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DPG,FALSE);
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRE);
	pComboBox->ResetContent();
	if(m_DreCnt>0){
		DeviceStatusChange(EEQUIP_TYPE_DRE,TRUE);
		if(m_DpgCnt>0)
		{
			pComboBox->AddString(_T("0"));
			for (w_ii=0; w_ii<UPDATE_DRE_MAX; w_ii++)
			{
				strPort.Format(_T("%d"),w_ii+1);
				pComboBox->AddString(strPort);
			}
		}else{
			pComboBox->EnableWindow(FALSE);
		}
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DRE,FALSE);
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG);
	pComboBox->ResetContent();
	if(m_DrmCnt>0)
	{
		DeviceStatusChange(EEQUIP_TYPE_DRM,TRUE);
		if(m_DpgCnt>0)
		{
			pComboBox->AddString(_T("0"));
			for (w_ii=0; w_ii<6; w_ii++)
			{
				strPort.Format(_T("%d"),w_ii+1);
				pComboBox->AddString(strPort);
			}	
		}
		else
		{
			pComboBox->EnableWindow(FALSE);
		}
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DRM,FALSE);
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DRE);
	pComboBox->ResetContent();
	if(m_DrmCnt>0)
	{
		pComboBox->AddString(_T("0"));
		for (w_ii=0; w_ii<8; w_ii++)
		{
			strPort.Format(_T("%d"),w_ii+1);
			pComboBox->AddString(strPort);
		}
		DeviceStatusChange(EEQUIP_TYPE_DRM,TRUE);
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DRM,FALSE);
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPA);
	pComboBox->ResetContent();
	if(m_DpaCnt>0)
	{	
		for (w_ii=0; w_ii<m_DpaCnt; w_ii++)
		{
			strPort.Format(_T("%d"),w_ii+1);
			pComboBox->AddString(strPort);
		}
		DeviceStatusChange(EEQUIP_TYPE_DPA,TRUE);
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DPA,FALSE);
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DMP);
	pComboBox->ResetContent();
	if(m_DmpCnt>0)
	{
		for (w_ii=0; w_ii<m_DmpCnt; w_ii++)
		{
			strPort.Format(_T("%d"),w_ii+1);
			pComboBox->AddString(strPort);
		}
		DeviceStatusChange(EEQUIP_TYPE_DMP,TRUE);
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DMP,FALSE);
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DSS);
	pComboBox->ResetContent();
	if(m_DssCnt>0)
	{
		for (w_ii=0; w_ii<m_DssCnt; w_ii++)
		{
			strPort.Format(_T("%d"),w_ii+1);
			pComboBox->AddString(strPort);
		}
		DeviceStatusChange(EEQUIP_TYPE_DSS,TRUE);
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DSS,FALSE);
	}

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DFR);
	pComboBox->ResetContent();
	if(m_DfrCnt>0)
	{
		for (w_ii=0; w_ii<m_DfrCnt; w_ii++)
		{
			strPort.Format(_T("%d"),w_ii+1);
			pComboBox->AddString(strPort);
		}
		DeviceStatusChange(EEQUIP_TYPE_DFR,TRUE);
	}
	else
	{
		DeviceStatusChange(EEQUIP_TYPE_DFR,FALSE);
	}

	OnChangeConnect();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDLUpdateDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDLUpdateDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

LRESULT CDLUpdateDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch(message)
	{
		case WM_CHANGEDSTATUS:
			DrawStatus();
			break;
		case WM_PROGRESS:
			switch(wParam) 
			{
				case EV_PROGRESS_START:
					break;
				case EV_PROGRESS_POS:
					m_ctrlProgressUpdate.SetPos((int)lParam);
					//m_ctrlProgressUpdate.Invalidate();
					break;
				case EV_PROGRESS_END:
					m_szState = _T("");
					UpdateData(FALSE);
					m_ctrlProgressUpdate.SetPos(0);
					break;
				default:
					break;
			}
			break;
		case WM_UPDATE_INFO:
			if(wParam == DEV_UPDATE_END)
			{
				//전제 정보조회가 완료된경우, 윈도우 조작가능하도록 해제처리
				MessageBox(_T("정보조회가 완료되었습니다."));
				EnableWindow(TRUE);
				break;
			}
			else if(wParam == DEV_UPDATE_ENDERROR)
			{
				MessageBox(_T("장치의 연결상태를 확인하여 주십시요."));
				EnableWindow(TRUE);
			}
			OnSendEquipInfo((int)wParam,(int)lParam);
			break;
		case WM_UPDATE_DATA:
			if(wParam == DEV_UPDATE_END)
			{
				if(m_bBatchAll)
				{
					//일괄 업그레이드가 완료된경우, 윈도우 조작가능하도록 해제처리
					MessageBox(_T("일괄 업그레이드가 완료되었습니다."));
					OnBatchUpdateAllEnd();
					m_bBatchAll = FALSE;
				}else{
					KillTimer(WM_TIMER_SENDFLASHCMD);
					//개별 업그레이드가 완료된경우, 윈도우 조작가능하도록 해제처리
					MessageBox(_T("업그레이드가 완료되었습니다."));
				}
				m_szState = _T("");
				m_nSendCur = 0;
				UpdateData(FALSE);
				EnableWindow(TRUE);				
				break;
			}
			OnBatchUpdateNext((int)wParam,(int)lParam);
			break;
	}


	return CDialog::DefWindowProc(message, wParam, lParam);
}

void CDLUpdateDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

BOOL CDLUpdateDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CDialog::OnEraseBkgnd(pDC);
}

void CDLUpdateDlg::DrawStatus()
{
	char	w_Str[MAX_PATH];
	char	w_DevStr[MAX_PATH];

	if(GetLowPA()->IsUpdateSerialOpened()) 
	{
		dl_strcpy(w_Str, "COM 포트상태 : Opened");
	} 
	else 
	{
		dl_strcpy(w_Str, "COM 포트상태 : Closed");
	}

	if(GetLowPA()->GetUpdateDeviceStatus()) 
	{
		dl_strcpy(w_DevStr, "DEVICE 상태 : Connected");
	} 
	else 
	{
		dl_strcpy(w_DevStr, "DEVICE 상태 : Disconnected");
	}

	DL_LOG("[UPDATE] : [%s:%s]", w_Str, w_DevStr);
	//UpdateData(FALSE);

}

void CDLUpdateDlg::Reload()
{
#if 0
	CMainFrame* pMainFrame = ((CMainFrame*)AfxGetMainWnd());
	CPAConfigDoc* pDoc = (CPAConfigDoc*)pMainFrame->GetActiveDocument();
	/*CPAConfigDoc**/ pDoc = GetDocument();
	m_PASheet.Reload(pDoc->m_Config, pMainFrame->GetSelectedSetIndex());
#endif
}

void CDLUpdateDlg::DeviceStatusChange(int a_DevType, int a_Status)
{
	CComboBox	*pComboBox=NULL;
	CEdit		*pEdit=NULL;
	CButton		*pBtnFlash=NULL;
	CButton		*pBtnVerify=NULL;
	CButton		*pBtnUpdate=NULL;
	CButton		*pBtnInfo=NULL;

	switch(a_DevType)
	{
		case EEQUIP_TYPE_DMT:
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DMT_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DMT_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DMT_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DMT_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DMT_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DSS:
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DSS);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DSS_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DSS_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DSS_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DSS_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DSS_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DPG:
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPG);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DPG_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DPG_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DPG_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DPG_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DPG_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DRM:
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DRE);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DRM_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DRM_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DRM_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DRM_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DRM_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DPA:
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPA);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DPA_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DPA_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DPA_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DPA_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DPA_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DMP:
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DMP);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DMP_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DMP_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DMP_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DMP_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DMP_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DRE:
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRE);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DRE_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DRE_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DRE_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DRE_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DRE_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DFR:
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DFR);
			if(pComboBox)
			{
				pComboBox->EnableWindow(a_Status);
			}
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DFR_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DFR_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DFR_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DFR_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DFR_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
		case EEQUIP_TYPE_DMX:
			pEdit = (CEdit*)GetDlgItem(IDC_EDIT_EQP_DMX_FLASH);
			if(pEdit)
			{
				pEdit->EnableWindow(a_Status);
			}
			pBtnFlash = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_EQP_DMX_FLASH);
			if(pBtnFlash)
			{
				pBtnFlash->EnableWindow(a_Status);
			}
			pBtnVerify = (CButton*)GetDlgItem(IDC_BUTTON_VERIFY_EQP_DMX_FLASH);
			if(pBtnVerify)
			{
				pBtnVerify->EnableWindow(a_Status);
			}
			pBtnUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DMX_FLASH);
			if(pBtnUpdate)
			{
				pBtnUpdate->EnableWindow(a_Status);
			}
			pBtnInfo = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_EQP_DMX_INFO);
			if(pBtnInfo)
			{
				pBtnInfo->EnableWindow(a_Status);
			}
			break;
	}

}

LRESULT CDLUpdateDlg::OnBatchProgress(WPARAM wParam, LPARAM lParam)
{
	return TRUE;
}

LRESULT CDLUpdateDlg::OnBatchFinished(WPARAM wParam, LPARAM lParam)
{
	return TRUE;
}

BOOL CDLUpdateDlg::FileExist(LPCTSTR lpPathName, BOOL bMsg)
{
    HANDLE hFind;
    WIN32_FIND_DATA fd;
	
    if ((hFind = ::FindFirstFile(lpPathName, &fd)) != INVALID_HANDLE_VALUE) 
	{
        FindClose (hFind);
        return TRUE;
    }
	
	/*
	if(bMsg && lstrlen(lpPathName) > 0)
	{
	m_strTemp.Format("%s이 존재하지 않습니다.", lpPathName);
	}   
	*/
    return FALSE;
}

HBRUSH CDLUpdateDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	static HBRUSH s_hbrWhite = CreateSolidBrush(RGB(255,255,255));

	// TODO:  여기서 DC의 특성을 변경합니다.
	switch(pWnd->GetDlgCtrlID())
	{
		case IDC_STATIC:

		case IDC_CHECKSTATIC_DMT:
		case IDC_CHECKSTATIC_DSS:
		case IDC_CHECKSTATIC_DPG:
		case IDC_CHECKSTATIC_DRM:
		case IDC_CHECKSTATIC_DPA:
		case IDC_CHECKSTATIC_DMP:
		case IDC_CHECKSTATIC_DRE:
		case IDC_CHECKSTATIC_DFR:

		//case EEQUIP_TYPE_DLR:
		//case EEQUIP_TYPE_DFC:
		//case EEQUIP_TYPE_DPS:
		//case EEQUIP_TYPE_MIX:

			// pDC->SetBkMode(TRANSPARENT);
			// return (HBRUSH)GetStockObject(WHITE_BRUSH);
			pDC->SetBkColor(RGB(255,255,255));
			return s_hbrWhite;
			break;

		default:
			// pDC->SetTextColor(RGB(0,0,0));
			// hbr = CreateSolidBrush(RGB(182,182,182));
			break;
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CDLUpdateDlg::EnumerateSerialPorts(CUIntArray& ports)
{
	CString str=_T("");
	//Make sure we clear out any elements which may already be in the array
	ports.RemoveAll();

	//Determine what OS we are running on
	OSVERSIONINFO osvi;
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	BOOL bGetVer = GetVersionEx(&osvi);

	//On NT use the QueryDosDevice API
	if (bGetVer && (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT))
	{
		//Use QueryDosDevice to look for all devices of the form COMx. This is a better
		//solution as it means that no ports have to be opened at all.
		TCHAR szDevices[65535];
		DWORD dwChars = QueryDosDevice(NULL, szDevices, 65535);
		if (dwChars)
		{
			int i=0;

			for (;;)
			{
				//Get the current device name
				TCHAR* pszCurrentDevice = &szDevices[i];
				str.Format(_T("%s"),szDevices);
				//If it looks like "COMX" then
				//add it to the array which will be returned
				int nLen = _tcslen(pszCurrentDevice);
				if (nLen > 3 && _tcsnicmp(pszCurrentDevice, _T("COM"), 3) == 0)
				{
					//Work out the port number
					int nPort = _ttoi(&pszCurrentDevice[3]);
					ports.Add(nPort);
				}

				// Go to next NULL character
				while(szDevices[i] != _T('\0'))
					i++;

				// Bump pointer to the next string
				i++;

				// The list is double-NULL terminated, so if the character is
				// now NULL, we're at the end
				if (szDevices[i] == _T('\0'))
					break;
			}
		}
		else
		{
			DL_LOG("Failed in call to QueryDosDevice, GetLastError:%d", GetLastError());
		}
	}
	else
	{
		//On 95/98 open up each port to determine their existence

		//Up to 255 COM ports are supported so we iterate through all of them seeing
		//if we can open them or if we fail to open them, get an access denied or general error error.
		//Both of these cases indicate that there is a COM port at that number. 
		for (UINT i=1; i<256; i++)
		{
			//Form the Raw device name
			CString sPort;
			sPort.Format(_T("\\\\.\\COM%d"), i);

			//Try to open the port
			BOOL bSuccess = FALSE;
			HANDLE hPort = ::CreateFile(sPort, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
			if (hPort == INVALID_HANDLE_VALUE)
			{
				DWORD dwError = GetLastError();

				//Check to see if the error was because some other app had the port open or a general failure
				if (dwError == ERROR_ACCESS_DENIED || dwError == ERROR_GEN_FAILURE)
					bSuccess = TRUE;
			}
			else
			{
				//The port was opened successfully
				bSuccess = TRUE;

				//Don't forget to close the port, since we are going to do nothing with it anyway
				CloseHandle(hPort);
			}

			//Add the port number to the array which will be returned
			if (bSuccess)
			{
				ports.Add(i);
			}
		}
	}
}

void CDLUpdateDlg::ClearFilePathAll()
{
	GetDlgItem(IDC_EDIT_EQP_DMT_FLASH)->SetWindowText(_T(""));
	GetDlgItem(IDC_EDIT_EQP_DSS_FLASH)->SetWindowText(_T(""));
	GetDlgItem(IDC_EDIT_EQP_DPG_FLASH)->SetWindowText(_T(""));
	
	GetDlgItem(IDC_EDIT_EQP_DRM_FLASH)->SetWindowText(_T(""));
	GetDlgItem(IDC_EDIT_EQP_DPA_FLASH)->SetWindowText(_T(""));
	GetDlgItem(IDC_EDIT_EQP_DMP_FLASH)->SetWindowText(_T(""));

	GetDlgItem(IDC_EDIT_EQP_DRE_FLASH)->SetWindowText(_T(""));
	GetDlgItem(IDC_EDIT_EQP_DFR_FLASH)->SetWindowText(_T(""));

	GetDlgItem(IDC_EDIT_EQP_DMX_FLASH)->SetWindowText(_T(""));
}

void CDLUpdateDlg::FinishBatchAll(BOOL bSuccess)
{
	m_bBatchAll = FALSE;
	//GetDlgItem(IDC_BUTTON_UPDATE_ALL)->EnableWindow(TRUE);

	if(bSuccess)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("일괄 업데이트가 완료되었습니다."));
		//m_szState = _T("일괄 업데이트가 완료되었습니다.");
		//MessageBox(g_LangTable[132]);
		//m_szState = g_LangTable[132];
		UpdateData(FALSE);
	} 
	else 
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("일괄 업데이트가 실패하였습니다."));
		//m_szState = _T("일괄 업데이트가 실패하였습니다.");
		//MessageBox(g_LangTable[133]);
	}
}

void CDLUpdateDlg::OnButtonEquipInfo(UINT nID)
{
	CString     strNumber = _T("");
	EEquipType	eEquip = 0;
	int			nIndex = 0;
	int			nCmdFlag = DEV_UPDATE_NORMAL;
	DWORD		nEquipNo = 0;
	DWORD		nDpgNo = 0;

	if(!m_bEquipInfoAll)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("전체조회명령을 먼저 수행 하신 후, 개별조회명령을 이용하실 수 있습니다."));
		//MessageBox(g_LangTable[113]);
		return;
	}

	memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
	//--------------------------------------------------------//
	//		EQUIP INFO

	if(nID == IDC_BUTTON_UPDATE_EQP_DMX_INFO)
	{
		eEquip = EEQUIP_TYPE_DMX;
		nEquipNo = 1;

		if( !GetLowPA()->IsMixerSerialOpened() )
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("메인장치와 연결되는 믹서 통신포트를 열 수 없습니다."));
			//MessageBox(g_LangTable[113]);
			return;
		}


		EnableWindow(FALSE);

		if( GetLowPA()->IsMixerSerialOpened() )
		{
			//RequestInfo( GET_SET_NUM(), eEquip, nEquipNo );
			GetLowPA()->SendMixerCmd(CMD_MIXER_INFO,0x0,0x0,(BYTE)nDpgNo,0x0,(BYTE)eEquip,(BYTE)nEquipNo);
		}
	}
	else
	{	
		switch(nID)
		{
			case IDC_BUTTON_UPDATE_EQP_DMT_INFO:
				eEquip = EEQUIP_TYPE_DMT;
				nEquipNo = 1;
				nCmdFlag = DEV_UPDATE_DMT;
				break;
			case IDC_BUTTON_UPDATE_EQP_DPG_INFO:
				eEquip = EEQUIP_TYPE_DPG;
				nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPG))->GetCurSel();
				if( nIndex != CB_ERR ){
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPG))->GetLBText(nIndex,strNumber);
					nEquipNo = dl_atoi1(CT2A(strNumber));
				}else{
					MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
					return;
				}
				nCmdFlag = DEV_UPDATE_DPG;
				break;
			case IDC_BUTTON_UPDATE_EQP_DSS_INFO:
				eEquip = EEQUIP_TYPE_DSS;
				nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DSS))->GetCurSel();
				if( nIndex != CB_ERR ){
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DSS))->GetLBText(nIndex,strNumber);
					nEquipNo = dl_atoi1(CT2A(strNumber));
				}else{
					MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
					return;
				}
				nCmdFlag = DEV_UPDATE_DSS;
				break;
			case IDC_BUTTON_UPDATE_EQP_DRM_INFO:
				eEquip = EEQUIP_TYPE_DRM;
				nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DRE))->GetCurSel();
				if( nIndex != CB_ERR ){
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DRE))->GetLBText(nIndex,strNumber);
					nEquipNo = dl_atoi1(CT2A(strNumber));
					nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG))->GetCurSel();
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG))->GetLBText(nIndex,strNumber);
					nDpgNo = dl_atoi1(CT2A(strNumber));
				}else{
					MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
					return;
				}
				nCmdFlag = DEV_UPDATE_DRM;
				break;
			case IDC_BUTTON_UPDATE_EQP_DPA_INFO:
				eEquip = EEQUIP_TYPE_DPA;
				nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPA))->GetCurSel();
				if( nIndex != CB_ERR ){
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPA))->GetLBText(nIndex,strNumber);
					nEquipNo = dl_atoi1(CT2A(strNumber));
				}else{
					MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
					return;
				}
				nCmdFlag = DEV_UPDATE_DPA;
				break;
			case IDC_BUTTON_UPDATE_EQP_DMP_INFO:
				eEquip = EEQUIP_TYPE_DMP;
				nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DMP))->GetCurSel();
				if( nIndex != CB_ERR ){
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DMP))->GetLBText(nIndex,strNumber);
					nEquipNo = dl_atoi1(CT2A(strNumber));
				}else{
					MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
					return;
				}
				nCmdFlag = DEV_UPDATE_DMP;
				break;
			case IDC_BUTTON_UPDATE_EQP_DRE_INFO:
				eEquip = EEQUIP_TYPE_DRE;
				nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRE))->GetCurSel();
				if( nIndex != CB_ERR ){
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRE))->GetLBText(nIndex,strNumber);
					nEquipNo = dl_atoi1(CT2A(strNumber));
				}else{
					MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
					return;
				}
				nCmdFlag = DEV_UPDATE_DRE;
				break;
			case IDC_BUTTON_UPDATE_EQP_DFR_INFO:
				eEquip = EEQUIP_TYPE_DFR;
				nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DFR))->GetCurSel();
				if( nIndex != CB_ERR ){
					((CComboBox*)GetDlgItem(IDC_COMBO_EQP_DFR))->GetLBText(nIndex,strNumber);
					nEquipNo = dl_atoi1(CT2A(strNumber));
				}else{
					MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
					return;
				}
				nCmdFlag = DEV_UPDATE_DFR;
				break;
		}

		if( !GetLowPA()->IsUpdateSerialOpened() )
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("메인장치와 연결되는 통신포트를 열 수 없습니다."));
			//MessageBox(g_LangTable[113]);
			return;
		}

		if( !GetCheckEquipInfo(eEquip,nEquipNo,nDpgNo) )
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("장치정보를 확인하신 후, \n올바른 장비번호(DRM인경우, DPG/DRE출력번호)를 선택하여, 정보조회를 하시기바랍니다."));
			//MessageBox(g_LangTable[113]);
			return;
		}


		switch(eEquip)
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
		case EEQUIP_TYPE_DMT:
			m_szState =			_T("DMT 정보조회 중입니다.");				
			break;
		case EEQUIP_TYPE_DSS:
			m_szState.Format(_T("DSS %d 정보조회 중입니다."),nEquipNo);
			break;
		case EEQUIP_TYPE_DPG:
			m_szState =			_T("DPG 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DRM:
			m_szState =			_T("DRM 정보조회 중입니다.");
			break;
		case EEQUIP_TYPE_DPA:
			m_szState =			_T("DPA 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DMP:
			m_szState.Format(_T("DMP %d 정보조회 중입니다."),nEquipNo);
			break;
		case EEQUIP_TYPE_DRE:
			m_szState =			_T("DRE 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DFR:
			m_szState.Format(_T("DFR %d 정보조회 중입니다."),nEquipNo);
			break;
		case EEQUIP_TYPE_DMX:
			m_szState =			_T("DMX 를 정보조회 중입니다.");				
			break;
		default:						
			return;
		}
		UpdateData(FALSE);

		EnableWindow(FALSE);

		//현재 전송명령 구조체
		stSendCurrent.nSetNo = 1;
		stSendCurrent.nEquipType = eEquip;
		stSendCurrent.nEquipNo = nEquipNo;
		stSendCurrent.nDpgNo = nDpgNo;
		stSendCurrent.nReTry++;

		//GetLowPA()->m_SendUpdateInfo = 1;

		OnUpdateInfoClear(stSendCurrent.nEquipType, stSendCurrent.nEquipNo, stSendCurrent.nDpgNo);

		GetLowPA()->m_UpdateDevStat = DEV_STAT_NORMAL;
		GetLowPA()->m_UpdateFlag = nCmdFlag;
		GetLowPA()->SendUpdateCmd(CMD_UPDATE_INFO,0x0,0x0,(BYTE)nDpgNo<<1,0x0,(BYTE)eEquip,(BYTE)nEquipNo);

		m_ctrlProgressUpdate.SetRange32(0, GetLowPA()->m_SendUpdateInfo);

		SetTimer(WM_TIMER_SENDCMD,UPDATE_INFO_TIMEVALUE,NULL);
	}

	//정보조회 명령 전송후 장치정보 다이얼로그 팝업
	OnButtonEquipInfoDialog();
}

void CDLUpdateDlg::OnButtonLoadFile(UINT nID)
{
	CString szFilePath;
	CString szFileName;

	CString path = _T("");
	TCHAR	pszPathName[_MAX_PATH];

	//--------------------------------------------------------//
	//		LOAD FLASH
	switch(nID)
	{
		case IDC_BUTTON_LOAD_EQP_DMT_FLASH:
		case IDC_BUTTON_LOAD_EQP_DPG_FLASH:
		case IDC_BUTTON_LOAD_EQP_DSS_FLASH:
		case IDC_BUTTON_LOAD_EQP_DRM_FLASH:
		case IDC_BUTTON_LOAD_EQP_DPA_FLASH:
		case IDC_BUTTON_LOAD_EQP_DMP_FLASH:
		case IDC_BUTTON_LOAD_EQP_DRE_FLASH:
		case IDC_BUTTON_LOAD_EQP_DFR_FLASH:
		case IDC_BUTTON_LOAD_EQP_DMX_FLASH:
		{
#ifdef _DEBUG
			const BOOL VistaStyle = FALSE;  
#else
			const BOOL VistaStyle = TRUE;  
#endif

			CFileDialog dlg(TRUE, _T("hex"), _T("*.hex"), OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT /*| OFN_NOCHANGEDIR *//*|OFN_ALLOWMULTISELECT*/,
				//ADD: Multi-Language String Table @HCH:2010.10.26
				_T("HEX 파일 (*.hex)|*.hex|모든 파일 (*.*)|*.*|")/*, VistaStyle*/);
			//g_LangTable[109], VistaStyle);

			::GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH); 
			PathRemoveFileSpec(pszPathName);
			szFilePath.Format(_T("%s"),pszPathName);
			path.Format(_T("%s\\FIRMWARE\\APP"),szFilePath);

			dlg.GetOFN().lpstrInitialDir = (LPCTSTR)path;

			if(IDOK == dlg.DoModal())
			{
				szFilePath = dlg.GetPathName();
				szFileName = dlg.GetFileName();

				switch(nID)
				{
					case IDC_BUTTON_LOAD_EQP_DMT_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DMT_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DSS_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DSS_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DPG_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DPG_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DRM_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DRM_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DPA_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DPA_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DMP_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DMP_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DRE_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DRE_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DFR_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DFR_FLASH)->SetWindowText(szFilePath);		
						break;
					case IDC_BUTTON_LOAD_EQP_DMX_FLASH:		
						GetDlgItem(IDC_EDIT_EQP_DMX_FLASH)->SetWindowText(szFilePath);
						break;
				}

//#ifdef _DEBUG
				if(dlg.GetFileExt().MakeUpper() == _T("BIN"))
				{

				}
				else if(dlg.GetFileExt().MakeUpper() == _T("HEX"))
				{
					BYTE abyBinData[MAX_SIZE_FIRMWARE];
					UINT nLen = 0;
					if(Hex2Bin(szFilePath, abyBinData, MAX_SIZE_FIRMWARE, &nLen) == ERROR_SUCCESS)
					{
						CHexDialog hexdlg(abyBinData, nLen, this);
						hexdlg.DoModal();
					}
				}
//#endif
			}
		}
		break;
	}
}

void CDLUpdateDlg::OnButtonVerifyFlash(UINT nID)
{
	CString		szFilePath = _T("");
	CString		szFileName = _T("");
	CString		strEquipNo = _T("");
	CString		strDpgNo = _T("");
	CString		strEquipName = _T("");
	CString		strContent = _T("");
	EEquipType	eEquip;
	DWORD		nEquipNo = 1;
	DWORD		nDpgNo = 0;
	int			w_ENoIndex=0;
	int			w_InfoFlag=FALSE;
	int			w_Error=FALSE;
	BYTE		w_Version[4];
	BYTE		w_DevVersion[4];

	CComboBox	*pComboBox = NULL;
	memset(w_Version,0x00,sizeof(w_Version));
	memset(w_DevVersion,0x00,sizeof(w_DevVersion));

	switch(nID)
	{
		case IDC_BUTTON_VERIFY_EQP_DMT_FLASH:		
			eEquip = EEQUIP_TYPE_DMT;		
			nEquipNo = 1;
			if(nEquipNo>0)
			{
				w_ENoIndex = nEquipNo - 1;
			}else{
				w_ENoIndex = nEquipNo;
			}
			GetDlgItem(IDC_EDIT_EQP_DMT_FLASH)->GetWindowText(szFilePath);

			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 3);

			if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DSS_FLASH:
			eEquip = EEQUIP_TYPE_DSS;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DSS);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				w_Error = TRUE;
			}
			if(nEquipNo>0)
			{
				w_ENoIndex = nEquipNo - 1;
			}else{
				w_ENoIndex = nEquipNo;
			}
			
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nApplVer) + 3);

			GetDlgItem(IDC_EDIT_EQP_DSS_FLASH)->GetWindowText(szFilePath);
			if( GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DPG_FLASH:		
			eEquip = EEQUIP_TYPE_DPG;		
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPG);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				w_Error = TRUE;
			}
			if(nEquipNo>0)
			{
				w_ENoIndex = nEquipNo - 1;
			}else{
				w_ENoIndex = nEquipNo;
			}
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nApplVer) + 3);
			GetDlgItem(IDC_EDIT_EQP_DPG_FLASH)->GetWindowText(szFilePath);

			if( GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DRM_FLASH:
			eEquip = EEQUIP_TYPE_DRM;

			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strDpgNo);
				nDpgNo = dl_atoi1(CT2A(strDpgNo));
			}else{
				w_Error = TRUE;
			}

			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DRE);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				w_Error = TRUE;
			}
			w_ENoIndex = nEquipNo+(nDpgNo)*9;
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nApplVer) + 3);
			GetDlgItem(IDC_EDIT_EQP_DRM_FLASH)->GetWindowText(szFilePath);

			if( GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DPA_FLASH:		
			eEquip = EEQUIP_TYPE_DPA;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPA);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				w_Error = TRUE;
			}
			if(nEquipNo>0)
			{
				w_ENoIndex = nEquipNo - 1;
			}else{
				w_ENoIndex = nEquipNo;
			}
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nApplVer) + 3);
			GetDlgItem(IDC_EDIT_EQP_DPA_FLASH)->GetWindowText(szFilePath);
			if( GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DMP_FLASH:		
			eEquip = EEQUIP_TYPE_DMP;		
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DMP);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				w_Error = TRUE;
			}
			if(nEquipNo>0)
			{
				w_ENoIndex = nEquipNo - 1;
			}else{
				w_ENoIndex = nEquipNo;
			}
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nApplVer) + 3);
			GetDlgItem(IDC_EDIT_EQP_DMP_FLASH)->GetWindowText(szFilePath);
			if( GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DRE_FLASH:		
			eEquip = EEQUIP_TYPE_DRE;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRE);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				w_Error = TRUE;
			}
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][nEquipNo].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][nEquipNo].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][nEquipNo].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DRE_IDX][nEquipNo].nApplVer) + 3);
			GetDlgItem(IDC_EDIT_EQP_DRE_FLASH)->GetWindowText(szFilePath);
			if( GetLowPA()->m_EquipInfo[LPA_DRE_IDX][nEquipNo].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DRE_IDX][nEquipNo].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DFR_FLASH:		
			eEquip = EEQUIP_TYPE_DFR;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DFR);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				w_Error = TRUE;
			}
			if(nEquipNo>0)
			{
				w_ENoIndex = nEquipNo - 1;
			}else{
				w_ENoIndex = nEquipNo;
			}
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nApplVer) + 3);
			GetDlgItem(IDC_EDIT_EQP_DFR_FLASH)->GetWindowText(szFilePath);
			if( GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		case IDC_BUTTON_VERIFY_EQP_DMX_FLASH:
			eEquip = EEQUIP_TYPE_DMX;
			nEquipNo = 1;
			if(nEquipNo>0)
			{
				w_ENoIndex = nEquipNo - 1;
			}else{
				w_ENoIndex = nEquipNo;
			}
			w_DevVersion[0] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nApplVer) + 0);
			w_DevVersion[1] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nApplVer) + 1);
			w_DevVersion[2] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nApplVer) + 2);
			w_DevVersion[3] = *(((BYTE*)&GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nApplVer) + 3);
			GetDlgItem(IDC_EDIT_EQP_DMX_FLASH)->GetWindowText(szFilePath);
			if( GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nBootVer>0 || 
				GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].arbyReserved[2]>0 )
			{
				w_InfoFlag = TRUE;
			}
			break;
		default:									
			ASSERT(FALSE);	
			return;
	}

	if(FileExist(szFilePath, FALSE) == FALSE)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("파일이 존재하지 않습니다."));
		//MessageBox(g_LangTable[111]);
		return;
	}

	switch(eEquip)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		case EEQUIP_TYPE_DMT:
			strEquipName.Format(_T("DMT"));
			break;
		case EEQUIP_TYPE_DSS:
			strEquipName.Format(_T("DSS %d"), nEquipNo);
			break;
		case EEQUIP_TYPE_DPG:
			strEquipName.Format(_T("DPG"));
			break;
		case EEQUIP_TYPE_DRM:
			strEquipName.Format(_T("DRM %d번(DPG출력 %d번)"), nEquipNo, nDpgNo);
			break;
		case EEQUIP_TYPE_DPA:
			strEquipName.Format(_T("DPA %d"), nEquipNo);
			break;
		case EEQUIP_TYPE_DMP:
			strEquipName.Format(_T("DMP %d"), nEquipNo);
			break;
		case EEQUIP_TYPE_DRE:
			strEquipName.Format(_T("DRE %d"), nEquipNo);
			break;
		case EEQUIP_TYPE_DFR:
			strEquipName.Format(_T("DFR %d"), nEquipNo);
			break;
		case EEQUIP_TYPE_DMX:
			strEquipName.Format(_T("DMX"));
			break;
	}

	szFileName = PathFindFileName(szFilePath);
	GetSelectedFilePathVersion(szFileName, w_Version);

	//장치의 버전정보와 파일버전정보 비교
	if(w_Error)
	{
		strContent.Format(_T("올바른 장치주소를 선택하여 주십시요."));
	}
	else
	{	
		if(w_InfoFlag)
		{
			if( memcmp(&w_DevVersion,&w_Version,sizeof(w_Version))==0 )
			{
				strContent.Format(_T("동일한 버전입니다."));
			}else{
				strContent.Format(_T("버전이 일치하지 않습니다.\n로컬파일[%s]: %d.%d.%d.%d\n장치파일[%s] : %d.%d.%d.%d"),strEquipName,w_Version[3],w_Version[2],w_Version[1],w_Version[0],
					strEquipName,w_DevVersion[3],w_DevVersion[2],w_DevVersion[1],w_DevVersion[0] );
			}
		}else{
			if(eEquip == EEQUIP_TYPE_DMX)
			{
				strContent.Format(_T("[%s] 정보조회를 하신후, 비교버튼을 클릭하여 주십시요."),strEquipName);
			}
			else
			{
				strContent.Format(_T("장비전체조회 및 [%s] 정보조회를 하신후, 비교버튼을 클릭하여 주십시요."),strEquipName);
			}
		}
	}

	MessageBox(strContent);

}

INT CDLUpdateDlg::OnButtonUpdateFlash(UINT nID)
{
	int			w_ii=0;
	CString		szFilePath = _T("");
	CString		szFileName = _T("");
	CString		strEquipNo = _T("");
	CString		strDpgNo = _T("");
	EEquipType	eEquip;
	DWORD		nEquipNo = 1;
	DWORD		nDpgNo = 0;

	CComboBox	*pComboBox = NULL;

	if(!m_bEquipInfoAll)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("전체조회명령을 먼저 수행 하신 후, 개별 업데이트를 이용하실 수 있습니다."));
		//MessageBox(g_LangTable[113]);
		return (0);
	}

	switch(nID)
	{
		case IDC_BUTTON_UPDATE_EQP_DMT_FLASH:		
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMT;
			eEquip = EEQUIP_TYPE_DMT;
			nEquipNo = 1; 
			GetDlgItem(IDC_EDIT_EQP_DMT_FLASH)->GetWindowText(szFilePath);		
			break;
		case IDC_BUTTON_UPDATE_EQP_DSS_FLASH:
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
			eEquip = EEQUIP_TYPE_DSS;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DSS);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
				return(0);
			}
			GetDlgItem(IDC_EDIT_EQP_DSS_FLASH)->GetWindowText(szFilePath);		
			break;
		case IDC_BUTTON_UPDATE_EQP_DPG_FLASH:
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPG;
			eEquip = EEQUIP_TYPE_DPG;		
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPG);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
				return(0);
			}
			GetDlgItem(IDC_EDIT_EQP_DPG_FLASH)->GetWindowText(szFilePath);		
			break;		
		case IDC_BUTTON_UPDATE_EQP_DRM_FLASH:		
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRM;
			eEquip = EEQUIP_TYPE_DRM;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strDpgNo);
				nDpgNo = dl_atoi1(CT2A(strDpgNo));
			}else{
				MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
				return(0);
			}

			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DRE);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{

			}
			GetDlgItem(IDC_EDIT_EQP_DRM_FLASH)->GetWindowText(szFilePath);		
			break;
		case IDC_BUTTON_UPDATE_EQP_DPA_FLASH:
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
			eEquip = EEQUIP_TYPE_DPA;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPA);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
				return(0);
			}
			GetDlgItem(IDC_EDIT_EQP_DPA_FLASH)->GetWindowText(szFilePath);		
			break;
		case IDC_BUTTON_UPDATE_EQP_DMP_FLASH:
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
			eEquip = EEQUIP_TYPE_DMP;		
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DMP);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
				return(0);
			}

			GetDlgItem(IDC_EDIT_EQP_DMP_FLASH)->GetWindowText(szFilePath);		
			break;
		case IDC_BUTTON_UPDATE_EQP_DRE_FLASH:
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRE;
			eEquip = EEQUIP_TYPE_DRE;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRE);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
				return(0);
			}
			GetDlgItem(IDC_EDIT_EQP_DRE_FLASH)->GetWindowText(szFilePath);
			break;
		case IDC_BUTTON_UPDATE_EQP_DFR_FLASH:
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
			eEquip = EEQUIP_TYPE_DFR;
			pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DFR);
			if( pComboBox->GetCurSel() != CB_ERR ){
				pComboBox->GetLBText(pComboBox->GetCurSel(),strEquipNo);
				nEquipNo = dl_atoi1(CT2A(strEquipNo));
			}else{
				MessageBox(_T("올바른 장치주소를 선택하여 주십시요."));
				return(0);
			}
			GetDlgItem(IDC_EDIT_EQP_DFR_FLASH)->GetWindowText(szFilePath);		
			break;
		case IDC_BUTTON_UPDATE_EQP_DMX_FLASH:
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMX;
			eEquip = EEQUIP_TYPE_DMX;
			nEquipNo = 1;
			GetDlgItem(IDC_EDIT_EQP_DMX_FLASH)->GetWindowText(szFilePath);

			//자료구조 클리어
			//OnBatchUpdateAllEnd2();

			//
			if(GetUpdateWnd()) 
			{
				if(GetUpdateWnd()->m_pEquipInfoDlg)
				{
					::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_DATA, 0, 0);
				}
			}
			break;
		default:									
			ASSERT(FALSE);	
			return FALSE;
	}

	if( !GetCheckEquipInfo(eEquip,nEquipNo,nDpgNo) )
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("장치정보를 확인하신 후, \n올바른 장비번호(DRM인경우, DPG/DRE출력번호)를 선택하여, 업데이트를 이용하시기 바랍니다."));
		//MessageBox(g_LangTable[113]);
		return (0);
	}

	memset(&stSendCurrent,0x00,sizeof(stSendCurrent));

	stSendCurrent.nSetNo = 1;
	stSendCurrent.nEquipType = eEquip;
	stSendCurrent.nEquipNo = nEquipNo;
	stSendCurrent.nDpgNo = nDpgNo;
	stSendCurrent.nReTry = 1;

	m_nSendCur = 0;
	switch(stSendCurrent.nEquipType)
	{
		case EEQUIP_TYPE_DMT:
			m_nSendCur = LPA_DMA_IDX;
			break;
		case EEQUIP_TYPE_DPG:
			m_nSendCur = LPA_DPG_IDX;
			break;
		case EEQUIP_TYPE_DRE:
			m_nSendCur = LPA_DRE_IDX;
			break;
		case EEQUIP_TYPE_DRM:
			m_nSendCur = LPA_DRM_IDX;
			break;
		case EEQUIP_TYPE_DPA:
			m_nSendCur = LPA_DMA_IDX;
			break;
		case EEQUIP_TYPE_DMP:
			m_nSendCur = LPA_DMP_IDX;
			break;
		case EEQUIP_TYPE_DSS:
			m_nSendCur = LPA_DSS_IDX;
			break;
		case EEQUIP_TYPE_DFR:
			m_nSendCur = LPA_DFR_IDX;
			break;
		case EEQUIP_TYPE_DMX:
			m_nSendCur = LPA_DMX_IDX;
			break;
	}
	
	stAllSendCurrent[m_nSendCur].nSetNo = stSendCurrent.nSetNo;
	stAllSendCurrent[m_nSendCur].nEquipType = stSendCurrent.nEquipType;
	stAllSendCurrent[m_nSendCur].nEquipNo = stSendCurrent.nEquipNo;
	stAllSendCurrent[m_nSendCur].nDpgNo = stSendCurrent.nDpgNo;
	stAllSendCurrent[m_nSendCur].nReTry = stSendCurrent.nReTry;

	szFileName = PathFindFileName(szFilePath);
	GetSelectedFilePathVersion(szFileName, stSendCurrent.nVersion);

	return UpdateFlash( stSendCurrent.nSetNo, stSendCurrent.nEquipType, stSendCurrent.nEquipNo, stSendCurrent.nDpgNo, szFilePath );
}

BOOL CDLUpdateDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	INT		nID = LOWORD(wParam);
	INT		nEvent = HIWORD(wParam);
	CString szFilePath=_T("");

	if(nEvent == BN_CLICKED)
	{
		//--------------------------------------------------------//
		//		LOAD FLASH
		switch(nID)
		{
			case IDC_BUTTON_LOAD_EQP_DMT_FLASH:
			case IDC_BUTTON_LOAD_EQP_DSS_FLASH:
			case IDC_BUTTON_LOAD_EQP_DPG_FLASH:
			case IDC_BUTTON_LOAD_EQP_DRM_FLASH:
			case IDC_BUTTON_LOAD_EQP_DPA_FLASH:
			case IDC_BUTTON_LOAD_EQP_DMP_FLASH:
			case IDC_BUTTON_LOAD_EQP_DRE_FLASH:
			case IDC_BUTTON_LOAD_EQP_DFR_FLASH:
			case IDC_BUTTON_LOAD_EQP_DMX_FLASH:
				OnButtonLoadFile(nID);
				break;
		}


		//--------------------------------------------------------//
		//		VERIFY FLASH
		switch(nID)
		{
			case IDC_BUTTON_VERIFY_EQP_DMT_FLASH:
			case IDC_BUTTON_VERIFY_EQP_DSS_FLASH:
			case IDC_BUTTON_VERIFY_EQP_DPG_FLASH:			
			case IDC_BUTTON_VERIFY_EQP_DRM_FLASH:
			case IDC_BUTTON_VERIFY_EQP_DPA_FLASH:
			case IDC_BUTTON_VERIFY_EQP_DMP_FLASH:
			case IDC_BUTTON_VERIFY_EQP_DRE_FLASH:
			case IDC_BUTTON_VERIFY_EQP_DFR_FLASH:
			case IDC_BUTTON_VERIFY_EQP_DMX_FLASH:
				OnButtonVerifyFlash(nID);	
				break;
		}

		//--------------------------------------------------------//
		//		UPDATE FLASH
		switch(nID)
		{
			case IDC_BUTTON_UPDATE_EQP_DMT_FLASH:
			case IDC_BUTTON_UPDATE_EQP_DSS_FLASH:
			case IDC_BUTTON_UPDATE_EQP_DPG_FLASH:		
			case IDC_BUTTON_UPDATE_EQP_DRM_FLASH:
			case IDC_BUTTON_UPDATE_EQP_DPA_FLASH:
			case IDC_BUTTON_UPDATE_EQP_DMP_FLASH:
			case IDC_BUTTON_UPDATE_EQP_DRE_FLASH:
			case IDC_BUTTON_UPDATE_EQP_DFR_FLASH:
			case IDC_BUTTON_UPDATE_EQP_DMX_FLASH:
				OnButtonUpdateFlash(nID);	
				break;
		}

		//--------------------------------------------------------//

		//--------------------------------------------------------//
		//		Device INFO
		switch(nID)
		{
		
		case IDC_BUTTON_UPDATE_EQP_DMT_INFO:
		case IDC_BUTTON_UPDATE_EQP_DSS_INFO:
		case IDC_BUTTON_UPDATE_EQP_DPG_INFO:		
		case IDC_BUTTON_UPDATE_EQP_DRM_INFO:
		case IDC_BUTTON_UPDATE_EQP_DPA_INFO:
		case IDC_BUTTON_UPDATE_EQP_DMP_INFO:
		case IDC_BUTTON_UPDATE_EQP_DRE_INFO:
		case IDC_BUTTON_UPDATE_EQP_DFR_INFO:
		case IDC_BUTTON_UPDATE_EQP_DMX_INFO:
			OnButtonEquipInfo(nID);	
			break;
		}

		if(nID == IDC_BUTTON_EQUIPINFO)
		{
			OnButtonEquipInfoDialog();
		}


		//--------------------------------------------------------//
		//저가형 1000
		if(nID == IDC_BUTTON_CONNECT2)
		{
			OnButtonConnect();
		}

		//저가형 5000일경우
		if(nID == IDC_BUTTON_CONNECT3)
		{
			OnButtonConnect2();
		}
	}
	return CDialog::OnCommand(wParam, lParam);
}


void CDLUpdateDlg::OnButtonConnect(void)
{
	CString strPort = _T("");
	DWORD nSelPort = 0;

	CComboBox *pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_COM_PORT);

	if(GetLowPA()->IsUpdateSerialOpened())
	{
		if(m_bUpdateFlag)
		{
			GetLowPA()->SendUpdateCmd(CMD_UPDATE_CLEAR);
			m_bUpdateFlag = FALSE;
		}
		GetLowPA()->UpdateStopSerial();
		

		m_WaitingDialog.SetRange(0, (short)3);
		ShowWaitDialog(TRUE, 3000);
	} 
	else 
	{	
		pComboBox->GetLBText(pComboBox->GetCurSel(),strPort);
		m_nPort = dl_atoi1(CT2A(strPort));
		if( !GetLowPA()->UpdateStartSerial(m_nPort, DEF_SERIAL_BAUD) )
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("메인장치와 연결되는 통신포트를 열 수 없습니다."));
			//MessageBox(g_LangTable[113]);
			return;
		}

		m_WaitingDialog.SetRange(0, (short)3);
		ShowWaitDialog(TRUE, 3000);
	}

	OnChangeConnect();
}

void CDLUpdateDlg::OnButtonConnect2(void)
{
	CString strPort = _T("");
	DWORD nSelPort = 0;

	CComboBox *pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_COM_PORT2);

	if(GetLowPA()->IsMixerSerialOpened())
	{
		if(m_bUpdateMixerFlag)
		{
			GetLowPA()->SendMixerCmd(CMD_MIXER_CLEAR);
			m_bUpdateMixerFlag = FALSE;
		}
		GetLowPA()->MixerStopSerial();
		
		m_WaitingDialog.SetRange(0, (short)3);
		ShowWaitDialog(TRUE, 3000);
	} 
	else 
	{
		pComboBox->GetLBText(pComboBox->GetCurSel(),strPort);
		m_nMixerPort = dl_atoi1(CT2A(strPort));
		if( !GetLowPA()->MixerStartSerial(m_nMixerPort, DEF_SERIAL_MIX_BAUD) )
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("메인장치와 연결되는 Mixer 통신포트를 열 수 없습니다."));
			//MessageBox(g_LangTable[113]);
			return;
		}

		m_WaitingDialog.SetRange(0, (short)3);
		ShowWaitDialog(TRUE, 3000);
	}

	OnChangeConnect2();
}

void CDLUpdateDlg::OnButtonEquipInfoDialog()
{
	if(m_pEquipInfoDlg)
	{
		m_pEquipInfoDlg->ShowWindow(SW_SHOW);
	}
}

void CDLUpdateDlg::OnChangeConnect()
{
	CButton		*pBtnAllSelect=NULL;
	CButton		*pBtnAllUpdate=NULL;
	CButton		*pBtnAllInfo=NULL;
	CButton		*pBtnShowInfo=NULL;

	CButton		*pBtnUpdateMode=NULL;
	CButton		*pBtnUpdateClear=NULL;

	CButton *pButton = (CButton*)GetDlgItem(IDC_BUTTON_CONNECT2);
	CComboBox *pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_COM_PORT);

	if( GetLowPA()->IsUpdateSerialOpened() )
	{
		pButton->SetWindowText(_T("해제"));
		pComboBox->EnableWindow(FALSE);
	} 
	else 
	{
		pButton->SetWindowText(_T("접속"));
		pComboBox->EnableWindow(TRUE);
	}

	if(m_bUpdateFlag)		//업데이트접속모드
	{
		pBtnUpdateMode = (CButton*)GetDlgItem(IDC_BUTTON_RESET_BOOT);
		pBtnUpdateMode->EnableWindow(FALSE);
		pBtnUpdateClear = (CButton*)GetDlgItem(IDC_BUTTON_FINISH_BOOT);
		pBtnUpdateClear->EnableWindow(TRUE);

		pBtnAllSelect = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_ALL);
		pBtnAllSelect->EnableWindow(TRUE);
		pBtnAllUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_ALL);
		pBtnAllUpdate->EnableWindow(TRUE);
		pBtnAllInfo = (CButton*)GetDlgItem(IDC_BUTTON_RESET_INFO);
		pBtnAllInfo->EnableWindow(TRUE);
		pBtnShowInfo = (CButton*)GetDlgItem(IDC_BUTTON_EQUIPINFO);
		pBtnShowInfo->EnableWindow(TRUE);
		
		if(m_DmtCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DMT,TRUE);
		}
		if(m_DpgCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DPG,TRUE);
		}
		if(m_DreCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DRE,TRUE);
		}
		if(m_DrmCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DRM,TRUE);
		}
		if(m_DrmCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DRM,TRUE);
		}
		if(m_DpaCnt>0){	
			DeviceStatusChange(EEQUIP_TYPE_DPA,TRUE);
		}
		if(m_DmpCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DMP,TRUE);
		}
		if(m_DssCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DSS,TRUE);
		}
		if(m_DfrCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DFR,TRUE);
		}
	}
	else					//업데이트헤제모드
	{
		pBtnAllSelect = (CButton*)GetDlgItem(IDC_BUTTON_LOAD_ALL);
		pBtnAllSelect->EnableWindow(FALSE);
		pBtnAllUpdate = (CButton*)GetDlgItem(IDC_BUTTON_UPDATE_ALL);
		pBtnAllUpdate->EnableWindow(FALSE);
		pBtnAllInfo = (CButton*)GetDlgItem(IDC_BUTTON_RESET_INFO);
		pBtnAllInfo->EnableWindow(FALSE);
		pBtnShowInfo = (CButton*)GetDlgItem(IDC_BUTTON_EQUIPINFO);
		pBtnShowInfo->EnableWindow(FALSE);

		pBtnUpdateMode = (CButton*)GetDlgItem(IDC_BUTTON_RESET_BOOT);
		pBtnUpdateMode->EnableWindow(TRUE);
		pBtnUpdateClear = (CButton*)GetDlgItem(IDC_BUTTON_FINISH_BOOT);
		pBtnUpdateClear->EnableWindow(FALSE);
		
		if(m_DmtCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DMT,FALSE);
		}
		if(m_DpgCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DPG,FALSE);
		}
		if(m_DreCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DRE,FALSE);
		}
		if(m_DrmCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DRM,FALSE);
		}
		if(m_DrmCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DRM,FALSE);
		}
		if(m_DpaCnt>0){	
			DeviceStatusChange(EEQUIP_TYPE_DPA,FALSE);
		}
		if(m_DmpCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DMP,FALSE);
		}
		if(m_DssCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DSS,FALSE);
		}
		if(m_DfrCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DFR,FALSE);
		}
	}
}

void CDLUpdateDlg::OnChangeConnect2()
{
	CButton		*pBtnAllSelect=NULL;
	CButton		*pBtnAllUpdate=NULL;
	CButton		*pBtnAllInfo=NULL;
	CButton		*pBtnShowInfo=NULL;

	CButton		*pBtnUpdateMode=NULL;
	CButton		*pBtnUpdateClear=NULL;

	CButton *pButton = (CButton*)GetDlgItem(IDC_BUTTON_CONNECT3);
	CComboBox *pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_COM_PORT2);

	if( GetLowPA()->IsMixerSerialOpened() )
	{
		pButton->SetWindowText(_T("해제"));
		pComboBox->EnableWindow(FALSE);
	} 
	else 
	{
		pButton->SetWindowText(_T("접속"));
		pComboBox->EnableWindow(TRUE);
	}

	if(m_bUpdateMixerFlag)
	{
		pBtnUpdateMode = (CButton*)GetDlgItem(IDC_BUTTON_RESET_BOOT2);
		pBtnUpdateMode->EnableWindow(FALSE);
		pBtnUpdateClear = (CButton*)GetDlgItem(IDC_BUTTON_FINISH_BOOT2);
		pBtnUpdateClear->EnableWindow(TRUE);

		if(m_DmxCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DMX,TRUE);
		}
	}
	else
	{
		pBtnUpdateMode = (CButton*)GetDlgItem(IDC_BUTTON_RESET_BOOT2);
		pBtnUpdateMode->EnableWindow(TRUE);
		pBtnUpdateClear = (CButton*)GetDlgItem(IDC_BUTTON_FINISH_BOOT2);
		pBtnUpdateClear->EnableWindow(FALSE);

		if(m_DmxCnt>0){
			DeviceStatusChange(EEQUIP_TYPE_DMX,FALSE);
		}
	}
}

INT CDLUpdateDlg::UpdateFlash(DWORD nSetNo, EEquipType eEquipType, DWORD nEquipNo, DWORD nDpgNo, CString szFilePath)
{
	UINT		nLen=0;
	int			w_Len=0;
	int			w_PRM3=0;

	CString		strPRM3 = _T("");
	CComboBox	*pComboBox = NULL;

	if(szFilePath.GetLength() == 0)
	{
		return -1;
	}
	else if(FileExist(szFilePath, FALSE) == FALSE)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("파일이 존재하지 않습니다."));
		//MessageBox(g_LangTable[111]);
		return FALSE;
	}

	if(m_pWriteData)
	{
		delete m_pWriteData;
	}
	m_pWriteData = (BYTE*)malloc(MAX_SIZE_FIRMWARE);
	
	if(Hex2Bin(szFilePath, m_pWriteData, MAX_SIZE_FIRMWARE, &nLen) != ERROR_SUCCESS)
	{
		if(m_bBatchAll == FALSE)
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("파일을 로드하는 중 에러가 발생되었습니다."));
			//MessageBox(g_LangTable[112]);
		}
		return (-1);
	}

	if(eEquipType == EEQUIP_TYPE_DMX)
	{
		if( GetLowPA()->IsMixerSerialOpened() == FALSE )
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("메인장치와 연결되는 믹서 통신포트를 열 수 없습니다."));
			//MessageBox(g_LangTable[113]);
			return FALSE;
		}
	}
	else
	{	
		if( GetLowPA()->IsUpdateSerialOpened() == FALSE )
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
			MessageBox(_T("메인장치와 연결되는 통신포트를 열 수 없습니다."));
			//MessageBox(g_LangTable[113]);
			return FALSE;
		}
	}

	switch(eEquipType)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		case EEQUIP_TYPE_DMT:
			m_szState =			_T("DMT 를 업데이트 중입니다.");				
			break;
		case EEQUIP_TYPE_DSS:
			m_szState.Format(	_T("DSS %d번을 업데이트 중입니다."), nEquipNo);	
			break;
		case EEQUIP_TYPE_DPG:
			m_szState.Format(	_T("DPG 를 업데이트 중입니다."));	
			break;
		case EEQUIP_TYPE_DRM:
			m_szState.Format(	_T("DRM [그룹 %d번] [DPG출력 %d번] [DRE출력 %d]을 업데이트 중입니다.") ,GetLowPA()->m_EquipInfo[LPA_DRM_IDX][nEquipNo+(nDpgNo)*9].arbyReserved[1]
																									 ,nDpgNo
																									 ,nEquipNo);
			//pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG);
			//pComboBox->GetLBText(pComboBox->GetCurSel(),strPRM3);
			//w_PRM3 = dl_atoi1(CT2A(strPRM3));
			w_PRM3 = nDpgNo;
			break;
		case EEQUIP_TYPE_DPA:
			m_szState.Format(	_T("DPA %d번을 업데이트 중입니다."), nEquipNo);	
			break;
		case EEQUIP_TYPE_DMP:
			m_szState.Format(	_T("DMP %d번을 업데이트 중입니다."), nEquipNo);	
			break;
		case EEQUIP_TYPE_DRE:
			m_szState.Format(	_T("DRE 0번 [DPG출력 %d]을 업데이트 중입니다."), nEquipNo);
			//m_szState.Format(	_T("DRE %d번을 업데이트 중입니다."), nEquipNo);	
			break;
		case EEQUIP_TYPE_DFR:
			m_szState.Format(	_T("DFR %d번을 업데이트 중입니다."), nEquipNo);	
			break;
		case EEQUIP_TYPE_DMX:
			m_szState =			_T("DMX 를 업데이트 중입니다.");				
			break;
		default:
			return FALSE;
	}
	UpdateData(FALSE);

	w_Len = GetLowPA()->MakeRomUpdateData(m_pWriteData,GetLowPA()->m_RomUpdate,nLen);
	GetLowPA()->m_RomUpdateLen = w_Len;
	if(eEquipType == EEQUIP_TYPE_DMP)
	{
		GetLowPA()->m_RomUpdatePage = (GetLowPA()->m_RomUpdateLen/LPA_PACKET_128LEN) + ((GetLowPA()->m_RomUpdateLen%LPA_PACKET_128LEN) ? 1 : 0);
	}
	else
	{
		GetLowPA()->m_RomUpdatePage = (GetLowPA()->m_RomUpdateLen/LPA_PACKET_256LEN) + ((GetLowPA()->m_RomUpdateLen%LPA_PACKET_256LEN) ? 1 : 0);
	}
	
	DL_LOG("[FLASH DOWNLOAD TOTAL:%d, PAGE:%d]",GetLowPA()->m_RomUpdateLen,GetLowPA()->m_RomUpdatePage);
	GetLowPA()->m_SendUpdatePage = 0;
	GetLowPA()->m_UpdateDevStat = DEV_STAT_SEND;
	m_ctrlProgressUpdate.SetRange32(0, GetLowPA()->m_RomUpdatePage);

	//반복횟수설정
	//stSendCurrent.nReTry = 1;
	stSendCurrent.nSendPage = GetLowPA()->m_SendUpdatePage;

	if(eEquipType == EEQUIP_TYPE_DMX)
	{
		GetLowPA()->MixerSetDevInfo(eEquipType, (BYTE)nEquipNo, w_PRM3 );
		w_Len = GetLowPA()->SendMixerData(GetLowPA()->m_SendUpdatePage);
	}
	else
	{
		GetLowPA()->UpdateSetDevInfo(eEquipType, (BYTE)nEquipNo, w_PRM3 );
		w_Len = GetLowPA()->SendUpdateData(GetLowPA()->m_SendUpdatePage);
	}

	if(w_Len>0)
	{
		m_ctrlProgressUpdate.SetPos(GetLowPA()->m_SendUpdatePage);
	}

	//
	SetTimer(WM_TIMER_SENDFLASHCMD,UPDATE_INFO_TIMEVALUE,NULL);

	return TRUE;
}

int CDLUpdateDlg::GetCheckEquipInfo(int a_EquipType, int a_EquipNo, int a_DpgNo)
{
	int w_Ret = FALSE;

	int	w_ii = 0;
	int w_CheckCount = 0;
	int	w_EquIdx = 0;
	int w_ENoIndex = 0;
	int	w_DevCnt = 0;

	switch(a_EquipType)
	{
		case EEQUIP_TYPE_DMT:
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DmtCnt;
			break;
		case EEQUIP_TYPE_DPG:
			w_EquIdx = LPA_DPG_IDX;
			w_DevCnt = m_DpgCnt;
			break;
		case EEQUIP_TYPE_DRE:
			w_EquIdx = LPA_DRE_IDX;
			w_DevCnt = 7;
			break;
		case EEQUIP_TYPE_DRM:
			w_EquIdx = LPA_DRM_IDX;
			w_DevCnt = 63;
			break;
		case EEQUIP_TYPE_DPA:
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DpaCnt;
			break;
		case EEQUIP_TYPE_DMP:
			w_EquIdx = LPA_DMP_IDX;
			w_DevCnt = m_DmpCnt;
			break;
		case EEQUIP_TYPE_DSS:
			w_EquIdx = LPA_DSS_IDX;
			w_DevCnt = m_DssCnt;
			break;
		case EEQUIP_TYPE_DFR:
			w_EquIdx = LPA_DFR_IDX;
			w_DevCnt = m_DfrCnt;
			break;
		case EEQUIP_TYPE_DMX:
			w_EquIdx = LPA_DMX_IDX;
			w_DevCnt = m_DmxCnt;
			break;
	}

	for(w_ii=0;w_ii<w_DevCnt;w_ii++)
	{
		if( GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].eEquipType == a_EquipType &&  
			GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].nEquipNo == a_EquipNo &&
			GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].arbyReserved[0] == a_DpgNo )
		{
			if( GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].arbyReserved[2] == LPA_DATA_SUCESS ||
				GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].arbyReserved[2] == LPA_DATA_FAILED ||
				GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].arbyReserved[2] == LPA_INFO_SUCESS ) 
			{
				w_Ret = TRUE;
				break;
			}
		}
	}

	return w_Ret;
}

int CDLUpdateDlg::GetCheckEquipCount(int a_EquipType)
{
	int	w_ii = 0;
	int w_CheckCount = 0;
	int	w_EquIdx = 0;
	int	w_DevCnt = 0;

	switch(a_EquipType)
	{
		case EEQUIP_TYPE_DMT:
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DmtCnt;
			break;
		case EEQUIP_TYPE_DPG:
			w_EquIdx = LPA_DPG_IDX;
			w_DevCnt = m_DpgCnt;
			break;
		case EEQUIP_TYPE_DRE:
			w_EquIdx = LPA_DRE_IDX;
			//if(m_DpgCnt>0)
			//{
			//	if(m_DreCnt>0)
			//	{
					w_DevCnt = 7;
			//	}
			//} else {
			//	if(m_DreCnt>0)
			//	{
			//		w_DevCnt = 1;
			//	}
			//}
			break;
		case EEQUIP_TYPE_DRM:
			w_EquIdx = LPA_DRM_IDX;
			w_DevCnt = 63;
			break;
		case EEQUIP_TYPE_DPA:
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DpaCnt;
			break;
		case EEQUIP_TYPE_DMP:
			w_EquIdx = LPA_DMP_IDX;
			w_DevCnt = m_DmpCnt;
			break;
		case EEQUIP_TYPE_DSS:
			w_EquIdx = LPA_DSS_IDX;
			w_DevCnt = m_DssCnt;
			break;
		case EEQUIP_TYPE_DFR:
			w_EquIdx = LPA_DFR_IDX;
			w_DevCnt = m_DfrCnt;
			break;
	}

	for(w_ii=0;w_ii<w_DevCnt;w_ii++)
	{
		if( stBatchAll[w_EquIdx][w_ii].eEquipType == a_EquipType )
		{
			//if( stBatchAll[w_EquIdx][w_ii].bResult && 
			//	(GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].arbyReserved[2] == LPA_INFO_SUCESS || 
			//	 GetLowPA()->m_EquipInfo[w_EquIdx][w_ii].arbyReserved[2] == LPA_DATA_SUCESS) ) 
			if( stBatchAll[w_EquIdx][w_ii].bResult ) 
			{
				w_CheckCount++;
			}
		}
	}

	return w_CheckCount;
}

int	CDLUpdateDlg::GetSelectedFilePathVersion(CString szSearchFilePath, BYTE* apVersion)
{
	CString strTok = _T("");
	CString strVer0 = _T("");
	CString strVer1 = _T("");
	CString strVer2 = _T("");
	CString strVer3 = _T("");

	if(szSearchFilePath.GetLength() > 0 )
	{	
		AfxExtractSubString( strTok, szSearchFilePath, 5, _T('_'));
		AfxExtractSubString( strVer0, strTok, 0, _T('.'));
		AfxExtractSubString( strVer1, strTok, 1, _T('.'));
		AfxExtractSubString( strVer2, strTok, 2, _T('.'));
		AfxExtractSubString( strVer3, strTok, 3, _T('.'));

		apVersion[0] = dl_atoi1(CT2A(strVer3));
		apVersion[1] = dl_atoi1(CT2A(strVer2));
		apVersion[2] = dl_atoi1(CT2A(strVer1));
		apVersion[3] = dl_atoi1(CT2A(strVer0));
	}

	return(0);
}

BOOL CDLUpdateDlg::SetFilePathName(CString szSearchFilePath, INT nEditId)
{
	CFileFind FileFinder;
	CString szFoundFilePath;

	if(FileFinder.FindFile(szSearchFilePath))
	{
		FileFinder.FindNextFile();
		szFoundFilePath = FileFinder.GetFilePath();
		GetDlgItem(nEditId)->SetWindowText(szFoundFilePath);

		return TRUE;
	}

	GetDlgItem(nEditId)->SetWindowText(_T(""));

	return FALSE;
}

void CDLUpdateDlg::SetFileNameBuilding()
{
	TCHAR *arlpszEquipName[NUMOF_EQUIP_TYPE] =
	{
		_T("ALL"),
		_T("PC"),
		_T("DMT"),
		_T("DSS"),
		_T("DPG"),
		_T(""),
		_T(""),
		_T("DRM"),
		_T("DPA"),
		_T(""),
		_T("DMP"),
		_T("DRE"),
		_T("DFR"),
		_T(""),
		_T(""),
		_T("MIX"),
	};

	CString szFolder = _T("");
	CString szPrefix = _T("");	// = _T("PABOOT_EQUIP_TYPE_");
	CString szFilePath = _T("");

	TCHAR	pszPathName[_MAX_PATH];

	::GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH); 
	PathRemoveFileSpec(pszPathName);
	szFilePath.Format(_T("%s"),pszPathName);
	szFolder.Format(_T("%s\\FIRMWARE\\APP\\"),szFilePath);

	// 	CString szSuffixExt = _T("*.") + CString(lpszExt);
	//CString szSearchFilePath;

#define SET_SEARCH_FILE_PATH(equiptype,ext)		(szFolder + szPrefix + arlpszEquipName[equiptype] + _T("*.") + ext)
	//szSearchFilePath = szFolder /*+ szPrefix */+ szEquipName[IDC_EDIT_EQP_DMT_FLASH] + _T("*.") + CString(lpszExt);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DMT, _T("hex")), IDC_EDIT_EQP_DMT_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DSS, _T("hex")), IDC_EDIT_EQP_DSS_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DPG, _T("hex")), IDC_EDIT_EQP_DPG_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DRM, _T("hex")), IDC_EDIT_EQP_DRM_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DPA, _T("hex")), IDC_EDIT_EQP_DPA_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DMP, _T("hex")), IDC_EDIT_EQP_DMP_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DRE, _T("hex")), IDC_EDIT_EQP_DRE_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DFR, _T("hex")), IDC_EDIT_EQP_DFR_FLASH);
	SetFilePathName(SET_SEARCH_FILE_PATH(EEQUIP_TYPE_DMX, _T("hex")), IDC_EDIT_EQP_DMX_FLASH);

	//--------------------------------------------------------//
}

void CDLUpdateDlg::OnButtonLoadAll()
{
	SetFileNameBuilding();
}

void CDLUpdateDlg::OnButtonFinishBoot() 
{
	if(GetLowPA()->IsUpdateSerialOpened() == FALSE)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("메인장치와 연결되는 통신포트를 열 수 없습니다."));
		//MessageBox(g_LangTable[113]);
		return;
	}
	
	OnBatchUpdateAllEnd();
	
	GetLowPA()->SendUpdateCmd(CMD_UPDATE_CLEAR);

	m_bUpdateFlag = FALSE;

	OnChangeConnect();

	m_WaitingDialog.SetRange(0, (short)5);
	ShowWaitDialog(TRUE, 5000);
}

void CDLUpdateDlg::OnButtonFinishBoot2() 
{
	if(GetLowPA()->IsMixerSerialOpened() == FALSE)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("메인장치와 연결되는 통신포트를 열 수 없습니다."));
		//MessageBox(g_LangTable[113]);
		return;
	}

	OnBatchUpdateAllEnd2();

	GetLowPA()->SendMixerCmd(CMD_MIXER_CLEAR);

	m_bUpdateMixerFlag = FALSE;

	OnChangeConnect2();

	m_WaitingDialog.SetRange(0, (short)3);
	ShowWaitDialog(TRUE, 3000);
}

void CDLUpdateDlg::OnButtonResetBoot() 
{
//	CMainFrame* pMainFrame = ((CMainFrame*)AfxGetMainWnd());
// 	CPASystemConfigDoc* pDoc = (CPASystemConfigDoc*)pMainFrame->GetActiveDocument();
	
	if( !GetLowPA()->IsUpdateSerialOpened() )
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("메인장치와 연결되는 통신포트를 열 수 없습니다."));
		//MessageBox(g_LangTable[113]);
		return;
	}

	GetLowPA()->SendUpdateCmd(CMD_UPDATE_RESET);
	m_bUpdateFlag = TRUE;

	OnChangeConnect();

	m_WaitingDialog.SetRange(0, (short)3);
	ShowWaitDialog(TRUE, 5000);
	//ShowWaitDialog(TRUE, 10000);

}

void CDLUpdateDlg::OnButtonResetBoot2() 
{
	//	CMainFrame* pMainFrame = ((CMainFrame*)AfxGetMainWnd());
	// 	CPASystemConfigDoc* pDoc = (CPASystemConfigDoc*)pMainFrame->GetActiveDocument();

	if( !GetLowPA()->IsMixerSerialOpened() )
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("메인장치와 연결되는 믹서 통신포트를 열 수 없습니다."));
		//MessageBox(g_LangTable[113]);
		return;
	}

	GetLowPA()->SendMixerCmd(CMD_MIXER_RESET);

	m_bUpdateMixerFlag = TRUE;

	OnChangeConnect2();
	//MessageBox(szText);

	m_WaitingDialog.SetRange(0, (short)3);
	ShowWaitDialog(TRUE, 5000);

}

void CDLUpdateDlg::OnButtonUpdateAll() 
{
	int			w_ii=0;
	int			w_jj=0;
	int			w_Idx=0;
	int			w_EquIdx=0;
	int			w_DreMax = 0;
	int			w_DrmMax = 0;
	int			w_DevCnt=0;
	int			w_Total=0;

	int			w_DpgNo=0;
	int			w_EquipNo=0;
	int			w_EquipType=0;

	CComboBox*	pComboBox=NULL;
	
	//일괄업그레이드를위해, 모든 콤보박스정보 클리어
	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPG);
	pComboBox->SetCurSel(-1);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRE);
	pComboBox->SetCurSel(-1);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DPG);
	pComboBox->SetCurSel(-1);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DRM_DRE);
	pComboBox->SetCurSel(-1);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DPA);
	pComboBox->SetCurSel(-1);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DMP);
	pComboBox->SetCurSel(-1);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DSS);
	pComboBox->SetCurSel(-1);

	pComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_EQP_DFR);
	pComboBox->SetCurSel(-1);

	m_nBatchAllCount = 0;
	if(!m_bEquipInfoAll)
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("전체조회명령을 먼저 수행 하신 후, 일괄업데이트를 이용하실 수 있습니다."));
		//MessageBox(g_LangTable[113]);
		return;
	}

	//GetDlgItem(IDC_BUTTON_UPDATE_ALL)->EnableWindow(FALSE);
	m_bBatchAll = TRUE;
	m_nSendCur = 0;
	memset(&stAllSendCurrent,0x00,sizeof(stAllSendCurrent));
	if(m_ctrlCSBoxDMT.GetCheck() && m_DmtCnt>0)
	{
		for(w_ii=0;w_ii<m_DmtCnt;w_ii++)
		{
			if( stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == EEQUIP_TYPE_DMT )
			{
				if( stBatchAll[LPA_DMA_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DMA_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DMA_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DMA_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMT;
	}
	if(m_ctrlCSBoxDPG.GetCheck() && m_DpgCnt>0)
	{
		for(w_ii=0;w_ii<m_DpgCnt;w_ii++)
		{
			if( stBatchAll[LPA_DPG_IDX][w_ii].eEquipType == EEQUIP_TYPE_DPG )
			{
				if( stBatchAll[LPA_DPG_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DPG_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DPG_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DPG_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		if(!m_ctrlCSBoxDMT.GetCheck())
		{
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPG;
		}
	}
	if(m_ctrlCSBoxDRE.GetCheck() && m_DreCnt>0)
	{
		w_DreMax = 7;
		for(w_ii=0;w_ii<w_DreMax;w_ii++)
		{
			if( stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == EEQUIP_TYPE_DRE )
			{
				if( stBatchAll[LPA_DRE_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DRE_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DRE_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DRE_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		if(!m_ctrlCSBoxDMT.GetCheck() && !m_ctrlCSBoxDPG.GetCheck())
		{
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRE;
		}
	}
	if(m_ctrlCSBoxDRM.GetCheck() && m_DrmCnt>0)
	{
		w_DrmMax = 63;
		for(w_ii=0;w_ii<w_DrmMax;w_ii++)
		{
			if( stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == EEQUIP_TYPE_DRM )
			{
				if( stBatchAll[LPA_DRM_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DRM_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DRM_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DRM_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		if(!m_ctrlCSBoxDMT.GetCheck() && !m_ctrlCSBoxDPG.GetCheck() && !m_ctrlCSBoxDRE.GetCheck() )
		{
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRM;
		}
	}
	if(m_ctrlCSBoxDPA.GetCheck() && m_DpaCnt>0)
	{
		for(w_ii=0;w_ii<m_DpaCnt;w_ii++)
		{
			if( stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == EEQUIP_TYPE_DPA )
			{
				if( stBatchAll[LPA_DMA_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DMA_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DMA_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DMA_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		if(!m_ctrlCSBoxDMT.GetCheck() && !m_ctrlCSBoxDPG.GetCheck() && !m_ctrlCSBoxDRE.GetCheck() && !m_ctrlCSBoxDRM.GetCheck() )
		{
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
		}
	}
	if(m_ctrlCSBoxDMP.GetCheck() && m_DmpCnt>0)
	{
		for(w_ii=0;w_ii<m_DmpCnt;w_ii++)
		{
			if( stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == EEQUIP_TYPE_DMP )
			{
				if( stBatchAll[LPA_DMP_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DMP_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DMP_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DMP_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		if( !m_ctrlCSBoxDMT.GetCheck() && !m_ctrlCSBoxDPG.GetCheck() && !m_ctrlCSBoxDRE.GetCheck() && !m_ctrlCSBoxDRM.GetCheck() &&
			!m_ctrlCSBoxDPA.GetCheck() )
		{
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
		}
	}
	if(m_ctrlCSBoxDSS.GetCheck() && m_DssCnt>0)
	{
		for(w_ii=0;w_ii<m_DssCnt;w_ii++)
		{
			if( stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == EEQUIP_TYPE_DSS )
			{
				if( stBatchAll[LPA_DSS_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DSS_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DSS_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DSS_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		if( !m_ctrlCSBoxDMT.GetCheck() && !m_ctrlCSBoxDPG.GetCheck() && !m_ctrlCSBoxDRE.GetCheck() && !m_ctrlCSBoxDRM.GetCheck() &&
			!m_ctrlCSBoxDPA.GetCheck() && !m_ctrlCSBoxDMP.GetCheck() )
		{
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
		}
	}
	if(m_ctrlCSBoxDFR.GetCheck() && m_DfrCnt>0)
	{
		for(w_ii=0;w_ii<m_DfrCnt;w_ii++)
		{
			if( stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == EEQUIP_TYPE_DFR )
			{
				if( stBatchAll[LPA_DFR_IDX][w_ii].bResult ) 
				{
					stAllSendCurrent[m_nBatchAllCount].nSetNo = stBatchAll[LPA_DFR_IDX][w_ii].nSetNo;
					stAllSendCurrent[m_nBatchAllCount].nEquipType = stBatchAll[LPA_DFR_IDX][w_ii].eEquipType;
					stAllSendCurrent[m_nBatchAllCount].nEquipNo = stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo;
					stAllSendCurrent[m_nBatchAllCount].nDpgNo = stBatchAll[LPA_DFR_IDX][w_ii].nDpgNo;
					m_nBatchAllCount++;
				}
			}
		}
		if( !m_ctrlCSBoxDMT.GetCheck() && !m_ctrlCSBoxDPG.GetCheck() && !m_ctrlCSBoxDRE.GetCheck() && !m_ctrlCSBoxDRM.GetCheck() &&
			!m_ctrlCSBoxDPA.GetCheck() && !m_ctrlCSBoxDMP.GetCheck() && !m_ctrlCSBoxDSS.GetCheck() )
		{
			GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
		}
	}

	if(m_nBatchAllCount==0)
	{
		MessageBox(_T("일괄 업데이트할 장치를 선택하여 주십시요."));
		//MessageBox(g_LangTable[113]);
		//GetDlgItem(IDC_BUTTON_UPDATE_ALL)->EnableWindow(TRUE);
		m_bBatchAll = FALSE;
		return;
	}

	//memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
	if(GetLowPA()->m_SendUpdateData > m_nBatchAllCount)
	{
		DL_LOG("[SEND DATA ERROR][SEND:%d, TOTAL:%d]",GetLowPA()->m_SendUpdateData, m_nBatchAllCount);
		return;
	}

	for(w_ii=0;w_ii<m_nBatchAllCount;w_ii++)
	{
		if(stAllSendCurrent[w_ii].nSendOk==0)
		{
			stSendCurrent.nSetNo = stAllSendCurrent[w_ii].nSetNo;
			stSendCurrent.nEquipType = stAllSendCurrent[w_ii].nEquipType;
			stSendCurrent.nEquipNo = stAllSendCurrent[w_ii].nEquipNo;
			stSendCurrent.nDpgNo = stAllSendCurrent[w_ii].nDpgNo;
			stSendCurrent.nReTry = stAllSendCurrent[w_ii].nReTry = 1;
			m_nSendCur = w_ii;
			break;
		}
	}


	//윈도우 조작방지
	EnableWindow(FALSE);

	CString szFileName = _T("");
	szFileName = PathFindFileName(GetSelectedFilePath(stSendCurrent.nEquipType, TRUE));

	GetSelectedFilePathVersion( szFileName, stSendCurrent.nVersion );

	OnBatchUpdateAll();

}

void CDLUpdateDlg::OnBatchUpdateNext(int a_EType, int a_ENo)
{
	int			w_ii=0;
	int			w_jj=0;
	int			w_EquIdx=0;
	int			w_DevCnt=0;
	int			w_Total=0;

	int			w_DpgNo=0;
	int			w_EquipNo=0;
	int			w_EquipType=0;

	switch(a_EType)
	{
		case EEQUIP_TYPE_DMT:
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DmtCnt;
			break;
		case EEQUIP_TYPE_DPG:
			w_EquIdx = LPA_DPG_IDX;
			w_DevCnt = m_DpgCnt;
			break;
		case EEQUIP_TYPE_DRE:
			w_EquIdx = LPA_DRE_IDX;
			w_DevCnt = 7;
			break;
		case EEQUIP_TYPE_DRM:
			w_EquIdx = LPA_DRM_IDX;
			break;
		case EEQUIP_TYPE_DPA:
			w_EquIdx = LPA_DMA_IDX;
			if(m_DpaCnt>0)
			{
				w_DevCnt = m_DpaCnt;
			}
			break;
		case EEQUIP_TYPE_DMP:
			w_EquIdx = LPA_DMP_IDX;
			if(m_DmpCnt>0)
			{
				w_DevCnt = m_DmpCnt;
			}
			break;
		case EEQUIP_TYPE_DSS:
			w_EquIdx = LPA_DSS_IDX;
			if(m_DssCnt>0)
			{
				w_DevCnt = m_DssCnt;
			}
			break;
		case EEQUIP_TYPE_DFR:
			w_EquIdx = LPA_DFR_IDX;
			if(m_DfrCnt>0)
			{
				w_DevCnt = m_DfrCnt;
			}
			break;
	}


	//Flash Update
	if(a_EType == EEQUIP_TYPE_DRM)
	{
		w_EquipNo = stBatchAll[w_EquIdx][a_ENo].nEquipNo;
		w_EquipType = stBatchAll[w_EquIdx][a_ENo].eEquipType;
		w_DpgNo = stBatchAll[w_EquIdx][a_ENo].nDpgNo;
	}
	else
	{
		for(w_ii=0;w_ii<w_DevCnt;w_ii++)
		{
			if( stBatchAll[w_EquIdx][w_ii].eEquipType == a_EType &&
				stBatchAll[w_EquIdx][w_ii].nEquipNo == a_ENo )
			{
				if(stBatchAll[w_EquIdx][w_ii].bResult)
				{
					w_EquipNo = stBatchAll[w_EquIdx][w_ii].nEquipNo;
					w_EquipType = stBatchAll[w_EquIdx][w_ii].eEquipType;
					w_DpgNo = stBatchAll[w_EquIdx][w_ii].nDpgNo;
					break;
				}
			}
		}
	}

	memset(&stSendCurrent,0x00,sizeof(stSendCurrent));

	//현재 전송명령 구조체
	stSendCurrent.nSetNo = 1;
	stSendCurrent.nEquipType = w_EquipType;
	stSendCurrent.nEquipNo = w_EquipNo;
	stSendCurrent.nDpgNo = w_DpgNo;
	stSendCurrent.nReTry++;

	CString szFileName = _T("");
	if(stSendCurrent.nEquipType>0)
	{
		szFileName = PathFindFileName(GetSelectedFilePath(stSendCurrent.nEquipType, TRUE));

		GetSelectedFilePathVersion( szFileName, stSendCurrent.nVersion );

		OnBatchUpdateAll();
	}
}

void CDLUpdateDlg::OnBatchUpdateAll()
{
	if(!UpdateFlash(stSendCurrent.nSetNo, stSendCurrent.nEquipType, stSendCurrent.nEquipNo, stSendCurrent.nDpgNo, GetSelectedFilePath(stSendCurrent.nEquipType, TRUE)))
	{
		return;
	}
}

void CDLUpdateDlg::OnUpdateInfoClear(int a_EquipType, int a_EquipNo, int a_DpgNo)
{
	int		w_ENoIndex = 0;

	//개별 정보조회 일경우
	if(a_EquipType == EEQUIP_TYPE_DRM)
	{
		w_ENoIndex = a_EquipNo+(a_DpgNo)*9;
	} else {
		if(a_EquipNo>0)
		{
			w_ENoIndex = a_EquipNo - 1;
		}else{
			w_ENoIndex = a_EquipNo;
		}
	}

	if(a_EquipType == EEQUIP_TYPE_DMT)
	{
		stBatchAll[LPA_DMA_IDX][a_EquipNo].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DPG)
	{
		stBatchAll[LPA_DPG_IDX][a_EquipNo].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DRE)
	{
		stBatchAll[LPA_DRE_IDX][a_EquipNo].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DRE_IDX][a_EquipNo].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DRM)
	{
		stBatchAll[LPA_DRM_IDX][w_ENoIndex].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DPA)
	{
		stBatchAll[LPA_DMA_IDX][w_ENoIndex].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DMP)
	{
		stBatchAll[LPA_DMP_IDX][w_ENoIndex].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DSS)
	{
		stBatchAll[LPA_DSS_IDX][w_ENoIndex].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DFR)
	{
		stBatchAll[LPA_DFR_IDX][w_ENoIndex].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_NORMAL;
	}
	else if(a_EquipType == EEQUIP_TYPE_DMX)
	{
		stBatchAll[LPA_DMX_IDX][0].bUseFlag = FALSE;
		GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] = LPA_DATA_NORMAL;
	}

	if(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd())
	{
		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
	}
}

void CDLUpdateDlg::OnBatchUpdateAllEnd2()
{
	//DMX
	//if(stBatchAll[LPA_DMX_IDX][0].bResult)
	{
		stBatchAll[LPA_DMX_IDX][0].bUseFlag = FALSE;
		//GetLowPA()->m_EquipInfo[LPA_DMX_IDX][0].arbyReserved[2] = LPA_DATA_NORMAL;
	}
}

void CDLUpdateDlg::OnBatchUpdateAllEnd()
{
	int		w_ii = 0;
	int		w_DreMax = 0;
	int		w_DrmMax = 0;

	//일괄업데이트버튼 활성화
	//GetDlgItem(IDC_BUTTON_UPDATE_ALL)->EnableWindow(TRUE);

	//일괄업데이트 구조체 초기화
	for(w_ii=0;w_ii<m_DmtCnt;w_ii++)
	{
		if(stBatchAll[LPA_DMA_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}
	for(w_ii=0;w_ii<m_DpgCnt;w_ii++)
	{
		if(stBatchAll[LPA_DPG_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DPG_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}

	w_DreMax = 7;
	for(w_ii=0;w_ii<w_DreMax;w_ii++)
	{
		if(stBatchAll[LPA_DRE_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}

	w_DrmMax = 63;
	for(w_ii=0;w_ii<w_DrmMax;w_ii++)
	{
		if(stBatchAll[LPA_DRM_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}

	for(w_ii=0;w_ii<m_DpaCnt;w_ii++)
	{
		if(stBatchAll[LPA_DMA_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}

	for(w_ii=0;w_ii<m_DmpCnt;w_ii++)
	{
		if(stBatchAll[LPA_DMP_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}

	for(w_ii=0;w_ii<m_DssCnt;w_ii++)
	{
		if(stBatchAll[LPA_DSS_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}

	for(w_ii=0;w_ii<m_DfrCnt;w_ii++)
	{
		if(stBatchAll[LPA_DFR_IDX][w_ii].bResult)
		{
			stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = FALSE;
			//GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] = LPA_DATA_NORMAL;
		}
	}
}

BOOL CDLUpdateDlg::IsSelectedUpdateEquip(EEquipType eEquipType, BOOL bFlash)
{
	BOOL bChecked = FALSE;

	switch(eEquipType)
	{
		case EEQUIP_TYPE_DMT:			
			bChecked = m_ctrlCSBoxDMT.GetCheck();	
			break;
		case EEQUIP_TYPE_DSS:			
			bChecked = m_ctrlCSBoxDSS.GetCheck();	
			break;
		case EEQUIP_TYPE_DPG:			
			bChecked = m_ctrlCSBoxDPG.GetCheck();	
			break;
		case EEQUIP_TYPE_DRM:			
			bChecked = m_ctrlCSBoxDRM.GetCheck();	
			break;
		case EEQUIP_TYPE_DPA:			
			bChecked = m_ctrlCSBoxDPA.GetCheck();	
			break;
		case EEQUIP_TYPE_DMP:			
			bChecked = m_ctrlCSBoxDMP.GetCheck();	
			break;
		case EEQUIP_TYPE_DRE:			
			bChecked = m_ctrlCSBoxDRE.GetCheck();	
			break;
		case EEQUIP_TYPE_DFR:			
			bChecked = m_ctrlCSBoxDFR.GetCheck();	
			break;
		default:
			return FALSE;
	}

	if(bChecked == FALSE)
		return FALSE;

	CString szFilePath = GetSelectedFilePath(eEquipType, bFlash);
	if(szFilePath.IsEmpty())
	{
		return FALSE;
	}

	if(FileExist(szFilePath, FALSE) == FALSE)
	{
		return FALSE;
	}

	return TRUE;
}

CString CDLUpdateDlg::GetSelectedFilePath(EEquipType eEquipType, BOOL bFlash)
{
	CString szFilePath = _T("");

	switch(eEquipType)
	{
		case EEQUIP_TYPE_DMT:			
			GetDlgItem(IDC_EDIT_EQP_DMT_FLASH)->GetWindowText(szFilePath);		
			break;
		case EEQUIP_TYPE_DSS:			
			GetDlgItem(IDC_EDIT_EQP_DSS_FLASH)->GetWindowText(szFilePath);		
			break;
		case EEQUIP_TYPE_DPG:			
			GetDlgItem(IDC_EDIT_EQP_DPG_FLASH)->GetWindowText(szFilePath);		
			break;
		case EEQUIP_TYPE_DRM:			
			GetDlgItem(IDC_EDIT_EQP_DRM_FLASH)->GetWindowText(szFilePath);		
			break;
		case EEQUIP_TYPE_DPA:			
			GetDlgItem(IDC_EDIT_EQP_DPA_FLASH)->GetWindowText(szFilePath);		
			break;
		case EEQUIP_TYPE_DMP:			
			GetDlgItem(IDC_EDIT_EQP_DMP_FLASH)->GetWindowText(szFilePath);		
			break;
		case EEQUIP_TYPE_DRE:			
			GetDlgItem(IDC_EDIT_EQP_DRE_FLASH)->GetWindowText(szFilePath);		
			break;
		case EEQUIP_TYPE_DFR:			
			GetDlgItem(IDC_EDIT_EQP_DFR_FLASH)->GetWindowText(szFilePath);		
			break;
		default:						
			ASSERT(FALSE);	
			return szFilePath;
	}

	return szFilePath;
}

BOOL CDLUpdateDlg::IsEnabledFilePath(EEquipType eEquipType, BOOL bFlash)
{
	switch(eEquipType)
	{
		case EEQUIP_TYPE_DMT:			
			return GetDlgItem(IDC_EDIT_EQP_DMT_FLASH)->IsWindowEnabled();
		case EEQUIP_TYPE_DSS:			
			return GetDlgItem(IDC_EDIT_EQP_DSS_FLASH)->IsWindowEnabled();
		case EEQUIP_TYPE_DPG:			
			return GetDlgItem(IDC_EDIT_EQP_DPG_FLASH)->IsWindowEnabled();
		case EEQUIP_TYPE_DRM:			
			return GetDlgItem(IDC_EDIT_EQP_DRM_FLASH)->IsWindowEnabled();
		case EEQUIP_TYPE_DPA:			
			return GetDlgItem(IDC_EDIT_EQP_DPA_FLASH)->IsWindowEnabled();
		case EEQUIP_TYPE_DMP:			
			return GetDlgItem(IDC_EDIT_EQP_DMP_FLASH)->IsWindowEnabled();
		case EEQUIP_TYPE_DRE:			
			return GetDlgItem(IDC_EDIT_EQP_DRE_FLASH)->IsWindowEnabled();
		case EEQUIP_TYPE_DFR:			
			return GetDlgItem(IDC_EDIT_EQP_DFR_FLASH)->IsWindowEnabled();
		default:						
			return FALSE;
	}

	return FALSE;
}

void CDLUpdateDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nID == SC_CLOSE)
	{
		if(GetLowPA()->IsUpdateSerialOpened())
		{
			OnButtonFinishBoot();

			//GetLowPA()->SendUpdateCmd(CMD_UPDATE_CLEAR);
			GetLowPA()->UpdateStopSerial();
		}

		if(GetLowPA()->IsMixerSerialOpened())
		{
			OnButtonFinishBoot2();

			//GetLowPA()->SendMixerCmd(CMD_MIXER_CLEAR);
			GetLowPA()->MixerStopSerial();
		}

		CDialog::OnCancel();
	}
	CDialog::OnSysCommand(nID, lParam);
}


DWORD CDLUpdateDlg::GetSelectedFlashPageSize(EEquipType eEquipType)
{
	DWORD dwPageSize = 0;

	switch(eEquipType)
	{
		case EEQUIP_TYPE_DMT:			
		case EEQUIP_TYPE_DSS:			
		case EEQUIP_TYPE_DPG:			
		case EEQUIP_TYPE_DRM:			
		case EEQUIP_TYPE_DPA:			
		case EEQUIP_TYPE_DRE:			
		case EEQUIP_TYPE_DFR:
			dwPageSize = 256;
			break;
		case EEQUIP_TYPE_DMP:			
			dwPageSize = 128;
			break;			
		default:
			dwPageSize = 256;
			break;
	}

	return dwPageSize;
}

void CDLUpdateDlg::OnButtonResetInfo()
{
	int			w_ii=0;
	int			w_jj=0;
	int			w_Idx=0;
	int			w_Cnt=0;
	int			w_Total=0;
	int			w_DreMax=0;
	int			w_DrmMax=0;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	w_Total = wp_PAData->m_DevCnt[LPA_DMA_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DSS_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DPG_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DFR_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DRM_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DRE_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DMP_IDX];

	memset(&GetLowPA()->m_EquipInfo,0x00,sizeof(GetLowPA()->m_EquipInfo));
	memset(&stBatchAll,0x00,sizeof(stBatchAll));
	memset(&stSendCurrent,0x00,sizeof(stSendCurrent));

	for(w_ii=0;w_ii<m_DmtCnt;w_ii++)
	{
		stBatchAll[LPA_DMA_IDX][w_ii].nSetNo = 1;
		stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo = w_ii+1;
		stBatchAll[LPA_DMA_IDX][w_ii].eEquipType = EEQUIP_TYPE_DMT;
	}

	for(w_ii=0;w_ii<m_DpgCnt;w_ii++)
	{
		stBatchAll[LPA_DPG_IDX][w_ii].nSetNo = 1;
		stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo = w_ii+1;
		stBatchAll[LPA_DPG_IDX][w_ii].eEquipType = EEQUIP_TYPE_DPG;
	}

	if(m_DpgCnt>0)
	{
		if(m_DreCnt>0)
		{
			w_DreMax = 7;
		}
	}
	else
	{
		if(m_DreCnt>0)
		{
			w_DreMax = 1;
		}
	}
	for(w_ii=0;w_ii<w_DreMax;w_ii++)
	{
		stBatchAll[LPA_DRE_IDX][w_ii].nSetNo = 1;
		stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo = w_ii;
		stBatchAll[LPA_DRE_IDX][w_ii].eEquipType = EEQUIP_TYPE_DRE;
	}

	w_DrmMax = 63;
	for(w_ii=0;w_ii<w_DrmMax;w_ii++)
	{
		stBatchAll[LPA_DRM_IDX][w_Cnt].nSetNo = 1;
		stBatchAll[LPA_DRM_IDX][w_Cnt].nEquipNo = (w_ii%9);
		stBatchAll[LPA_DRM_IDX][w_Cnt].eEquipType = EEQUIP_TYPE_DRM;
		if( w_ii>0 && (w_ii%9)==0 )
		{
			w_Idx++;
		}
		stBatchAll[LPA_DRM_IDX][w_Cnt].nDpgNo = (w_Idx)%7;

		w_Cnt++;
	}

	for(w_ii=0;w_ii<m_DpaCnt;w_ii++)
	{
		stBatchAll[LPA_DMA_IDX][w_ii].nSetNo = 1;
		stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo = w_ii+1;
		stBatchAll[LPA_DMA_IDX][w_ii].eEquipType = EEQUIP_TYPE_DPA;
	}

	for(w_ii=0;w_ii<m_DmpCnt;w_ii++)
	{
		stBatchAll[LPA_DMP_IDX][w_ii].nSetNo = 1;
		stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo = w_ii+1;
		stBatchAll[LPA_DMP_IDX][w_ii].eEquipType = EEQUIP_TYPE_DMP;
	}

	for(w_ii=0;w_ii<m_DssCnt;w_ii++)
	{
		stBatchAll[LPA_DSS_IDX][w_ii].nSetNo = 1;
		stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo = w_ii+1;
		stBatchAll[LPA_DSS_IDX][w_ii].eEquipType = EEQUIP_TYPE_DSS;
	}

	for(w_ii=0;w_ii<m_DfrCnt;w_ii++)
	{
		stBatchAll[LPA_DFR_IDX][w_ii].nSetNo = 1;
		stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo = w_ii+1;
		stBatchAll[LPA_DFR_IDX][w_ii].eEquipType = EEQUIP_TYPE_DFR;
	}

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if( !GetLowPA()->IsUpdateSerialOpened() )
	{
		//ADD: Multi-Language String Table @HCH:2010.10.26
		MessageBox(_T("메인장치와 연결되는 통신포트를 열 수 없습니다."));
		//MessageBox(g_LangTable[113]);
		return;
	}

	OnButtonEquipInfoDialog();

	m_bEquipInfoAll = TRUE;
	
	//일괄 조회 명령
	EnableWindow(FALSE);
	m_ctrlProgressUpdate.SetRange32(0, w_Total);

	//전체정보조회
	GetLowPA()->m_UpdateDevStat = DEV_STAT_NEXT;
	GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMT;

	//현재 전송명령 구조체
	stSendCurrent.nSetNo = 1;
	stSendCurrent.nEquipType = EEQUIP_TYPE_DMT;
	stSendCurrent.nEquipNo = 1;
	stSendCurrent.nDpgNo = 0;
	stSendCurrent.nReTry++;

	GetLowPA()->SendUpdateCmd( CMD_UPDATE_INFO
							  ,0x0
							  ,0x0
							  ,(BYTE)stSendCurrent.nDpgNo
							  ,0x0
							  ,(BYTE)stSendCurrent.nEquipType
							  ,(BYTE)stSendCurrent.nEquipNo );

	SetTimer(WM_TIMER_SENDCMD,UPDATE_INFO_TIMEVALUE,NULL);
}

void CDLUpdateDlg::OnSendEquipInfo(int a_EType, int a_ENo)
{
	int			w_ii=0;
	int			w_jj=0;
	int			w_EquIdx=0;
	int			w_DevCnt=0;
	int			w_Total=0;

	int			w_DpgNo=0;
	int			w_EquipNo=0;
	int			w_EquipType=0;

	KillTimer(WM_TIMER_SENDCMD);
	memset(&stSendCurrent,0x00,sizeof(stSendCurrent));

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}

	w_Total = wp_PAData->m_DevCnt[LPA_DMA_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DSS_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DPG_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DFR_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DRM_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DRE_IDX] + 
			  wp_PAData->m_DevCnt[LPA_DMP_IDX];


	if(GetLowPA()->m_SendUpdateInfo > w_Total)
	{
		DL_LOG("[SEND INFO ERROR][SEND:%d, TOTAL:%d]",GetLowPA()->m_SendUpdateInfo, w_Total);
		return;
	}

	if(GetLowPA()->m_UpdateDevStat == DEV_STAT_NEXT)
	{
		switch(a_EType)
		{
			case EEQUIP_TYPE_DMT:
				w_EquIdx = LPA_DMA_IDX;
				w_DevCnt = m_DmtCnt;
				break;
			case EEQUIP_TYPE_DPG:
				w_EquIdx = LPA_DPG_IDX;
				w_DevCnt = m_DpgCnt;
				break;
			case EEQUIP_TYPE_DRE:
				w_EquIdx = LPA_DRE_IDX;
				if(m_DpgCnt>0)
				{
					if(m_DreCnt>0)
					{
						w_DevCnt = 7;
					}
				} else {
					if(m_DreCnt>0)
					{
						w_DevCnt = 1;
					}
				}
				break;
			case EEQUIP_TYPE_DRM:
				w_EquIdx = LPA_DRM_IDX;
				w_DevCnt = 63;
				break;
			case EEQUIP_TYPE_DPA:
				w_EquIdx = LPA_DMA_IDX;
				if(m_DpaCnt>0)
				{
					w_DevCnt = m_DpaCnt;
				}
				break;
			case EEQUIP_TYPE_DMP:
				w_EquIdx = LPA_DMP_IDX;
				if(m_DmpCnt>0)
				{
					w_DevCnt = m_DmpCnt;
				}
				break;
			case EEQUIP_TYPE_DSS:
				w_EquIdx = LPA_DSS_IDX;
				if(m_DssCnt>0)
				{
					w_DevCnt = m_DssCnt;
				}
				break;
			case EEQUIP_TYPE_DFR:
				w_EquIdx = LPA_DFR_IDX;
				if(m_DfrCnt>0)
				{
					w_DevCnt = m_DfrCnt;
				}
				break;
		}

		if(a_EType == EEQUIP_TYPE_DRM)
		{
			if(!stBatchAll[w_EquIdx][a_ENo].bResult)
			{
				stSendCurrent.nEquipIndex = a_ENo;
				w_DpgNo = stBatchAll[w_EquIdx][a_ENo].nDpgNo;
				w_EquipNo = stBatchAll[w_EquIdx][a_ENo].nEquipNo;
				w_EquipType = stBatchAll[w_EquIdx][a_ENo].eEquipType;
				stBatchAll[w_EquIdx][a_ENo].bUseFlag = TRUE;
			}
		}
		else
		{
			for(w_ii=0;w_ii<w_DevCnt;w_ii++)
			{
				if( stBatchAll[w_EquIdx][w_ii].eEquipType == a_EType &&
					stBatchAll[w_EquIdx][w_ii].nEquipNo == a_ENo )
				{
					if(!stBatchAll[w_EquIdx][w_ii].bResult)
					{
						w_EquipNo = stBatchAll[w_EquIdx][w_ii].nEquipNo;
						w_EquipType = stBatchAll[w_EquIdx][w_ii].eEquipType;
						stBatchAll[w_EquIdx][w_ii].bUseFlag = TRUE;
						break;
					}
				}
			}
		}

		switch(a_EType)
		{
			//ADD: Multi-Language String Table @HCH:2010.10.26
		case EEQUIP_TYPE_DMT:
			m_szState =			_T("DMT 정보조회 중입니다.");				
			break;
		case EEQUIP_TYPE_DSS:
			m_szState.Format(_T("DSS %d 정보조회 중입니다."), a_ENo);
			break;
		case EEQUIP_TYPE_DPG:
			m_szState =			_T("DPG 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DRM:
			m_szState =			_T("DRM 정보조회 중입니다.");
			break;
		case EEQUIP_TYPE_DPA:
			m_szState =			_T("DPA 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DMP:
			m_szState.Format(_T("DMP %d 정보조회 중입니다."),a_ENo);
			break;
		case EEQUIP_TYPE_DRE:
			m_szState =			_T("DRE 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DFR:
			m_szState.Format(_T("DFR %d 정보조회 중입니다."),a_ENo);
			break;
		case EEQUIP_TYPE_DMX:
			m_szState =			_T("DMX 를 정보조회 중입니다.");				
			break;
		default:						
			//ASSERT(FALSE);
			return;
		}
		UpdateData(FALSE);

		//현재 전송명령 구조체
		stSendCurrent.nSetNo = 1;
		stSendCurrent.nEquipType = w_EquipType;
		stSendCurrent.nEquipNo = w_EquipNo;
		stSendCurrent.nDpgNo = w_DpgNo;
		stSendCurrent.nReTry++;

		GetLowPA()->SendUpdateCmd(  CMD_UPDATE_INFO
									,0x0
									,0x0
									,(BYTE)(stSendCurrent.nDpgNo<<1)
									,0x0
									,(BYTE)stSendCurrent.nEquipType
									,(BYTE)stSendCurrent.nEquipNo );

		SetTimer(WM_TIMER_SENDCMD,UPDATE_INFO_TIMEVALUE,NULL);
	}

}

void CDLUpdateDlg::DoReSendUpdateFlash2()
{
	int		w_ii=0;
	int		w_jj=0;

	int		w_DpgNo=0;
	int		w_EquipNo=0;
	int		w_EquipType=0;
	int		w_EquIdx=0;
	int		w_ENoIndex=0;

	int		w_Rtry=0;
	int		w_SendNum=0;


	//전체 FLASH 업데이트인경우
	if( m_bBatchAll )
	{
		for(w_ii=0;w_ii<m_nBatchAllCount;w_ii++)
		{
			if( stAllSendCurrent[w_ii].nSendOk==0 && w_ii == m_nSendCur )
			{
				w_DpgNo= stAllSendCurrent[w_ii].nDpgNo;
				w_EquipNo=stAllSendCurrent[w_ii].nEquipNo;
				w_EquipType=stAllSendCurrent[w_ii].nEquipType;
				w_Rtry = stAllSendCurrent[w_ii].nReTry;
				break;
			}
		}

		if(w_Rtry>3)
		{
			stAllSendCurrent[m_nSendCur].nSendOk = TRUE;
			if(stAllSendCurrent[m_nSendCur].nEquipType == EEQUIP_TYPE_DRM)
			{
				w_ENoIndex = stAllSendCurrent[m_nSendCur].nEquipNo+(stAllSendCurrent[m_nSendCur].nDpgNo)*9;
			} else {
				if(w_EquipNo>0 && stAllSendCurrent[m_nSendCur].nEquipType != EEQUIP_TYPE_DRE)
				{
					w_ENoIndex = stAllSendCurrent[m_nSendCur].nEquipNo - 1;
				}else{
					w_ENoIndex = stAllSendCurrent[m_nSendCur].nEquipNo;
				}
			}

			switch(stAllSendCurrent[m_nSendCur].nEquipType)
			{
				case EEQUIP_TYPE_DMT:
					w_EquIdx = LPA_DMA_IDX;
					break;
				case EEQUIP_TYPE_DPG:
					w_EquIdx = LPA_DPG_IDX;
					break;
				case EEQUIP_TYPE_DRE:
					w_EquIdx = LPA_DRE_IDX;
					break;
				case EEQUIP_TYPE_DRM:
					w_EquIdx = LPA_DRM_IDX;
					break;
				case EEQUIP_TYPE_DPA:
					w_EquIdx = LPA_DMA_IDX;
					break;
				case EEQUIP_TYPE_DMP:
					w_EquIdx = LPA_DMP_IDX;
					break;
				case EEQUIP_TYPE_DSS:
					w_EquIdx = LPA_DSS_IDX;
					break;
				case EEQUIP_TYPE_DFR:
					w_EquIdx = LPA_DFR_IDX;
					break;
			}
			GetLowPA()->m_EquipInfo[w_EquIdx][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			GetLowPA()->m_SendUpdateData++;
			m_nSendCur++;
			if( GetLowPA()->m_SendUpdateData >= m_nBatchAllCount )
			{
				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
				GetLowPA()->m_SendUpdateData = 0;
				PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
				PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);

				if(GetUpdateWnd()->m_pEquipInfoDlg)
				{
					::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_DATA, 0, 0);
				}
				return;
			}
		}
		else
		{
			if( (stAllSendCurrent[m_nSendCur].nSendPage == GetLowPA()->m_SendUpdatePage) &&
				(stAllSendCurrent[m_nSendCur].nEquipType == stSendCurrent.nEquipType) &&
				(stAllSendCurrent[m_nSendCur].nEquipNo == stSendCurrent.nEquipNo) &&
				(stAllSendCurrent[m_nSendCur].nDpgNo == stSendCurrent.nDpgNo) )
			{
				stAllSendCurrent[m_nSendCur].nReTry++;
			}else{
				stAllSendCurrent[m_nSendCur].nReTry = 1;
				stAllSendCurrent[m_nSendCur].nSendPage = GetLowPA()->m_SendUpdatePage;
			}
			GetLowPA()->SendUpdateData(GetLowPA()->m_SendUpdatePage);
			SetTimer(WM_TIMER_SENDFLASHCMD,UPDATE_INFO_TIMEVALUE,NULL);
			DL_LOG("[RETRY:%d][TYPE:%d][NO:%d][PGNO:%d]",stAllSendCurrent[m_nSendCur].nReTry,stAllSendCurrent[m_nSendCur].nEquipType, stAllSendCurrent[m_nSendCur].nEquipNo,stAllSendCurrent[m_nSendCur].nDpgNo);
			return;
		}
	}
	else
	{
		switch(stSendCurrent.nEquipType)
		{
			case EEQUIP_TYPE_DMT:
				w_EquIdx = LPA_DMA_IDX;
				break;
			case EEQUIP_TYPE_DPG:
				w_EquIdx = LPA_DPG_IDX;
				break;
			case EEQUIP_TYPE_DRE:
				w_EquIdx = LPA_DRE_IDX;
				break;
			case EEQUIP_TYPE_DRM:
				w_EquIdx = LPA_DRM_IDX;
				break;
			case EEQUIP_TYPE_DPA:
				w_EquIdx = LPA_DMA_IDX;
				break;
			case EEQUIP_TYPE_DMP:
				w_EquIdx = LPA_DMP_IDX;
				break;
			case EEQUIP_TYPE_DSS:
				w_EquIdx = LPA_DSS_IDX;
				break;
			case EEQUIP_TYPE_DFR:
				w_EquIdx = LPA_DFR_IDX;
				break;
			case EEQUIP_TYPE_DMX:
				w_EquIdx = LPA_DMX_IDX;
				break;
		}
		m_nSendCur = w_EquIdx;

		w_Rtry = stAllSendCurrent[m_nSendCur].nReTry;
		if(w_Rtry>3)
		{
			stSendCurrent.nSendOk = TRUE;
			if(stSendCurrent.nEquipType == EEQUIP_TYPE_DRM)
			{
				w_ENoIndex = stSendCurrent.nEquipNo+(stSendCurrent.nDpgNo)*9;
			} else {
				if(stSendCurrent.nEquipNo>0 && stSendCurrent.nEquipType != EEQUIP_TYPE_DRE)
				{
					w_ENoIndex = stSendCurrent.nEquipNo - 1;
				}else{
					w_ENoIndex = stSendCurrent.nEquipNo;
				}
			}

			GetLowPA()->m_EquipInfo[w_EquIdx][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;

			GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
			GetLowPA()->m_SendUpdateData = 0;
			PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
			PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);

			if(GetUpdateWnd()->m_pEquipInfoDlg)
			{
				::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_DATA, 0, 0);
			}
			return;
		}
		else
		{
			if( (stAllSendCurrent[m_nSendCur].nSendPage == GetLowPA()->m_SendUpdatePage) &&
				(stAllSendCurrent[m_nSendCur].nEquipType == stSendCurrent.nEquipType) &&
				(stAllSendCurrent[m_nSendCur].nEquipNo == stSendCurrent.nEquipNo) &&
				(stAllSendCurrent[m_nSendCur].nDpgNo == stSendCurrent.nDpgNo) )
			{
				stAllSendCurrent[m_nSendCur].nReTry++;
			}else{
				stAllSendCurrent[m_nSendCur].nReTry = 1;
				stAllSendCurrent[m_nSendCur].nSendPage = GetLowPA()->m_SendUpdatePage;
			}
			GetLowPA()->SendUpdateData(GetLowPA()->m_SendUpdatePage);
			SetTimer(WM_TIMER_SENDFLASHCMD,UPDATE_INFO_TIMEVALUE,NULL);
			DL_LOG("[RETRY IND:%d][TYPE:%d][NO:%d][PGNO:%d]",stAllSendCurrent[m_nSendCur].nReTry,stAllSendCurrent[m_nSendCur].nEquipType, stAllSendCurrent[m_nSendCur].nEquipNo,stAllSendCurrent[m_nSendCur].nDpgNo);
			return;
		}
	}

	if(stAllSendCurrent[m_nSendCur].nEquipType == EEQUIP_TYPE_DRM)
	{
		w_SendNum = stAllSendCurrent[m_nSendCur].nEquipNo+(stAllSendCurrent[m_nSendCur].nDpgNo)*9;
	}else{
		w_SendNum = stAllSendCurrent[m_nSendCur].nEquipNo;
	}
	stAllSendCurrent[m_nSendCur].nReTry++;
	PostMessage(WM_UPDATE_DATA, stAllSendCurrent[m_nSendCur].nEquipType, w_SendNum);
	if(GetUpdateWnd()->m_pEquipInfoDlg)
	{
		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_DATA, 0, 0);
	}



}

//업데이트 타임아웃발생시, 다음장치 업데이트 루틴 처리함수
void CDLUpdateDlg::DoReSendUpdateFlash()
{
	int		w_ii=0;
	int		w_jj=0;
	int		w_DevCnt=0;
	int		w_Total=0;

	int		w_DpgNo=0;
	int		w_EquipNo=0;
	int		w_EquipType=0;
	int		w_EquIdx=0;
	int		w_ENoIndex=0;

	int		w_CheckDmt=0;
	int		w_CheckDpg=0;
	int		w_CheckDre=0;
	int		w_CheckDrm=0;
	int		w_CheckDpa=0;
	int		w_CheckDmp=0;
	int		w_CheckDss=0;
	int		w_CheckDfr=0;
	int		w_CheckDrmMax=0;

	int		w_Rtry=0;

	int		w_SendNum=0;

	CString	szFilePath = _T("");
	CString	szFileName = _T("");
	CString strContent = _T("");
	CString strEquipName = _T("");

	if(stSendCurrent.nEquipType == EEQUIP_TYPE_DRM)
	{
		w_ENoIndex = stSendCurrent.nEquipNo+(stSendCurrent.nDpgNo>>1)*9;
	} else {
		if(stSendCurrent.nEquipNo>0)
		{
			w_ENoIndex = stSendCurrent.nEquipNo - 1;
		}else{
			w_ENoIndex = stSendCurrent.nEquipNo;
		}
	}

	//전체 FLASH 업데이트인경우
	if( m_bBatchAll )
	{
		w_CheckDmt = ( m_ctrlCSBoxDMT.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DMT) : 0;
		w_CheckDpg = ( m_ctrlCSBoxDPG.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DPG) : 0;
		w_CheckDre = ( m_ctrlCSBoxDRE.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DRE) : 0;
		w_CheckDrm = ( m_ctrlCSBoxDRM.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DRM) : 0;
		w_CheckDpa = ( m_ctrlCSBoxDPA.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DPA) : 0;
		w_CheckDmp = ( m_ctrlCSBoxDMP.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DMP) : 0;
		w_CheckDss = ( m_ctrlCSBoxDSS.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DSS) : 0;
		w_CheckDfr = ( m_ctrlCSBoxDFR.GetCheck() ) ? GetCheckEquipCount(EEQUIP_TYPE_DFR) : 0;

		if (GetLowPA()->m_UpdateFlag == DEV_UPDATE_DMT)
		{
			DL_LOG("[DMT][SEND TIMEOUT]");
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DmtCnt;
			GetLowPA()->m_SendUpdateData = w_CheckDmt;

			//반복횟수를 판별하여, 동일장비의 같은페이지의 에러가 2회 발생시, 다음장비로 스킵처리
			if(stSendCurrent.nReTry>2)
			{
				GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
				if( GetLowPA()->m_SendUpdateData >= m_nBatchAllCount )
				{
					GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
					GetLowPA()->m_SendUpdateData = 0;
					PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
					PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
				}else{
					if(w_CheckDpg>0)
					{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPG;
					}else{
						if(w_CheckDre)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRE;
						}else{		
							if(w_CheckDrm>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRM;
							}else{
								if(w_CheckDpa>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
								}else{
									if(w_CheckDmp>0)
									{
										GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
									}else{
										if(w_CheckDss>0)
										{
											GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
										}else{
											if(w_CheckDfr>0)
											{
												GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
											}else{
												GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
												GetLowPA()->m_SendUpdateData = 0;
												PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
												PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
											}
										}
									}
								}
							}
						}
					}
					stSendCurrent.nReTry = 1;
					stSendCurrent.nSendPage = 0;
				}
			}
			else
			{
				if(stSendCurrent.nSendPage == GetLowPA()->m_SendUpdatePage)
				{
					GetLowPA()->SendUpdateData(GetLowPA()->m_SendUpdatePage);
					SetTimer(WM_TIMER_SENDFLASHCMD,UPDATE_INFO_TIMEVALUE,NULL);
					stSendCurrent.nReTry++;
				}
			}
		}

		if (GetLowPA()->m_UpdateFlag == DEV_UPDATE_DPG)
		{
			w_EquIdx = LPA_DPG_IDX;
			w_DevCnt = m_DpgCnt;
			if(stSendCurrent.nReTry==1)
			{
				for(w_ii=0;w_ii<w_CheckDpg;w_ii++)
				{
					if( stBatchAll[LPA_DPG_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
						stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo )
					{
						DL_LOG("[DPG][SEND TIMEOUT]");
						GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
						GetLowPA()->m_SendUpdateData++;
						break;
					}else{
						w_SendNum = stBatchAll[LPA_DPG_IDX][w_ii].nEquipNo;
						break;
					}
				}
			}

			if( stSendCurrent.nReTry>2 )
			{
				if( GetLowPA()->m_SendUpdateData >=  m_nBatchAllCount )
				{
					GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
					GetLowPA()->m_SendUpdateData = 0;
					PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
					PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
				}
				else
				{
					if(GetLowPA()->m_SendUpdateData >= w_CheckDmt + w_CheckDpg)
					{
						if(w_CheckDre>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRE;
						}
						else
						{					
							if(w_CheckDrm>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRM;
							}
							else
							{
								if(w_CheckDpa>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
								}else{
									if(w_CheckDmp>0)
									{
										GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
									}else{
										if(w_CheckDss>0)
										{
											GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
										}else{
											if(w_CheckDfr>0)
											{
												GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
											}else{
												GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
												GetLowPA()->m_SendUpdateData = 0;
												PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
												PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
											}
										}
									}
								}
							}
						}
						stSendCurrent.nReTry = 1;
					}
					else
					{
						PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DPG, w_SendNum);
					}
				}
			}
			else
			{
				if(stSendCurrent.nReTry==1 && stSendCurrent.nSendPage==0)
				{
					PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DPG, w_SendNum);
					//stSendCurrent.nReTry++;
				}
				else
				{				
					if(stSendCurrent.nSendPage == GetLowPA()->m_SendUpdatePage)
					{
						GetLowPA()->SendUpdateData(GetLowPA()->m_SendUpdatePage);
						SetTimer(WM_TIMER_SENDFLASHCMD,UPDATE_INFO_TIMEVALUE,NULL);
						stSendCurrent.nReTry++;
					}
				}
			}
		}

		if (GetLowPA()->m_UpdateFlag == DEV_UPDATE_DRE)
		{
			w_EquIdx = LPA_DRE_IDX;
			w_DevCnt = 7;
			for(w_ii=0;w_ii<w_DevCnt;w_ii++)
			{
				if( stBatchAll[LPA_DRE_IDX][w_ii].bResult )
				{
					if( stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
						stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
					{
						DL_LOG("[DRE][SEND TIMEOUT]");
						GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
						GetLowPA()->m_SendUpdateData++;
					}else{
						if( stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag )
						{
							if( stBatchAll[LPA_DRE_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
								stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
							{
								DL_LOG("[DRE][SEND TIMEOUT]");
								GetLowPA()->m_EquipInfo[LPA_DRE_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
								GetLowPA()->m_SendUpdateData++;
							}
						} else {
							stBatchAll[LPA_DRE_IDX][w_ii].bUseFlag = TRUE;
							w_SendNum = stBatchAll[LPA_DRE_IDX][w_ii].nEquipNo;
							break;
						}
					}
				}
			}

			if( GetLowPA()->m_SendUpdateData >=  m_nBatchAllCount )
			{
				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
				GetLowPA()->m_SendUpdateData = 0;
				PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
				PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
			}
			else
			{
				if( GetLowPA()->m_SendUpdateData >=	w_CheckDmt + 
													w_CheckDpg + 
													w_CheckDre )
				{
					if(w_CheckDrm>0)
					{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRM;
					}else{
						if(w_CheckDpa>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
						}else{
							if(w_CheckDmp>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
							}else{
								if(w_CheckDss>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
								}else{
									if(w_CheckDfr>0)
									{
										GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
									}else{
										GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
										GetLowPA()->m_SendUpdateData = 0;
										PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
										PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
									}
								}
							}
						}
					}
				}
				else
				{
					PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DRE, w_SendNum);
				}
			}
		}

		if (GetLowPA()->m_UpdateFlag == DEV_UPDATE_DRM)
		{
			w_EquIdx = LPA_DRM_IDX;
			w_DevCnt = 63;

			DL_LOG("[DRM CHECK][TYPE:%d][NO:%d][PGNO:%d]",stSendCurrent.nEquipType,stSendCurrent.nEquipNo,stSendCurrent.nDpgNo);

			for(w_ii=0;w_ii<w_DevCnt;w_ii++)
			{
				if( stBatchAll[LPA_DRM_IDX][w_ii].bResult )
				{
					if( stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
						stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo &&
						stBatchAll[LPA_DRM_IDX][w_ii].nDpgNo == stSendCurrent.nDpgNo )
					{
						DL_LOG("[DRM][SEND TIMEOUT]");
						GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
						GetLowPA()->m_SendUpdateData++;
					}
					else
					{
						if( stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag )
						{
							if( stBatchAll[LPA_DRM_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
								stBatchAll[LPA_DRM_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo &&
								stBatchAll[LPA_DRM_IDX][w_ii].nDpgNo == stSendCurrent.nDpgNo )
							{
								DL_LOG("[DRM][SEND TIMEOUT]");
								GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
								GetLowPA()->m_SendUpdateData++;
							}
						} else {
							GetUpdateWnd()->stBatchAll[LPA_DRM_IDX][w_ii].bUseFlag = TRUE;
							w_SendNum = w_ii;
							break;
						}
					}
				}
			}

			if( GetLowPA()->m_SendUpdateData >=  m_nBatchAllCount )
			{
				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
				GetLowPA()->m_SendUpdateData = 0;
				PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
				PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
			}
			else
			{
				if( GetLowPA()->m_SendUpdateData >=	w_CheckDmt + 
													w_CheckDpg + 
													w_CheckDre +
													w_CheckDrm )
				{
					if(w_CheckDpa>0)
					{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
					}else{
						if(w_CheckDmp>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
						}else{
							if(w_CheckDss>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
							}else{
								if(w_CheckDfr>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
								}else{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
									GetLowPA()->m_SendUpdateData = 0;
									PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
									PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
								}
							}
						}
					}
				}
				else
				{
					PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DRM, w_SendNum);
				}
			}
		}

		if (GetLowPA()->m_UpdateFlag == DEV_UPDATE_DPA)
		{
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DpaCnt;
			for(w_ii=0;w_ii<w_DevCnt;w_ii++)
			{
				if( stBatchAll[LPA_DMA_IDX][w_ii].bResult )
				{
					if( stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
						stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
					{
						GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
						GetLowPA()->m_SendUpdateData++;
					}else{
						if( stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag )
						{
							if( stBatchAll[LPA_DMA_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
								stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
							{
								GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
								GetLowPA()->m_SendUpdateData++;
							}
						} else {
							stBatchAll[LPA_DMA_IDX][w_ii].bUseFlag = TRUE;
							w_SendNum = stBatchAll[LPA_DMA_IDX][w_ii].nEquipNo;
							break;
						}
					}
				}
			}
			if( GetLowPA()->m_SendUpdateData >=  m_nBatchAllCount )
			{
				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
				GetLowPA()->m_SendUpdateData = 0;
				PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
				PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
			}else{
				if( GetLowPA()->m_SendUpdateData >=	w_CheckDmt + 
													w_CheckDpg + 
													w_CheckDre +
													w_CheckDrm +
													w_CheckDpa )
				{
					if(w_CheckDmp>0)
					{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
					}else{
						if(w_CheckDss>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
						}else{
							if(w_CheckDfr>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
							}else{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
								GetLowPA()->m_SendUpdateData = 0;
								PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
								PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
							}
						}
					}
				}
				else
				{
					PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DPA, w_SendNum);
				}
			}
		}

		if ( GetLowPA()->m_UpdateFlag == DEV_UPDATE_DMP )
		{
			w_EquIdx = LPA_DMP_IDX;
			w_DevCnt = m_DmpCnt;
			for(w_ii=0;w_ii<w_DevCnt;w_ii++)
			{
				if( stBatchAll[LPA_DMP_IDX][w_ii].bResult )
				{
					if( stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
						stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
					{
						GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
						GetLowPA()->m_SendUpdateData++;
					}
					else
					{
						if( stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag )
						{
							if( stBatchAll[LPA_DMP_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
								stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
							{
								GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
								GetLowPA()->m_SendUpdateData++;
							}
						} else {
							stBatchAll[LPA_DMP_IDX][w_ii].bUseFlag = TRUE;
							w_SendNum = stBatchAll[LPA_DMP_IDX][w_ii].nEquipNo;
							break;
						}
					}
				}
			}
			if( GetLowPA()->m_SendUpdateData >=  m_nBatchAllCount )
			{
				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
				GetLowPA()->m_SendUpdateData = 0;
				PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
				PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
			}
			else
			{
				if( GetLowPA()->m_SendUpdateData >=	w_CheckDmt + 
													w_CheckDpg + 
													w_CheckDre +
													w_CheckDrm +
													w_CheckDpa +
													w_CheckDmp )
				{
					if(w_CheckDss>0)
					{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
					}else{
						if(w_CheckDfr>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
						}else{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
							GetLowPA()->m_SendUpdateData = 0;
							PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
							PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
						}
					}
				}
				else
				{
					PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DMP, w_SendNum);
				}
			}
		}

		if ( GetLowPA()->m_UpdateFlag == DEV_UPDATE_DSS )
		{
			w_EquIdx = LPA_DSS_IDX;
			w_DevCnt = m_DssCnt;

			for(w_ii=0;w_ii<w_DevCnt;w_ii++)
			{
				if( stBatchAll[LPA_DSS_IDX][w_ii].bResult )
				{
					if( stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
						stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
					{
						GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
						GetLowPA()->m_SendUpdateData++;
					}else{
						if( stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag)
						{
							if( stBatchAll[LPA_DSS_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
								stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
							{
								GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
								GetLowPA()->m_SendUpdateData++;
							}
						} else {
							stBatchAll[LPA_DSS_IDX][w_ii].bUseFlag = TRUE;
							w_SendNum = stBatchAll[LPA_DSS_IDX][w_ii].nEquipNo;
							break;
						}
					}
				}
			}
			if( GetLowPA()->m_SendUpdateData >=  m_nBatchAllCount )
			{
				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
				GetLowPA()->m_SendUpdateData = 0;
				PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
				PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
			}else{
				if( GetLowPA()->m_SendUpdateData >=	w_CheckDmt + 
													w_CheckDpg + 
													w_CheckDre +
													w_CheckDrm +
													w_CheckDpa +
													w_CheckDmp +
													w_CheckDss )
				{
					if(w_CheckDfr>0)
					{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
					}else{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
						GetLowPA()->m_SendUpdateData = 0;
						PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
						PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
					}
				}
				else
				{
					PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DSS, w_SendNum);
				}
			}
		}

		if ( GetLowPA()->m_UpdateFlag == DEV_UPDATE_DFR )
		{
			w_EquIdx = LPA_DFR_IDX;
			w_DevCnt = m_DfrCnt;
			for(w_ii=0;w_ii<w_DevCnt;w_ii++)
			{
				if( stBatchAll[LPA_DFR_IDX][w_ii].bResult )
				{
					if( stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
						stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
					{
						GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
						GetLowPA()->m_SendUpdateData++;
					}else{
						if( stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag )
						{
							if( stBatchAll[LPA_DFR_IDX][w_ii].eEquipType == stSendCurrent.nEquipType && 
								stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo == stSendCurrent.nEquipNo)
							{
								GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ii].arbyReserved[2] = LPA_DATA_FAILED;
								GetLowPA()->m_SendUpdateData++;
							}
						} else {
							stBatchAll[LPA_DFR_IDX][w_ii].bUseFlag = TRUE;
							w_SendNum = stBatchAll[LPA_DFR_IDX][w_ii].nEquipNo;
							break;
						}
					}
				}
			}
			if( GetLowPA()->m_SendUpdateData >=  m_nBatchAllCount )
			{
				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
				GetLowPA()->m_SendUpdateData = 0;
				PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
				PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
			}
			else
			{
				if( GetLowPA()->m_SendUpdateData >=	w_CheckDmt + 
													w_CheckDpg + 
													w_CheckDre +
													w_CheckDrm +
													w_CheckDpa +
													w_CheckDmp +
													w_CheckDss +
													w_CheckDfr )
				{
					GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
					GetLowPA()->m_SendUpdateData = 0;
					PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
					PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
				}
				else
				{
					PostMessage(WM_UPDATE_DATA, EEQUIP_TYPE_DFR, w_SendNum);
				}
			}
		}
	}
	else
	{
		switch(stSendCurrent.nEquipType)
		{
		case EEQUIP_TYPE_DMT:
			strEquipName.Format(_T("DMT"));
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DSS:
			strEquipName.Format(_T("DSS %d"), stSendCurrent.nEquipNo);
			GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DPG:
			strEquipName.Format(_T("DPG"));
			GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DRM:
			strEquipName.Format(_T("DRM %d번(DPG출력 %d번)"), stSendCurrent.nEquipNo, stSendCurrent.nDpgNo);
			GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DPA:
			strEquipName.Format(_T("DPA %d번"), stSendCurrent.nEquipNo);
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DMP:
			strEquipName.Format(_T("DMP %d번"), stSendCurrent.nEquipNo);
			GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DRE:
			strEquipName.Format(_T("DRE %d번"), stSendCurrent.nEquipNo);
			GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DFR:
			strEquipName.Format(_T("DFR %d번"), stSendCurrent.nEquipNo);
			GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		case EEQUIP_TYPE_DMX:
			strEquipName.Format(_T("DMX"));
			GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nSetNo = 1;
			GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
			GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
			GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].arbyReserved[2] = LPA_DATA_FAILED;
			break;
		}
		strContent.Format(_T("%s 장치가 존재하지 않습니다.\n올바른 장치주소[DRM일경우, DPG/DRE 출력번호]를 선택하신 후, \n업데이트를 하시기 바랍니다."), strEquipName);
		MessageBox(strContent);

		//개별 업그레이드시
		GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
		GetLowPA()->m_SendUpdateData = 0;
		PostMessage(WM_UPDATE_DATA, DEV_UPDATE_END, 0);
		PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateData);
	}

	if(GetUpdateWnd()->m_pEquipInfoDlg)
	{
		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_DATA, 0, 0);
	}
}

void CDLUpdateDlg::DoReSendUpdateInfo()
{
	int		w_ii=0;
	int		w_jj=0;
	int		w_DevCnt=0;
	int		w_Total=0;

	int		w_DpgNo=0;
	int		w_EquipNo=0;
	int		w_EquipType=0;
	int		w_EquIdx=0;
	int		w_ENoIndex=0;

	int		w_DreEquNo=0;
	int		w_CheckDpg=0;
	int		w_CheckDre=0;
	int		w_CheckDrmMax=0;

	int		w_Find=0;

	CString strContent = _T("");
	CString strEquipName = _T("");

	if(stSendCurrent.nEquipType == EEQUIP_TYPE_DRM)
	{
		w_ENoIndex = stSendCurrent.nEquipNo+(stSendCurrent.nDpgNo)*9;
	} else {
		if(stSendCurrent.nEquipNo>0)
		{
			w_ENoIndex = stSendCurrent.nEquipNo - 1;
		}else{
			w_ENoIndex = stSendCurrent.nEquipNo;
		}
	}

	//전체정보 조회인경우
	if( GetLowPA()->m_UpdateDevStat == DEV_STAT_NEXT)
	{
		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DMT)
		{
			DL_LOG("[DMT][SEND TIMEOUT]");
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DmtCnt;

			{
				stSendCurrent.nReTry++;
				if(stSendCurrent.nReTry>2)
				{
					if(GetUpdateWnd()) 
					{
						PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateInfo);

						if(GetUpdateWnd()->m_pEquipInfoDlg)
						{
							::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
						}
					}
					//stSendCurrent.nEquipNo++;
					if(stSendCurrent.nEquipNo > w_DevCnt || stSendCurrent.nReTry>2)
					{
						GetLowPA()->m_SendUpdateInfo = m_DmtCnt;

						DL_LOG("[DMT][SEND EQUIPINFO END]");
						memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
						GetLowPA()->m_SendUpdateInfo = 0;
						PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
						PostMessage(WM_UPDATE_INFO, DEV_UPDATE_ENDERROR, 0);
						return;
					}
				}
			}
		}

		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DPG)
		{
			DL_LOG("[DPG][SEND TIMEOUT]");
			w_EquIdx = LPA_DPG_IDX;
			w_DevCnt = m_DpgCnt;
			//if(stSendCurrent.nEquipNo > w_DevCnt)
			//{
			//	if(m_DreCnt > 0)
			//	{
			//		stSendCurrent.nEquipType = EEQUIP_TYPE_DRE;
			//		stSendCurrent.nEquipNo = 1;
			//	}
			//	else
			//	{
			//		if(m_DrmCnt>0)
			//		{
			//			stSendCurrent.nEquipType = EEQUIP_TYPE_DRM;
			//			stSendCurrent.nEquipNo = 1;
			//		} else {
			//			if(m_DpaCnt>0)
			//			{
			//				stSendCurrent.nEquipType = EEQUIP_TYPE_DPA;
			//				stSendCurrent.nEquipNo = 1;
			//			} else {
			//				if(m_DmpCnt>0)
			//				{
			//					stSendCurrent.nEquipType = EEQUIP_TYPE_DMP;
			//					stSendCurrent.nEquipNo = 1;
			//				} else {
			//					if(m_DssCnt>0)
			//					{
			//						stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
			//						stSendCurrent.nEquipNo = 1;
			//					} else {
			//						if(m_DfrCnt>0)
			//						{
			//							stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
			//							stSendCurrent.nEquipNo = 1;
			//						} else {
			//							DL_LOG("[DRE][SEND EQUIPINFO END]");
			//							memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
			//							GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
			//							GetLowPA()->m_SendUpdateInfo = 0;
			//							PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
			//							PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
			//							return;
			//						}
			//					}
			//				}
			//			}
			//		}
			//	}
			//}
			//else
			{
				//stSendCurrent.nReTry++;
				//if(stSendCurrent.nReTry>3)
				{
					//GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nSetNo = 1;
					//GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					//GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					//GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					//GetLowPA()->m_SendUpdateInfo++;
					//if(GetUpdateWnd()) 
					//{
					//	//PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateInfo);

					//	if(GetUpdateWnd()->m_pEquipInfoDlg)
					//	{
					//		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
					//	}
					//}
					stSendCurrent.nEquipNo++;
					if(stSendCurrent.nEquipNo > w_DevCnt)
					{
						if(m_DreCnt > 0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRE;
							stSendCurrent.nEquipType = EEQUIP_TYPE_DRE;
							stSendCurrent.nEquipNo = 1;
						}
						else
						{
							if(m_DrmCnt>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRM;
								stSendCurrent.nEquipType = EEQUIP_TYPE_DRM;
								stSendCurrent.nEquipNo = 1;
							} else {
								if(m_DpaCnt>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
									stSendCurrent.nEquipType = EEQUIP_TYPE_DPA;
									stSendCurrent.nEquipNo = 0;
								} else {
									if(m_DmpCnt>0)
									{
										GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
										stSendCurrent.nEquipType = EEQUIP_TYPE_DMP;
										stSendCurrent.nEquipNo = 0;
									} else {
										if(m_DssCnt>0)
										{
											GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
											stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
											stSendCurrent.nEquipNo = 0;
										} else {
											if(m_DfrCnt>0)
											{
												GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
												stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
												stSendCurrent.nEquipNo = 0;
											} else {
												DL_LOG("[DRE][SEND EQUIPINFO END]");
												memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
												GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
												GetLowPA()->m_SendUpdateInfo = 0;
												PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
												PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
												return;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DRE)
		{
			DL_LOG("[DRE][SEND TIMEOUT]");
			w_EquIdx = LPA_DRE_IDX;
			w_DevCnt = 6;
			{
				//stSendCurrent.nReTry++;
				//if(stSendCurrent.nReTry>3)
				{
					//GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].nSetNo = 1;
					//GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].nEquipNo = stSendCurrent.nEquipNo;
					//GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].eEquipType = stSendCurrent.nEquipType;
					//GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].arbyReserved[2] = LPA_INFO_FAILED;
					//GetLowPA()->m_SendUpdateInfo++;
					//if(GetUpdateWnd()) 
					//{
					//	if(GetUpdateWnd()->m_pEquipInfoDlg)
					//	{
					//		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
					//	}
					//}
					w_CheckDpg = GetCheckEquipCount(EEQUIP_TYPE_DPG);
					if(w_CheckDpg>0)
					{
						w_DreEquNo = stSendCurrent.nEquipNo+1;
					}else{
						w_DreEquNo = 0;
					}
					if(stSendCurrent.nEquipNo == w_DreEquNo)
					{
						stSendCurrent.nReTry++;						
					}
					stSendCurrent.nEquipNo = w_DreEquNo;
					if(stSendCurrent.nEquipNo > w_DevCnt || stSendCurrent.nReTry > 1 )
					{
						GetLowPA()->m_SendUpdateInfo = m_DmtCnt+m_DpgCnt+m_DreCnt;
						if(m_DrmCnt>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DRM;
							stSendCurrent.nEquipType = EEQUIP_TYPE_DRM;
							stSendCurrent.nEquipNo = 1;

							w_CheckDpg = GetCheckEquipCount(EEQUIP_TYPE_DPG);
							w_CheckDre = GetCheckEquipCount(EEQUIP_TYPE_DRE);
							if(w_CheckDpg>0 || w_CheckDre>0)
							{
								stBatchAll[LPA_DRM_IDX][0].bUseFlag = TRUE;
							}
							for(w_ii=0;w_ii<7;w_ii++)
							{
								w_CheckDrmMax = w_ii*9 + 9;
								if(stBatchAll[LPA_DRE_IDX][w_ii].bResult)
								{
									if(!stBatchAll[LPA_DRE_IDX][w_ii].bError)
									{
										stBatchAll[LPA_DRM_IDX][w_ii*9].bUseFlag = TRUE;
									}else{
										for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
										{
											stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
										}
									}
								} 
								else 
								{
									for(w_jj=w_ii*9+1;w_jj<w_CheckDrmMax;w_jj++)
									{
										stBatchAll[LPA_DRM_IDX][w_jj].bUseFlag = TRUE;
									}
								}
							}

						} else {
							if(m_DpaCnt>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
								stSendCurrent.nEquipType = EEQUIP_TYPE_DPA;
								stSendCurrent.nEquipNo = 0;
							} else {
								if(m_DmpCnt>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
									stSendCurrent.nEquipType = EEQUIP_TYPE_DMP;
									stSendCurrent.nEquipNo = 0;
								} else {
									if(m_DssCnt>0)
									{
										GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
										stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
										stSendCurrent.nEquipNo = 0;
									} else {
										if(m_DfrCnt>0)
										{
											GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
											stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
											stSendCurrent.nEquipNo = 0;
										} else {
											DL_LOG("[DRE][SEND EQUIPINFO END]");
											memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
											GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
											GetLowPA()->m_SendUpdateInfo = 0;
											PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
											PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
											return;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DRM)
		{
			DL_LOG("[DRM][SEND TIMEOUT]");
			w_EquIdx = LPA_DRM_IDX;
			w_DevCnt = 63;

			//if(stSendCurrent.nEquipIndex > w_DevCnt)
			//{
			//	if(m_DpaCnt>0)
			//	{
			//		stSendCurrent.nEquipType = EEQUIP_TYPE_DPA;
			//		stSendCurrent.nEquipNo = 1;
			//	} else {
			//		if(m_DmpCnt>0)
			//		{
			//			stSendCurrent.nEquipType = EEQUIP_TYPE_DMP;
			//			stSendCurrent.nEquipNo = 1;
			//		} else {
			//			if(m_DssCnt>0)
			//			{
			//				stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
			//				stSendCurrent.nEquipNo = 1;
			//			} else {
			//				if(m_DfrCnt>0)
			//				{
			//					stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
			//					stSendCurrent.nEquipNo = 1;
			//				} else {
			//					DL_LOG("[DPE][SEND EQUIPINFO END]");
			//					memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
			//					GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
			//					GetLowPA()->m_SendUpdateInfo = 0;
			//					PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
			//					PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
			//					return;
			//				}
			//			}
			//		}
			//	}
			//}
			//else
			{
				for(w_ii=stSendCurrent.nEquipIndex;w_ii<w_DevCnt;w_ii++)
				{
					if( stBatchAll[w_EquIdx][w_ii].eEquipType == stSendCurrent.nEquipType &&
						!stBatchAll[w_EquIdx][w_ii].bUseFlag )
					{
						w_Find = TRUE;
						break;
					}
				}
				if(w_Find){
					stSendCurrent.nEquipIndex = w_ii;
				}
				if(stSendCurrent.nEquipIndex > w_DevCnt || !w_Find)
				{
					GetLowPA()->m_SendUpdateInfo = m_DmtCnt+m_DpgCnt+m_DreCnt+m_DrmCnt;
					if(m_DpaCnt>0)
					{
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_DPA;
						stSendCurrent.nEquipType = EEQUIP_TYPE_DPA;
						stSendCurrent.nEquipNo = 0;
						stSendCurrent.nDpgNo = 0;
						stSendCurrent.nEquipIndex = 0;
						stSendCurrent.nReTry = 1;
					} else {
						if(m_DmpCnt>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
							stSendCurrent.nEquipType = EEQUIP_TYPE_DMP;
							stSendCurrent.nEquipNo = 0;
							stSendCurrent.nDpgNo = 0;
							stSendCurrent.nEquipIndex = 0;
							stSendCurrent.nReTry = 1;
						} else {
							if(m_DssCnt>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
								stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
								stSendCurrent.nEquipNo = 0;
								stSendCurrent.nDpgNo = 0;
								stSendCurrent.nEquipIndex = 0;
								stSendCurrent.nReTry = 1;
							} else {
								if(m_DfrCnt>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
									stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
									stSendCurrent.nEquipNo = 0;
									stSendCurrent.nDpgNo = 0;
									stSendCurrent.nEquipIndex = 0;
									stSendCurrent.nReTry = 1;
								} else {
									DL_LOG("[DPE][SEND EQUIPINFO END]");
									memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
									GetLowPA()->m_SendUpdateInfo = 0;
									PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
									PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
									return;
								}
							}
						}
					}
				}
			}
		}
		
		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DPA)
		{
			w_EquIdx = LPA_DMA_IDX;
			w_DevCnt = m_DpaCnt;
			//if(stSendCurrent.nEquipNo > m_DpaCnt)
			//{
			//	if(m_DmpCnt>0)
			//	{
			//		w_EquIdx = LPA_DMP_IDX;
			//		stSendCurrent.nEquipType = EEQUIP_TYPE_DMP;
			//		stSendCurrent.nEquipNo = 0;
			//		stSendCurrent.nReTry = 1;
			//	}else{
			//		if(m_DssCnt>0)
			//		{
			//			w_EquIdx = LPA_DSS_IDX;
			//			stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
			//			stSendCurrent.nEquipNo = 0;
			//			stSendCurrent.nReTry = 1;
			//		}
			//		else
			//		{
			//			if(m_DfrCnt>0)
			//			{
			//				w_EquIdx = LPA_DFR_IDX;
			//				stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
			//				stSendCurrent.nEquipNo = 0;
			//				stSendCurrent.nReTry = 1;
			//			}
			//			else
			//			{
			//				DL_LOG("[DPA][SEND EQUIPINFO END]");
			//				memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
			//				GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
			//				GetLowPA()->m_SendUpdateInfo = 0;
			//				PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
			//				PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
			//				return;
			//			}
			//		}
			//	}
			//}
			//else
			{
				//stSendCurrent.nReTry++;
				//if(stSendCurrent.nReTry>3)
				{
					//GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nSetNo = 1;
					//GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					//GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					//GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					//GetLowPA()->m_SendUpdateInfo++;
					//if(GetUpdateWnd()) 
					//{
					//	//PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateInfo);

					//	if(GetUpdateWnd()->m_pEquipInfoDlg)
					//	{
					//		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
					//	}
					//}
					stSendCurrent.nEquipNo++;
					if(stSendCurrent.nEquipNo > m_DpaCnt)
					{
						GetLowPA()->m_SendUpdateInfo = m_DmtCnt+m_DpgCnt+m_DreCnt+m_DrmCnt+m_DpaCnt;
						if(m_DmpCnt>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DMP;
							w_EquIdx = LPA_DMP_IDX;
							stSendCurrent.nEquipType = EEQUIP_TYPE_DMP;
							stSendCurrent.nEquipNo = 0;
							stSendCurrent.nReTry = 1;
						}else{
							if(m_DssCnt>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
								w_EquIdx = LPA_DSS_IDX;
								stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
								stSendCurrent.nEquipNo = 0;
								stSendCurrent.nReTry = 1;
							}
							else
							{
								if(m_DfrCnt>0)
								{
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
									w_EquIdx = LPA_DFR_IDX;
									stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
									stSendCurrent.nEquipNo = 0;
									stSendCurrent.nReTry = 1;
								}
								else
								{
									DL_LOG("[DPA][SEND EQUIPINFO END]");
									memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
									GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
									GetLowPA()->m_SendUpdateInfo = 0;
									PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
									PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
									return;
								}
							}
						}
					}
				}
			}
		}
		
		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DMP)
		{
			w_EquIdx = LPA_DMP_IDX;
			w_DevCnt = m_DmpCnt;
			//if(stSendCurrent.nEquipNo > m_DmpCnt)
			//{
			//	if(m_DssCnt>0)
			//	{
			//		w_EquIdx = LPA_DSS_IDX;
			//		stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
			//		stSendCurrent.nEquipNo = 0;
			//		stSendCurrent.nReTry = 1;
			//	}else{
			//		if(m_DfrCnt>0)
			//		{
			//			w_EquIdx = LPA_DFR_IDX;
			//			stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
			//			stSendCurrent.nEquipNo = 0;
			//			stSendCurrent.nReTry = 1;
			//		}
			//		else
			//		{
			//			DL_LOG("[DMP][SEND EQUIPINFO END]");
			//			memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
			//			GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
			//			GetLowPA()->m_SendUpdateInfo = 0;
			//			PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
			//			PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
			//			return;
			//		}
			//	}
			//}
			//else
			{
				//stSendCurrent.nReTry++;
				//if(stSendCurrent.nReTry>3)
				{
					//GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nSetNo = 1;
					//GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					//GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					//GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					//GetLowPA()->m_SendUpdateInfo++;
					//if(GetUpdateWnd()) 
					//{
					//	//PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateInfo);

					//	if(GetUpdateWnd()->m_pEquipInfoDlg)
					//	{
					//		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
					//	}
					//}
					stSendCurrent.nEquipNo++;
					if(stSendCurrent.nEquipNo > m_DmpCnt)
					{
						GetLowPA()->m_SendUpdateInfo = m_DmtCnt+m_DpgCnt+m_DreCnt+m_DrmCnt+m_DpaCnt+m_DmpCnt;
						if(m_DssCnt>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DSS;
							w_EquIdx = LPA_DSS_IDX;
							stSendCurrent.nEquipType = EEQUIP_TYPE_DSS;
							stSendCurrent.nEquipNo = 0;
							stSendCurrent.nReTry = 1;
						}else{
							if(m_DfrCnt>0)
							{
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
								w_EquIdx = LPA_DFR_IDX;
								stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
								stSendCurrent.nEquipNo = 0;
								stSendCurrent.nReTry = 1;
							}
							else
							{
								DL_LOG("[DMP][SEND EQUIPINFO END]");
								memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
								GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
								GetLowPA()->m_SendUpdateInfo = 0;
								PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
								PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
								return;
							}
						}
					}
				}
			}
		}
		
		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DSS)
		{
			w_EquIdx = LPA_DSS_IDX;
			w_DevCnt = m_DssCnt;
			//if(stSendCurrent.nEquipNo > m_DssCnt)
			//{
			//	if(m_DfrCnt>0)
			//	{
			//		w_EquIdx = LPA_DFR_IDX;
			//		stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
			//		stSendCurrent.nEquipNo = 0;
			//		stSendCurrent.nReTry = 1;
			//	}
			//	else
			//	{
			//		DL_LOG("[DSS][SEND EQUIPINFO END]");
			//		memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
			//		GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
			//		GetLowPA()->m_SendUpdateInfo = 0;
			//		PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
			//		PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
			//		return;
			//	}
			//}
			//else
			{
				//stSendCurrent.nReTry++;
				//if(stSendCurrent.nReTry>3)
				{
					//GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nSetNo = 1;
					//GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					//GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					//GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					//GetLowPA()->m_SendUpdateInfo++;
					//if(GetUpdateWnd()) 
					//{
					//	//PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateInfo);

					//	if(GetUpdateWnd()->m_pEquipInfoDlg)
					//	{
					//		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
					//	}
					//}
					stSendCurrent.nEquipNo++;
					if(stSendCurrent.nEquipNo > m_DssCnt)
					{
						GetLowPA()->m_SendUpdateInfo = m_DmtCnt+m_DpgCnt+m_DreCnt+m_DrmCnt+m_DpaCnt+m_DmpCnt+m_DssCnt;
						if(m_DfrCnt>0)
						{
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_DFR;
							w_EquIdx = LPA_DFR_IDX;
							stSendCurrent.nEquipType = EEQUIP_TYPE_DFR;
							stSendCurrent.nEquipNo = 0;
							stSendCurrent.nReTry = 1;
						}
						else
						{
							DL_LOG("[DSS][SEND EQUIPINFO END]");
							memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
							GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
							GetLowPA()->m_SendUpdateInfo = 0;
							PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
							PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
							return;
						}
					}
				}
			}
		}
		
		if (stSendCurrent.nEquipType == EEQUIP_TYPE_DFR)
		{
			w_EquIdx = LPA_DFR_IDX;
			w_DevCnt = m_DfrCnt;
			//if(stSendCurrent.nEquipNo > m_DfrCnt)
			//{
			//	DL_LOG("[DFR][SEND EQUIPINFO END]");
			//	memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
			//	GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
			//	GetLowPA()->m_SendUpdateInfo = 0;
			//	PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
			//	PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
			//	return;
			//}
			//else
			{
				//stSendCurrent.nReTry++;
				//if(stSendCurrent.nReTry>3)
				{
					//GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nSetNo = 1;
					//GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					//GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					//GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					//GetLowPA()->m_SendUpdateInfo++;
					//if(GetUpdateWnd()) 
					//{
					//	//PostMessage(WM_PROGRESS, EV_PROGRESS_POS, GetLowPA()->m_SendUpdateInfo);

					//	if(GetUpdateWnd()->m_pEquipInfoDlg)
					//	{
					//		::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
					//	}
					//}
					stSendCurrent.nEquipNo++;
					if(stSendCurrent.nEquipNo > m_DfrCnt)
					{
						GetLowPA()->m_SendUpdateInfo = m_DmtCnt+m_DpgCnt+m_DreCnt+m_DrmCnt+m_DpaCnt+m_DmpCnt+m_DssCnt+m_DfrCnt;
						DL_LOG("[DFR][SEND EQUIPINFO END]");
						memset(&stSendCurrent,0x00,sizeof(stSendCurrent));
						GetLowPA()->m_UpdateFlag = DEV_UPDATE_END;
						GetLowPA()->m_SendUpdateInfo = 0;
						PostMessage(WM_PROGRESS, EV_PROGRESS_END, GetLowPA()->m_SendUpdateInfo);
						PostMessage(WM_UPDATE_INFO, DEV_UPDATE_END, 0);
						return;
					}
				}
			}
		}
	}
	else
	{
		DL_LOG("[SINGLE DEV INFO]");
		switch(stSendCurrent.nEquipType)
		{
			case EEQUIP_TYPE_DMT:
				w_EquIdx = LPA_DMA_IDX;
				break;
			case EEQUIP_TYPE_DPG:
				w_EquIdx = LPA_DPG_IDX;
				break;
			case EEQUIP_TYPE_DRE:
				w_EquIdx = LPA_DRE_IDX;
				break;
			case EEQUIP_TYPE_DRM:
				w_EquIdx = LPA_DRM_IDX;
				break;
			case EEQUIP_TYPE_DPA:
				w_EquIdx = LPA_DMA_IDX;
				break;
			case EEQUIP_TYPE_DMP:
				w_EquIdx = LPA_DMP_IDX;
				break;
			case EEQUIP_TYPE_DSS:
				w_EquIdx = LPA_DSS_IDX;
				break;
			case EEQUIP_TYPE_DFR:
				w_EquIdx = LPA_DFR_IDX;
				break;
			case EEQUIP_TYPE_DMX:
				w_EquIdx = LPA_DMX_IDX;
				break;
		}

		stSendCurrent.nReTry++;
		if(stSendCurrent.nReTry > 3)
		{
			switch(stSendCurrent.nEquipType)
			{
				case EEQUIP_TYPE_DMT:
					strEquipName.Format(_T("DMT"));
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DSS:
					strEquipName.Format(_T("DSS %d"), stSendCurrent.nEquipNo);
					GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DSS_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DPG:
					strEquipName.Format(_T("DPG"));
					GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DPG_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DRM:
					strEquipName.Format(_T("DRM %d번(DPG출력 %d번)"), stSendCurrent.nEquipNo, stSendCurrent.nDpgNo);
					GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DRM_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DPA:
					strEquipName.Format(_T("DPA %d번"), stSendCurrent.nEquipNo);
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DMA_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DMP:
					strEquipName.Format(_T("DMP %d번"), stSendCurrent.nEquipNo);
					GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DMP_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DRE:
					strEquipName.Format(_T("DRE %d번"), stSendCurrent.nEquipNo);
					GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DRE_IDX][stSendCurrent.nEquipNo].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DFR:
					strEquipName.Format(_T("DFR %d번"), stSendCurrent.nEquipNo);
					GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DFR_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
				case EEQUIP_TYPE_DMX:
					strEquipName.Format(_T("DMX"));
					GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nSetNo = 1;
					GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].nEquipNo = stSendCurrent.nEquipNo;
					GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].eEquipType = stSendCurrent.nEquipType;
					GetLowPA()->m_EquipInfo[LPA_DMX_IDX][w_ENoIndex].arbyReserved[2] = LPA_INFO_FAILED;
					break;
			}
			strContent.Format(_T("%s 장치가 존재하지 않습니다.\n올바른 장치주소[DRM일경우, DPG/DRE 출력번호]를 선택하신 후, \n장비조회를 하시기 바랍니다."), strEquipName);
			MessageBox(strContent);

			if(GetUpdateWnd()->m_pEquipInfoDlg)
			{
				::PostMessage(GetUpdateWnd()->m_pEquipInfoDlg->GetSafeHwnd(), WM_UPDATE_INFO, 0, 0);
			}

			PostMessage(WM_PROGRESS, EV_PROGRESS_END, 0);
			EnableWindow(TRUE);
			return;
		}
	}


	if(stSendCurrent.nEquipType == EEQUIP_TYPE_DRM)
	{
		if( GetLowPA()->m_UpdateDevStat == DEV_STAT_NEXT)
		{
			stBatchAll[w_EquIdx][stSendCurrent.nEquipIndex].bUseFlag = TRUE;
			stSendCurrent.nDpgNo = stBatchAll[w_EquIdx][stSendCurrent.nEquipIndex].nDpgNo;
			stSendCurrent.nEquipNo = stBatchAll[w_EquIdx][stSendCurrent.nEquipIndex].nEquipNo;
			stSendCurrent.nEquipType = stBatchAll[w_EquIdx][stSendCurrent.nEquipIndex].eEquipType;
		}
		else
		{		
			stBatchAll[w_EquIdx][w_ENoIndex].bUseFlag = TRUE;
			stSendCurrent.nDpgNo = stBatchAll[w_EquIdx][w_ENoIndex].nDpgNo;
			stSendCurrent.nEquipNo = stBatchAll[w_EquIdx][w_ENoIndex].nEquipNo;
			stSendCurrent.nEquipType = stBatchAll[w_EquIdx][w_ENoIndex].eEquipType;
		}
	}
	else
	{	
		for(w_ii=0;w_ii<w_DevCnt;w_ii++)
		{
			if( stBatchAll[w_EquIdx][w_ii].eEquipType == stSendCurrent.nEquipType &&
				stBatchAll[w_EquIdx][w_ii].nEquipNo == stSendCurrent.nEquipNo )
			{
				if(!stBatchAll[w_EquIdx][w_ii].bResult)
				{
					stBatchAll[w_EquIdx][w_ii].bUseFlag = TRUE;
					break;
				}
			}
		}
	}

	switch(stSendCurrent.nEquipType)
	{
		case EEQUIP_TYPE_DMT:
			m_szState =			_T("DMT 정보조회 중입니다.");				
			break;
		case EEQUIP_TYPE_DSS:
			m_szState.Format(_T("DSS %d 정보조회 중입니다."),stSendCurrent.nEquipNo);	
			break;
		case EEQUIP_TYPE_DPG:
			m_szState =			_T("DPG 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DRM:
			m_szState =			_T("DRM 정보조회 중입니다.");
			break;
		case EEQUIP_TYPE_DPA:
			m_szState =			_T("DPA 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DMP:
			m_szState.Format(_T("DMP %d 정보조회 중입니다."),stSendCurrent.nEquipNo);
			break;
		case EEQUIP_TYPE_DRE:
			m_szState =			_T("DRE 정보조회 중입니다.");	
			break;
		case EEQUIP_TYPE_DFR:
			m_szState.Format(_T("DFR %d 정보조회 중입니다."),stSendCurrent.nEquipNo);
			break;
		case EEQUIP_TYPE_DMX:
			m_szState =			_T("DMX 를 정보조회 중입니다.");				
			break;
		default:						
			break;
	}
	UpdateData(FALSE);

	GetLowPA()->SendUpdateCmd(  CMD_UPDATE_INFO
								,0x0
								,0x0
								,(BYTE)(stSendCurrent.nDpgNo<<1)
								,0x0
								,(BYTE)stSendCurrent.nEquipType
								,(BYTE)stSendCurrent.nEquipNo );

	SetTimer(WM_TIMER_SENDCMD,UPDATE_INFO_TIMEVALUE,NULL);
}

void CDLUpdateDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == WM_TIMER_SENDCMD)
	{
		KillTimer(WM_TIMER_SENDCMD);
		//명령전송중 타임아웃발생시, 처리함수
		if(GetLowPA()->m_UpdateFlag == DEV_UPDATE_END)
		{
			DL_LOG("[UPDATE EQUIP INFO CMD END]");
		}else{
			DL_LOG("[UPDATE EQUIP INFO CMD TIMEOUT AND NEXT SEND CMD]");
			DoReSendUpdateInfo();
		}
	}
	else if(nIDEvent == WM_TIMER_SENDFLASHCMD)
	{
		KillTimer(WM_TIMER_SENDFLASHCMD);
		//명령전송중 타임아웃발생시, 처리함수
		if(GetLowPA()->m_UpdateFlag == DEV_UPDATE_END)
		{
			DL_LOG("[UPDATE EQUIP FLASH CMD END]");
		}else{
			DL_LOG("[UPDATE EQUIP FLASH CMD TIMEOUT AND NEXT SEND CMD]");
			DoReSendUpdateFlash2();
		}
	}

	CDialog::OnTimer(nIDEvent);
}

void CDLUpdateDlg::ShowWaitDialog(int a_show, int a_timeout)
{
	m_WaitingDialog.CenterWindow(this);
	m_WaitingDialog.DoWait(a_show, a_timeout, FALSE);
}

int	CDLUpdateDlg::WaitingCloseCB(int a_Event)
{
	//::GetLowPA()->m_DevStat = DEV_STAT_NORMAL;
	if(a_Event == EV_WAIT_TIMEOUT) 
	{
		DL_LOG("[EV_WAIT_TIMEOUT]");
	} 
	else if(a_Event == EV_WAIT_ERROR) 
	{
		DL_LOG("[EV_WAIT_ERROR]");
	}
	return(1);
}