#pragma once
#include "afxwin.h"
#include "CheckStatic.h"
#include "DLEquipInfoDlg.h"
#include "WaitDialog.h"

#define WM_UPDATEDLG_BATCH_PROGRESS			(WM_USER+1000)
#define WM_UPDATEDLG_BATCH_FINISHED			(WM_USER+1001)

#define GET_SET_NUM()						1
#define UPDATE_DMT_MAX						1
#define UPDATE_DPG_MAX						1
#define UPDATE_DRE_MAX						6
#define UPDATE_DRM_MAX						48
#define UPDATE_DPA_MAX						10
#define UPDATE_DMP_MAX						1
#define UPDATE_DFR_MAX						1
#define UPDATE_DSS_MAX						20

#define UPDATE_INFO_TIMEVALUE				2000 //4000

typedef struct _ST_SEND_PROC_
{
	INT			nSetNo;				//세트번호
	INT			nEquipType;			//장치타입
	INT			nEquipIndex;		//장치타입이 DRM인경우, 배열의인덱스
	INT			nEquipNo;			//장치번호
	INT			nDpgNo;				//DPG출력번호(DRM인 경우)
	INT			nReTry;				//재시도횟수
	INT			nSendPage;			//현재 전송 페이지
	INT			nSendOk;			//0:기본, 1:전송완료(성공/실패)
	BYTE		nVersion[4];		//펌웨어버전
} ST_SEND_ALL, *PST_SEND_ALL;

// CDLUpdateDlg 대화 상자입니다.

class CDLUpdateDlg : public CDialog
{
	DECLARE_DYNAMIC(CDLUpdateDlg)

public:
	CDLUpdateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDLUpdateDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLUPDATE_DIALOG };

public:
	BYTE	*m_pWriteData;
	typedef struct _ST_BATCH_PROC_
	{
		DWORD		nSetNo;				//세트번호
		EEquipType	eEquipType;			//장치타입
		INT			nEquipNo;			//장치번호
		INT			nDpgNo;				//DPG출력번호(DRM인 경우)
		BOOL		bEEP;				//EEPROM 보유여부
		BOOL		bUseFlag;			//사용여부
		BOOL		bResult;			//전송결과 0:기본, 1:성공
		BOOL		bError;				//다른장치연결 : DRM , DRE일 경우만
		INT			nTry;
	} ST_BATCH_ALL, *PST_BATCH_ALL;

	int				m_DmtCnt;
	int				m_DpgCnt;
	int				m_DreCnt;
	int				m_DrmCnt;
	int				m_DpaCnt;
	int				m_DmpCnt;
	int				m_DfrCnt;
	int				m_DssCnt;
	int				m_DmxCnt;
	int				m_bUpdateFlag;						//0:업데이트헤제, 1:업데이트모드
	int				m_bUpdateMixerFlag;					//0:믹서업데이트헤제, 1:믹서업데이트모드

	ST_BATCH_ALL	stBatchAll[LPA_MMACRO_ITEM][LPA_DRM_MAX+15];
	ST_SEND_ALL		stSendCurrent;
	ST_SEND_ALL		stAllSendCurrent[WNC_MAX_ITEM];		//일괄업데이트용
	int				m_nSendCur;
	int				m_nBatchAllCount;
	BOOL			m_bBatchAll;
	BOOL			m_bEquipInfoAll;
	DWORD			m_nPort;
	DWORD			m_nMixerPort;

	CCheckStatic	m_ctrlCSBoxDMT;
	CCheckStatic	m_ctrlCSBoxDSS;
	CCheckStatic	m_ctrlCSBoxDPG;
	CCheckStatic	m_ctrlCSBoxDRM;
	CCheckStatic	m_ctrlCSBoxDPA;
	CCheckStatic	m_ctrlCSBoxDMP;
	CCheckStatic	m_ctrlCSBoxDRE;
	CCheckStatic	m_ctrlCSBoxDFR;
	
	CProgressCtrl	m_ctrlProgressUpdate;
	CString			m_szState;

	CWaitDialog			m_WaitingDialog;
	static int			WaitingCloseCB(int a_Event);
	void				ShowWaitDialog(int a_show, int a_timeout);

	//장치정보 다이얼로그(전체 및 개별조회되는 장치정보를 표시하는 다이얼로그)
	CDLEquipInfoDlg	*m_pEquipInfoDlg;

	void			DrawStatus();
	void			DoReSendUpdateInfo();
	void			DoReSendUpdateFlash();
	void			DoReSendUpdateFlash2();
	void			OnSendEquipInfo(int a_EType, int a_ENo);
	void			Reload();
	void			DeviceStatusChange(int a_DevType, int a_Status);			//장치 그룹별 윈도우 활성화 상태 0:disable, 1:enable
	BOOL			FileExist(LPCTSTR lpPathName, BOOL bMsg);

	//시리얼 포트 검색
	CUIntArray		m_Ports;
	CUIntArray		m_MixerPorts;
	void			EnumerateSerialPorts(CUIntArray& ports);

	void			ClearFilePathAll(void);

	void			OnButtonEquipInfo(UINT nID);
	void			OnButtonLoadFile(UINT nID);
	INT				OnButtonUpdateFlash(UINT nID);
	void			OnButtonVerifyFlash(UINT nID);

	INT				UpdateFlash(DWORD nSetNo, EEquipType eEquipType, DWORD nEquipNo, DWORD nDpgNo, CString szFilePath);
	void			FinishBatchAll(BOOL bSuccess);

	int				GetCheckEquipInfo(int a_EquipType=0, int a_EquipNo=0, int a_DpgNo=0);
	int				GetCheckEquipCount(int a_EquipType);
	int				GetSelectedFilePathVersion(CString szSearchFilePath, BYTE* apVersion);
	void			SetFileNameBuilding();
	BOOL			SetFilePathName(CString szSearchFilePath, INT nEditId);

	CString			GetSelectedFilePath(EEquipType eEquipType, BOOL bFlash);
	BOOL			IsSelectedUpdateEquip(EEquipType eEquipType, BOOL bFlash);
	BOOL			IsEnabledFilePath(EEquipType eEquipType, BOOL bFlash);
	
	void			OnUpdateInfoClear(int a_EquipType, int a_EquipNo, int a_DpgNo);
	void			OnBatchUpdateAll();
	void			OnBatchUpdateAllEnd();
	void			OnBatchUpdateAllEnd2();
	void			OnBatchUpdateNext(int a_EType, int a_ENo);

	void			OnButtonConnect(void);
	void			OnButtonConnect2(void);
	void			OnButtonEquipInfoDialog(void);
	void			OnChangeConnect();
	void			OnChangeConnect2();

	DWORD			GetSelectedFlashPageSize(EEquipType eEquipType);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSize(UINT nType, int cx, int cy);
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg LRESULT OnBatchProgress(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBatchFinished(WPARAM wParam, LPARAM lParam);
	afx_msg void OnButtonLoadAll();
	afx_msg void OnButtonFinishBoot();
	afx_msg void OnButtonFinishBoot2();
	afx_msg void OnButtonResetBoot();
	afx_msg void OnButtonResetBoot2();
	afx_msg void OnButtonUpdateAll();
	afx_msg void OnButtonResetInfo();
protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
