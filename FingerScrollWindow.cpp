/*!
 * @file      FingerScrollWindow.cpp
 * @brief     
 * @author    www.constructor.co.kr(constructor@constructor.co.kr)
 * @date      2014-03-13 
 * @warning   
 */

// FingerScrollWindow.cpp : implementation file
//

#include "stdafx.h"
#include "FingerScrollWindow.h"

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const UINT RWM_ONDESTROYWINDOWSCROLLER = RegisterWindowMessage(RWM_OnDestroyWindowScrollerString);

#define THRESHHOLD_MOVED_DISTANCE_AS_DRAG			25

/////////////////////////////////////////////////////////////////////////////
// CFingerScrollWindow

CFingerScrollWindow::CFingerScrollWindow()
{
	m_bDraged = FALSE;
}

CFingerScrollWindow::~CFingerScrollWindow()
{
}


BEGIN_MESSAGE_MAP(CFingerScrollWindow, CScrollWindow)
	//{{AFX_MSG_MAP(CFingerScrollWindow)
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
    ON_WM_VSCROLL()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE (RWM_ONDESTROYWINDOWSCROLLER, OnDestroyScroller)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFingerScrollWindow message handlers

BOOL CFingerScrollWindow::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd != m_hWnd)
	{
#if 1
		switch(pMsg->message)
		{
		case WM_MOUSEMOVE:

			//TRACE(_T("WM_MOUSEMOVE - m_bDragStarted:%d, m_bDraged:%d\r\n"), m_bDragStarted, m_bDraged);

			if(m_bDragStarted && m_bDraged)
			{
				SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
			}
			else {

				CPoint ptCursorCurr, ptMove;
				GetCursorPos(&ptCursorCurr);
				ScreenToClient(&ptCursorCurr);

				m_ptPrevCursor = ptCursorCurr;

				if(m_bDragStarted && m_bDraged == FALSE)
				{
					SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
				}
			}
			break;

		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
		case WM_MBUTTONDOWN:

		case WM_MOUSEHWHEEL:

			// 자식 윈도우에 대한 마우스 조작 시 스크롤 이벤트 전달
			SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
			break;

		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
		case WM_MBUTTONUP:

			// 자식 윈도우에 대한 마우스 조작 시 스크롤 이벤트 전달
			//SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);

		default:
			break;
		}

		if(pMsg->message == WM_LBUTTONUP)
		{
			// 자식 윈도우 위치에 클릭하고 드래그 했을 때 버튼 클릭을 안되게
			if(m_bDragStarted && m_bDraged)
			{
				SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
				return TRUE;
			}
		}
#else

		if(pMsg->message == WM_MOUSEMOVE)
		{
			CPoint ptCursorCurr, ptMove;
			GetCursorPos(&ptCursorCurr);
			ScreenToClient(&ptCursorCurr);

			m_ptPrevCursor = ptCursorCurr;
		}
		else if(pMsg->message == WM_LBUTTONUP)
		{
			// 자식 윈도우를 클릭하고 드래그 했을 때 버튼 클릭을 안되게
			if(m_bDragStarted && m_bDraged)
				return TRUE;
		}
#endif
	}

	return CScrollWindow::PreTranslateMessage(pMsg);
}

void CFingerScrollWindow::OnPaint() 
{
	CPaintDC dc(this); // device context for painting


	CRect rect;
	GetClientRect(rect);

	CBrush brush(RGB(255,255,255));
	dc.FillRect(rect,&brush);

	// Do not call CComboBox::OnPaint() for painting messages
}


BOOL CFingerScrollWindow::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	
	//return m_bEraseBk ? CFingerScrollWindow::OnEraseBkgnd(pDC) : TRUE;

	return TRUE;

	return CScrollWindow::OnEraseBkgnd(pDC);
}

void CFingerScrollWindow::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 100)
	{
		INT	nScrollX = GetScrollPos(SB_HORZ);
		INT	nScrollY = GetScrollPos(SB_VERT);

		nScrollX = nScrollX + ((INT)(m_dScrollVelocity / 100) * ((m_eDirect == SCROLL_DIRECT_LEFT) ? -1 : 1));
		nScrollY = nScrollY + ((INT)(m_dScrollVelocity / 100) * ((m_eDirect == SCROLL_DIRECT_TOP) ? -1 : 1));

		BOOL	bFinishedX = ((GetScrollLimit(SB_HORZ) <= nScrollX && m_eDirect == SCROLL_DIRECT_RIGHT) ||
							(GetScrollPos(SB_HORZ) <= 0 && m_eDirect == SCROLL_DIRECT_LEFT));
		BOOL	bFinishedY = ((GetScrollLimit(SB_VERT) <= nScrollY) && m_eDirect == SCROLL_DIRECT_BOTTOM ||
							(GetScrollPos(SB_VERT) <= 0 && m_eDirect == SCROLL_DIRECT_TOP));

		if(bFinishedX)
			nScrollX = (m_eDirect == SCROLL_DIRECT_RIGHT) ? GetScrollLimit(SB_HORZ) : 0;

		if(bFinishedY)
			nScrollY = (m_eDirect == SCROLL_DIRECT_BOTTOM) ? GetScrollLimit(SB_VERT) : 0;
		
		if(bFinishedX || bFinishedY)
			StopFingerScroll();

		nScrollX = max(nScrollX, 0);
		nScrollY = max(nScrollY, 0);

		if(m_eDirect == SCROLL_DIRECT_LEFT || m_eDirect == SCROLL_DIRECT_RIGHT)
		{
			nScrollY = GetScrollPos(SB_VERT);
		}
		else if(m_eDirect == SCROLL_DIRECT_TOP || m_eDirect == SCROLL_DIRECT_BOTTOM)
		{
			nScrollX = GetScrollPos(SB_HORZ);
		}

		ScrollWindow(GetScrollPos(SB_HORZ) - nScrollX, GetScrollPos(SB_VERT) - nScrollY);
		
		if(m_eDirect == SCROLL_DIRECT_LEFT || m_eDirect == SCROLL_DIRECT_RIGHT)
		{
			SetScrollPos(SB_HORZ, nScrollX);
			OnChangedScrollX();
		}
		else if(m_eDirect == SCROLL_DIRECT_TOP || m_eDirect == SCROLL_DIRECT_BOTTOM)
		{
			SetScrollPos(SB_VERT, nScrollY);
		}
		
		//TRACE(_T("X:%d, Y:%d\r\n"), nScrollX, nScrollY);

		m_dScrollVelocity = m_dScrollVelocity - (m_dScrollVelocity / 100.0);
		if(m_dScrollVelocity <= 10)
			StopFingerScroll();

		//Invalidate();
	}
	
	CScrollWindow::OnTimer(nIDEvent);
}

//--------------------------------------------------------//
//		for CWindowScroller
LRESULT CFingerScrollWindow::OnDestroyScroller(WPARAM, LPARAM)
{
    // TRACE (_T("In CFingerScrollWindow::OnDestroyScroller\n"));
    // re-enable the default WM_ERASEBKGND message processing
    m_bEraseBk = TRUE;
    return 0;
}
//--------------------------------------------------------//

void CFingerScrollWindow::PrepareFingerScroll()
{
	StopFingerScroll();
	
	SetCapture();
	
	m_nTick = GetTickCount();
	GetCursorPos(&m_ptStartCursor);
	ScreenToClient(&m_ptStartCursor);
	
	m_bDraged = FALSE;
	
	//--------------------------------------------------------//
	//		for CWindowScroller
	m_bDragStarted = TRUE;
	//--------------------------------------------------------//
}

void CFingerScrollWindow::StopFingerScroll()
{
	KillTimer(100);
}

void CFingerScrollWindow::StartFingerScroll()
{
	CPoint		ptCurrCursor;
	GetCursorPos(&ptCurrCursor);
	ScreenToClient(&ptCurrCursor);

	DWORD nCurrTick = GetTickCount();
	
	DWORD nTick = nCurrTick - m_nTick;
	
	//두점의 거리를 계산
	DOUBLE dTmp = (DOUBLE)(ptCurrCursor.x-m_ptStartCursor.x)*(ptCurrCursor.x-m_ptStartCursor.x) + (DOUBLE)(ptCurrCursor.y-m_ptStartCursor.y)*(ptCurrCursor.y-m_ptStartCursor.y);
	DOUBLE dDistance = sqrt(dTmp);
	
	m_dScrollVelocity = dDistance / (DOUBLE)nTick * 1000.0F;

	//TRACE(_T("Velocity : %f\r\n"), m_dScrollVelocity);

	if(m_dScrollVelocity < 300)
		return;

	if(dDistance < THRESHHOLD_MOVED_DISTANCE_AS_DRAG)
		return;

	if(nTick > 250)
		return;

	INT iVarHor = ptCurrCursor.x-m_ptStartCursor.x;
	INT iVarVer = ptCurrCursor.y-m_ptStartCursor.y;

	INT iDistX = abs(iVarHor);
	INT iDistY = abs(iVarVer);

	DOUBLE dGrade = atan2((DOUBLE)iDistY,(DOUBLE)iDistX) * 180 / 3.14159265;			// 기울기 (0 ~ 90)
	DOUBLE dAngle = atan2((DOUBLE)(-1*iVarVer),(DOUBLE)(-1*iVarHor)) * 180 / 3.14159265;
	dAngle = dAngle < 0 ? dAngle + 360 : dAngle;						// 각도 (0 ~ 360)
	//TRACE(_T("Angle : %f\r\n"), dAngle);

	if(dGrade < 45 + 10 && dGrade > 45 - 10)							// 35 ~ 55 기울기는 취소
		return;

	if(dAngle >= 315 || dAngle < 45)
		m_eDirect = SCROLL_DIRECT_RIGHT;
	else if(dAngle >= 45  && dAngle < 135)
		m_eDirect = SCROLL_DIRECT_BOTTOM;
	else if(dAngle > 135  && dAngle < 225)
		m_eDirect = SCROLL_DIRECT_LEFT;
	else
		m_eDirect = SCROLL_DIRECT_TOP;

	CRect rect;
	GetClientRect(&rect);
	
	if(m_eDirect == SCROLL_DIRECT_LEFT || m_eDirect == SCROLL_DIRECT_RIGHT)
	{
		if(m_sizeTotal.cx <= rect.Width())
			return;
	} else {
		if(m_sizeTotal.cy <= rect.Height())
			return;
	}
	
	SetTimer(100, 10, NULL);
}

void CFingerScrollWindow::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CScrollWindow::OnHScroll(nSBCode, nPos, pScrollBar);
	
	OnChangedScrollX();
}

void CFingerScrollWindow::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CScrollWindow::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CFingerScrollWindow::OnLButtonUp(UINT nFlags, CPoint point) 
{
	ReleaseCapture();								//마우스 이벤트 유지해제...
	
	StartFingerScroll();
	
	CScrollWindow::OnLButtonUp(nFlags, point);
}

void CFingerScrollWindow::OnLButtonDown(UINT nFlags, CPoint point) 
{
	PrepareFingerScroll();
	
	CScrollWindow::OnLButtonDown(nFlags, point);
}

void CFingerScrollWindow::OnMButtonUp(UINT nFlags, CPoint point) 
{
	StopFingerScroll();

	//--------------------------------------------------------//
	//		for CWindowScroller
	m_bDragStarted = FALSE;
	//--------------------------------------------------------//

	CScrollWindow::OnMButtonUp(nFlags, point);
}

void CFingerScrollWindow::OnMButtonDown(UINT nFlags, CPoint point) 
{
	StopFingerScroll();

	//--------------------------------------------------------//
	//		for CWindowScroller
	int pixels = 10;
    UINT elapse = 10;
    int direction = 3;		/*ver:1 hor:2 both:3*/
	
    // Create the CWindowScroller window
    CWindowScroller *ws = new CWindowScroller(this, point, pixels, elapse, direction);
	
    // check that the CWindowScroller window creation was successful
    if (ws && IsWindow(ws->m_hWnd))
	{
        // disable the default WM_ERASEBKGND message processing to prevent flicker
		m_bEraseBk = FALSE;
	}
	//--------------------------------------------------------//
	
	CScrollWindow::OnMButtonDown(nFlags, point);
}

void CFingerScrollWindow::OnRButtonUp(UINT nFlags, CPoint point) 
{
	StopFingerScroll();
	
	CScrollWindow::OnRButtonUp(nFlags, point);
}

void CFingerScrollWindow::OnRButtonDown(UINT nFlags, CPoint point) 
{
	StopFingerScroll();
	
	CScrollWindow::OnRButtonDown(nFlags, point);
}

void CFingerScrollWindow::OnMouseMove(UINT nFlags, CPoint point) 
{
	CPoint ptCursorCurr, ptMove;
	GetCursorPos(&ptCursorCurr);
	ScreenToClient(&ptCursorCurr);
	
	CPoint OriginalPoint (GetScrollPos(SB_HORZ), GetScrollPos(SB_VERT));
	CPoint NewPoint = OriginalPoint;
	
	ptMove.x = ptCursorCurr.x - m_ptPrevCursor.x;
	ptMove.y = ptCursorCurr.y - m_ptPrevCursor.y;
	
	if(m_bDragStarted && (nFlags == MK_LBUTTON))
	{
		int xMax = GetScrollLimit(SB_HORZ);
        int yMax = GetScrollLimit(SB_VERT);
		
		NewPoint.x -= ptMove.x;
		NewPoint.y -= ptMove.y;
		
		if (NewPoint.x < 0)
            NewPoint.x = 0;
        else if (NewPoint.x > xMax)
            NewPoint.x = xMax;
		
        if (NewPoint.y < 0)
            NewPoint.y = 0;
        else if (NewPoint.y > yMax)
            NewPoint.y = yMax;
		
		if (NewPoint.x < 0)
            NewPoint.x = 0;
        else if (NewPoint.x > xMax)
            NewPoint.x = xMax;
		
        if (NewPoint.y < 0)
            NewPoint.y = 0;
        else if (NewPoint.y > yMax)
            NewPoint.y = yMax;
		
		if(NewPoint != OriginalPoint)
		{
// 			if(ptCursorCurr.x = m_ptPrevCursor.x && ptCursorCurr.y - m_ptPrevCursor.y)
// 				TRACE(_T("        X:%d, Y:%d\r\n"), ptMove.x, ptMove.x);

			SetScrollPos(SB_HORZ, NewPoint.x);
			SetScrollPos(SB_VERT, NewPoint.y);
			ScrollWindow(OriginalPoint.x - NewPoint.x, OriginalPoint.y - NewPoint.y);

			OnChangedScrollX();
			OnChangedScrollY();
			
			CPoint ptCursor;
			GetCursorPos(&ptCursor);
			//SetCursorPos(ptCursor.x - ptMove.x, ptCursor.y - ptMove.y);
			
			//Invalidate();
			//RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW | */RDW_NOERASE | RDW_NOCHILDREN);
			
			//PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));
#ifdef _DEBUG
			// 	CString szPoint;
			// 	szPoint.Format(_T("%d : %d\r\n"), ptMove.x, ptMove.y);
			// 	//CDC *pDC = GetWindowDC();
			// 	//pDC->SetTextColor(RGB(0, 0, 0));
			// 	//pDC->TextOut(10, 10, szPoint);
			// 	TRACE(szPoint);
#endif
		}

		{
			CPoint		ptCurrCursor;
			GetCursorPos(&ptCurrCursor);
			ScreenToClient(&ptCurrCursor);

			//두점의 거리를 계산
			DOUBLE dTmp = (DOUBLE)(ptCurrCursor.x-m_ptStartCursor.x)*(ptCurrCursor.x-m_ptStartCursor.x) + (DOUBLE)(ptCurrCursor.y-m_ptStartCursor.y)*(ptCurrCursor.y-m_ptStartCursor.y);
			DOUBLE dDistance = sqrt(dTmp);

			if(dDistance >= THRESHHOLD_MOVED_DISTANCE_AS_DRAG)
				m_bDraged = TRUE;
		}
		
	} else {
		m_bDragStarted = FALSE;
	}
	
	//TRACE(_T(" ptPrevCursor X:%d,Y:%d    ptCursorCurr X:%d,Y:%d\n"), m_ptPrevCursor.x, m_ptPrevCursor.y, ptCursorCurr.x, ptCursorCurr.y);

	m_ptPrevCursor = ptCursorCurr;
	
	CScrollWindow::OnMouseMove(nFlags, point);
}
