/*!
 * @file      FingerScrollWindow.h
 * @brief     
 * @author    www.constructor.co.kr(constructor@constructor.co.kr)
 * @date      2014-03-13 
 * @warning   
 */

#if !defined(AFX_FINGERSCROLLWINDOW_H__9570ADE0_4010_458B_B9BF_7263D6A59B8C__INCLUDED_)
#define AFX_FINGERSCROLLWINDOW_H__9570ADE0_4010_458B_B9BF_7263D6A59B8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FingerScrollWindow.h : header file
//

#include "ScrollWindow.h"
#include "WindowScroller.h"

/////////////////////////////////////////////////////////////////////////////
// CFingerScrollWindow window

class CFingerScrollWindow : public CScrollWindow
{
// Construction
public:
	CFingerScrollWindow();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFingerScrollWindow)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFingerScrollWindow();

protected:

	enum
	{
		SCROLL_DIRECT_LEFT,
		SCROLL_DIRECT_RIGHT,
		SCROLL_DIRECT_TOP,
		SCROLL_DIRECT_BOTTOM

	} m_eDirect;

	DWORD		m_nTick;
	DOUBLE		m_dScrollVelocity;
	CPoint		m_ptStartCursor;
	BOOL		m_bDraged;

	//--------------------------------------------------------//
	//		for CWindowScroller
	CPoint		m_ptPrevCursor;
	BOOL		m_bDragStarted;
	BOOL		m_bEraseBk;
	//--------------------------------------------------------//
	
	void		PrepareFingerScroll();
	void		StartFingerScroll();
	void		StopFingerScroll();

	virtual		void OnChangedScrollX()	{};
	virtual		void OnChangedScrollY()	{};

	// Generated message map functions
protected:
	//{{AFX_MSG(CFingerScrollWindow)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LRESULT OnDestroyScroller(WPARAM wp, LPARAM lp);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FINGERSCROLLWINDOW_H__9570ADE0_4010_458B_B9BF_7263D6A59B8C__INCLUDED_)
