// GateStatic.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "GateStatic.h"


// CGateStatic

IMPLEMENT_DYNAMIC(CGateStatic, CStatic)

CGateStatic::CGateStatic()
{

}

CGateStatic::~CGateStatic()
{
}


BEGIN_MESSAGE_MAP(CGateStatic, CStatic)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()



// CGateStatic 메시지 처리기입니다.

void CGateStatic::Draw()
{
#if defined(DEV_5000)
	CRect		w_TempRect;
	CRect		w_Rect;
	CBrush		w_Brush1(COLOR_WHITE); 
	CBrush		w_Brush2(COLOR_BLUE); 
	LOGBRUSH	wp_lb;

	CBrush		*wp_oldBrush = NULL;
	CPen		*wp_oldPen = NULL;
	CPen		w_Pen1Pixel;
	int			w_ii;
	int			w_Gap;
	int			w_PosX=0;
	int			w_X, w_Y, w_W, w_H;
	CDC*		pDC = GetDC();
	WORD		w_Gate = 0;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	w_Gate = wp_PAData->m_Gate;


	GetClientRect(w_Rect);
	pDC->FillRect(w_Rect, &w_Brush1);

	w_PosX = w_Rect.Width()/2 - 340/2;

	wp_lb.lbColor = RGB(87,87,87);
	wp_lb.lbStyle = BS_SOLID;
	w_Pen1Pixel.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_SQUARE | PS_JOIN_ROUND, 1, &wp_lb);

	if(!wp_oldBrush) {
		wp_oldBrush = pDC->SelectObject(&w_Brush1);
	} else {
		pDC->SelectObject(&w_Brush1);
	}

	if(!wp_oldPen) {
		wp_oldPen = pDC->SelectObject(&w_Pen1Pixel);
	} else {
		pDC->SelectObject(&w_Pen1Pixel);
	}
		// DEVICE NAME
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(COLOR_BLACK);
	pDC->SelectObject(m_Font);

	w_ii = 0;
	for(w_ii=0;w_ii< GATE_LINE_NUM;w_ii++) {
		pDC->MoveTo(w_Rect.left+w_PosX+1, w_Rect.top+1+w_ii*GATE_LINE_YGAP);
		pDC->LineTo(w_Rect.right-1, w_Rect.top+1+w_ii*GATE_LINE_YGAP);
	}
	pDC->MoveTo(GATE_TITLE_POS, w_Rect.top+1+3*GATE_LINE_YGAP+(GATE_LINE_YGAP/2));
	pDC->LineTo(w_Rect.right-1, w_Rect.top+1+3*GATE_LINE_YGAP+(GATE_LINE_YGAP/2));

	pDC->MoveTo(GATE_TITLE_POS, w_Rect.top+1+4*GATE_LINE_YGAP+(GATE_LINE_YGAP/2));
	pDC->LineTo(w_Rect.right-1, w_Rect.top+1+4*GATE_LINE_YGAP+(GATE_LINE_YGAP/2));

	pDC->MoveTo(w_Rect.left+1, w_Rect.top+1);
	pDC->LineTo(w_Rect.left+1, w_Rect.bottom-1);

	pDC->MoveTo(w_Rect.right-1, w_Rect.top+1);
	pDC->LineTo(w_Rect.right-1, w_Rect.bottom-1);


	pDC->MoveTo(GATE_TITLE_POS, w_Rect.top+1+1*GATE_LINE_YGAP);
	pDC->LineTo(GATE_TITLE_POS, w_Rect.top+1+(GATE_LINE_NUM-1)*GATE_LINE_YGAP);

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_GROUP_NUM;
	for(w_ii=1;w_ii<GATE_GROUP_NUM;w_ii++) {
		pDC->MoveTo(GATE_TITLE_POS+w_ii*w_Gap, w_Rect.top+1+3*GATE_LINE_YGAP);
		pDC->LineTo(GATE_TITLE_POS+w_ii*w_Gap, w_Rect.top+1+4*GATE_LINE_YGAP);
	}

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_BGM_NUM;
	for(w_ii=1;w_ii<GATE_BGM_NUM;w_ii++) {
		pDC->MoveTo(GATE_TITLE_POS+w_ii*w_Gap, w_Rect.top+1+4*GATE_LINE_YGAP);
		pDC->LineTo(GATE_TITLE_POS+w_ii*w_Gap, w_Rect.top+1+5*GATE_LINE_YGAP);
	}

	w_X = w_Rect.left+1;
	w_Y = w_Rect.top+1+(0*GATE_LINE_YGAP);
	w_W = w_Rect.right - w_Rect.left;
	w_H = GATE_LINE_YGAP;

	w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
	pDC->DrawText(CString("GATE 설정"), w_TempRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);


	CString	w_Title[GATE_LINE_NUM];
	w_Title[0] = "화재/비상/Ex";
	w_Title[1] = "TIME";
	w_Title[2] = "DRM Group";
	w_Title[3] = "BGM";

	for(w_ii=0;w_ii<4;w_ii++) {
		w_X = w_Rect.left+1;
		w_Y = w_Rect.top+1+((w_ii+1)*GATE_LINE_YGAP);
		w_W = GATE_TITLE_POS - w_Rect.left;
		w_H = GATE_LINE_YGAP;

		w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		pDC->DrawText(w_Title[w_ii], w_TempRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	}

	pDC->MoveTo(GATE_TITLE_POS, w_Rect.top+1+1*GATE_LINE_YGAP);
	pDC->LineTo(GATE_TITLE_POS, w_Rect.top+1+(GATE_LINE_NUM-1)*GATE_LINE_YGAP);

	char w_DrmText[20] = {'A', 0x00 };

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_GROUP_NUM;
	for(w_ii=0;w_ii<GATE_GROUP_NUM;w_ii++) {
		w_X = GATE_TITLE_POS+w_ii*w_Gap;
		w_Y = w_Rect.top+1+3*GATE_LINE_YGAP;
		w_W = w_Gap;
		w_H = GATE_LINE_YGAP/2;

		w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);

		pDC->DrawText(CString(w_DrmText), w_TempRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
		w_DrmText[0] += 1;
	}

	char w_BgmText[20] = {'1', 0x00 };

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_BGM_NUM;
	for(w_ii=0;w_ii<GATE_BGM_NUM;w_ii++) {
		w_X = GATE_TITLE_POS+w_ii*w_Gap;
		w_Y = w_Rect.top+1+4*GATE_LINE_YGAP;
		w_W = w_Gap;
		w_H = GATE_LINE_YGAP/2;

		w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);

		pDC->DrawText(CString(w_BgmText), w_TempRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);
		w_BgmText[0] += 1;
	}

	// CHECK BOX
	w_X = GATE_TITLE_POS + ((w_Rect.right - GATE_TITLE_POS - GATE_BTN_SIZE)/2);
	w_Y = (w_Rect.top+1+(1*GATE_LINE_YGAP)) + ((GATE_LINE_YGAP-GATE_BTN_SIZE)/2);
	w_W = GATE_BTN_SIZE;
	w_H = GATE_BTN_SIZE;
	w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);

	if(IS_GATE_ON(w_Gate, GATE_EM)) {
		pDC->FillRect(w_TempRect, &w_Brush2);
	} else {
		pDC->Rectangle(w_TempRect);
	}

	w_X = GATE_TITLE_POS + ((w_Rect.right - GATE_TITLE_POS - GATE_BTN_SIZE)/2);
	w_Y = (w_Rect.top+1+(2*GATE_LINE_YGAP)) + ((GATE_LINE_YGAP-GATE_BTN_SIZE)/2);
	w_W = GATE_BTN_SIZE;
	w_H = GATE_BTN_SIZE;
	w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);

	if(IS_GATE_ON(w_Gate, GATE_TIME)) {
		pDC->FillRect(w_TempRect, &w_Brush2);
	} else {
		pDC->Rectangle(w_TempRect);
	}

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_GROUP_NUM;
	for(w_ii=0;w_ii<GATE_GROUP_NUM;w_ii++) {
		w_X = GATE_TITLE_POS+w_ii*w_Gap + (w_Gap-GATE_BTN_SIZE)/2;
		w_Y = w_Rect.top+1+3*GATE_LINE_YGAP + (GATE_LINE_YGAP/2) + ((GATE_LINE_YGAP/2)-GATE_BTN_SIZE)/2;
		w_W = GATE_BTN_SIZE;
		w_H = GATE_BTN_SIZE;

		w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);

		if(IS_GATE_ON(w_Gate, GATE_DRM_A+w_ii)) {
			pDC->FillRect(w_TempRect, &w_Brush2);
		} else {
			pDC->Rectangle(w_TempRect);
		}
	}

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_BGM_NUM;
	for(w_ii=0;w_ii<GATE_BGM_NUM;w_ii++) {
		w_X = GATE_TITLE_POS+w_ii*w_Gap + (w_Gap-GATE_BTN_SIZE)/2;
		w_Y = w_Rect.top+1+4*GATE_LINE_YGAP + (GATE_LINE_YGAP/2) + ((GATE_LINE_YGAP/2)-GATE_BTN_SIZE)/2;
		w_W = GATE_BTN_SIZE;
		w_H = GATE_BTN_SIZE;

		w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);

		if(IS_GATE_ON(w_Gate, GATE_BGM_1+w_ii)) {
			pDC->FillRect(w_TempRect, &w_Brush2);
		} else {
			pDC->Rectangle(w_TempRect);
		}
	}

	if(wp_oldPen) {
		pDC->SelectObject(wp_oldPen);
	}
	if(wp_oldBrush) {
		pDC->SelectObject(wp_oldBrush);
	}

	ReleaseDC(pDC);
#endif
}





void CGateStatic::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CStatic::OnPaint()을(를) 호출하지 마십시오.

	Draw();

}

BOOL CGateStatic::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return FALSE;
	//return CStatic::OnEraseBkgnd(pDC);
}

int CGateStatic::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CStatic::OnCreate(lpCreateStruct) == -1)
		return -1;

	ModifyStyle(0, SS_NOTIFY);

	m_Font.CreateFont(
		-13,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		0,
		DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH,
		DEF_FONT_NAME);


	return 0;
}

void CGateStatic::OnLButtonDown(UINT nFlags, CPoint point)
{
#if defined(DEV_5000)
	int		w_X, w_Y, w_W, w_H;
	int		w_PosX, w_PosY;
	int		w_Gap;
	int		w_ii;
	CRect	w_Rect;
	CRect	w_TempRect;

	LPA_DATA_T *wp_PAData = GetPAData();
	if(!wp_PAData) {
		return;
	}
	GetClientRect(w_Rect);

	w_PosX = point.x;
	w_PosY = point.y;


	// CHECK BOX
	w_X = GATE_TITLE_POS + ((w_Rect.right - GATE_TITLE_POS - GATE_BTN_SIZE)/2);
	w_Y = (w_Rect.top+1+(1*GATE_LINE_YGAP)) + ((GATE_LINE_YGAP-GATE_BTN_SIZE)/2);
	w_W = GATE_BTN_SIZE;
	w_H = GATE_BTN_SIZE;

//	DL_LOG("(%d, %d) (%d, %d, %d, %d)", w_PosX, w_PosY, w_X, w_Y, w_X+w_W, w_Y+w_H);

	if(	   (w_X <= w_PosX && w_PosX <= w_X+w_W) 
		&& (w_Y <= w_PosY && w_PosY <= w_Y+w_H)
	  ) {
		TOGGLE_GATE(wp_PAData->m_Gate, GATE_EM);
		w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		InvalidateRect(w_TempRect);
	}

	w_X = GATE_TITLE_POS + ((w_Rect.right - GATE_TITLE_POS - GATE_BTN_SIZE)/2);
	w_Y = (w_Rect.top+1+(2*GATE_LINE_YGAP)) + ((GATE_LINE_YGAP-GATE_BTN_SIZE)/2);
	w_W = GATE_BTN_SIZE;
	w_H = GATE_BTN_SIZE;

	if(	   (w_X <= w_PosX && w_PosX <= w_X+w_W) 
		&& (w_Y <= w_PosY && w_PosY <= w_Y+w_H)
	  ) {
		TOGGLE_GATE(wp_PAData->m_Gate, GATE_TIME);
		w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
		InvalidateRect(w_TempRect);
	}

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_GROUP_NUM;
	for(w_ii=0;w_ii<GATE_GROUP_NUM;w_ii++) {
		w_X = GATE_TITLE_POS+w_ii*w_Gap + (w_Gap-GATE_BTN_SIZE)/2;
		w_Y = w_Rect.top+1+3*GATE_LINE_YGAP + (GATE_LINE_YGAP/2) + ((GATE_LINE_YGAP/2)-GATE_BTN_SIZE)/2;
		w_W = GATE_BTN_SIZE;
		w_H = GATE_BTN_SIZE;

		if(	   (w_X <= w_PosX && w_PosX <= w_X+w_W) 
			&& (w_Y <= w_PosY && w_PosY <= w_Y+w_H)
		  ) {
			TOGGLE_GATE(wp_PAData->m_Gate, GATE_DRM_A+w_ii);
			w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
			InvalidateRect(w_TempRect);
		}
	}

	w_Gap = (w_Rect.right - GATE_TITLE_POS) / GATE_BGM_NUM;
	for(w_ii=0;w_ii<GATE_BGM_NUM;w_ii++) {
		w_X = GATE_TITLE_POS+w_ii*w_Gap + (w_Gap-GATE_BTN_SIZE)/2;
		w_Y = w_Rect.top+1+4*GATE_LINE_YGAP + (GATE_LINE_YGAP/2) + ((GATE_LINE_YGAP/2)-GATE_BTN_SIZE)/2;
		w_W = GATE_BTN_SIZE;
		w_H = GATE_BTN_SIZE;

		if(	   (w_X <= w_PosX && w_PosX <= w_X+w_W) 
			&& (w_Y <= w_PosY && w_PosY <= w_Y+w_H)
		  ) {
			TOGGLE_GATE(wp_PAData->m_Gate, GATE_BGM_1+w_ii);
			w_TempRect = CRect(w_X, w_Y, w_X+w_W, w_Y+w_H);
			InvalidateRect(w_TempRect);
		}

	}
//	CStatic::OnLButtonDown(nFlags, point);
#endif
}
