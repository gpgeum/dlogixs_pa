#pragma once

#define	GATE_LINE_YGAP	50
#define	GATE_LINE_NUM	6

#define	GATE_GROUP_NUM	6
#define	GATE_BGM_NUM	8

#define	GATE_TITLE_POS	100

#define	GATE_BTN_SIZE	14

enum {
	GATE_EM=0,
	GATE_TIME,
	GATE_DRM_A,
	GATE_DRM_B,
	GATE_DRM_C,
	GATE_DRM_D,
	GATE_DRM_E,
	GATE_DRM_F,
	GATE_BGM_1,
	GATE_BGM_2,
	GATE_BGM_3,
	GATE_BGM_4,
	GATE_BGM_5,
	GATE_BGM_6,
	GATE_BGM_7,
	GATE_BGM_8
};

#define			IS_GATE_ON(x, y)		(((x)>>(y)) & 0x01)

#define			TOGGLE_GATE(x, y)		(x) ^= (0x01 << (y))


// CGateStatic

class CGateStatic : public CStatic
{
	DECLARE_DYNAMIC(CGateStatic)

public:
	CFont	m_Font;


public:
	CGateStatic();
	virtual ~CGateStatic();
	
	void	Draw();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};


