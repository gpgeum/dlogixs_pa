///////////////////////////////////////////////////////////////////////////////
//
//    Module : HexConverter.cpp
//    Project : Win32 LIB for Intel HEX file Convertion Library
//    
//    Copyright : (C) 2006 MinSeok Oh (mk3358@paran.com)
//
//    Module Description :
//    This file is used to convert 8bit Intel HEX file to binary ROM file.
//
//    
//    Revised :
//            2006-09-20    Version Alpha Step-1
//
//
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include <stdio.h>

#include "HexConverter.h"

#define MAX_FIELD_LENGTH				522


#define ERROR_HEXFILE_OPEN				-1
#define ERROR_RECORD_MARK				-10
#define ERROR_RECORD_LENGTH				-20
#define ERROR_RECORD_OFFSET				-30
#define ERROR_RECORD_TYPE				-40
#define ERROR_RECORD_CHKSUM				-50
#define ERROR_RECORD_INVALID_DATA		-60
#define ERROR_CALCULATION_CHKSUM1		-71
#define ERROR_CALCULATION_CHKSUM2		-72


int htoi(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	else if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	else if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	else
		return -1;
}

int htob(char *str, BYTE *len)
{
	int i;
	*len = 0;

	for ( i=0 ; i < 2 ; i++) {
		if (htoi(str[i]) != -1) {
			*len += htoi(str[i])<<(4*(1-i));
		}
		else 
			return -1;
	}
	return 0;
}

int htow(char *str, USHORT *type)
{
	int i;
	*type = 0;

	for ( i=0 ; i < 4 ; i++) {
		if (htoi(str[i]) != -1)
			*type += htoi(str[i])<<(4*(3-i));
		else 
			return -1;
	}
	return 0;
}

int CheckRecordMark(char *rec)
{
	if (rec[0] != ':' ) return ERROR_RECORD_MARK;
	else return 0;
}

int SetBinData(BYTE *BIN, char *rec, BYTE len, ULONG offset)
{
	unsigned int data;
	int i, j;

	for ( i = 0 ; i < len ; i++ ) {
		data = 0;
		for ( j = 0 ; j < 2 ; j++ ) {
			if (htoi(rec[j]) != -1)
				data += htoi(rec[j])<<(4*(1-j));
			else {
				return -1;
			}
		}
//		printf("%02X ", data);
		BIN[offset+i] = data;
		rec += 2;
	}

	return 0;

}


///////////////////////////////////////////////////////////////////////////////
//
//    Hex2Bin() : This function will be called by your applications.
//
///////////////////////////////////////////////////////////////////////////////
int Hex2Bin(LPCTSTR lpszHexFile, BYTE *pBinBuffer, UINT uBufSize, UINT *pnReadSize)
{

	FILE *fp;
	char szRecBuf[MAX_FIELD_LENGTH];
	BYTE RecLen;
	USHORT RecOffset;
	BYTE RecType;
	BYTE RecChkSum, CalcChkSum, i;
	int nRet;
	
	int b1stRecOffset = FALSE;
	USHORT us1StOffset = 0;
	ULONG ulCrrLinearAddr = 0;
	ULONG ul1stLinearAddr = 0;
	int b1stulLinearAddr = FALSE;
	ULONG ulLoadAddr = 0;
	ULONG ulSegBaseAddr = 0;

	*pnReadSize = 0;

#ifdef _UNICODE
	fp = _wfopen(lpszHexFile, _T("r"));
#else
	fp = fopen(lpszHexFile, "r");
#endif

	if (!fp) {
		return ERROR_HEXFILE_OPEN;
	}

	memset(pBinBuffer, 0xFF, uBufSize);
	
	while(fgets(szRecBuf, MAX_FIELD_LENGTH, fp))
	{
		/* Check Record Mark */
		nRet = CheckRecordMark(&szRecBuf[0]);
		if ( nRet < 0 ) return nRet;

		/* Record Length */
		nRet = htob(&szRecBuf[1], &RecLen);
		if ( nRet < 0 ) { nRet = ERROR_RECORD_LENGTH; break; }


		/* Record Offset */
		nRet = htow(&szRecBuf[3], &RecOffset);
		if ( nRet < 0 ) { nRet = ERROR_RECORD_OFFSET; break; }


		/* Record Type */
		nRet = htob(&szRecBuf[7], &RecType);
		if ( nRet < 0 ) { nRet = ERROR_RECORD_TYPE; break; }

		if(RecType == 0x04)		// Extended Linear Address
		{
			RecOffset = 0;

			// 	if(RecLen == 4)
			// 	{
			// 		nRet = htow(&szRecBuf[9], &RecOffset);
			// 		if ( nRet < 0 ) { nRet = ERROR_RECORD_OFFSET; break; }
			// 	}
			// 	else 
			if(RecLen == 2)
			{
				nRet = htow(&szRecBuf[9], &RecOffset);
				if ( nRet < 0 ) { nRet = ERROR_RECORD_OFFSET; break; }
			}

			if(/*RecLen == 4 ||*/ RecLen == 2)
			{
				ulCrrLinearAddr = RecOffset;
				ulCrrLinearAddr = ulCrrLinearAddr << 16;

				if(b1stulLinearAddr == FALSE)
				{
					ul1stLinearAddr = ulCrrLinearAddr;
					b1stulLinearAddr = TRUE;
				}
				else
				{
					us1StOffset = 0;		// 확장 선형 주소를 두 번째부터 받으면 데이터 offset 주소를 초기화 함
				}
			}
		}

		else if(RecType == 0x02)		// Extended Segmented Address Record
		{
			RecOffset = 0;

			if(RecLen == 2)
			{
				nRet = htow(&szRecBuf[9], &RecOffset);
				if ( nRet < 0 ) { nRet = ERROR_RECORD_OFFSET; break; }

				ulSegBaseAddr = RecOffset;
				ulSegBaseAddr = ulSegBaseAddr * 16;
			}
		}

	if(RecType != 0x00)
		continue;

	if(RecType == 0x00 && b1stRecOffset == FALSE)
	{
		us1StOffset = RecOffset;
		b1stRecOffset = TRUE;
	}

	RecOffset -= us1StOffset;
	ulLoadAddr = RecOffset + (ulCrrLinearAddr - ul1stLinearAddr);
	ulLoadAddr += ulSegBaseAddr;

		/* Data */
		nRet = SetBinData(pBinBuffer, &szRecBuf[9], RecLen, ulLoadAddr);
		if ( nRet < 0 ) { nRet = ERROR_RECORD_INVALID_DATA; break; }

		/* CheckSum */
		nRet = htob(&szRecBuf[9 + RecLen*2], &RecChkSum);
		if ( nRet < 0 ) {nRet = ERROR_RECORD_CHKSUM; break; }


		/* Evaluate CheckSum */
		CalcChkSum = 0;

		*pnReadSize += RecLen;

		for ( i = 0 ; i < RecLen + 4; i++ )
		{
			BYTE temp = 0;
			if ( htob(&szRecBuf[1 + 2*i], &temp) != -1 )
			{
				CalcChkSum += temp;
			}
			else
			{
				nRet = ERROR_CALCULATION_CHKSUM1;
				fclose(fp);
				return nRet;
			}
		}

		CalcChkSum = ( ~CalcChkSum + 1 ) % 256;
		if (CalcChkSum != RecChkSum) 
		{
			nRet = ERROR_CALCULATION_CHKSUM2;
//			printf("CalcChkSum(%02X) vs. RecChkSum(%02X)\n", CalcChkSum, RecChkSum);
			break;
		}
		memset(szRecBuf, 0x00, MAX_FIELD_LENGTH);
	};
	
	fclose(fp);

//	if (nRet == 0 ) printf("\nOK\n");
	return nRet;
}
