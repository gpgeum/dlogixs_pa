///////////////////////////////////////////////////////////////////////////////
//
//    Module : HexConverter.h
//    Project : Win32 LIB for Intel HEX file Convertion Library
//    
//    Copyright : (C) 2006 MinSeok Oh (mk3358@paran.com)
//
//    Module Description :
//    This file is main header file for HexConverter Library. And your 
//    application should include this header file to link with the library.
//    
//    Revised :
//            2006-09-20    Version Alpha Step-1
//
//
///////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

int Hex2Bin(LPCTSTR lpszHexFile, BYTE *pBinBuffer, UINT uBufSize, UINT *pnReadSize);

#ifdef __cplusplus
}
#endif