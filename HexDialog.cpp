// HexDialog.cpp : implementation file
//

#include "stdafx.h"
#include "HexDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHexDialog dialog


CHexDialog::CHexDialog(BYTE* pBuf, DWORD dwLen, CWnd* pParent /*=NULL*/)
	: CDialog(CHexDialog::IDD, pParent)
{
	m_pHexCtrl = NULL;
	m_pBuf = pBuf;
	m_dwLen = dwLen;

	//{{AFX_DATA_INIT(CHexDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CHexDialog::~CHexDialog()
{
	if (m_pHexCtrl)
	{
		m_pHexCtrl->DestroyWindow();
		delete m_pHexCtrl;
	}
	m_pHexCtrl = NULL;
}


void CHexDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHexDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHexDialog, CDialog)
	//{{AFX_MSG_MAP(CHexDialog)
	//ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHexDialog message handlers

BOOL CHexDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	MoveWindow(0, 0, 626, 610);

	CenterWindow(m_pParentWnd);

	m_pHexCtrl = new CPPDumpCtrl;
	m_pHexCtrl->Create(CRect(0, 0, 620, 580), this, 100);
	
	m_pHexCtrl->SetSpecialCharView(_TCHAR('.'));
	//m_pHexCtrl->SetFont(_T("����"), 9);	// (*GetFont(), TRUE);
	m_pHexCtrl->SetBeginAddress(0);
	m_pHexCtrl->SetTrackMouseMove(FALSE);
	//m_pHexCtrl->SetReadOnly(TRUE);
	m_pHexCtrl->ModifyStyles(PPDUMP_SEPARATOR_LINES | PPDUMP_READ_ONLY | PPDUMP_NAMED_FIELDS, PPDUMP_BAR_ALL);

// default
// PPDUMP_FIELD_ADDRESS
// PPDUMP_FIELD_HEX
// PPDUMP_FIELD_ASCII
// PPDUMP_BAR_ADDRESS
// PPDUMP_BAR_DEC
// PPDUMP_BAR_HEX
// PPDUMP_BAR_BIN
// PPDUMP_BAR_ASCII

// 	BYTE nNewData [1000];
// 	BYTE nOldData [1000];
// 
 	m_pHexCtrl->SetPointerData(m_dwLen, m_pBuf);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//void CHexDialog::OnDestroy() 
//{
//	CDialog::OnDestroy();
//	
//	// TODO: Add your message handler code here
//	
//	if (m_pHexCtrl)
//		delete m_pHexCtrl;
//}

