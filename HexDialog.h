#if !defined(AFX_HEXDIALOG_H__15505BC8_AFB4_42CF_930C_07D7F54DA091__INCLUDED_)
#define AFX_HEXDIALOG_H__15505BC8_AFB4_42CF_930C_07D7F54DA091__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HexDialog.h : header file
//

#include "PPDumpCtrl.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CHexDialog dialog

class CHexDialog : public CDialog
{
// Construction
public:
	CHexDialog(BYTE* pBuf, DWORD dwLen, CWnd* pParent = NULL);   // standard constructor
	virtual ~CHexDialog();
// Dialog Data
	//{{AFX_DATA(CHexDialog)
	enum { IDD = IDD_DIALOG_HEX };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
protected:

	CPPDumpCtrl*	m_pHexCtrl;
	BYTE*			m_pBuf;
	DWORD			m_dwLen;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHexDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHexDialog)
	virtual BOOL OnInitDialog();
	//afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEXDIALOG_H__15505BC8_AFB4_42CF_930C_07D7F54DA091__INCLUDED_)
