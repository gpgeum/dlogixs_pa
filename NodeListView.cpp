// ChildView.cpp : implementation of the CNodeListView class
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "NodeListView.h"
#include "SystemInformation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define		COLOR_TITLE			COLOR_YELLOW
#define		COLOR_ITEM			COLOR_WHITE
#define		COLOR_ITEM_SELECTED	COLOR_GREEN

#define		DEF_ITEM_WIDTH		100
#define		DEF_ITEM_HEIGHT		35

#define		DEF_ITEM_NUM		8

#define		DEF_LEFT_WIDTH		20			// 좌측 메뉴 WIDTH
#define		DEF_TITLE_WIDTH		DEF_LEFT_WIDTH + (DEF_ITEM_WIDTH * DEF_ITEM_NUM)
#define		DEF_TITLE_HEIGHT	20


#define		DEF_BASE_X		30
#define		DEF_BASE_Y		30
#define		DEF_GAP_Y		50

/////////////////////////////////////////////////////////////////////////////
// CNodeListView

CNodeListView::CNodeListView()
{
	m_SelAddr = 1;		// 선택 Devide 주소
	m_SelItem = 1;		// 선택 Device Item(Output)
}

CNodeListView::~CNodeListView()
{
}


BEGIN_MESSAGE_MAP(CNodeListView, CScrollWindow)
	//{{AFX_MSG_MAP(CNodeListView)
	//}}AFX_MSG_MAP
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNodeListView

void CNodeListView::OnDraw(CDC* pDC)
{
	CDC			w_memDC;
	LOGBRUSH	wp_lb;
	CBrush		*wp_oldBrush = NULL;
	CPen		*wp_oldPen = NULL;
	CPen		w_Pen1Pixel;
	CRect		w_GroupRect;
	CRect		w_Rect;
	int			w_ii;
	int			w_jj;
	int			w_kk;
	int			w_X, w_Y;
	CString		w_Str;

	CBrush		w_Brush1(COLOR_WHITE);
	CBrush		w_Brush2(COLOR_TITLE);
	CBrush		w_Brush3(COLOR_LITEGRAY);

	CBrush		w_Brush4(COLOR_ITEM_SELECTED);
	int			w_DevCnt;

	BYTE		w_Color1 = 0;
	BYTE		w_Color2 = 0;
	BYTE		w_Color3 = 0;

	CString CA2TTemp;


	LPA_DATA_T *wp_PAData = GetPAData();
	if (!wp_PAData) {
		return;
	}

	m_SelAddr = 1;
	m_SelItem = 1;
	switch (GetLowPA()->m_SelPage) {
	case LPA_SEL_DPG:
		if (GetDpgWnd()) {
			m_SelAddr = GetDpgWnd()->m_SelAddr;
			m_SelItem = GetDpgWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_PCMACRO:
		if (GetPCMacroWnd()) {
			m_SelAddr = GetPCMacroWnd()->m_SelAddr;
			m_SelItem = GetPCMacroWnd()->m_SelItem;
		}
		break;
#if defined(DEV_5000)
	case LPA_SEL_DMTMACRO:
		if (GetDMTMacroWnd()) {
			m_SelAddr = GetDMTMacroWnd()->m_SelAddr;
			m_SelItem = GetDMTMacroWnd()->m_SelItem;
		}
		break;
#endif
	case LPA_SEL_TIMERMACRO:
		if (GetTimerMacroWnd()) {
			m_SelAddr = GetTimerMacroWnd()->m_SelAddr;
			m_SelItem = GetTimerMacroWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_MMACRO:
		if (GetMMacroWnd()) {
			m_SelAddr = GetMMacroWnd()->m_SelAddr;
			m_SelItem = GetMMacroWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_DFR:
		if (GetDfrWnd()) {
			m_SelAddr = GetDfrWnd()->m_SelAddr;
			m_SelItem = GetDfrWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_DLC:
		if (GetDLCWnd()) {
			m_SelAddr = GetDLCWnd()->m_SelAddr;
			m_SelItem = GetDLCWnd()->m_SelItem;
		}
		break;
	}
	w_DevCnt = wp_PAData->m_DevCnt[LPA_DMA_IDX] + wp_PAData->m_DevCnt[LPA_DSS_IDX];

	SetScrollSizes(CSize(DEF_BASE_X + DEF_TITLE_WIDTH, DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_DevCnt * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y))));

	GetClientRect(w_Rect);
	w_Rect.top = 0;
	w_Rect.left = 0;
	if (m_sizeTotal.cx > w_Rect.right) {
		w_Rect.right = m_sizeTotal.cx;
	}
	if (m_sizeTotal.cy > w_Rect.bottom) {
		w_Rect.bottom = m_sizeTotal.cy;
	}
	pDC->FillRect(w_Rect, &w_Brush1);

	w_memDC.CreateCompatibleDC(pDC);

	wp_lb.lbColor = RGB(87, 87, 87);
	wp_lb.lbStyle = BS_SOLID;

	// 전체선택 버튼..상태
	memset(m_Group, 0x00, sizeof(m_Group));
	memset(m_SubGroup1, 0x00, sizeof(m_SubGroup1));
	memset(m_SubGroup2, 0x00, sizeof(m_SubGroup2));

	w_Pen1Pixel.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_SQUARE | PS_JOIN_ROUND, 1, &wp_lb);

	for (w_jj = 0; w_jj < w_DevCnt; w_jj++) {
		m_Group[w_jj] = 1;					// 전체선택 버튼 ON
		m_SubGroup1[w_jj] = 1;				// 전체선택 버튼 ON
		m_SubGroup2[w_jj] = 1;				// 전체선택 버튼 ON

		w_X = DEF_BASE_X;
		w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y));

		if (!wp_oldBrush) {
			wp_oldBrush = pDC->SelectObject(&w_Brush2);
		}
		else {
			pDC->SelectObject(&w_Brush2);
		}
		if (!wp_oldPen) {
			wp_oldPen = pDC->SelectObject(&w_Pen1Pixel);
		}
		else {
			pDC->SelectObject(&w_Pen1Pixel);
		}
		// TITLE을 그림
		w_Rect = CRect(w_X, w_Y, w_X + DEF_TITLE_WIDTH, w_Y + DEF_TITLE_HEIGHT);
		pDC->Rectangle(w_Rect);


		// DEVICE NAME
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(COLOR_BLACK);
		pDC->SelectObject(m_Font);

		// TITLE문자를 쓴다.
		if (w_jj >= wp_PAData->m_DevCnt[LPA_DMA_IDX])
		{
			w_kk = w_jj - wp_PAData->m_DevCnt[LPA_DMA_IDX];
			if (wp_PAData->m_DssData[w_kk].m_DevName[0])
			{
				CA2TTemp = CA2T(wp_PAData->m_DssData[w_kk].m_DevName);
				w_Str.Format(_T("%s"), CA2TTemp);
			}
			else
			{
				w_Str.Format(_T("DSS %d"), w_kk + 1);
			}

		}
		else {
			if (wp_PAData->m_DmtData[w_jj].m_DevName[0])
			{
				CA2TTemp = CA2T(wp_PAData->m_DmtData[w_jj].m_DevName);
				w_Str.Format(_T("%s"), CA2TTemp);
			}
			else
			{
#if defined(DEV_5000)
				w_Str.Format(_T("DPA %d"), w_jj + 1);
#else
				w_Str.Format(_T("DMT %d"), w_jj + 1);
#endif
			}
		}
		w_Rect = CRect(w_X + 10 + 20, w_Y, w_X + DEF_TITLE_WIDTH - (10 + 20), w_Y + DEF_TITLE_HEIGHT);
		pDC->DrawText(w_Str, w_Rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

		w_Rect = CRect(w_X, w_Y + DEF_TITLE_HEIGHT, w_X + DEF_LEFT_WIDTH, w_Y + DEF_TITLE_HEIGHT + DEF_ITEM_HEIGHT);
		pDC->Rectangle(w_Rect);

		w_Rect = CRect(w_X, w_Y + DEF_TITLE_HEIGHT + DEF_ITEM_HEIGHT, w_X + DEF_LEFT_WIDTH, w_Y + DEF_TITLE_HEIGHT + DEF_ITEM_HEIGHT + DEF_ITEM_HEIGHT);
		pDC->Rectangle(w_Rect);


		pDC->SelectObject(&w_Brush1);

		w_X += DEF_LEFT_WIDTH;
		w_Y += DEF_TITLE_HEIGHT;
		for (w_ii = 0; w_ii < LPA_TERMINAL_ITEM; w_ii++) {

			w_Rect = CRect(w_X, w_Y, w_X + DEF_ITEM_WIDTH, w_Y + DEF_ITEM_HEIGHT);
			if (GetLowPA()->m_SelPage == LPA_SEL_DPG && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == m_SelAddr
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == m_SelItem) {
					pDC->SelectObject(&w_Brush4);
				}
				else if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0) {
					pDC->SelectObject(&w_Brush3);
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}
				}
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_PCMACRO && wp_PAData->m_PCMacroCnt > 0) {
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0 && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					pDC->SelectObject(&w_Brush3);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
				else if (m_SelAddr > 0 && m_SelItem > 0 && wp_PAData->m_PCMacroData[m_SelAddr - 1].m_Cnt > 0) {
					if (GetLowPA()->IsBitOn(wp_PAData->m_PCMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag, w_jj, w_ii)) {
						pDC->SelectObject(&w_Brush4);
					}
					else {
						pDC->SelectObject(&w_Brush1);
						m_Group[w_jj] = 0;
						if (w_ii < 8) {
							m_SubGroup1[w_jj] = 0;
						}
						else {
							m_SubGroup2[w_jj] = 0;
						}

					}
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_DLC && wp_PAData->m_DLCCnt > 0) { // PG 영향 안 받게 수정 필요
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0 && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					pDC->SelectObject(&w_Brush3);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
				else if (m_SelAddr > 0 && m_SelItem > 0 && wp_PAData->m_DLCData[m_SelAddr - 1].m_Cnt > 0) {
					if (GetLowPA()->IsBitOn(wp_PAData->m_DLCTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag, w_jj, w_ii)) {
						pDC->SelectObject(&w_Brush4);
					}
					else {
						pDC->SelectObject(&w_Brush1);
						m_Group[w_jj] = 0;
						if (w_ii < 8) {
							m_SubGroup1[w_jj] = 0;
						}
						else {
							m_SubGroup2[w_jj] = 0;
						}

					}
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}
				}


#if defined(DEV_5000)
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_DMTMACRO && wp_PAData->m_DMTMacroCnt > 0) {
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0 && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					pDC->SelectObject(&w_Brush3);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
				else if (m_SelAddr > 0 && m_SelItem > 0 && wp_PAData->m_DMTMacroData[m_SelAddr - 1].m_Cnt > 0) {
					if (GetLowPA()->IsBitOn(wp_PAData->m_DMTMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag, w_jj, w_ii)) {
						pDC->SelectObject(&w_Brush4);
					}
					else {
						pDC->SelectObject(&w_Brush1);
						m_Group[w_jj] = 0;
						if (w_ii < 8) {
							m_SubGroup1[w_jj] = 0;
						}
						else {
							m_SubGroup2[w_jj] = 0;
						}

					}
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

			}
#endif
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_TIMERMACRO &&
#if defined(DEV_5000) 
				GetLowPA()->m_TimerMacroMode == 0 &&
#endif
				wp_PAData->m_TimerMacroCnt > 0) {
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0 && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					pDC->SelectObject(&w_Brush3);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
				else if (m_SelAddr > 0 && m_SelItem > 0 && wp_PAData->m_TimerMacroData[m_SelAddr - 1].m_Cnt > 0) {
					if (GetLowPA()->IsBitOn(wp_PAData->m_TimerMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag, w_jj, w_ii)) {
						pDC->SelectObject(&w_Brush4);
					}
					else {
						pDC->SelectObject(&w_Brush1);
						m_Group[w_jj] = 0;
						if (w_ii < 8) {
							m_SubGroup1[w_jj] = 0;
						}
						else {
							m_SubGroup2[w_jj] = 0;
						}

					}
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
#if defined(DEV_5000)
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_TIMERMACRO &&
				GetLowPA()->m_TimerMacroMode == 1 &&
				wp_PAData->m_EmMacroCnt > 0) {

				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0 && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					pDC->SelectObject(&w_Brush3);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
				else if (m_SelAddr > 0 && m_SelItem > 0 && wp_PAData->m_EmMacroData[m_SelAddr - 1].m_Cnt > 0) {
					if (GetLowPA()->IsBitOn(wp_PAData->m_EmMacroTerm[m_SelAddr - 1][m_SelItem - 2].m_SSFlag, w_jj, w_ii)) {
						pDC->SelectObject(&w_Brush4);
					}
					else {
						pDC->SelectObject(&w_Brush1);
						m_Group[w_jj] = 0;
						if (w_ii < 8) {
							m_SubGroup1[w_jj] = 0;
						}
						else {
							m_SubGroup2[w_jj] = 0;
						}

					}
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
#endif
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_MMACRO && wp_PAData->m_MMacroCnt > 0) {
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0 && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					pDC->SelectObject(&w_Brush3);
					m_Group[w_jj] = 0;
				}
				else if (m_SelAddr > 0 && m_SelItem > 0 && wp_PAData->m_MMacroData[m_SelAddr - 1].m_Cnt > 0) {
					if (GetLowPA()->IsBitOn(wp_PAData->m_MMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag, w_jj, w_ii)) {
						pDC->SelectObject(&w_Brush4);
					}
					else {
						pDC->SelectObject(&w_Brush1);
						m_Group[w_jj] = 0;
						if (w_ii < 8) {
							m_SubGroup1[w_jj] = 0;
						}
						else {
							m_SubGroup2[w_jj] = 0;
						}

					}
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_DFR && wp_PAData->m_DevCnt[LPA_DFR_IDX] > 0) {
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0 && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					pDC->SelectObject(&w_Brush3);
					m_Group[w_jj] = 0;
				}
				else if (m_SelAddr > 0 && m_SelItem > 0 && wp_PAData->m_DfrData[m_SelAddr - 1].m_Cnt > 0) {
					if (GetLowPA()->IsBitOn(wp_PAData->m_DfrTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag, w_jj, w_ii)) {
						pDC->SelectObject(&w_Brush4);
					}
					else {
						pDC->SelectObject(&w_Brush1);
						m_Group[w_jj] = 0;
						if (w_ii < 8) {
							m_SubGroup1[w_jj] = 0;
						}
						else {
							m_SubGroup2[w_jj] = 0;
						}

					}
				}
				else {
					pDC->SelectObject(&w_Brush1);
					m_Group[w_jj] = 0;
					if (w_ii < 8) {
						m_SubGroup1[w_jj] = 0;
					}
					else {
						m_SubGroup2[w_jj] = 0;
					}

				}
			}
			else {
				pDC->SelectObject(&w_Brush1);
				m_Group[w_jj] = 0;
				if (w_ii < 8) {
					m_SubGroup1[w_jj] = 0;
				}
				else {
					m_SubGroup2[w_jj] = 0;
				}

			}
			pDC->Rectangle(w_Rect);

			if (GetLowPA()->m_SelPage == LPA_SEL_DPG && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {		// 사용된 경우 해당 DPG 상/좌단에 표시
				if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
					&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0) {

					// Color를 여러색으로표시
					w_Color1 = 0;
					if (wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
						w_Color2 = (255 / wp_PAData->m_DevCnt[LPA_DPG_IDX]) * (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr - 1);
					}
					else {
						w_Color2 = (255 / LPA_DPG_MAX) * (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr - 1);
					}
					w_Color3 = (255 / LPA_DPG_ITEM) * (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput - 1);
					if ((wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput) % 2 == 1) {
						w_Color2 = 255 - w_Color2;
						w_Color3 = 255 - w_Color3;
					}

					pDC->SetBkMode(OPAQUE);
					pDC->SetBkColor(RGB(w_Color1, w_Color2, w_Color3));
					pDC->SetTextColor(COLOR_RED);
					pDC->SelectObject(m_FontSmall);
					w_Rect.top += 1;
					w_Rect.left += 1;
					w_Str.Format(_T("%d-%d"), wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr, wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput);
					pDC->DrawText(w_Str, w_Rect, DT_LEFT | DT_TOP | DT_SINGLELINE);
				}
			}

			pDC->SetBkMode(TRANSPARENT);
			pDC->SelectObject(m_Font);

			if (w_jj >= wp_PAData->m_DevCnt[LPA_DMA_IDX]) {
				w_kk = w_jj - wp_PAData->m_DevCnt[LPA_DMA_IDX];
				if (wp_PAData->m_DssData[w_kk].m_Item[w_ii].m_Name[0]) {
					pDC->SetTextColor(RGB(225, 90, 0));
					CA2TTemp = CA2T(wp_PAData->m_DssData[w_kk].m_Item[w_ii].m_Name);
					w_Str.Format(_T("%s"), CA2TTemp);
				}
				else {
					pDC->SetTextColor(COLOR_BLACK);
					w_Str.Format(_T("%d"), w_ii + 1);
				}
			}
			else {
				if (wp_PAData->m_DmtData[w_jj].m_Item[w_ii].m_Name[0]) {
					pDC->SetTextColor(RGB(225, 90, 0));
					CA2TTemp = CA2T(wp_PAData->m_DmtData[w_jj].m_Item[w_ii].m_Name);
					w_Str.Format(_T("%s"), CA2TTemp);
				}
				else {
					pDC->SetTextColor(COLOR_BLACK);
					w_Str.Format(_T("%d"), w_ii + 1);
				}
			}

			//문자열 길이가 10이상, 20글자미만 체크
			int		w_Len = 0;
			TCHAR	str[64];
			char	w_Buff[64];

			memset(&str, 0x00, sizeof(str));
			memset(&w_Buff, 0x00, sizeof(w_Buff));
			wsprintf(str, _T("%s"), w_Str);
			dl_strcpy(w_Buff, CT2A(w_Str));
			//sprintf(w_Buff,"%s", CA2TTemp);
			w_Len = lstrlenA(w_Buff);

			//별칭이름 바이트 길이로 체크
			if (w_Len > 10)
			{
				w_Rect.top = w_Rect.top + 5;
				pDC->DrawText(w_Str, w_Rect, DT_CENTER | DT_VCENTER | DT_WORDBREAK | DT_EDITCONTROL);
			}
			else {
				pDC->DrawText(w_Str, w_Rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}

			if ((w_ii%DEF_ITEM_NUM) == (DEF_ITEM_NUM - 1)) {
				w_Y += DEF_ITEM_HEIGHT;
				w_X = DEF_BASE_X + DEF_LEFT_WIDTH;
			}
			else {
				w_X += DEF_ITEM_WIDTH;
			}
		}

		w_X = DEF_BASE_X;
		w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y));

		w_GroupRect = CRect(w_X + 10, w_Y + 3, w_X + 10 + 14, w_Y + 2 + 14);
		if (m_Group[w_jj]) {
			pDC->SelectObject(&w_Brush4);
		}
		else {
			pDC->SelectObject(&w_Brush1);
		}
		pDC->Ellipse(w_GroupRect);

		w_Y += DEF_TITLE_HEIGHT;

		w_GroupRect = CRect(w_X + 3, w_Y + 7, w_X + 3 + 14, w_Y + 6 + 14);
		if (m_SubGroup1[w_jj]) {
			pDC->SelectObject(&w_Brush4);
		}
		else {
			pDC->SelectObject(&w_Brush1);
		}

		w_Y += DEF_ITEM_HEIGHT;
		pDC->Ellipse(w_GroupRect);
		w_GroupRect = CRect(w_X + 3, w_Y + 7, w_X + 3 + 14, w_Y + 6 + 14);
		if (m_SubGroup2[w_jj]) {
			pDC->SelectObject(&w_Brush4);
		}
		else {
			pDC->SelectObject(&w_Brush1);
		}
		pDC->Ellipse(w_GroupRect);

	}

	pDC->SelectObject(wp_oldPen);
	pDC->SelectObject(wp_oldBrush);
}

/////////////////////////////////////////////////////////////////////////////
// CNodeListView message handlers

BOOL CNodeListView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CScrollWindow::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), HBRUSH(COLOR_WINDOW + 1), NULL);

	return TRUE;
}

int CNodeListView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollWindow::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_Font.CreateFont(
		-12,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		0,
		DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH,
		DEF_FONT_NAME);

	m_FontSmall.CreateFont(
		-10,
		0,
		0,
		0,
		FW_NORMAL,
		FALSE,
		FALSE,
		0,
		DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH,
		DEF_FONT_NAME);

	GetLowPA()->SetTermWnd(this);

	return 0;
}



BOOL CNodeListView::OnEraseBkgnd(CDC* pDC)
{
	//OnDraw(pDC);

	return FALSE;
	//	return CScrollWindow::OnEraseBkgnd(pDC);
}


void CNodeListView::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect		w_Rect;
	int			w_ii;
	int			w_jj;
	int			w_mm;
	int			w_nn;
	int			w_X, w_Y;
	CString		w_Str;
	CPoint		w_sPoint;


	w_sPoint = GetScrollPosition();
	point += w_sPoint;

	int			w_DevCnt;
	LPA_DATA_T *wp_PAData = GetPAData();
	if (!wp_PAData) {
		return;
	}
	if (GetLowPA()->m_SelPage == LPA_SEL_EQUIP
		|| GetLowPA()->m_SelPage == LPA_SEL_ETC) { // DO NOTHING
		return;
	}
	m_SelAddr = 1;
	m_SelItem = 1;
	switch (GetLowPA()->m_SelPage) {
	case LPA_SEL_DPG:
		if (GetDpgWnd()) {
			m_SelAddr = GetDpgWnd()->m_SelAddr;
			m_SelItem = GetDpgWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_PCMACRO:
		if (GetPCMacroWnd()) {
			m_SelAddr = GetPCMacroWnd()->m_SelAddr;
			m_SelItem = GetPCMacroWnd()->m_SelItem;
		}
		break;
#if defined(DEV_5000)
	case LPA_SEL_DMTMACRO:
		if (GetDMTMacroWnd()) {
			m_SelAddr = GetDMTMacroWnd()->m_SelAddr;
			m_SelItem = GetDMTMacroWnd()->m_SelItem;
		}
		break;
#endif
	case LPA_SEL_TIMERMACRO:
		if (GetTimerMacroWnd()) {
			m_SelAddr = GetTimerMacroWnd()->m_SelAddr;
			m_SelItem = GetTimerMacroWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_MMACRO:
		if (GetMMacroWnd()) {
			m_SelAddr = GetMMacroWnd()->m_SelAddr;
			m_SelItem = GetMMacroWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_DFR:
		if (GetDfrWnd()) {
			m_SelAddr = GetDfrWnd()->m_SelAddr;
			m_SelItem = GetDfrWnd()->m_SelItem;
		}
		break;
	case LPA_SEL_DLC:
		if (GetDLCWnd()) {
			m_SelAddr = GetDLCWnd()->m_SelAddr;
			m_SelItem = GetDLCWnd()->m_SelItem;
		}
		break;
	}

	if (m_SelAddr == 0 || m_SelItem == 0) {
		MessageBox(_T("DEVICE 및 OUTPUT을 먼저 선택하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
		return;
	}
	int	w_GroupMode = 0;
	int	w_Start = 0;
	int	w_End = 0;

	w_DevCnt = wp_PAData->m_DevCnt[LPA_DMA_IDX] + wp_PAData->m_DevCnt[LPA_DSS_IDX];
	for (w_jj = 0; w_jj < w_DevCnt; w_jj++) {
		w_X = DEF_BASE_X;
		w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y));

		// 전체선택
		w_Rect = CRect(w_X, w_Y, w_X + 100, w_Y + DEF_TITLE_HEIGHT);
		if ((w_Rect.left <= point.x && point.x <= w_Rect.right)
			&& (w_Rect.top <= point.y && point.y <= w_Rect.bottom)) {
			w_GroupMode = 1;
			w_Start = 0;
			w_End = LPA_TERMINAL_ITEM;
		}

		// 위 8개 전체 선택
		w_Rect = CRect(w_X, w_Y + DEF_TITLE_HEIGHT, w_X + DEF_LEFT_WIDTH, w_Y + DEF_TITLE_HEIGHT + DEF_ITEM_HEIGHT);
		if ((w_Rect.left <= point.x && point.x <= w_Rect.right)
			&& (w_Rect.top <= point.y && point.y <= w_Rect.bottom)) {
			w_GroupMode = 2;

			w_Start = 0;
			w_End = LPA_TERMINAL_ITEM / 2;
		}

		// 아래 8개 전체 선택
		w_Rect = CRect(w_X, w_Y + DEF_TITLE_HEIGHT + DEF_ITEM_HEIGHT, w_X + DEF_LEFT_WIDTH, w_Y + DEF_TITLE_HEIGHT + DEF_ITEM_HEIGHT + DEF_ITEM_HEIGHT);
		if ((w_Rect.left <= point.x && point.x <= w_Rect.right)
			&& (w_Rect.top <= point.y && point.y <= w_Rect.bottom)) {
			w_GroupMode = 3;

			w_Start = LPA_TERMINAL_ITEM / 2;
			w_End = LPA_TERMINAL_ITEM;
		}

		if (w_GroupMode) {
			if (GetLowPA()->m_SelPage == LPA_SEL_DPG && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;

					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == m_SelAddr
							&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == m_SelItem) {
							wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = 0;
							wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = 0;

							GetLowPA()->SetModified(1);
						}

						// 관련 MACRO들 CLEAR
						for (w_nn = 0; w_nn < LPA_PCMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_PCMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_PCMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
						for (w_nn = 0; w_nn < LPA_TIMERMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_TIMERMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_TimerMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
							}
#if defined(DEV_5000)
						for (w_nn = 0; w_nn < LPA_EMMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_EMMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_EmMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
#endif
						for (w_nn = 0; w_nn < LPA_MMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_MMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_MMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
						for (w_nn = 0; w_nn < LPA_DFR_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_DFR_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_DfrTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
						}

					}
				else {
					//					m_Group[w_jj] = 1;
					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0 && wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0) {
							wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = m_SelAddr;
							wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = m_SelItem;

							GetLowPA()->SetModified(1);
						}
					}

				}
				if (GetDpgWnd()) {
					GetDpgWnd()->RedrawCount(m_SelItem);
				}
				}
			else if (GetLowPA()->m_SelPage == LPA_SEL_PCMACRO) {
				if (wp_PAData->m_PCMacroData[m_SelAddr - 1].m_Cnt < 1) {
					MessageBox(_T("매크로를 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
					return;
				}

				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;
					if (w_GroupMode == 1) {
						wp_PAData->m_PCMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] = 0x00;
					}
					else if (w_GroupMode == 2) {
						wp_PAData->m_PCMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0xff00;
					}
					else if (w_GroupMode == 3) {
						wp_PAData->m_PCMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0x00ff;
					}

					GetLowPA()->SetModified(1);
				}
				else {
					//					m_Group[w_jj] = 1;

					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
							|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
							GetLowPA()->SetBit(wp_PAData->m_PCMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
								w_jj, w_ii, 1);

							GetLowPA()->SetModified(1);
						}
					}

				}
				if (GetPCMacroWnd()) {
					GetPCMacroWnd()->RedrawCount(m_SelItem);
				}
#if defined(DEV_5000)
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_DMTMACRO) {
				if (wp_PAData->m_DMTMacroData[m_SelAddr - 1].m_Cnt < 1) {
					MessageBox(_T("매크로를 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
					return;
				}

				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;
					if (w_GroupMode == 1) {
						wp_PAData->m_DMTMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] = 0x00;
					}
					else if (w_GroupMode == 2) {
						wp_PAData->m_DMTMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0xff00;
					}
					else if (w_GroupMode == 3) {
						wp_PAData->m_DMTMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0x00ff;
					}

					GetLowPA()->SetModified(1);
				}
				else {
					//					m_Group[w_jj] = 1;

					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
							|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
							GetLowPA()->SetBit(wp_PAData->m_DMTMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
								w_jj, w_ii, 1);

							GetLowPA()->SetModified(1);
						}
					}

				}
				if (GetDMTMacroWnd()) {
					GetDMTMacroWnd()->RedrawCount(m_SelItem);
				}
#endif
				}
			else if (GetLowPA()->m_SelPage == LPA_SEL_TIMERMACRO
#if defined(DEV_5000)
				&& GetLowPA()->m_TimerMacroMode == 0
#endif
				) {
				if (wp_PAData->m_TimerMacroData[m_SelAddr - 1].m_Cnt < 1) {
					wp_PAData->m_TimerMacroData[m_SelAddr - 1].m_Cnt = 1;
				}

				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;
					if (w_GroupMode == 1) {
						wp_PAData->m_TimerMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] = 0x00;
					}
					else if (w_GroupMode == 2) {
						wp_PAData->m_TimerMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0xff00;
					}
					else if (w_GroupMode == 3) {
						wp_PAData->m_TimerMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0x00ff;
					}

					GetLowPA()->SetModified(1);
				}
				else {
					//					m_Group[w_jj] = 1;

					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
							|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
							GetLowPA()->SetBit(wp_PAData->m_TimerMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
								w_jj, w_ii, 1);

							GetLowPA()->SetModified(1);
						}
					}

				}
				if (GetTimerMacroWnd()) {
					GetTimerMacroWnd()->RedrawTimerCount(m_SelItem);
				}
#if defined(DEV_5000)
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_TIMERMACRO && GetLowPA()->m_TimerMacroMode == 1) {
				if (wp_PAData->m_EmMacroData[m_SelAddr - 1].m_Cnt < 1) {
					wp_PAData->m_EmMacroData[m_SelAddr - 1].m_Cnt = 1;
				}

				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;
					if (w_GroupMode == 1) {
						wp_PAData->m_EmMacroTerm[m_SelAddr - 1][m_SelItem - 2].m_SSFlag[w_jj] = 0x00;
					}
					else if (w_GroupMode == 2) {
						wp_PAData->m_EmMacroTerm[m_SelAddr - 1][m_SelItem - 2].m_SSFlag[w_jj] &= 0xff00;
					}
					else if (w_GroupMode == 3) {
						wp_PAData->m_EmMacroTerm[m_SelAddr - 1][m_SelItem - 2].m_SSFlag[w_jj] &= 0x00ff;
					}

					GetLowPA()->SetModified(1);
				}
				else {
					//					m_Group[w_jj] = 1;

					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
							|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
							GetLowPA()->SetBit(wp_PAData->m_EmMacroTerm[m_SelAddr - 1][m_SelItem - 2].m_SSFlag,
								w_jj, w_ii, 1);

							GetLowPA()->SetModified(1);
						}
					}

				}
				if (GetTimerMacroWnd()) {
					GetTimerMacroWnd()->RedrawTimerCount(m_SelItem);
				}
#endif
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_MMACRO && wp_PAData->m_MMacroCnt > 0) {
				if (wp_PAData->m_MMacroData[m_SelAddr - 1].m_Cnt < 1) {
					MessageBox(_T("매크로를 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
					return;
				}
				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;
					if (w_GroupMode == 1) {
						wp_PAData->m_MMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] = 0x00;
					}
					else if (w_GroupMode == 2) {
						wp_PAData->m_MMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0xff00;
					}
					else if (w_GroupMode == 3) {
						wp_PAData->m_MMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0x00ff;
					}
					GetLowPA()->SetModified(1);
				}
				else {
					//					m_Group[w_jj] = 1;
					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
							|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
							GetLowPA()->SetBit(wp_PAData->m_MMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
								w_jj, w_ii, 1);

							GetLowPA()->SetModified(1);
						}
					}
				}
				if (GetMMacroWnd()) {
					GetMMacroWnd()->RedrawCount(m_SelItem);
					GetMMacroWnd()->RedrawAddrCount(m_SelAddr);
				}
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_DFR && wp_PAData->m_DevCnt[LPA_DFR_IDX] > 0) {
				if (wp_PAData->m_DfrData[m_SelAddr - 1].m_Cnt < 1) {
					MessageBox(_T("항목을 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
					return;
				}

				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;
					if (w_GroupMode == 1) {
						wp_PAData->m_DfrTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] = 0x00;
					}
					else if (w_GroupMode == 2) {
						wp_PAData->m_DfrTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0xff00;
					}
					else if (w_GroupMode == 3) {
						wp_PAData->m_DfrTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0x00ff;
					}

					GetLowPA()->SetModified(1);
				}
				else {
					//					m_Group[w_jj] = 1;
					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
							|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
							GetLowPA()->SetBit(wp_PAData->m_DfrTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
								w_jj, w_ii, 1);

							GetLowPA()->SetModified(1);

						}
					}
				}
				if (GetDfrWnd()) {
					GetDfrWnd()->RedrawCount(m_SelItem);
				}
			}
			else if (GetLowPA()->m_SelPage == LPA_SEL_DLC && wp_PAData->m_DLCCnt > 0) {
				if (wp_PAData->m_DLCData[m_SelAddr - 1].m_Cnt < 1) {
					wp_PAData->m_DLCData[m_SelAddr - 1].m_Cnt = 2;
				}
				if ((m_Group[w_jj] == 1 && w_GroupMode == 1) || (m_SubGroup1[w_jj] == 1 && w_GroupMode == 2) || (m_SubGroup2[w_jj] == 1 && w_GroupMode == 3)) {
					//					m_Group[w_jj] = 0;
					if (w_GroupMode == 1) {
						wp_PAData->m_DLCTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] = 0x00;
					}
					else if (w_GroupMode == 2) {
						wp_PAData->m_DLCTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0xff00;
					}
					else if (w_GroupMode == 3) {
						wp_PAData->m_DLCTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag[w_jj] &= 0x00ff;
					}
					GetLowPA()->SetModified(1);
				}
				else {
					//					m_Group[w_jj] = 1;
					for (w_ii = w_Start; w_ii < w_End; w_ii++) {
						if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
							|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
							GetLowPA()->SetBit(wp_PAData->m_DLCTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
								w_jj, w_ii, 1);

							GetLowPA()->SetModified(1);
						}
					}
				}
				if (GetDLCWnd()) {
					GetDLCWnd()->RedrawDLCCount(m_SelItem);
				}
			}
			// 해당 DEVICE 전체 재표시
			w_X = DEF_BASE_X - w_sPoint.x;
			w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y)) - w_sPoint.y;
			w_Rect = CRect(w_X, w_Y, w_X + DEF_TITLE_WIDTH, w_Y + DEF_TITLE_HEIGHT + (DEF_ITEM_HEIGHT * 2));
			InvalidateRect(w_Rect);

			break;
				}

		w_X = DEF_BASE_X + DEF_LEFT_WIDTH;
		w_Y += DEF_TITLE_HEIGHT;
		for (w_ii = 0; w_ii < LPA_TERMINAL_ITEM; w_ii++) {

			w_Rect = CRect(w_X, w_Y, w_X + DEF_ITEM_WIDTH, w_Y + DEF_ITEM_HEIGHT);
			if ((w_Rect.left <= point.x && point.x <= w_Rect.right)
				&& (w_Rect.top <= point.y && point.y <= w_Rect.bottom)) {
				//DL_LOG("SEL : (%d, %d)", w_ii, w_jj);

				if (GetLowPA()->m_SelPage == LPA_SEL_DPG && wp_PAData->m_DevCnt[LPA_DPG_IDX] > 0) {
					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == m_SelAddr
						&& wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == m_SelItem) {
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = 0;
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = 0;

						GetLowPA()->SetModified(1);

						// 관련 MACRO들 CLEAR
						for (w_nn = 0; w_nn < LPA_PCMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_PCMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_PCMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
						for (w_nn = 0; w_nn < LPA_TIMERMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_TIMERMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_TimerMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
#if defined(DEV_5000)
						for (w_nn = 0; w_nn < LPA_EMMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_EMMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_EmMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
#endif

						for (w_nn = 0; w_nn < LPA_MMACRO_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_MMACRO_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_MMacroTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}
						for (w_nn = 0; w_nn < LPA_DFR_MAX; w_nn++) {
							for (w_mm = 0; w_mm < LPA_DFR_ITEM; w_mm++) {
								GetLowPA()->SetBit(wp_PAData->m_DfrTerm[w_nn][w_mm].m_SSFlag, w_jj, w_ii, 0);
							}
						}

							}
					else if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr == 0 && wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput == 0) {
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr = m_SelAddr;
						wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput = m_SelItem;

						GetLowPA()->SetModified(1);

					}
					if (GetDpgWnd()) {
						GetDpgWnd()->RedrawCount(m_SelItem);
					}
						}
				else if (GetLowPA()->m_SelPage == LPA_SEL_PCMACRO && wp_PAData->m_PCMacroCnt > 0) {
					if (wp_PAData->m_PCMacroData[m_SelAddr - 1].m_Cnt < 1) {
						MessageBox(_T("매크로를 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
						return;
					}
					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
						|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
						GetLowPA()->ToggleBit(wp_PAData->m_PCMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
							w_jj, w_ii);

						if (GetPCMacroWnd()) {
							GetPCMacroWnd()->RedrawCount(m_SelItem);
						}
					}
#if defined(DEV_5000)
				}
				else if (GetLowPA()->m_SelPage == LPA_SEL_DMTMACRO && wp_PAData->m_DMTMacroCnt > 0) {
					if (wp_PAData->m_DMTMacroData[m_SelAddr - 1].m_Cnt < 1) {
						MessageBox(_T("매크로를 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
						return;
					}
					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
						|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
						GetLowPA()->ToggleBit(wp_PAData->m_DMTMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
							w_jj, w_ii);

						if (GetDMTMacroWnd()) {
							GetDMTMacroWnd()->RedrawCount(m_SelItem);
						}
					}
#endif
					}
				else if (GetLowPA()->m_SelPage == LPA_SEL_TIMERMACRO &&
#if defined(DEV_5000)
					GetLowPA()->m_TimerMacroMode == 0 &&
#endif
					wp_PAData->m_TimerMacroCnt > 0) {
					if (wp_PAData->m_TimerMacroData[m_SelAddr - 1].m_Cnt < 1) {
						wp_PAData->m_TimerMacroData[m_SelAddr - 1].m_Cnt = 1;
					}
					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
						|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
						GetLowPA()->ToggleBit(wp_PAData->m_TimerMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
							w_jj, w_ii);

						if (GetTimerMacroWnd()) {
							GetTimerMacroWnd()->RedrawTimerCount(m_SelItem);
						}
					}
#if defined(DEV_5000)
				}
				else if (GetLowPA()->m_SelPage == LPA_SEL_TIMERMACRO && GetLowPA()->m_TimerMacroMode == 1 && wp_PAData->m_EmMacroCnt > 0) {
					if (wp_PAData->m_EmMacroData[m_SelAddr - 1].m_Cnt < 1) {
						wp_PAData->m_EmMacroData[m_SelAddr - 1].m_Cnt = 1;
					}
					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
						|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
						GetLowPA()->ToggleBit(wp_PAData->m_EmMacroTerm[m_SelAddr - 1][m_SelItem - 2].m_SSFlag,
							w_jj, w_ii);

						if (GetTimerMacroWnd()) {
							GetTimerMacroWnd()->RedrawTimerCount(m_SelItem);
						}
					}
#endif
				}
				else if (GetLowPA()->m_SelPage == LPA_SEL_DLC && wp_PAData->m_DLCCnt > 0) {
					if (wp_PAData->m_DLCData[m_SelAddr - 1].m_Cnt < 1) {
						wp_PAData->m_DLCData[m_SelAddr - 1].m_Cnt = 2;
					}
					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
						|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
						GetLowPA()->ToggleBit(wp_PAData->m_DLCTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
							w_jj, w_ii);

						GetLowPA()->SetModified(1);

						if (GetDLCWnd()) {
							GetDLCWnd()->RedrawDLCCount(m_SelItem);
						}
					}

				}
				else if (GetLowPA()->m_SelPage == LPA_SEL_MMACRO && wp_PAData->m_MMacroCnt > 0) {
					if (wp_PAData->m_MMacroData[m_SelAddr - 1].m_Cnt < 1) {
						MessageBox(_T("매크로를 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
						return;
					}

					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
						|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
						GetLowPA()->ToggleBit(wp_PAData->m_MMacroTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
							w_jj, w_ii);

						GetLowPA()->SetModified(1);

						if (GetMMacroWnd()) {
							GetMMacroWnd()->RedrawCount(m_SelItem);
							GetMMacroWnd()->RedrawAddrCount(m_SelAddr);
						}
					}
				}
				else if (GetLowPA()->m_SelPage == LPA_SEL_DFR && wp_PAData->m_DevCnt[LPA_DFR_IDX] > 0) {
					if (wp_PAData->m_DfrData[m_SelAddr - 1].m_Cnt < 1) {
						MessageBox(_T("매크로를 먼저 추가하십시요."), _T("실행오류"), MB_OK | MB_ICONWARNING);
						return;
					}
					if (wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgAddr != 0
						|| wp_PAData->m_TermDpg[w_jj][w_ii].m_DpgOutput != 0 || wp_PAData->m_DevCnt[LPA_DPG_IDX] == 0) {
						GetLowPA()->ToggleBit(wp_PAData->m_DfrTerm[m_SelAddr - 1][m_SelItem - 1].m_SSFlag,
							w_jj, w_ii);

						GetLowPA()->SetModified(1);

						if (GetDfrWnd()) {
							GetDfrWnd()->RedrawCount(m_SelItem);
						}
					}
				}

				// 해당 DEVICE 전체 재표시
				w_X = DEF_BASE_X - w_sPoint.x;
				w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y)) - w_sPoint.y;
				w_Rect = CRect(w_X, w_Y, w_X + DEF_TITLE_WIDTH, w_Y + DEF_TITLE_HEIGHT + (DEF_ITEM_HEIGHT * 2));
				InvalidateRect(w_Rect);

				break;
				}


			if ((w_ii%DEF_ITEM_NUM) == (DEF_ITEM_NUM - 1)) {
				w_Y += DEF_ITEM_HEIGHT;
				w_X = DEF_BASE_X + DEF_LEFT_WIDTH;
			}
			else {
				w_X += DEF_ITEM_WIDTH;
			}
					}
				}
			}

void CNodeListView::OnSize(UINT nType, int cx, int cy)
{
	CScrollWindow::OnSize(nType, cx, cy);
}

void CNodeListView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CRect		w_Rect;
	CRect		w_TitleRect;
	int			w_ii;
	int			w_jj;
	int			w_kk = 0;
	int			w_X, w_Y;
	CString		w_Str;
	CPoint		w_sPoint;
	int			w_DevCnt;
	int			w_DevType;

	w_sPoint = GetScrollPosition();
	point += w_sPoint;

	LPA_DATA_T *wp_PAData = GetPAData();
	if (!wp_PAData) {
		return;
	}

	//
	if (GetLowPA()->m_SelPage != LPA_SEL_EQUIP)
	{
		return;
	}

	//지역정보 별칭 입력
	CDLLowPAPopupDlg	w_DLLowPAPopupDlg;

	w_DevCnt = wp_PAData->m_DevCnt[LPA_DMA_IDX] + wp_PAData->m_DevCnt[LPA_DSS_IDX];
	for (w_jj = 0; w_jj < w_DevCnt; w_jj++) {
		w_X = DEF_BASE_X;
		w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y));

		DL_LOG("=>POS : [%d:%d] [%d,%d]", w_X, w_Y, point.x, point.y);
		w_TitleRect = CRect(w_X, w_Y, w_X + (DEF_ITEM_WIDTH * 16), w_Y + DEF_TITLE_HEIGHT);
		if ((w_TitleRect.left <= point.x && point.x <= w_TitleRect.right) && (w_TitleRect.top <= point.y && point.y <= w_TitleRect.bottom))
		{
			DL_LOG("OnLButtonDblClk : TITLE (%d)", w_jj);

			if (w_jj >= wp_PAData->m_DevCnt[LPA_DMA_IDX]) {
				w_DevType = DEV_TYPE_DSS;

				w_kk = w_jj - wp_PAData->m_DevCnt[LPA_DMA_IDX];

				w_DLLowPAPopupDlg.m_StrMsg.Format(_T("DSS %d - 장치 별칭을 입력해주세요!"), w_kk + 1);
				if (wp_PAData->m_DssData[w_kk].m_DevName[0]) {
					w_DLLowPAPopupDlg.m_StrName = wp_PAData->m_DssData[w_kk].m_DevName;
			}
				else {
					w_DLLowPAPopupDlg.m_StrName = _T("");
				}

		}
			else {
				w_DevType = DEV_TYPE_DMT;
#if defined(DEV_5000)
				w_DLLowPAPopupDlg.m_StrMsg.Format(_T("DPA %d - 장치 별칭을 입력해주세요!"), w_jj + 1);
#else
				w_DLLowPAPopupDlg.m_StrMsg.Format(_T("DMT %d - 장치 별칭을 입력해주세요!"), w_jj + 1);
#endif
				if (wp_PAData->m_DmtData[w_jj].m_DevName[0]) {
					w_DLLowPAPopupDlg.m_StrName = wp_PAData->m_DmtData[w_jj].m_DevName;
				}
				else {
					w_DLLowPAPopupDlg.m_StrName = _T("");
				}
			}
			if (w_DLLowPAPopupDlg.DoModal() == IDOK) {
				if (w_DevType == DEV_TYPE_DSS) {
					dl_strcpy(wp_PAData->m_DssData[w_kk].m_DevName, CT2A(w_DLLowPAPopupDlg.m_StrName));
				}
				else {
					dl_strcpy(wp_PAData->m_DmtData[w_jj].m_DevName, CT2A(w_DLLowPAPopupDlg.m_StrName));
				}

				// 해당 DEVICE 전체 재표시
				w_X = DEF_BASE_X - w_sPoint.x;
				w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y)) - w_sPoint.y;
				w_Rect = CRect(w_X, w_Y, w_X + DEF_TITLE_WIDTH, w_Y + DEF_TITLE_HEIGHT + (DEF_ITEM_HEIGHT * 2));
				InvalidateRect(w_Rect);
			}
	}
		w_Y += DEF_TITLE_HEIGHT;
		for (w_ii = 0; w_ii < LPA_TERMINAL_ITEM; w_ii++) {

			w_Rect = CRect(w_X, w_Y, w_X + DEF_ITEM_WIDTH, w_Y + DEF_ITEM_HEIGHT);
			if ((w_Rect.left <= point.x && point.x <= w_Rect.right)
				&& (w_Rect.top <= point.y && point.y <= w_Rect.bottom)) {
				DL_LOG("OnLButtonDblClk : (%d, %d)", w_ii, w_jj);

				if (w_jj >= wp_PAData->m_DevCnt[LPA_DMA_IDX]) {
					w_DevType = DEV_TYPE_DSS;

					w_kk = w_jj - wp_PAData->m_DevCnt[LPA_DMA_IDX];

					w_DLLowPAPopupDlg.m_StrMsg.Format(_T("DSS %d - %d의 별칭을 입력해주세요!"), w_kk + 1, w_ii + 1);
					if (wp_PAData->m_DssData[w_kk].m_Item[w_ii].m_Name[0]) {
						w_DLLowPAPopupDlg.m_StrName = wp_PAData->m_DssData[w_kk].m_Item[w_ii].m_Name;
					}
					else {
						w_DLLowPAPopupDlg.m_StrName = _T("");
					}

				}
				else {
					w_DevType = DEV_TYPE_DMT;

#if defined(DEV_5000)
					w_DLLowPAPopupDlg.m_StrMsg.Format(_T("DPA %d - %d의 별칭을 입력해주세요!"), w_jj + 1, w_ii + 1);
#else
					w_DLLowPAPopupDlg.m_StrMsg.Format(_T("DMT %d - %d의 별칭을 입력해주세요!"), w_jj + 1, w_ii + 1);
#endif
					if (wp_PAData->m_DmtData[w_jj].m_Item[w_ii].m_Name[0]) {
						w_DLLowPAPopupDlg.m_StrName = wp_PAData->m_DmtData[w_jj].m_Item[w_ii].m_Name;
					}
					else {
						w_DLLowPAPopupDlg.m_StrName = _T("");
					}
				}
				if (w_DLLowPAPopupDlg.DoModal() == IDOK) {
					if (w_DevType == DEV_TYPE_DSS) {
						dl_strcpy(wp_PAData->m_DssData[w_kk].m_Item[w_ii].m_Name, CT2A(w_DLLowPAPopupDlg.m_StrName));
					}
					else {
						dl_strcpy(wp_PAData->m_DmtData[w_jj].m_Item[w_ii].m_Name, CT2A(w_DLLowPAPopupDlg.m_StrName));
					}

					// 해당 DEVICE 전체 재표시
					w_X = DEF_BASE_X - w_sPoint.x;
					w_Y = DEF_BASE_Y + DEF_TITLE_HEIGHT + (w_jj * (DEF_ITEM_HEIGHT * 2 + DEF_GAP_Y)) - w_sPoint.y;
					w_Rect = CRect(w_X, w_Y, w_X + DEF_TITLE_WIDTH, w_Y + DEF_TITLE_HEIGHT + (DEF_ITEM_HEIGHT * 2));
					InvalidateRect(w_Rect);
				}
				break;
			}
			if ((w_ii%DEF_ITEM_NUM) == (DEF_ITEM_NUM - 1)) {
				w_Y += DEF_ITEM_HEIGHT;
				w_X = DEF_BASE_X;
			}
			else {
				w_X += DEF_ITEM_WIDTH;
			}
		}
		}

			}
