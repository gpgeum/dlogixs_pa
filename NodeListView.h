// ChildView.h : interface of the CNodeListView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILDVIEW_H__910AFC24_0C68_4023_86D1_D28269A1C731__INCLUDED_)
#define AFX_CHILDVIEW_H__910AFC24_0C68_4023_86D1_D28269A1C731__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ScrollWindow.h"

/////////////////////////////////////////////////////////////////////////////
// CNodeListView window

class CNodeListView : public CScrollWindow
{
// Construction
public:
	CNodeListView();

// Attributes
public:
	CFont		m_Font;
	CFont		m_FontSmall;

	int			m_SelAddr;					// 선택 Devide 주소
	int			m_SelItem;					// 선택 Device Item(Output)

	int			m_Group[LPA_DMA_MAX+LPA_DSS_MAX];
	int			m_SubGroup1[LPA_DMA_MAX+LPA_DSS_MAX];	// SUB GROUP 1
	int			m_SubGroup2[LPA_DMA_MAX+LPA_DSS_MAX];	// SUB GROUP 2

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNodeListView)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
protected:
    virtual void OnDraw(CDC* pDC);

// Implementation
public:
	virtual ~CNodeListView();

	// Generated message map functions
protected:
	//{{AFX_MSG(CNodeListView)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__910AFC24_0C68_4023_86D1_D28269A1C731__INCLUDED_)
