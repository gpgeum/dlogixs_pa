/******************************************************************************/
//
//		PAEQUIP
//
//		File Name	: PAEquip.h
//		Description	:
//
//
//
//		Date		: 2009.Mar.18
//		Author		: Choi Kyung Ho ( uhruh@naver.com )
//
/******************************************************************************/

#ifndef _PAEQUIP_H_
#define _PAEQUIP_H_

/******************************************************************************/
/*						INCLUDE FILES										  */
/******************************************************************************/


/******************************************************************************/
/*						DEFINITIONS											  */
/******************************************************************************/
#define EQUIP_TYPE_ALL		0		// 모든장비
#define EQUIP_TYPE_PC		1		// PC
#define EQUIP_TYPE_DMT		2		// DMT
#define EQUIP_TYPE_DSS		3		// DSS
#define EQUIP_TYPE_DPG		4		// DPG
#define EQUIP_TYPE_DRM		7		// REMOTE AMP
#define EQUIP_TYPE_DPA		8		// DPA
#define EQUIP_TYPE_DMP		10		// DMP
#define EQUIP_TYPE_DRE		11		// DRE
#define EQUIP_TYPE_DFR		12		// DFR
#define EQUIP_TYPE_DMX		15		// MIXER

typedef enum
{
	EEQUIP_TYPE_ALL	=		EQUIP_TYPE_ALL,
	EEQUIP_TYPE_PC =		EQUIP_TYPE_PC,
	EEQUIP_TYPE_DMT =		EQUIP_TYPE_DMT,
	EEQUIP_TYPE_DSS =		EQUIP_TYPE_DSS,
	EEQUIP_TYPE_DPG =		EQUIP_TYPE_DPG,
	EEQUIP_TYPE_DRM =		EQUIP_TYPE_DRM,
	EEQUIP_TYPE_DPA =		EQUIP_TYPE_DPA,
	EEQUIP_TYPE_DMP =		EQUIP_TYPE_DMP,
	EEQUIP_TYPE_DRE =		EQUIP_TYPE_DRE,
	EEQUIP_TYPE_DFR =		EQUIP_TYPE_DFR,
	
	EEQUIP_TYPE_DMX =		EQUIP_TYPE_DMX,
	NUMOF_EQUIP_TYPE,

	EEQUIP_TYPE_NONE = NUMOF_EQUIP_TYPE
	
} _EEquipType_;

typedef unsigned char		EEquipType;

#define MAX_NUM_OF_SPEAKER_DRG			16
#define MAX_NUM_OF_SPEAKER_DLR			32
#define MAX_NUM_OF_SPEAKER_SSL			MAX_NUM_OF_SPEAKER_DLR

#define MAX_NUM_OF_CHANNEL_DFC			18
#define MAX_NUM_OF_CHANNEL_DFE			64

#define MAX_NUM_OF_SINGLELINE_DRM		32	// 일원화 리모트 최대 개수

/******************************************************************************/
/*						SYSTEM CONSTANTS									  */
/******************************************************************************/


/******************************************************************************/
/*						MACROS												  */
/******************************************************************************/
#define GET_MAX_SPEAKER(equip)			( (equip==EEQUIP_TYPE_DSS) ? MAX_NUM_OF_SPEAKER_DRG : 0 )
#define IS_SPEAKER_SELECTOR(equip)		( (equip==EEQUIP_TYPE_DSS) ? TRUE : FALSE )


/******************************************************************************/
/*						GLOBAL VARIABLES									  */
/******************************************************************************/


/******************************************************************************/
/*						LOCAL VARIABLES										  */
/******************************************************************************/


/******************************************************************************/
/*						FUNCTION PROTOTYPES									  */
/******************************************************************************/

#ifndef _PAEQUIP_C_

#endif



#endif	// _PAEQUIP_H_
