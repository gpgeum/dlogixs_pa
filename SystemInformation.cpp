// SystemInformation.cpp: implementation of the CSystemInformation class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SystemInformation.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

namespace System
{

BOOL CSystemInformation::m_bInitialized = FALSE;
DWORD CSystemInformation::m_dwVersion;
BOOL CSystemInformation::m_bWin95;
BOOL CSystemInformation::m_bWin4;
BOOL CSystemInformation::m_bMarked4;

CSystemInformation CSystemInformation::m_SystemInformation;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSystemInformation::CSystemInformation()
{
    VERIFY(m_bInitialized == 0);// Cannot instantiate this class.

    m_bInitialized = TRUE;

    m_dwVersion = ::GetVersion();
    m_bWin95 = (m_dwVersion & 0x80000000) != 0;
    m_bWin4 = (BYTE)m_dwVersion >= 4;

    // Determine various metrics based on EXE subsystem version mark    
    if (m_bWin4)
    {
        m_bMarked4 = (GetProcessVersion(0) >= 0x00040000);
    }
    else
    {
        m_bMarked4 = FALSE;
    }
}

CSystemInformation::~CSystemInformation()
{

}

//////////////////////////////////////////////////////////////////////
// Operations
//////////////////////////////////////////////////////////////////////

BOOL CSystemInformation::IsWin95()
{
    return m_bWin95;
}

BOOL CSystemInformation::IsWin4()
{
    return m_bWin4;
}

BOOL CSystemInformation::IsMarked4()
{
    return m_bMarked4;
}

BOOL CSystemInformation::GetScrollBarWidth()
{
    if(m_bMarked4)
    {
        return GetSystemMetrics(SM_CXVSCROLL) + CX_BORDER;
    }
    else
    {
        return GetSystemMetrics(SM_CXVSCROLL);
    }
}

BOOL CSystemInformation::GetScrollBarHeight()
{
    if(m_bMarked4)
    {
        return GetSystemMetrics(SM_CYHSCROLL) + CY_BORDER;
    }
    else
    {
        return GetSystemMetrics(SM_CYHSCROLL);
    }
}

}// namespace
