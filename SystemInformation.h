// SystemInformation.h: interface for the CSystemInformation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SYSTEMINFORMATION_H__56010378_2DA7_4D71_AFFF_077A93A3B192__INCLUDED_)
#define AFX_SYSTEMINFORMATION_H__56010378_2DA7_4D71_AFFF_077A93A3B192__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

namespace System
{

/**
 * Provides some system information.
 */
class CSystemInformation  
{
// Constants
public:
    enum
    {
        CX_BORDER = 1,
        CY_BORDER = 1,
    };

// Construction/Destruction
public:
    CSystemInformation();
	virtual ~CSystemInformation();

// Operations
public:
    /**
     * Returns TRUE if the operating system is Windows 95.
     */
    static BOOL IsWin95();

    /**
     * Returns TRUE if the operating system is Windows NT 4.0.
     */
    static BOOL IsWin4();
    
    /**
     * Returns TRUE if the running application is marked as a Windows NT 4.0 application.
     */
    static BOOL IsMarked4();
    
    /**
     * Returns the width of the vertical scroll bar.
     */
    static int GetScrollBarWidth();
    
    /**
     * Returns the height of the horizontal scroll bar.
     */
    static int GetScrollBarHeight();

// Implementation
protected:
    static CSystemInformation m_SystemInformation;

    static BOOL m_bInitialized;
    static DWORD m_dwVersion;
    static BOOL m_bWin95;
    static BOOL m_bWin4;
    static BOOL m_bMarked4;
};

}
#endif // !defined(AFX_SYSTEMINFORMATION_H__56010378_2DA7_4D71_AFFF_077A93A3B192__INCLUDED_)
