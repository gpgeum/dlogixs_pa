// WaitDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DLLowPASetup.h"
#include "WaitDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CWaitDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CWaitDialog, CDialog)

CWaitDialog::CWaitDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CWaitDialog::IDD, pParent)
{
	m_WaitTimer = -1;
	m_Timeout = 0;

	mp_CloseCB = NULL;
}

CWaitDialog::~CWaitDialog()
{
}

void CWaitDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS, m_Progress);
	DDX_Control(pDX, IDC_BTN_STOP, m_BtnCancel);
}


BEGIN_MESSAGE_MAP(CWaitDialog, CDialog)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_STOP, &CWaitDialog::OnBnClickedBtnStop)
END_MESSAGE_MAP()


// CWaitDialog 메시지 처리기입니다.

BOOL CWaitDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CWaitDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CWaitDialog::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == WAIT_TIMER) {
		DoWait(0);
		if(mp_CloseCB) {
			mp_CloseCB(EV_WAIT_TIMEOUT);
		}
	}

	CDialog::OnTimer(nIDEvent);
}

void CWaitDialog::ResetTimer()
{
	if(m_Timeout > 0 && m_WaitTimer > 0) {
		StopTimer();
//		DL_LOG("RESET TIMER : %d", m_Timeout)
		m_WaitTimer = SetTimer(WAIT_TIMER, m_Timeout, NULL);
	}
}


void CWaitDialog::StartTimer(int a_time)
{
	m_Timeout = a_time;
	if(a_time > 0) {
		StopTimer();
		m_WaitTimer = SetTimer(WAIT_TIMER, m_Timeout, NULL);
	}
}

void CWaitDialog::StopTimer()
{
	if(m_WaitTimer >= 0) {
		KillTimer(m_WaitTimer);
	}
	m_WaitTimer = -1;
}


void CWaitDialog::OnDestroy()
{
	CDialog::OnDestroy();

	StopTimer();
}

BOOL CWaitDialog::Pump ()
{
	MSG msg;

	// Retrieve and dispatch any waiting messages.
	while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)) {
		if (!AfxGetApp ()->PumpMessage ()) {
			::PostQuitMessage (0);
			return FALSE;
		}
	}

	// Simulate the framework's idle processing mechanism.
	LONG lIdle = 0;
	while (AfxGetApp ()->OnIdle (lIdle++));
	return TRUE;
}



void CWaitDialog::DoWait(int a_show, int a_timeout, int a_Cancel)
{
	if(a_show) {
		GetParent()->EnableWindow(FALSE);
		StartTimer(a_timeout);
		ShowWindow(SW_SHOW);
		Pump ();
	} else {
		StopTimer();
		GetParent()->EnableWindow(TRUE);
		ShowWindow(SW_HIDE);
	}
	if(a_Cancel) {
		m_Progress.ShowWindow(SW_SHOW);
		m_BtnCancel.ShowWindow(SW_SHOW);
	} else {
		m_Progress.ShowWindow(SW_HIDE);
		m_BtnCancel.ShowWindow(SW_HIDE);
	}
}
void CWaitDialog::OnBnClickedBtnStop()
{
	DoWait(0);

	if(mp_CloseCB) {
		mp_CloseCB(EV_WAIT_CANCEL);
	}
}


void CWaitDialog::SetCloseCB(int (*ap_CloseCB)(int a_Timeout))
{
	mp_CloseCB = ap_CloseCB;
}
