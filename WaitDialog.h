#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#define	EV_WAIT_END				0
#define	EV_WAIT_CANCEL			1
#define	EV_WAIT_TIMEOUT			2
#define	EV_WAIT_ERROR			3

// CWaitDialog 대화 상자입니다.
#define		WAIT_TIMER		1

class CWaitDialog : public CDialog
{
	DECLARE_DYNAMIC(CWaitDialog)

public:
	CWaitDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWaitDialog();

	int m_WaitTimer;
	int	m_Timeout;

	void StartTimer(int a_time);
	void ResetTimer();
	void StopTimer();

	void DoWait(int a_show, int a_timeout=5000, int a_Cancel=1);
	BOOL Pump ();

	void SetRange(short a_Min, short a_Max)	{	m_Progress.SetRange(a_Min, a_Max);				};
	void SetPos(short a_Pos)				{	ResetTimer();m_Progress.SetPos(a_Pos);			};

	int		(*mp_CloseCB)(int a_Event);
	void	SetCloseCB(int (*ap_CloseCB)(int a_Event));


// 대화 상자 데이터입니다.
	enum { IDD = IDD_WAITDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	CProgressCtrl m_Progress;
	afx_msg void OnBnClickedBtnStop();
	CButton m_BtnCancel;
};
