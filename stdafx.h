
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#ifdef _DEBUG
#include "vld.h"
#endif

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#pragma warning(disable : 4786)	// too long identifiers.
#include "./MultiPaneCtrl/MultiPaneCtrl.h"

//#define		DEV_5000

#ifndef		DEV_5000
#define		DEV_1000
#endif

#include	"PAEquip.h"
#include	"resource.h"
#include	"Color.h"
#include	"ColorEdit.h"
#include	"ColorStatic.h"
#include	"GateStatic.h"

#include	"DLLowPAData.h"
#include	"NodeListView.h"
#include	"DLLowPASetupDlg.h"
#include	"DLLowPAPopupDlg.h"
#include	"WaitDialog.h"

#define		DL_LOWPA_PROCESSNAME			_T("DLLowPA.exe")

//메인타이틀
#if defined(DEV_5000)
#define		MAIN_APP_TITLE					_T("전관(5000) 설정프로그램")
#define		MAIN_MPCC_STATE					_T("DLLowPAConfig5000_v1.04")				// 현재 STATE 저장명
#else
#define		MAIN_APP_TITLE					_T("전관(1000) 설정프로그램")
#define		MAIN_MPCC_STATE					_T("DLLowPAConfig1000_v1.04")				// 현재 STATE 저장명
#endif

#define		DEF_SCR_WIDTH			1024
#define		DEF_SCR_HEIGHT			768
//#define		DEF_SCR_WIDTH			1300
//#define		DEF_SCR_HEIGHT			850

#define		WM_SELPAGE				(WM_USER+2233)
#define		WM_CHANGEDEQUIP			(WM_USER+2234)
#define		WM_CHANGEDSTATUS		(WM_USER+2235)
#define		WM_INVALIDATE			(WM_USER+2236)	
#define		WM_PROGRESS				(WM_USER+2237)
#define		WM_DPADSS_ERROR			(WM_USER+2238)	
#define		WM_LOCALFILE_SAVE		(WM_USER+2239)	
#define		WM_LOCALFILE_CHECK		(WM_USER+2240)
#define		WM_UPDATE_INFO			(WM_USER+2241)
#define		WM_UPDATE_DATA			(WM_USER+2242)

#define		WM_TIMER_SENDCMD		(WM_USER+2243)
#define		WM_TIMER_SENDFLASHCMD	(WM_USER+2244)

// SEND/RECV PROGRESS
#define		EV_PROGRESS_START		0
#define		EV_PROGRESS_RANGE		1
#define		EV_PROGRESS_POS			2
#define		EV_PROGRESS_POLL		3
#define		EV_PROGRESS_END			4

// SEND/RECV RESULT
#define		EV_SEND_OK				1
#define		EV_RECV_OK				2
#define		EV_RECV_ERROR			-100
#define		EV_SEND_ERROR			-200

// DEVICE(DMT/DSS/DPG/DRM/DMP/DPA/DRE/DFR) UPDATE
#define		MAX_SIZE_FIRMWARE			0x80000

#include	"DLOs.h"
#include	"DLUtils.h"

extern CFont g_titleFont;							//size : 20

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <Afxtempl.h>
#include <shlwapi.h>
#include <afxcontrolbars.h>

extern int g_LowPADevPort;

//#define LOG_TO_CONSOLE

#if defined(LOG_TO_CONSOLE)

#if defined(_DEBUG) && !defined(_USRDLL)
#pragma comment(linker, "/verbose:lib")
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console")
#endif

#endif